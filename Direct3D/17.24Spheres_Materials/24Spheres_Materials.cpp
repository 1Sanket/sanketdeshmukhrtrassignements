#include <windows.h>
#include <stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>
#include "Sphere.h"
#include "XNAMath\xnamath.h"
#include "vmath.h"

using namespace vmath;

#pragma warning (disable:4838)		
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"D3dcompiler.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

bool gbIs_X_AxisRotationOfLight = true;
bool gbIs_Y_AxisRotationOfLight = false;
bool gbIs_Z_AxisRotationOfLight = false;

bool bIsLkeyPressed;

float gClearColor[4];

float lightRotationAngle = 0.0f;

D3D11_VIEWPORT d3dViewport;

IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device  *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader_PerPixel = NULL;
ID3D11PixelShader *gpID3D11PixelShader_PerPixel = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;
ID3D11Buffer *gpID3D11Buffer_IndexBuffers = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

bool isLightEnabled;

int giWidth, giHeight;

float gAnglePyramid = 0.0f;
float gAngleCube = 0.0f;

float light_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float light_Diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float light_Specular[] = { 1.0f,1.0f,1.0f,1.0f };
float light_Position[] = { 0.0f,0.0f,0.0f,0.0f };

float material_ambient[][4] = { { 0.0215f,0.1745f,0.0215f,1.0f },
{ 0.135f,0.2225f,0.1575f,1.0f },
{ 0.05375f,0.05f,0.06625f,1.0f },
{ 0.25f,0.20725f,0.20725f,1.0f },
{ 0.1745f,0.01175f,0.01175f,1.0f },
{ 0.1f,0.18725f,0.1745f,1.0f },
{ 0.329412f,0.223529f,0.027451f,1.0f },
{ 0.2125f,0.1275f,0.054f,1.0f },
{ 0.25f,0.25f,0.25f,1.0f },
{ 0.19125f,0.0735f,0.0225f,1.0f },
{ 0.24725f,0.1995f,0.0745f,1.0f },
{ 0.19225f,0.19225f,0.19225f,1.0f },
{ 0.0f,0.0f,0.0f,1.0f },
{ 0.0f,0.1f,0.06f,1.0f },
{ 0.0f,0.0f,0.0f,1.0f },
{ 0.0f,0.0f,0.0f,1.0f },
{ 0.0f,0.0f,0.0f,1.0f },
{ 0.0f,0.0f,0.0f,1.0f },
{ 0.02f,0.02f,0.02f,1.0f },
{ 0.0f,0.05f,0.05f,1.0f },
{ 0.0f,0.05f,0.0f,1.0f },
{ 0.05f,0.0f,0.0f,1.0f },
{ 0.05f,0.05f,0.05f,1.0f },
{ 0.05f,0.05f,0.0f,1.0f } };

float material_diffuse[][4] = { { 0.07568f,0.61424f,0.07568f,1.0f },
{ 0.54f,0.89f,0.63f,1.0f },
{ 0.18275f,0.17f,0.22525f,1.0f },
{ 1.0f,0.829f,0.829f,1.0f },
{ 0.61424f,0.04136f,0.04136f,1.0f },
{ 0.396f,0.74151f,0.69102f,1.0f },
{ 0.780392f,0.568627f,0.113725f,1.0f },
{ 0.714f,0.4284f,0.18144f,1.0f },
{ 0.4f,0.4f,0.4f,1.0f },
{ 0.7038f,0.27048f,0.0828f,1.0f },
{ 0.75164f,0.60648f,0.22648f,1.0f },
{ 0.50754f,0.50754f,0.50754f,1.0f },
{ 0.01f,0.01f,0.01f,1.0f },
{ 0.0f,0.50980392f,0.50980392f,1.0f },
{ 0.0f,0.35f,0.1f,1.0f },
{ 0.5f,0.0f,0.0f,1.0f },
{ 0.55f,0.55f,0.55f,1.0f },
{ 0.5f,0.5f,0.0f,1.0f },
{ 0.01f,0.01f,0.01f,1.0f },
{ 0.4f,0.5f,0.5f,1.0f },
{ 0.4f,0.5f,0.4f,1.0f },
{ 0.5f,0.4f,0.4f,1.0f },
{ 0.5f,0.5f,0.5f,1.0f },
{ 0.5f,0.5f,0.4f,1.0f } };

float material_specular[][4] = { { 0.633f,0.7278f,0.633f,1.0f },
{ 0.316228f,0.316228f,0.316228f,1.0f },
{ 0.332741f,0.328634f,0.346435f,1.0f },
{ 0.296648f,0.296648f,0.296648f,1.0f },
{ 0.727811f,0.626959f,0.626959f,1.0f },
{ 0.297254f,0.30829f,0.306678f,1.0f },
{ 0.992157f,0.941176f,0.807843f,1.0f },
{ 0.393548f,0.271906f,0.166721f,1.0f },
{ 0.774597f,0.774597f,0.774597f,1.0f },
{ 0.256777f,0.137622f,0.086014f,1.0f },
{ 0.628281f,0.555802f,0.366065f,1.0f },
{ 0.508273f,0.508273f,0.508273f,1.0f },
{ 0.50f,0.50f,0.50f,1.0f },
{ 0.50196078f,0.50196078f,0.50196078f,1.0f },
{ 0.45f,0.55f,0.45f,1.0f },
{ 0.7f,0.6f,0.6f,1.0f },
{ 0.70f,0.70f,0.70f,1.0f },
{ 0.60f,0.60f,0.50f,1.0f },
{ 0.4f,0.4f,0.4f,1.0f },
{ 0.04f,0.7f,0.7f,1.0f },
{ 0.04f,0.7f,0.04f,1.0f },
{ 0.7f,0.04f,0.04f,1.0f },
{ 0.7f,0.7f,0.7f,1.0f },
{ 0.7f,0.7f,0.04f,1.0f } };

float material_shinyness[] = { 0.6 * 128,
0.1 * 128,
0.3 * 128,
0.088 * 128,
0.6 * 128,
0.1 * 128,
0.21794872 * 128,
0.2 * 128,
0.6 * 128,
0.1 * 128,
0.4 * 128,
0.4 * 128,
0.25 * 128,
0.25 * 128,
0.25 * 128,
0.25 * 128,
0.25 * 128,
0.25 * 128,
0.078125 * 128,
0.078125 * 128,
0.078125 * 128,
0.078125 * 128,
0.078125 * 128,
0.078125 * 128
};


struct CBUFFER
{
	XMMATRIX ModelMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR u_La;
	XMVECTOR u_Ld;
	XMVECTOR u_Ls;
	XMVECTOR u_Ka;
	XMVECTOR u_Kd;
	XMVECTOR u_Ks;
	float u_Material_shininess;
	XMVECTOR u_light_position;
	unsigned int u_LKeyPressed;
};


XMMATRIX gPerspectiveProjectionMatrix;

// entrypoint

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void updateAngle(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Direct3D11");
	bool bDone = false;

	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created....\n Exiting...."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully opened.\n");
		fclose(gpFile);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndClass);

	hwnd = CreateWindow(szClassName,
		TEXT("Direct3D11 Materials"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() failed . Exiting now.....\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() succeeded .\n");
		fclose(gpFile);
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			updateAngle();

			display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return ((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wParam, LPARAM lParam)
{
	HRESULT resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	HRESULT hr;

	switch (imsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));

			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() failed.\n");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() succeeded.\n");
				fclose(gpFile);
			}
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //f or F
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		case 0x4C:  //L or l
			if (bIsLkeyPressed == false)
			{
				isLightEnabled = true;
				bIsLkeyPressed = true;
			}
			else
			{
				isLightEnabled = false;
				bIsLkeyPressed = false;
			}
			break;

		case 0x58: // for 'x' or 'X'

			gbIs_X_AxisRotationOfLight =true;
			gbIs_Y_AxisRotationOfLight = false;
			gbIs_Z_AxisRotationOfLight = false;

			break;

		case 0x59: // for 'y' or 'Y'

			gbIs_X_AxisRotationOfLight = false;
			gbIs_Y_AxisRotationOfLight = true;
			gbIs_Z_AxisRotationOfLight = false;

			break;

		case 0x5A: // for 'z' or 'Z'

			gbIs_X_AxisRotationOfLight = false;
			gbIs_Y_AxisRotationOfLight = false;
			gbIs_Z_AxisRotationOfLight = true;

			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, imsg, wParam, lParam));
}

void ToggleFullscreen(void)
{

	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void updateAngle()
{
	if (lightRotationAngle > 360.0f)
		lightRotationAngle = 360.0f - lightRotationAngle;

	lightRotationAngle = lightRotationAngle + 0.005f;

}

HRESULT initialize(void)
{
	void uninitialize(void);
	HRESULT resize(int, int);

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;

	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain
		(NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
		fprintf_s(gpFile, "The chosen driver is of ");

		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type. \n");
		}

		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "Warp Type. \n");
		}

		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type. \n");
		}

		else
		{
			fprintf_s(gpFile, "Unknown Type. \n");
		}

		fprintf_s(gpFile, "The Supported highest feature level is :");

		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0 \n");
		}

		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile, "10.1 \n");
		}

		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0 \n");
		}

		else
		{
			fprintf_s(gpFile, "Unknown \n");
		}

		fclose(gpFile);
	}

	const char *vertexShaderSourceCode =

		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 modelMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 u_La;"\
		"float4 u_Ld;"\
		"float4 u_Ls;"\
		"float4 u_Ka;"\
		"float4 u_Kd;"\
		"float4 u_Ks;"\
		"float u_Material_shininess;"\
		"float4 u_light_position;"\
		"uint u_LKeyPressed;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 transformed_normal: NORMAL0;"\
		"float3 lightDirection:NORMAL1;"\
		"float3 viewerVector:NORMAL2;"\
		"};"\
		"vertex_output main(float4 pos:POSITION,float4 normal:NORMAL)"\
		"{"\
		"vertex_output output;"\
		"if(u_LKeyPressed == 1)"
		"{"\
		"float4 eyeCoordinates = mul(modelMatrix,pos);"\
		"eyeCoordinates = mul(viewMatrix,eyeCoordinates);"\
		"output.transformed_normal = normalize(mul((float3x3)mul(modelMatrix,viewMatrix),(float3)normal));"\
		"output.lightDirection = (float3) normalize(u_light_position - eyeCoordinates);"\
		"output.viewerVector = -eyeCoordinates.xyz;"\
		"}"\
		"float4 world = mul(modelMatrix , pos);"\
		"float4 worldView = mul(viewMatrix , world);"\
		"output.position = mul(projectionMatrix , worldView);"\
		"return (output);"\
		"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode, lstrlenA(vertexShaderSourceCode) + 1, "VS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "vs_5_0", 0, 0, &pID3DBlob_VertexShaderCode, &pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for vertex shader: %s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() succeeded for vertex shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader_PerPixel);


	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateVertexShader() failed.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateVertexShader() succeeded .\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PerPixel, 0, 0);

	const char *pixelShaderSourceCode =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 modelMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 u_La;"\
		"float4 u_Ld;"\
		"float4 u_Ls;"\
		"float4 u_Ka;"\
		"float4 u_Kd;"\
		"float4 u_Ks;"\
		"float u_Material_shininess;"\
		"float4 u_light_position;"\
		"uint u_LKeyPressed;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 transformed_normal: NORMAL0;"\
		"float3 lightDirection:NORMAL1;"\
		"float3 viewerVector:NORMAL2;"\
		"};"\
		"float4 main(float4 pos:SV_POSITION,vertex_output input):SV_TARGET"\
		"{"\
		"float4 outputColor;"\
		"if(u_LKeyPressed == 1)"
		"{"\
		"float3 normalized_transformed_normals=normalize(input.transformed_normal);"\
		"float3 normalized_light_direction = normalize(input.lightDirection);"\
		"float3 normalized_viewer_vector = normalize(input.viewerVector);"\
		"float3 ambient = u_La * u_Ka ;"\
		"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"\
		"float3 diffuse = u_Ld * u_Kd * tn_dot_ld; "\
		"float3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\
		"float3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_Material_shininess); "\
		"float3 color = ambient + diffuse + specular;  "\
		"outputColor = float4(color,1.0);"\
		"}"\
		"else"\
		"{"\
		"outputColor = float4(1.0,1.0,1.0,1.0);"\
		"}"\
		"return(outputColor);"\
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode, lstrlenA(pixelShaderSourceCode) + 1, "PS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "ps_5_0", 0, 0, &pID3DBlob_PixelShaderCode, &pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for pixel shader: %s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() succeeded for pixel shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader_PerPixel);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreatePixelShader() failed.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreatePixelShader() succeeded .\n");
		fclose(gpFile);
	}

	
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PerPixel, 0, 0);


	// create and set input layout.      

	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];

	// Position.

	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].InstanceDataStepRate = 0;

	// Color.

	inputElementDesc[1].SemanticName = "NORMAL";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(inputElementDesc, 2, pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateInputLayout() failed.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateInputLayout() succeeded .\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;

	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// Sphere Position.

	D3D11_BUFFER_DESC bufferDesc;

	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Sphere_Position);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateBuffer() failed for sphere position vertex buffers.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateBuffer() succeeded for sphere position vertex buffers.\n");
		fclose(gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE mappedSubresource;

	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Sphere_Position, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, sizeof(sphere_vertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Sphere_Position, 0);


	// Sphere Normal

	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Sphere_Normal);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For sphere normal Vertex buffer. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For sphere normal Vertex buffer.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Sphere_Normal, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_normals, sizeof(sphere_normals));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Sphere_Normal, 0);


	//Sphere Elements.

	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_elements);
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_IndexBuffers);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For index Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For index Buffer Vertex Shader.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_IndexBuffers, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, sizeof(sphere_elements));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_IndexBuffers, 0);



	// define and set constant buffer

	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));

	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateBuffer() failed for constant buffer.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateInputLayout() succeeded for constant buffer.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	gClearColor[0] = 0.5f;
	gClearColor[1] = 0.5f;
	gClearColor[2] = 0.5f;
	gClearColor[3] = 0.5f;

	//following code is required to off back face culling.

	D3D11_RASTERIZER_DESC rasterizerDesc;

	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0;
	rasterizerDesc.SlopeScaledDepthBias = 0.0;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	// call resize for first time

	hr = resize(WIN_WIDTH, WIN_HEIGHT);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() failed .\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() succeeded .\n");
		fclose(gpFile);
	}

	isLightEnabled = false;

	return(S_OK);
}


HRESULT resize(int width, int height)
{
	HRESULT hr = S_OK;

	giWidth = width;
	giHeight = height;

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}


	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize buffers accordingly

	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// get back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_backBuffer;

	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_backBuffer);


	// get render target view from d3d11 device using above back buffer

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_backBuffer, NULL, &gpID3D11RenderTargetView);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateRenderTargetView() failed .\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateRenderTargetView() succeeded .\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_backBuffer->Release();
	pID3D11Texture2D_backBuffer = NULL;


	// DSV 

	D3D11_TEXTURE2D_DESC textureDesc;

	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1,
		textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	ID3D11Texture2D *pID3D11Texture2D = NULL;

	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateTexture2D() failed .\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateTexture2D() succeeded .\n");
		fclose(gpFile);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;

	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D, &depthStencilViewDesc, &gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateDepthStencilView() failed .\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateDepthStencilView() succeeded .\n");
		fclose(gpFile);
	}

	pID3D11Texture2D->Release();
	pID3D11Texture2D = NULL;

	// set render target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	
	d3dViewport.TopLeftX = 0;
	d3dViewport.TopLeftY = 0;
	d3dViewport.Width = (float)width;
	d3dViewport.Height = (float)height;
	d3dViewport.MinDepth = 0.0f;
	d3dViewport.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewport);

	// set perspective matrix

	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return(hr);
}

void display(void)
{

	/*gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PerPixel, 0, 0);

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PerPixel, 0, 0);*/

	// clear render target view to chosen color.

	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	int SphereNumber = 0;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 5; j >= 0; j--)
		{

			D3D11_VIEWPORT d3dViewPort;
			ZeroMemory(&d3dViewPort, sizeof(D3D11_VIEWPORT));
			d3dViewPort.TopLeftX = 0 + (i * (float)giWidth / 4);
			d3dViewPort.TopLeftY = 0 + (j* (float)giHeight / 6);
			d3dViewPort.Width = (float)giWidth / 4;
			d3dViewPort.Height = (float)giHeight / 6;
			d3dViewPort.MinDepth = 0.0f;
			d3dViewPort.MaxDepth = 1.0f;

			gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

			gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)(giWidth / 4) / (float)(giHeight / 6), 0.1f, 100.0f);


			UINT stride = sizeof(float) * 3;
			UINT offset = 0;

			XMMATRIX worldMatrix = XMMatrixIdentity();
			XMMATRIX viewMatrix = XMMatrixIdentity();
			XMMATRIX wvMatrix = XMMatrixIdentity();

			CBUFFER constantBuffer;


			// draw cube
			gpID3D11DeviceContext->IASetVertexBuffers(0,//input slot
				1,  //how many buffers you have
				&gpID3D11Buffer_VertexBuffer_Sphere_Position,
				&stride,
				&offset);


			gpID3D11DeviceContext->IASetVertexBuffers(1,//input slot
				1,  //how many buffers you have
				&gpID3D11Buffer_VertexBuffer_Sphere_Normal,
				&stride,
				&offset);

			//index buffer
			gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_IndexBuffers, DXGI_FORMAT_R16_UINT, 0);

			//select geometry primitive
			gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			worldMatrix = XMMatrixIdentity();
			viewMatrix = XMMatrixIdentity();

			worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 2.0f);


			ZeroMemory(&constantBuffer, sizeof(CBUFFER));
			constantBuffer.ModelMatrix = worldMatrix;
			constantBuffer.ViewMatrix = viewMatrix;
			constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
			constantBuffer.u_La = XMVectorSet(light_Ambient[0], light_Ambient[1], light_Ambient[2], light_Ambient[3]);
			constantBuffer.u_Ld = XMVectorSet(light_Diffuse[0], light_Diffuse[1], light_Diffuse[2], light_Diffuse[3]);
			constantBuffer.u_Ls = XMVectorSet(light_Specular[0], light_Specular[1], light_Specular[2], light_Specular[3]);

			constantBuffer.u_Ka = XMVectorSet(material_ambient[SphereNumber][0], material_ambient[SphereNumber][1], material_ambient[SphereNumber][2], material_ambient[SphereNumber][3]);
			constantBuffer.u_Kd = XMVectorSet(material_diffuse[SphereNumber][0], material_diffuse[SphereNumber][1], material_diffuse[SphereNumber][2], material_diffuse[SphereNumber][3]);
			constantBuffer.u_Ks = XMVectorSet(material_specular[SphereNumber][0], material_specular[SphereNumber][1], material_specular[SphereNumber][2], material_specular[SphereNumber][3]);

			constantBuffer.u_Material_shininess = material_shinyness[SphereNumber];
			//light properties
			if (gbIs_X_AxisRotationOfLight) {
				//x-axis
				light_Position[0] = 0.0f;
				light_Position[1] = 80.0f * cos(lightRotationAngle);
				light_Position[2] = 80.0f * sin(lightRotationAngle);
			}
			else if (gbIs_Y_AxisRotationOfLight) {
				//y-axis
				light_Position[0] = 80.0f * cos(lightRotationAngle);
				light_Position[1] = 0.0f;
				light_Position[2] = 80.0f * sin(lightRotationAngle);
			}
			else if (gbIs_Z_AxisRotationOfLight)
			{
				//z-axis
				light_Position[0] = 80.0f * cos(lightRotationAngle);
				light_Position[1] = 80.0f * sin(lightRotationAngle);
				light_Position[2] = 0.0f;
			}
			constantBuffer.u_light_position = XMVectorSet(light_Position[0], light_Position[1], light_Position[2], light_Position[3]);
			if (isLightEnabled == true)
			{
				constantBuffer.u_LKeyPressed = 1;
			}
			else
			{
				constantBuffer.u_LKeyPressed = 0;
			}
			gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
				0,	//index in shader
				NULL, //D3D_BOX bounding box compute shader cube of data
				&constantBuffer,
				0, //width of face of bounding box
				0);	//depth of bounding box	

			gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);
			
			SphereNumber++;
		}
	}

	
	// switch between front and back buffers

	gpIDXGISwapChain->Present(0, 0);
}


void uninitialize(void)
{
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_IndexBuffers)
	{
		gpID3D11Buffer_IndexBuffers->Release();
		gpID3D11Buffer_IndexBuffers = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Sphere_Position)
	{
		gpID3D11Buffer_VertexBuffer_Sphere_Position->Release();
		gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Sphere_Normal)
	{
		gpID3D11Buffer_VertexBuffer_Sphere_Normal->Release();
		gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;
	}



	if (gpID3D11VertexShader_PerPixel)
	{
		gpID3D11VertexShader_PerPixel->Release();
		gpID3D11VertexShader_PerPixel = NULL;
	}

	if (gpID3D11VertexShader_PerPixel)
	{
		gpID3D11VertexShader_PerPixel->Release();
		gpID3D11VertexShader_PerPixel = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Uninitialize() succeeded .\n");
		fprintf_s(gpFile, "Log file is successfully closed .\n");
		fclose(gpFile);
	}
}
