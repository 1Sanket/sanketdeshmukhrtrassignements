#include <windows.h>
#include <stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning (disable:4838)
#include "XNAMath\xnamath.h"

#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"D3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM,LPARAM);

FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

float gClearColor[4];

float anglePyramid = 0.0f;
float angleCube = 0.0f;

IDXGISwapChain *gpIDXGISwapChain= NULL;
ID3D11Device  *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Triangle_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Triangle_Color = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Square_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Square_Color = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix;
};


XMMATRIX gPerspectiveProjectionMatrix;

// entrypoint

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void updateAngle(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Direct3D11");
	bool bDone = false;

	if (fopen_s(&gpFile,gszLogFileName,"w")!=0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created....\n Exiting...."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully opened.\n");
		fclose(gpFile);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndClass);

	hwnd = CreateWindow(szClassName,
		TEXT("Direct3D11 3D Rotation"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() failed . Exiting now.....\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() succeeded .\n");
		fclose(gpFile);
	}

	while (bDone == false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			updateAngle();

			display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return ((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg,WPARAM wParam,LPARAM lParam)
{
	HRESULT resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	HRESULT hr;

	switch (imsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));

			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() failed.\n");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() succeeded.\n");
				fclose(gpFile);
			}
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;
		case 0x46 : //f or F
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	
	default:
			break;
	}

	return (DefWindowProc(hwnd, imsg, wParam, lParam));
}

void ToggleFullscreen(void)
{

	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void updateAngle()
{
	anglePyramid = anglePyramid + 0.001f;
	angleCube = angleCube- 0.001f;
	if (anglePyramid >= 360.0f)
		anglePyramid = 0.0f;
	if (angleCube <= 0.0f)
		angleCube = 360.0f;

}

	HRESULT initialize(void)
	{
		void uninitialize(void);
		HRESULT resize(int, int);

		HRESULT hr;

		D3D_DRIVER_TYPE d3dDriverType;
		D3D_DRIVER_TYPE d3dDriverTypes[] = {D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE};
		D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
		D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
		UINT createDeviceFlags = 0;
		UINT numDriverTypes = 0;
		UINT numFeatureLevels = 1;

		numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

		DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;

		ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

		dxgiSwapChainDesc.BufferCount = 1;
		dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
		dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
		dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
		dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
		dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		dxgiSwapChainDesc.OutputWindow = ghwnd;
		dxgiSwapChainDesc.SampleDesc.Count = 1;
		dxgiSwapChainDesc.SampleDesc.Quality = 0;
		dxgiSwapChainDesc.Windowed = TRUE;

		for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
		{
			d3dDriverType = d3dDriverTypes[driverTypeIndex];

			hr = D3D11CreateDeviceAndSwapChain
			(   NULL,
				d3dDriverType,
				NULL,
				createDeviceFlags,
				&d3dFeatureLevel_required,
				numFeatureLevels,
				D3D11_SDK_VERSION,
				&dxgiSwapChainDesc,
				&gpIDXGISwapChain,
				&gpID3D11Device,
				&d3dFeatureLevel_acquired,
				&gpID3D11DeviceContext
			);

			if (SUCCEEDED(hr))
				break;
		}

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() failed.\n");
			fclose(gpFile);
			return(hr);
		}
		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
			fprintf_s(gpFile, "The chosen driver is of ");

			if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
			{
				fprintf_s(gpFile, "Hardware Type. \n");
			}

			else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
			{
				fprintf_s(gpFile, "Warp Type. \n");
			}

			else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
			{
				fprintf_s(gpFile, "Reference Type. \n");
			}

			else
			{
				fprintf_s(gpFile, "Unknown Type. \n");
			}

			fprintf_s(gpFile, "The Supported highest feature level is :");

			if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
			{
				fprintf_s(gpFile, "11.0 \n");
			}

			else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
			{
				fprintf_s(gpFile, "10.1 \n");
			}

			else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
			{
				fprintf_s(gpFile, "10.0 \n");
			}

			else
			{
				fprintf_s(gpFile, "Unknown \n");
			}

			fclose(gpFile);
		}

		const char *vertexShaderSourceCode =
			"cbuffer ConstantBuffer" \
			"{" \
			"float4x4 worldViewProjectionMatrix;" \
			"};" \
			"struct vertex_output"\
			"{" \
			"float4 position : SV_POSITION;" \
			"float4 color : COLOR;" \
			"};" \
			"vertex_output main(float4 pos : POSITION,float4 col:COLOR)" \
			"{" \
			"vertex_output output;" \
			"output.position = mul(worldViewProjectionMatrix, pos);" \
			"output.color = col;" \
			"return(output);" \
			"}";

		ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
		ID3DBlob *pID3DBlob_Error = NULL;

		hr = D3DCompile(vertexShaderSourceCode, lstrlenA(vertexShaderSourceCode) + 1, "VS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "vs_5_0", 0, 0, &pID3DBlob_VertexShaderCode, &pID3DBlob_Error);

		if (FAILED(hr))
		{
			if (pID3DBlob_Error != NULL)
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "D3DCompile() failed for vertex shader: %s.\n",(char*)pID3DBlob_Error->GetBufferPointer());
				fclose(gpFile);
				pID3DBlob_Error->Release();
				pID3DBlob_Error = NULL;
				return(hr);
			}
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() succeeded for vertex shader.\n");
			fclose(gpFile);
		}

		hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader);

		if (FAILED(hr))
		{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "D3D11Device::CreateVertexShader() failed.\n");
				fclose(gpFile);
				return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateVertexShader() succeeded .\n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);

		const char *pixelShaderSourceCode =
			"float4 main(float4 position:SV_POSITION,float4 color:COLOR) : SV_TARGET" \
			"{" \
			"float4 outputColor = color; " \
			"return(outputColor);" \
			"}";

		ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
		pID3DBlob_Error = NULL;

		hr = D3DCompile(pixelShaderSourceCode, lstrlenA(pixelShaderSourceCode) + 1, "PS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "ps_5_0", 0, 0, &pID3DBlob_PixelShaderCode, &pID3DBlob_Error);

		if (FAILED(hr))
		{
			if (pID3DBlob_Error != NULL)
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "D3DCompile() failed for pixel shader: %s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
				fclose(gpFile);
				pID3DBlob_Error->Release();
				pID3DBlob_Error = NULL;
				return(hr);
			}
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() succeeded for pixel shader.\n");
			fclose(gpFile);
		}

		hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreatePixelShader() failed.\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreatePixelShader() succeeded .\n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);

		float PyramidVertices[] =
		{
			// front side

			0.0f,1.0f,0.0f,     // top
			-1.0f,-1.0f,1.0f,    // left
			1.0f,-1.0f,1.0f,     // right

			// right side

			0.0f,1.0f,0.0f,
			1.0f,-1.0f,1.0f,
			1.0f,-1.0f,-1.0f,

			// back side

			0.0f,1.0f,0.0f,
			1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,

			// left side

			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,1.0f


		};

		float PyramidColor[] =
		{
			// front 
			1.0f,0.0f,0.0f,
			0.0f,0.0f,1.0f,
			0.0f,1.0f,0.0f,

			// right
			1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,0.0f,1.0f,

			// back
			1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,0.0f,1.0f,

			// left
			1.0f,0.0f,0.0f,
			0.0f,0.0f,1.0f,
			0.0f,1.0f,0.0f,
		};

		float CubeVertices[] =
		{
			// side 1 (top)
			// triangle 1
			-1.0f,1.0f,1.0f,
			1.0f,1.0f,1.0f,
			-1.0f,1.0f,-1.0f,

			// triangle 2
			-1.0f,1.0f,-1.0f,
			1.0f,1.0f,1.0f,
			1.0f,1.0f,-1.0f,

			// side 2 (bottom)
			// triangle 1
			1.0f,-1.0f,-1.0f,
			1.0f,-1.0f,1.0f,
			-1.0f,-1.0f,-1.0f,

			// triangle 2
			-1.0f,-1.0f,-1.0f,
			1.0f,-1.0f,1.0f,
			-1.0f,-1.0f,1.0f,

			// side 3 (front)
			// triangle 1
			-1.0f,1.0f,-1.0f,
			1.0f,1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,

			// triangle 2
			-1.0f,-1.0f,-1.0f,
			1.0f,1.0f,-1.0f,
			1.0f,-1.0f,-1.0f,

			// side 4 (back)
			// triangle 1
			1.0f,-1.0f,1.0f,
			1.0f,1.0f,1.0f,
			-1.0f,-1.0f,1.0f,

			// triangle 2
			-1.0f,-1.0f,1.0f,
			1.0f,1.0f,1.0f,
			-1.0f,1.0f,1.0f,

			// side 5 (left)
			// triangle 1
			-1.0f,1.0f,1.0f,
			-1.0f,1.0f,-1.0f,
			-1.0f,-1.0f,1.0f,

			// triangle 2
			-1.0f,-1.0f,1.0f,
			-1.0f,1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,

			// side 6 (right)
			// triangle 1
			 1.0f,-1.0f,-1.0f,
			 1.0f,1.0f,-1.0f,
			 1.0f,-1.0f,1.0f,

			// triangle 2
			 1.0f,-1.0f,1.0f,
			 1.0f,1.0f,-1.0f,
			 1.0f,1.0f,1.0f,
		};

		float SquareColor[] =
		{
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,

			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,

			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,

			0.0f,1.0f,1.0f,
			0.0f,1.0f,1.0f,
			0.0f,1.0f,1.0f,
			0.0f,1.0f,1.0f,
			0.0f,1.0f,1.0f,
			0.0f,1.0f,1.0f,

			1.0f,0.0f,1.0f,
			1.0f,0.0f,1.0f,
			1.0f,0.0f,1.0f,
			1.0f,0.0f,1.0f,
			1.0f,0.0f,1.0f,
			1.0f,0.0f,1.0f,

			1.0f,1.0f,0.0f,
			1.0f,1.0f,0.0f,
			1.0f,1.0f,0.0f,
			1.0f,1.0f,0.0f,
			1.0f,1.0f,0.0f,
			1.0f,1.0f,0.0f,
		};


		// Triangle

		// create vertex buffer for position.

		D3D11_BUFFER_DESC bufferDesc;

		ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));

		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(PyramidVertices);
		bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Triangle_Position);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateBuffer() failed for vertex buffer for position.\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateBuffer() succeeded for vertex buffer for position.\n");
			fclose(gpFile);
		}

		D3D11_MAPPED_SUBRESOURCE mappedSubresource;

		ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Triangle_Position, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
		memcpy(mappedSubresource.pData, PyramidVertices, sizeof(PyramidVertices));
		gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Triangle_Position, NULL);

		
		// create vertex buffer for color. 

		ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));

		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(PyramidColor);
		bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Triangle_Color);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateBuffer() failed for vertex buffer for color.\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateBuffer() succeeded for vertex buffer for color.\n");
			fclose(gpFile);
		}


		ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Triangle_Color, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
		memcpy(mappedSubresource.pData, PyramidColor, sizeof(PyramidColor));
		gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Triangle_Color, NULL);

		
		// Square
		
		ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));

		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(CubeVertices);
		bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Square_Position);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateBuffer() failed for vertex buffer for position.\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateBuffer() succeeded for vertex buffer for position.\n");
			fclose(gpFile);
		}

		ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Square_Position, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
		memcpy(mappedSubresource.pData, CubeVertices, sizeof(CubeVertices));
		gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Square_Position, NULL);


		// create vertex buffer for color. 

		ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));

		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(SquareColor);
		bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Square_Color);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateBuffer() failed for vertex buffer for color.\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateBuffer() succeeded for vertex buffer for color.\n");
			fclose(gpFile);
		}


		ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Square_Color, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
		memcpy(mappedSubresource.pData, SquareColor, sizeof(SquareColor));
		gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Square_Color, NULL);

		// create and set input layout.      

		D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];

		// Position.

		inputElementDesc[0].SemanticName = "POSITION";
		inputElementDesc[0].SemanticIndex = 0;
		inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputElementDesc[0].InputSlot = 0;
		inputElementDesc[0].AlignedByteOffset = 0;
		inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		inputElementDesc[0].InstanceDataStepRate = 0;

		// Color.

		inputElementDesc[1].SemanticName = "COLOR";
		inputElementDesc[1].SemanticIndex = 0;
		inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputElementDesc[1].InputSlot = 1;
		inputElementDesc[1].AlignedByteOffset = 0;
		inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		inputElementDesc[1].InstanceDataStepRate = 0;

		hr = gpID3D11Device->CreateInputLayout(inputElementDesc, 2, pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateInputLayout() failed.\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateInputLayout() succeeded .\n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;

		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;

		
		// define and set constant buffer

		D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
		ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));

		bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
		bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

		hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateBuffer() failed for constant buffer.\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3D11Device::CreateInputLayout() succeeded for constant buffer.\n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);


		gClearColor[0] = 0.0f;
		gClearColor[1] = 0.0f;
		gClearColor[2] = 0.0f;
		gClearColor[3] = 0.0f;

		//following code is required to off back face culling.

		D3D11_RASTERIZER_DESC rasterizerDesc;

		ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

		rasterizerDesc.AntialiasedLineEnable = FALSE;
		rasterizerDesc.MultisampleEnable = FALSE;
		rasterizerDesc.DepthBias = 0;
		rasterizerDesc.DepthBiasClamp = 0.0;
		rasterizerDesc.SlopeScaledDepthBias = 0.0;
		rasterizerDesc.CullMode = D3D11_CULL_NONE;
		rasterizerDesc.DepthClipEnable = TRUE;
		rasterizerDesc.FillMode = D3D11_FILL_SOLID;
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.ScissorEnable = FALSE;

		gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

		gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

		gPerspectiveProjectionMatrix = XMMatrixIdentity();

		// call resize for first time

		hr = resize(WIN_WIDTH, WIN_HEIGHT);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "resize() failed .\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "resize() succeeded .\n");
			fclose(gpFile);
		}

		return(S_OK);
	}


	HRESULT resize(int width, int height)
	{
		HRESULT hr = S_OK;
	
		if (gpID3D11DepthStencilView)
		{
			gpID3D11DepthStencilView->Release();
			gpID3D11DepthStencilView = NULL;
		}


		if (gpID3D11RenderTargetView)
		{
			gpID3D11RenderTargetView->Release();
			gpID3D11RenderTargetView = NULL;
		}

		// resize buffers accordingly

		gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

		// get back buffer from swap chain
		ID3D11Texture2D *pID3D11Texture2D_backBuffer;

		gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_backBuffer);

		
		// get render target view from d3d11 device using above back buffer

		hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_backBuffer, NULL, &gpID3D11RenderTargetView);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "gpID3D11Device::CreateRenderTargetView() failed .\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "gpID3D11Device::CreateRenderTargetView() succeeded .\n");
			fclose(gpFile);
		}

		pID3D11Texture2D_backBuffer->Release();
		pID3D11Texture2D_backBuffer = NULL;


		// DSV 

		D3D11_TEXTURE2D_DESC textureDesc;

		ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

		textureDesc.Width = width;
		textureDesc.Height = height;
		textureDesc.ArraySize = 1;
		textureDesc.MipLevels = 1;
		textureDesc.SampleDesc.Count = 1,
		textureDesc.SampleDesc.Quality = 0;
		textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;

		ID3D11Texture2D *pID3D11Texture2D = NULL;

		hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "gpID3D11Device::CreateTexture2D() failed .\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "gpID3D11Device::CreateTexture2D() succeeded .\n");
			fclose(gpFile);
		}

		D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;

		ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

		depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
		depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

		hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D, &depthStencilViewDesc, &gpID3D11DepthStencilView);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "gpID3D11Device::CreateDepthStencilView() failed .\n");
			fclose(gpFile);
			return(hr);
		}

		else
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "gpID3D11Device::CreateDepthStencilView() succeeded .\n");
			fclose(gpFile);
		}

		pID3D11Texture2D->Release();
		pID3D11Texture2D = NULL;

		// set render target view as render target
		gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView,gpID3D11DepthStencilView);

		D3D11_VIEWPORT d3dViewport;
		d3dViewport.TopLeftX = 0;
		d3dViewport.TopLeftY = 0;
		d3dViewport.Width = (float)width;
		d3dViewport.Height = (float)height;
		d3dViewport.MinDepth = 0.0f;
		d3dViewport.MaxDepth = 1.0f;

		gpID3D11DeviceContext->RSSetViewports(1, &d3dViewport);

		// set perspective matrix

		gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),(float)width/(float)height,0.1f,100.0f);
		
		return(hr);
	}

	void display(void)
	{
		// clear render target view to chosen color.

		gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

		gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,D3D11_CLEAR_DEPTH,1.0f,0);

		// select which vertex buffer to display

		UINT stride = sizeof(float) * 3;
		UINT offset = 0;

		
		// Pyramid

		// position.
		gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Triangle_Position, &stride, &offset);

		// color.
		gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Triangle_Color, &stride, &offset);


		// select geometry primitive

		gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


		// translation is concerned with world matrix transformation.

		XMMATRIX worldMatrix = XMMatrixIdentity();
		XMMATRIX viewMatrix = XMMatrixIdentity();
		XMMATRIX rotationMatrix = XMMatrixIdentity();

		worldMatrix = XMMatrixTranslation(-1.5f, 0.0f, 6.0f);

		// rotation matrix

		rotationMatrix = XMMatrixRotationY(anglePyramid);

		worldMatrix = rotationMatrix * worldMatrix;

		// final WorldViewProjectionMatrix
		
		XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

		// load the data into the constant buffer.
		CBUFFER constantBuffer;

		constantBuffer.WorldViewProjectionMatrix = wvpMatrix;

		gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

		// draw vertex buffer to render target
		gpID3D11DeviceContext->Draw(12, 0);

		// Cube

		// position.
		gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Square_Position, &stride, &offset);

		// color.
		gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Square_Color, &stride, &offset);


		// select geometry primitive

		gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


		// translation is concerned with world matrix transformation.

	    worldMatrix = XMMatrixIdentity();
	    viewMatrix = XMMatrixIdentity();
		wvpMatrix = XMMatrixIdentity();
		rotationMatrix = XMMatrixIdentity();

		worldMatrix = XMMatrixTranslation(1.5f, 0.0f, 6.0f);

		// rotation matrix

		XMMATRIX R1 = XMMatrixRotationX(angleCube);
		XMMATRIX R2 = XMMatrixRotationY(angleCube);
		XMMATRIX R3 = XMMatrixRotationZ(angleCube);

		rotationMatrix = R1 * R2 * R3;

		XMMATRIX scaleMatrix = XMMatrixScaling(0.75f, 0.75f, 0.75f);

		worldMatrix = scaleMatrix * rotationMatrix * worldMatrix;

		// final WorldViewProjectionMatrix

		wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

		// load the data into the constant buffer.
		ZeroMemory(&constantBuffer,sizeof(CBUFFER));

		constantBuffer.WorldViewProjectionMatrix = wvpMatrix;

		gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

		// draw vertex buffer to render target
		gpID3D11DeviceContext->Draw(6, 0);
		gpID3D11DeviceContext->Draw(6, 6);
		gpID3D11DeviceContext->Draw(6, 12);
		gpID3D11DeviceContext->Draw(6, 18);
		gpID3D11DeviceContext->Draw(6, 24);
		gpID3D11DeviceContext->Draw(6, 30);

		// switch between front and back buffers

		gpIDXGISwapChain->Present(0, 0);
	}


	void uninitialize(void)
	{
		if(gpID3D11Buffer_ConstantBuffer)
		{
			gpID3D11Buffer_ConstantBuffer->Release();
			gpID3D11Buffer_ConstantBuffer = NULL;
		}

		if (gpID3D11InputLayout)
		{
			gpID3D11InputLayout->Release();
			gpID3D11InputLayout = NULL;
		}

		if (gpID3D11Buffer_VertexBuffer_Square_Color)
		{
			gpID3D11Buffer_VertexBuffer_Square_Color->Release();
			gpID3D11Buffer_VertexBuffer_Square_Color = NULL;
		}

		if (gpID3D11Buffer_VertexBuffer_Square_Position)
		{
			gpID3D11Buffer_VertexBuffer_Square_Position->Release();
			gpID3D11Buffer_VertexBuffer_Square_Position = NULL;
		}

		if (gpID3D11Buffer_VertexBuffer_Triangle_Color)
		{
			gpID3D11Buffer_VertexBuffer_Triangle_Color->Release();
			gpID3D11Buffer_VertexBuffer_Triangle_Color = NULL;
		}

		if (gpID3D11Buffer_VertexBuffer_Triangle_Position)
		{
			gpID3D11Buffer_VertexBuffer_Triangle_Position->Release();
			gpID3D11Buffer_VertexBuffer_Triangle_Position = NULL;
		}

		if (gpID3D11PixelShader)
		{
			gpID3D11PixelShader->Release();
			gpID3D11PixelShader = NULL;
		}

		if (gpID3D11VertexShader)
		{
			gpID3D11VertexShader->Release();
			gpID3D11VertexShader = NULL;
		}

		if (gpID3D11DepthStencilView)
		{
			gpID3D11DepthStencilView->Release();
			gpID3D11DepthStencilView = NULL;
		}

		if (gpID3D11RenderTargetView)
		{
			gpID3D11RenderTargetView->Release();
			gpID3D11RenderTargetView = NULL;
		}

		if (gpIDXGISwapChain)
		{
			gpIDXGISwapChain->Release();
			gpIDXGISwapChain = NULL;
		}

		if (gpID3D11DeviceContext)
		{
			gpID3D11DeviceContext->Release();
			gpID3D11DeviceContext = NULL;
		}

		if (gpID3D11Device)
		{
			gpID3D11Device->Release();
			gpID3D11Device = NULL;
		}

		if (gpFile)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "Uninitialize() succeeded .\n");
			fprintf_s(gpFile, "Log file is successfully closed .\n");
			fclose(gpFile);
		}
	}
	