#include <windows.h>
#include <stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>
#include "Sphere.h"

#pragma warning (disable:4838)
#include "XNAMath\xnamath.h"

#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"D3dcompiler.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

bool gbPerVertexLighting;
bool gbPerFragmentLighting;

bool bIsLkeyPressed;
bool gbQKeyIsPressed = false;

float gClearColor[4];

float angleCube = 0.0f;

IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device  *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader_PerPixel = NULL;
ID3D11PixelShader *gpID3D11PixelShader_PerPixel = NULL;
ID3D11VertexShader *gpID3D11VertexShader_PerVertex = NULL;
ID3D11PixelShader *gpID3D11PixelShader_PerVertex = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;
ID3D11Buffer *gpID3D11Buffer_IndexBuffers = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

bool isLightEnabled;

float angleRedLight = 0.0f;
float angleBlueLight = 0.0f;
float angleGreenLight = 0.0f;

// Arrays required for lighting.
float light0_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float light0_diffuse[] = { 1.0f,0.0f,0.0f,0.0f };  // red light
float light0_specular[] = { 1.0f,0.0f,0.0f,0.0f };
float light0_position[] = { 0.0f,0.0f,0.0f,0.0f };

float light1_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float light1_diffuse[] = { 0.0f,1.0f,0.0f,0.0f };  // green light
float light1_specular[] = { 0.0f,1.0f,0.0f,0.0f };
float light1_position[] = { 0.0f,0.0f,0.0f,0.0f };

float light2_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float light2_diffuse[] = { 0.0f,0.0f,1.0f,0.0f };   //blue light
float light2_specular[] = { 0.0f,0.0f,1.0f,0.0f };
float light2_position[] = { 0.0f,0.0f,0.0f,0.0f };

float material_Ambient[] = { 0.0f,0.0f,0.0f,1.0f };
float material_Diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_Specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 50.0f;

struct CBUFFER
{
	XMMATRIX ModelMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR u_La0;
	XMVECTOR u_Ld0;
	XMVECTOR u_Ls0;
	XMVECTOR u_La1;
	XMVECTOR u_Ld1;
	XMVECTOR u_Ls1;
	XMVECTOR u_La2;
	XMVECTOR u_Ld2;
	XMVECTOR u_Ls2;
	XMVECTOR u_Ka;
	XMVECTOR u_Kd;
	XMVECTOR u_Ks;
	float u_Material_shininess;
	XMVECTOR u_light0_position;
	XMVECTOR u_light1_position;
	XMVECTOR u_light2_position;
	unsigned int u_LKeyPressed;
};


XMMATRIX gPerspectiveProjectionMatrix;

// entrypoint

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void updateAngle(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Direct3D11");
	bool bDone = false;

	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created....\n Exiting...."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully opened.\n");
		fclose(gpFile);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndClass);

	hwnd = CreateWindow(szClassName,
		TEXT("Direct3D11 Three Lights"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() failed . Exiting now.....\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() succeeded .\n");
		fclose(gpFile);
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			updateAngle();

			display();

			if (gbActiveWindow == true)
			{
				if (gbQKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return ((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wParam, LPARAM lParam)
{
	HRESULT resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	HRESULT hr;
	static bool bIsVKeyPressed = false;
	static bool bIsFKeyPressed = false;

	switch (imsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));

			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() failed.\n");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() succeeded.\n");
				fclose(gpFile);
			}
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x51: //'Q' or 'q'

			if (gbQKeyIsPressed == false)
				gbQKeyIsPressed = true;
			break;

		case VK_ESCAPE:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		case 0x46: // for 'F' or 'f'

			if (bIsFKeyPressed == false)
			{
				gbPerFragmentLighting = true;
				gbPerVertexLighting = false;
				bIsFKeyPressed = true;
				bIsVKeyPressed = false;
			}

			break;

		case 0x56: // for 'V' or 'v'

			if (bIsVKeyPressed == false)
			{
				gbPerVertexLighting = true;
				gbPerFragmentLighting = false;
				bIsVKeyPressed = true;
				bIsFKeyPressed = false;
			}

			break;

		case 0x4C:  //L or l
			if (bIsLkeyPressed == false)
			{
				isLightEnabled = true;
				bIsLkeyPressed = true;
			}
			else
			{
				isLightEnabled = false;
				bIsLkeyPressed = false;
			}
			break;
		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, imsg, wParam, lParam));
}

void ToggleFullscreen(void)
{

	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void updateAngle()
{
	angleRedLight = angleRedLight + 0.0005f;
	if (angleRedLight >= 360.0f)
		angleRedLight = 0.0f;

	angleGreenLight = angleGreenLight + 0.0005f;
	if (angleGreenLight >= 360.0f)
		angleGreenLight = 0.0f;

	angleBlueLight = angleBlueLight + 0.0005f;
	if (angleBlueLight >= 360.0f)
		angleBlueLight = 0.0f;

}

HRESULT initialize(void)
{
	void uninitialize(void);
	HRESULT resize(int, int);

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;

	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain
		(NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
		fprintf_s(gpFile, "The chosen driver is of ");

		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type. \n");
		}

		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "Warp Type. \n");
		}

		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type. \n");
		}

		else
		{
			fprintf_s(gpFile, "Unknown Type. \n");
		}

		fprintf_s(gpFile, "The Supported highest feature level is :");

		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0 \n");
		}

		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile, "10.1 \n");
		}

		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0 \n");
		}

		else
		{
			fprintf_s(gpFile, "Unknown \n");
		}

		fclose(gpFile);
	}


	// Per pixel changes 

	const char *vertexShaderSourceCode_PerPixel =

		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 modelMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 u_La0;"\
		"float4 u_Ld0;"\
		"float4 u_Ls0;"\
		"float4 u_La1;"\
		"float4 u_Ld1;"\
		"float4 u_Ls1;"\
		"float4 u_La2;"\
		"float4 u_Ld2;"\
		"float4 u_Ls2;"\
		"float4 u_Ka;"\
		"float4 u_Kd;"\
		"float4 u_Ks;"\
		"float u_Material_shininess;"\
		"float4 u_light0_position;"\
		"float4 u_light1_position;"\
		"float4 u_light2_position;"\
		"uint u_LKeyPressed;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 transformed_normal: NORMAL0;"\
		"float3 light0Direction:NORMAL1;"\
		"float3 light1Direction:NORMAL2;"\
		"float3 light2Direction:NORMAL3;"\
		"float3 viewerVector:NORMAL4;"\
		"};"\
		"vertex_output main(float4 pos:POSITION,float4 normal:NORMAL)"\
		"{"\
		"vertex_output output;"\
		"if(u_LKeyPressed == 1)"
		"{"\
		"float4 eyeCoordinates = mul(modelMatrix,pos);"\
		"eyeCoordinates = mul(viewMatrix,eyeCoordinates);"\
		"output.transformed_normal = normalize(mul((float3x3)mul(modelMatrix,viewMatrix),(float3)normal));"\
		"output.light0Direction = (float3) normalize(u_light0_position - eyeCoordinates);"\
		"output.light1Direction = (float3) normalize(u_light1_position - eyeCoordinates);"\
		"output.light2Direction = (float3) normalize(u_light2_position - eyeCoordinates);"\
		"output.viewerVector = -eyeCoordinates.xyz;"\
		"}"\
		"float4 world = mul(modelMatrix , pos);"\
		"float4 worldView = mul(viewMatrix , world);"\
		"output.position = mul(projectionMatrix , worldView);"\
		"return (output);"\
		"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode_PerPixel, lstrlenA(vertexShaderSourceCode_PerPixel) + 1, "VS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "vs_5_0", 0, 0, &pID3DBlob_VertexShaderCode, &pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for vertex shader: %s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() succeeded for vertex shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader_PerPixel);


	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateVertexShader() failed.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateVertexShader() succeeded .\n");
		fclose(gpFile);
	}

	//gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);

	const char *pixelShaderSourceCode_PerPixel =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 modelMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 u_La0;"\
		"float4 u_Ld0;"\
		"float4 u_Ls0;"\
		"float4 u_La1;"\
		"float4 u_Ld1;"\
		"float4 u_Ls1;"\
		"float4 u_La2;"\
		"float4 u_Ld2;"\
		"float4 u_Ls2;"\
		"float4 u_Ka;"\
		"float4 u_Kd;"\
		"float4 u_Ks;"\
		"float u_Material_shininess;"\
		"float4 u_light0_position;"\
		"float4 u_light1_position;"\
		"float4 u_light2_position;"\
		"uint u_LKeyPressed;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 transformed_normal: NORMAL0;"\
		"float3 light0Direction:NORMAL1;"\
		"float3 light1Direction:NORMAL2;"\
		"float3 light2Direction:NORMAL3;"\
		"float3 viewerVector:NORMAL4;"\
		"};"\
		"float4 main(float4 pos:SV_POSITION,vertex_output input):SV_TARGET"\
		"{"\
		"float4 outputColor;"\
		"if(u_LKeyPressed == 1)"
		"{"\
		"float3 normalized_transformed_normals=normalize(input.transformed_normal);"\
		"float3 normalized_light0_direction = normalize(input.light0Direction);"\
		"float3 normalized_light1_direction = normalize(input.light1Direction);"\
		"float3 normalized_light2_direction = normalize(input.light2Direction);"\
		"float3 normalized_viewer_vector = normalize(input.viewerVector);"\
		"float3 ambient0 = u_La0 * u_Ka ;"\
		"float tn_dot_ld0 = max(dot(normalized_transformed_normals,normalized_light0_direction),0.0);"\
		"float3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0; "\
		"float3 reflection_vector0 = reflect(-normalized_light0_direction,normalized_transformed_normals);"\
		"float3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0,normalized_viewer_vector),0.0),u_Material_shininess); "\
		"float3 ambient1 = u_La1 * u_Ka ;"\
		"float tn_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light1_direction),0.0);"\
		"float3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1; "\
		"float3 reflection_vector1 = reflect(-normalized_light1_direction,normalized_transformed_normals);"\
		"float3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1,normalized_viewer_vector),0.0),u_Material_shininess); "\
		"float3 ambient2 = u_La2 * u_Ka ;"\
		"float tn_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light2_direction),0.0);"\
		"float3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2; "\
		"float3 reflection_vector2 = reflect(-normalized_light2_direction,normalized_transformed_normals);"\
		"float3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2,normalized_viewer_vector),0.0),u_Material_shininess); "\
		"float3 color = ambient0 + diffuse0 + specular0 + ambient1 + diffuse1 + specular1 + ambient2 + diffuse2 + specular2;  "\
		"outputColor = float4(color,1.0);"\
		"}"\
		"else"\
		"{"\
		"outputColor = float4(1.0,1.0,1.0,1.0);"\
		"}"\
		"return(outputColor);"\
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode_PerPixel, lstrlenA(pixelShaderSourceCode_PerPixel) + 1, "PS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "ps_5_0", 0, 0, &pID3DBlob_PixelShaderCode, &pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for pixel shader: %s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() succeeded for pixel shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader_PerPixel);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreatePixelShader() failed.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreatePixelShader() succeeded .\n");
		fclose(gpFile);
	}

	//gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);


	// per vertex changes.

	const char *vertexShaderSourceCode_PerVertex =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 modelMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 u_La0;"\
		"float4 u_Ld0;"\
		"float4 u_Ls0;"\
		"float4 u_La1;"\
		"float4 u_Ld1;"\
		"float4 u_Ls1;"\
		"float4 u_La2;"\
		"float4 u_Ld2;"\
		"float4 u_Ls2;"\
		"float4 u_Ka;"\
		"float4 u_Kd;"\
		"float4 u_Ks;"\
		"float u_Material_shininess;"\
		"float4 u_light0_position;"\
		"float4 u_light1_position;"\
		"float4 u_light2_position;"\
		"uint u_LKeyPressed;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float4 color : COLOR;"\
		"};"\
		"vertex_output main(float4 pos:POSITION,float4 normal:NORMAL)"\
		"{"\
		"vertex_output output;"\
		"if(u_LKeyPressed == 1)"
		"{"\
		"float4 eyeCoordinates = mul(modelMatrix,pos);"\
		"eyeCoordinates = mul(viewMatrix,eyeCoordinates);"\
		"float3 viewer_vector = normalize(-eyeCoordinates.xyz);"\
		"float3 transformed_normals = normalize(mul((float3x3)mul(modelMatrix,viewMatrix),(float3)normal));"\
		"float3 light0_direction = (float3) normalize(u_light0_position - eyeCoordinates);"\
		"float tn_dot_ld0 = max(dot(transformed_normals,light0_direction),0.0);"\
		"float3 ambient0 = u_La0 * u_Ka;"\
		"float3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;"\
		"float3 reflection_vector0 = reflect(-light0_direction,transformed_normals);"\
		"float3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0,viewer_vector),0.0),u_Material_shininess);"\
		"float3 light1_direction = (float3) normalize(u_light1_position - eyeCoordinates);"\
		"float tn_dot_ld1 = max(dot(transformed_normals,light1_direction),0.0);"\
		"float3 ambient1 = u_La1 * u_Ka;"\
		"float3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;"\
		"float3 reflection_vector1 = reflect(-light1_direction,transformed_normals);"\
		"float3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1,viewer_vector),0.0),u_Material_shininess);"\
		"float3 light2_direction = (float3) normalize(u_light2_position - eyeCoordinates);"\
		"float tn_dot_ld2 = max(dot(transformed_normals,light2_direction),0.0);"\
		"float3 ambient2 = u_La2 * u_Ka;"\
		"float3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;"\
		"float3 reflection_vector2 = reflect(-light2_direction,transformed_normals);"\
		"float3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2,viewer_vector),0.0),u_Material_shininess);"\
		"output.color = float4(ambient0 + diffuse0 + specular0 + ambient1 + diffuse1 + specular1 + ambient2 + diffuse2 + specular2,1.0);"\
		"}"\
		"else"\
		"{"\
		"output.color = float4(1.0,1.0,1.0,1.0);"\
		"}"\
		"float4 world = mul(modelMatrix , pos);"\
		"float4 worldView = mul(viewMatrix , world);"\
		"output.position = mul(projectionMatrix , worldView);"\
		"return (output);"\
		"}";


	pID3DBlob_VertexShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode_PerVertex, lstrlenA(vertexShaderSourceCode_PerVertex) + 1, "VS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "vs_5_0", 0, 0, &pID3DBlob_VertexShaderCode, &pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for vertex shader (per vertex): %s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() succeeded for vertex shader (per vertex).\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader_PerVertex);


	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateVertexShader() failed for per vertex.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateVertexShader() succeeded for per vertex .\n");
		fclose(gpFile);
	}

	//gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);

	const char *pixelShaderSourceCode_PerVertex =
		"float4 main(float4 pos:SV_POSITION,float4 color:COLOR):SV_TARGET"\
		"{"\
		"return(color);"\
		"}";

	pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode_PerVertex, lstrlenA(pixelShaderSourceCode_PerVertex) + 1, "PS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "ps_5_0", 0, 0, &pID3DBlob_PixelShaderCode, &pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for pixel shader per vertex: %s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() succeeded for pixel shader per vertex.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader_PerVertex);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreatePixelShader() failed for per vertex.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreatePixelShader() succeeded for per vertex .\n");
		fclose(gpFile);
	}

	// create and set input layout.      

	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];

	// Position.

	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].InstanceDataStepRate = 0;

	// Color.

	inputElementDesc[1].SemanticName = "NORMAL";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(inputElementDesc, 2, pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateInputLayout() failed.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateInputLayout() succeeded .\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;

	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// Sphere Position.

	D3D11_BUFFER_DESC bufferDesc;

	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Sphere_Position);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateBuffer() failed for sphere position vertex buffers.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateBuffer() succeeded for sphere position vertex buffers.\n");
		fclose(gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE mappedSubresource;

	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Sphere_Position, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, sizeof(sphere_vertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Sphere_Position, 0);


	// Sphere Normal

	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_VertexBuffer_Sphere_Normal);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For sphere normal Vertex buffer. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For sphere normal Vertex buffer.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Sphere_Normal, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_normals, sizeof(sphere_normals));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Sphere_Normal, 0);


	//Sphere Elements.

	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_elements);
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		nullptr,
		&gpID3D11Buffer_IndexBuffers);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For index Buffer Vertex Shader. Exitting Now...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For index Buffer Vertex Shader.\n");
		fclose(gpFile);
	}

	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_IndexBuffers, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, sizeof(sphere_elements));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_IndexBuffers, 0);



	// define and set constant buffer

	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));

	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateBuffer() failed for constant buffer.\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11Device::CreateInputLayout() succeeded for constant buffer.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 0.0f;

	//following code is required to off back face culling.

	D3D11_RASTERIZER_DESC rasterizerDesc;

	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0;
	rasterizerDesc.SlopeScaledDepthBias = 0.0;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	// call resize for first time

	hr = resize(WIN_WIDTH, WIN_HEIGHT);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() failed .\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() succeeded .\n");
		fclose(gpFile);
	}

	isLightEnabled = false;

	return(S_OK);
}


HRESULT resize(int width, int height)
{
	HRESULT hr = S_OK;

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}


	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize buffers accordingly

	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// get back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_backBuffer;

	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_backBuffer);


	// get render target view from d3d11 device using above back buffer

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_backBuffer, NULL, &gpID3D11RenderTargetView);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateRenderTargetView() failed .\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateRenderTargetView() succeeded .\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_backBuffer->Release();
	pID3D11Texture2D_backBuffer = NULL;


	// DSV 

	D3D11_TEXTURE2D_DESC textureDesc;

	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1,
		textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	ID3D11Texture2D *pID3D11Texture2D = NULL;

	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateTexture2D() failed .\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateTexture2D() succeeded .\n");
		fclose(gpFile);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;

	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D, &depthStencilViewDesc, &gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateDepthStencilView() failed .\n");
		fclose(gpFile);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "gpID3D11Device::CreateDepthStencilView() succeeded .\n");
		fclose(gpFile);
	}

	pID3D11Texture2D->Release();
	pID3D11Texture2D = NULL;

	// set render target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	D3D11_VIEWPORT d3dViewport;
	d3dViewport.TopLeftX = 0;
	d3dViewport.TopLeftY = 0;
	d3dViewport.Width = (float)width;
	d3dViewport.Height = (float)height;
	d3dViewport.MinDepth = 0.0f;
	d3dViewport.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewport);

	// set perspective matrix

	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return(hr);
}

void display(void)
{

	if (gbPerVertexLighting == true)
	{
		gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PerVertex, 0, 0);
		gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PerVertex, 0, 0);
	}
	else
	{
		gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PerPixel, 0, 0);
		gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PerPixel, 0, 0);
	}

	// clear render target view to chosen color.

	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	// select which vertex buffer to display

	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0,//input slot
		1,  //how many buffers you have
		&gpID3D11Buffer_VertexBuffer_Sphere_Position,
		&stride,
		&offset);


	gpID3D11DeviceContext->IASetVertexBuffers(1,//input slot
		1,  //how many buffers you have
		&gpID3D11Buffer_VertexBuffer_Sphere_Normal,
		&stride,
		&offset);

	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_IndexBuffers, DXGI_FORMAT_R16_UINT, 0); // R16 maps with 'short'

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX wvMatrix = XMMatrixIdentity();

	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();

	worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 2.0f);

	// load the data into the constant buffer.

	CBUFFER constantBuffer;

	ZeroMemory(&constantBuffer, sizeof(CBUFFER));

	constantBuffer.ModelMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	constantBuffer.u_La0 = XMVectorSet(light0_ambient[0], light0_ambient[1], light0_ambient[2], light0_ambient[3]);
	constantBuffer.u_Ld0 = XMVectorSet(light0_diffuse[0], light0_diffuse[1], light0_diffuse[2], light0_diffuse[3]);
	constantBuffer.u_Ls0 = XMVectorSet(light0_specular[0], light0_specular[1], light0_specular[2], light0_specular[3]);

	constantBuffer.u_La1 = XMVectorSet(light1_ambient[0], light1_ambient[1], light1_ambient[2], light1_ambient[3]);
	constantBuffer.u_Ld1 = XMVectorSet(light1_diffuse[0], light1_diffuse[1], light1_diffuse[2], light1_diffuse[3]);
	constantBuffer.u_Ls1 = XMVectorSet(light1_specular[0],light1_specular[1],light1_specular[2],light1_specular[3]);

	constantBuffer.u_La2 = XMVectorSet(light2_ambient[0], light2_ambient[1], light2_ambient[2], light2_ambient[3]);
	constantBuffer.u_Ld2 = XMVectorSet(light2_diffuse[0], light2_diffuse[1], light2_diffuse[2], light2_diffuse[3]);
	constantBuffer.u_Ls2 = XMVectorSet(light2_specular[0],light2_specular[1],light2_specular[2],light2_specular[3]);

	constantBuffer.u_Ka = XMVectorSet(material_Ambient[0], material_Ambient[1], material_Ambient[2], material_Ambient[3]);
	constantBuffer.u_Kd = XMVectorSet(material_Diffuse[0], material_Diffuse[1], material_Diffuse[2], material_Diffuse[3]);
	constantBuffer.u_Ks = XMVectorSet(material_Specular[0], material_Specular[1], material_Specular[2], material_Specular[3]);

	constantBuffer.u_Material_shininess = material_shininess;

	constantBuffer.u_light0_position = XMVectorSet(light0_position[0], light0_position[1], light0_position[2], light0_position[3]);
	constantBuffer.u_light1_position = XMVectorSet(light1_position[0], light1_position[1], light1_position[2], light1_position[3]);
	constantBuffer.u_light2_position = XMVectorSet(light2_position[0], light2_position[1], light2_position[2], light2_position[3]);

	if (isLightEnabled == true)
	{
		light0_position[1] = 10 * cos(angleRedLight);
		light0_position[2] = 10 * sin(angleRedLight);

		light1_position[0] = 10 * cos(-angleGreenLight);
		light1_position[2] = 10 * sin(-angleGreenLight);

		light2_position[0] = 10 * cos(angleBlueLight);
		light2_position[1] = 10 * sin(angleBlueLight);

		constantBuffer.u_LKeyPressed = 1;
	}
	else
	{
		constantBuffer.u_LKeyPressed = 0;
	}

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);


	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// switch between front and back buffers

	gpIDXGISwapChain->Present(0, 0);
}


void uninitialize(void)
{
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_IndexBuffers)
	{
		gpID3D11Buffer_IndexBuffers->Release();
		gpID3D11Buffer_IndexBuffers = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Sphere_Position)
	{
		gpID3D11Buffer_VertexBuffer_Sphere_Position->Release();
		gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Sphere_Normal)
	{
		gpID3D11Buffer_VertexBuffer_Sphere_Normal->Release();
		gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;
	}



	if (gpID3D11VertexShader_PerPixel)
	{
		gpID3D11VertexShader_PerPixel->Release();
		gpID3D11VertexShader_PerPixel = NULL;
	}

	if (gpID3D11VertexShader_PerPixel)
	{
		gpID3D11VertexShader_PerPixel->Release();
		gpID3D11VertexShader_PerPixel = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Uninitialize() succeeded .\n");
		fprintf_s(gpFile, "Log file is successfully closed .\n");
		fclose(gpFile);
	}
}
