
/// GLXContextWithFBConfigs

//headers
#include <iostream>
#include <stdio.h> //for printf()
#include <stdlib.h> //for exit()
#include <memory.h> //for memset()

//headers for XServer
#include <X11/Xlib.h> //analogous to windows.h
#include <X11/Xutil.h> //for visuals
#include <X11/XKBlib.h> //XkbKeycodeToKeysym()
#include <X11/keysym.h> //for 'Keysym'

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h> //for 'glx' functions

#include<SOIL/SOIL.h>

#include "vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variable declarations
FILE *gpFile = NULL;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext; //parallel to HGLRC
GLsizei giWidth ,giHeight;

bool gbFullscreen = false;

using namespace vmath;

// Shader related variables.

enum 
{
   VDG_ATTRIBUTE_VERTEX =0,
   VDG_ATTRIBUTE_COLOR,
   VDG_ATTRIBUTE_NORMAL,
   VDG_ATTRIBUTE_TEXTURE0,
};

mat4 gPerspectiveProjectionMatrix;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Cube;
GLuint gVbo_Cube_Position;
GLuint gVbo_Cube_Normal;

GLuint gModelViewMatrixUniform,gProjectionMatrixUniform; 
GLuint gLdUniform,gKdUniform,gLightPositionUniform;
GLuint gLKeyPressedUniform;

GLfloat gAngle =0.0f;

bool gbAnimate;
bool gbLight;


//entry-point function
int main(int argc, char *argv[])
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void resize(int,int);
	void display(void);
    void uninitialize(void);
    void spin(void);
    
	//code
	// create log file
	gpFile=fopen("Log.txt", "w");
	if (gpFile==NULL)
	{
		printf("Log File Can Not Be Created. Exiting Now ...\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}
	
	// create the window
	CreateWindow();
	
	//initialize()
	initialize();
	
	//Message Loop

	//variable declarations
	XEvent event; //parallel to 'MSG' structure
	KeySym keySym;
	int winWidth;
	int winHeight;
    bool bDone=false;
    static bool bIsAKeyPressed = false;
    static bool bIsLKeyPressed = false;
	
	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event); //parallel to GetMessage()
			switch(event.type) //parallel to 'iMsg'
			{
				case MapNotify: //parallel to WM_CREATE
					break;
				case KeyPress: //parallel to WM_KEYDOWN
					keySym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keySym)
					{
						case XK_Escape:
							bDone=true;
							break;
						case XK_F:
						case XK_f:
							if(gbFullscreen==false)
							{
								ToggleFullscreen();
								gbFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbFullscreen=false;
							}
                            break;
                        case XK_A:
                        case XK_a:
                            if(bIsAKeyPressed == false)
                            {
                                gbAnimate =true;
                                bIsAKeyPressed = true;
                            }
                            else
                            {
                                gbAnimate =false;
                                bIsAKeyPressed = false;
                            }
                            break;
                        case XK_L:
                        case XK_l:
                                if(bIsLKeyPressed == false)
                                {
                                    gbLight =true;
                                    bIsLKeyPressed = true;
                                }
                                else
                                {
                                    gbLight =false;
                                    bIsLKeyPressed = false;
                                }
                            break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1: //Left Button
							break;
						case 2: //Middle Button
							break;
						case 3: //Right Button
							break;
						default: 
							break;
					}
					break;
				case MotionNotify: //parallel to WM_MOUSEMOVE
					break;
				case ConfigureNotify: //parallel to WM_SIZE
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose: //parallel to WM_PAINT
					break;
				case DestroyNotify:
					break;
				case 33: //close button, system menu -> close
					bDone=true;
					break;
				default:
					break;
			}
		}
        
        display();
        
        if(gbAnimate == true)
        {
            spin();
        }
	}
	
	uninitialize();
	return(0);
}



void CreateWindow(void)
{
    void uninitialize(void);

    XSetWindowAttributes winAttribs;
    GLXFBConfig *pGLXFBConfigs=NULL;
    GLXFBConfig  bestGLXFBConfig;
    XVisualInfo *pTempXVisualInfo =NULL;
    int iNumFBConfigs=0;
    int styleMask;
    int i;

    static int frameBufferAttributes[]=
    {
        GLX_X_RENDERABLE,True,
        GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
        GLX_RENDER_TYPE,GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
        GLX_RED_SIZE,8,
        GLX_GREEN_SIZE,8,
        GLX_BLUE_SIZE,8,
        GLX_ALPHA_SIZE,8,
        GLX_DEPTH_SIZE,24,
        GLX_STENCIL_SIZE,8,
        GLX_DOUBLEBUFFER,True,
        // GLX_SAMPLE_BUFFERS,1,
        //GLX_SAMPLES,4,
        None
    };

    gpDisplay=XOpenDisplay(NULL);
    
    if(gpDisplay== NULL)
    {
        printf("Error: Unable to obtain X display.\n");
        uninitialize();
        exit(1);
    }

    pGLXFBConfigs=glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);

    if(pGLXFBConfigs==NULL)
    {
        printf("Error : Failed to get valid framebuffer config.Exiting now......\n");
        uninitialize();
        exit(1);
    }

    printf("%d matching FB Configs found.\n",iNumFBConfigs);

    int  bestFramebufferconfig=-1;
    int  worstFramebufferConfig=-1;
    int  bestNumberOfSamples=-1;
    int  worstNumberOfSamples=999;

    for(i=0;i<iNumFBConfigs;i++)
    {
        pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
        
        if(pTempXVisualInfo)
        {
            int sampleBuffer,samples;

            glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
            glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
            printf("Matching framebuffer config = %d : Visual id = 0x%lu : SAMPLE_BUFFER =%d :SAMPLES =%d\n",i,pTempXVisualInfo->visualid,sampleBuffer,samples);

            if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
            {
                bestFramebufferconfig=i;
                bestNumberOfSamples=samples;
            }

            if(worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
            {
                worstFramebufferConfig=i;
                worstNumberOfSamples=samples;
            }

        }

        XFree(pTempXVisualInfo);
    }

    bestGLXFBConfig=pGLXFBConfigs[bestFramebufferconfig];

    gGLXFBConfig=bestGLXFBConfig;

    XFree(pGLXFBConfigs);

    gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
    printf("Chosen visual ID=0x%lu\n",gpXVisualInfo->visualid);

    winAttribs.border_pixel=0;
    winAttribs.background_pixmap=0;
    winAttribs.colormap= XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
    winAttribs.event_mask=StructureNotifyMask|KeyPressMask|ButtonPressMask|ExposureMask|VisibilityChangeMask|PointerMotionMask;
    
    styleMask= CWBorderPixel|CWEventMask|CWColormap;
    gColormap=winAttribs.colormap;

    gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,WIN_WIDTH,WIN_HEIGHT,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);

    if(!gWindow)
    {
        printf("Failure in window creation.\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay,gWindow,"OpenGL Programmable Pipeline Diffused Light");
    Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);

}

void ToggleFullscreen(void)
{
    Atom wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

    XEvent event;
    memset(&event,0,sizeof(XEvent));

    event.type=ClientMessage;
    event.xclient.window=gWindow;
    event.xclient.message_type=wm_state;
    event.xclient.format=32;
    event.xclient.data.l[0]=gbFullscreen?0:1;

    Atom fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    event.xclient.data.l[1]=fullscreen;

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&event);

}


void initialize(void)
{
    void uninitialize(void);
    void resize(int,int);

    glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

    GLint attribs[] ={GLX_CONTEXT_MAJOR_VERSION_ARB,3,
                      GLX_CONTEXT_MINOR_VERSION_ARB,1,
                      GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
                       0};
    
    gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

    if(!gGLXContext)
    {
        GLint attribs[]={
            GLX_CONTEXT_MAJOR_VERSION_ARB,1,
            GLX_CONTEXT_MINOR_VERSION_ARB,0,
            0
        };
    
        printf("Failed to create GLX 4.5 context.Hence using old style GLX context\n");
        gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
    }

    else
    {
        printf("OpenGL context 4.5 is created.\n");
    }

    if(!glXIsDirect(gpDisplay,gGLXContext))
    {
        printf("Indirect GLX Rendering context obtained\n");
    }

    else
    {
        printf("Direct GLX Rendering context obtained\n");
    }

    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

    if(glewInit() != GLEW_OK)
	{
        fprintf(gpFile, "Failed to init. Exiting application \n");
        
        uninitialize();
	
	}

     else
    {
          fprintf(gpFile, "glew init successful.\n");
    }

    /// **** Vertex shader **** ///
    
    // Create shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // Provide source code to shader
    const GLchar *vertexShaderSourceCode = 
    "#version 450 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;"\
    "uniform mat4 u_model_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_LKeyPressed;"\
    "uniform vec3 u_Ld;"\
    "uniform vec3 u_Kd;"\
    "uniform vec4 u_light_position;"\
    "out vec3 diffuse_light ;" \
    "void main(void)" \
    "{" \
    "if (u_LKeyPressed == 1)"\
    "{"\
    "vec4 eyeCoordinates = u_model_view_matrix * vPosition  ;"\
    "vec3 tnorm = normalize(mat3(u_model_view_matrix) * vNormal);"\
    "vec3 s = normalize(vec3(u_light_position - eyeCoordinates ));"\
    "diffuse_light = u_Ld * u_Kd * max(dot(s,tnorm),0.0);"\
    /* "diffuse_light = vec3(1.0f,0.0f,0.0f)  ;"\ */
    "}"\
    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition ;"\
    "}";


    glShaderSource(gVertexShaderObject,1,(const GLchar**) &vertexShaderSourceCode,NULL);

    // Compile shader 
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength =0;
    GLint iShaderCompileStatus =0;
    char *szInfoLog = NULL;

    glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE)
    {
      glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

       if(iInfoLogLength > 0)
       {
          szInfoLog = (char *) malloc(iInfoLogLength);
           
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
       }

    }

    /// *** Fragment Shader *** ///

    // Create shader 

    gFragmentShaderObject=  glCreateShader(GL_FRAGMENT_SHADER);

    // provide source code to shader 
    const GLchar *fragmentShaderSourceCode =
    "#version 450 core" \
    "\n" \
    "in vec3 diffuse_light;" \
    "out vec4 FragColor;" \
    "uniform int u_LKeyPressed;" \
    "void main(void)" \
    "{" \
    "vec4 color;"\
    "if (u_LKeyPressed == 1)"\
    "{"\
    "color = vec4(diffuse_light,1.0);"\
    "}"\
    "else"\
    "{"\
    "color = vec4(1.0,1.0,1.0,1.0);"\
    "}"\
    "FragColor = color ;" \
    "}" ;

    glShaderSource(gFragmentShaderObject,1, (const GLchar **) &fragmentShaderSourceCode,NULL);

    // Compile shader

    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);

    if (iShaderCompileStatus == GL_FALSE)
    {
       glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
         
          if(iInfoLogLength >0)
          {
             szInfoLog = (char*) malloc(iInfoLogLength);

            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"Fragment Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
          }
    }

    /// *** Shader Program *** ///

    // Create
    gShaderProgramObject = glCreateProgram();

    // Attach vertex shader to shader program.

    glAttachShader(gShaderProgramObject,gVertexShaderObject);

    // Attach fragment shader to shader program.

    glAttachShader(gShaderProgramObject,gFragmentShaderObject);

    // Pre-link binding of shader program object with vertex shader position attribute.

    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

    // Pre-link binding of shader program object with vertex shader color attribute
    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");
    
    // Link shader

    glLinkProgram(gShaderProgramObject);
    
    GLint iShaderProgramLinkStatus = 0;

    glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

    if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *) malloc (iInfoLogLength);
            
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"Shader Program Link Log :  %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }

    }

    // get uniform location
    gModelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_model_view_matrix");

    gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_projection_matrix");

    gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject,"u_LKeyPressed");

    gLdUniform = glGetUniformLocation(gShaderProgramObject,"u_Ld");

    gKdUniform = glGetUniformLocation(gShaderProgramObject,"u_Kd");

    gLightPositionUniform = glGetUniformLocation(gShaderProgramObject,"u_light_position");

    GLfloat cubeVertices[] =
	{
		// 1.0f, 1.0f, -1.0f,
		// -1.0f, 1.0f, -1.0f,
		// -1.0f, 1.0f, 1.0f,
		// 1.0f, 1.0f, 1.0f,

		// 1.0f, -1.0f, 1.0f,
		// -1.0f, -1.0f, 1.0f,
		// -1.0f, -1.0f, -1.0f,
		// 1.0f, -1.0f, -1.0f,

		// 1.0f, 1.0f, 1.0f,
		// -1.0f, 1.0f, 1.0f,
		// -1.0f, -1.0f, 1.0f,
		// 1.0f, -1.0f, 1.0f,

		// 1.0f, -1.0f, -1.0f,
		// -1.0f, -1.0f, -1.0f,
		// -1.0f, 1.0f, -1.0f,
		// 1.0f, 1.0f, -1.0f,

		// -1.0f, 1.0f, 1.0f,
		// -1.0f, 1.0f, -1.0f,
		// -1.0f, -1.0f, -1.0f,
		// -1.0f, -1.0f, 1.0f,

		// 1.0f, 1.0f, -1.0f,
		// 1.0f, 1.0f, 1.0f,
		// 1.0f, -1.0f, 1.0f,
        // 1.0f, -1.0f, -1.0f,
        
        1.0f, 1.0f, -1.0f,   // Right-top corner of top face 
        -1.0f, 1.0f, -1.0f, // left-top corner of the top face
        -1.0f, 1.0f, 1.0f,   //left bottom corner of front face
        1.0f, 1.0f, 1.0f, //right bottom corner of front face
      
                        //Bottom face
        
        1.0f, -1.0f, -1.0f,   // Right-top corner of bottom face 
        -1.0f, -1.0f, -1.0f, // left-top corner of the bottom face
        -1.0f, -1.0f, 1.0f,   //left bottom corner of bottom face
        1.0f, -1.0f, 1.0f, //right bottom corner of bottom face
      
                         //Front face
    
        1.0f, 1.0f, 1.0f,   // Right-top corner of front face 
        -1.0f, 1.0f, 1.0f, // left-top corner of the front face
        -1.0f, -1.0f, 1.0f,   //left bottom corner of front face
        1.0f, -1.0f, 1.0f, //right bottom corner of front face
      
      
                         //Back face
      
        1.0f, 1.0f, -1.0f,   // Right-top corner of back face 
        -1.0f, 1.0f, -1.0f, // left-top corner of the back face
        -1.0f, -1.0f, -1.0f,   //left bottom corner of back face
        1.0f, -1.0f, -1.0f, //right bottom corner of back face
      
                        //Right face
      
        1.0f, 1.0f, -1.0f,   // Right-top corner of right face 
        1.0f, 1.0f, 1.0f, // left-top corner of the right face
        1.0f, -1.0f, 1.0f,   //left bottom corner of right face
        1.0f, -1.0f, -1.0f, //right bottom corner of right face
      
                        //Left face
    
        -1.0f, 1.0f, 1.0f,   // Right-top corner of left face 
        -1.0f, 1.0f, -1.0f, // left-top corner of the left face
        -1.0f, -1.0f, -1.0f,   //left bottom corner of left face
        -1.0f, -1.0f, 1.0f //right bottom corner of left face
      
	};

	for (int i = 0; i<72; i++)
	{
		if (cubeVertices[i]<0.0f)
			cubeVertices[i] = cubeVertices[i] + 0.25f;
		else if (cubeVertices[i]>0.0f)
			cubeVertices[i] = cubeVertices[i] - 0.25f;
		else
			cubeVertices[i] = cubeVertices[i];
	}

	const GLfloat cubeNormals[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,

		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,

		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		-1.0f, 0.0f, 1.0f,
		-1.0f, 0.0f, 1.0f,
		-1.0f, 0.0f, 1.0f,
		-1.0f, 0.0f, 1.0f
	};
    
    // Cube Vao

    glGenVertexArrays(1,&gVao_Cube);
    glBindVertexArray(gVao_Cube);

    // Cube position Vbo

    glGenBuffers(1,&gVbo_Cube_Position);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_Cube_Position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(cubeVertices),cubeVertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0);  // Unbind position Vbo

    // Sqaure normal vbo

    glGenBuffers(1,&gVbo_Cube_Normal);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_Cube_Normal);
    glBufferData(GL_ARRAY_BUFFER,sizeof(cubeNormals),cubeNormals,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER,0);  // unbind color Vbo

    glBindVertexArray(0); // unbind square vao

    glShadeModel(GL_SMOOTH);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);

    //glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

    //glEnable(GL_CULL_FACE);
    
    glEnable(GL_TEXTURE_2D);

    glClearColor(0.0f,0.0f,0.0f,0.0f);

    // set orthographic projection matrix to identity matrix.

    gPerspectiveProjectionMatrix = mat4 :: identity();

    gbAnimate = false;
    gbLight = false;

    resize(WIN_WIDTH,WIN_HEIGHT);
}


void resize(int width,int height)
{
    if(height==0)
        height=1;
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    gPerspectiveProjectionMatrix = perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);
}

void spin(void)
{
    gAngle = gAngle + 0.3f;
    
    if(gAngle>=360.0f)
        gAngle = gAngle - 360.0f;
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Start using OpenGL Program object.

    glUseProgram(gShaderProgramObject);

    if (gbLight == true)
    {
        glUniform1i(gLKeyPressedUniform,1);

        glUniform3f(gLdUniform,1.0f,1.0f,1.0f);
        glUniform3f(gKdUniform,0.5f,0.5f,0.5f);

        float lightPosition[]={0.0f,0.0f,2.0f,1.0f};
        glUniform4fv(gLightPositionUniform,1,(GLfloat *)lightPosition);
    }
    
    else
    {
        glUniform1i(gLKeyPressedUniform,0);
    }

    // OpenGL drawing 
    // Set modelview and modelviewprojection matrices to identity

    mat4 modelMatrix = mat4:: identity();
    mat4 modelViewMatrix = mat4::identity();
    mat4 rotationMatrix = mat4 :: identity();

    // Translate model view matrix.

    modelMatrix = translate(0.0f,0.0f,-5.0f);

    // All axes rotation by gAngle angle

    rotationMatrix = rotate(gAngle,gAngle,gAngle);

    // Multiply rotation matrix and model matrix to get modelView matrix

    modelViewMatrix = modelMatrix * rotationMatrix ;


    // pass modelview matrix to the vertex shader in 'u_model_view_matrix' shader variable
    // whose position value we already calculated by using glGetUniformLocation()
    
    glUniformMatrix4fv(gModelViewMatrixUniform,1,GL_FALSE,modelViewMatrix);

    // pass projection matrix to the vertex shader in 'u_projection_matrix' shader variable
    // whose position value we already calculated by using glGetUniformLocation()
    
    glUniformMatrix4fv(gProjectionMatrixUniform,1,GL_FALSE,gPerspectiveProjectionMatrix);
    

    // *** Bind Cube Vao ***//

    glBindVertexArray(gVao_Cube);

    // Draw Cube

    glDrawArrays(GL_TRIANGLE_FAN,0,4); 
    glDrawArrays(GL_TRIANGLE_FAN,4,4); 
    glDrawArrays(GL_TRIANGLE_FAN,8,4); 
    glDrawArrays(GL_TRIANGLE_FAN,12,4); 
    glDrawArrays(GL_TRIANGLE_FAN,16,4); 
    glDrawArrays(GL_TRIANGLE_FAN,20,4); 

    // unbind vao

    glBindVertexArray(0);

    glUseProgram(0);

    glXSwapBuffers(gpDisplay,gWindow);

}

void uninitialize(void)
{

    // Destroy Cube Vao 
    if (gVao_Cube)
    {
        glDeleteVertexArrays(1,&gVao_Cube);
        gVao_Cube=0;
    }

    // Destroy Cube position Vbo 
    if (gVbo_Cube_Position)
    {
        glDeleteVertexArrays(1,&gVbo_Cube_Position);
        gVbo_Cube_Position=0;
    }

    // Destroy Cube normal Vbo 
    if (gVbo_Cube_Normal)
    {
        glDeleteVertexArrays(1,&gVbo_Cube_Normal);
        gVbo_Cube_Normal=0;
    }

    // detach vertex shader from shader program object 

    glDetachShader(gShaderProgramObject,gVertexShaderObject);

    // detach fragment shader from shader program object

    glDetachShader(gShaderProgramObject,gFragmentShaderObject);

    // delete vertex shader object

    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject =0;

    // delete fragment shader object

    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject =0;

    // delete shader program object

    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject =0;

    // unlink shader program
    glUseProgram(0);

    GLXContext currentContext=glXGetCurrentContext();

    if(currentContext!=NULL && currentContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo=NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay=NULL;
    }

    if(gpFile)
    {
        fprintf(gpFile,"Log file is successfully closed.\n");
        fclose(gpFile);
        gpFile=NULL;
    }
}
