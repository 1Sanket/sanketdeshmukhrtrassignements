
 // XOpenGL MonkeyHead Mesh Loading 
 #include<iostream>
 #include<stdio.h>
 #include<stdlib.h>
 #include<memory.h>
 //#include<math.h>
 
 #include<X11/Xlib.h>
 #include<X11/Xutil.h>
 #include<X11/XKBlib.h>
 #include<X11/keysym.h>
 
 #include<GL/gl.h>
 #include<GL/glx.h>
 #include<GL/glu.h>
 
 // Symbolic Constants

 #define FOY_ANGLE 45    // Field of view in y direction
 #define ZNEAR 0.1
 #define ZFAR 200.0
 
 #define VIEWPORT_BOTTOMLEFT_X 0 
 #define VIEWPORT_BOTTOMLETF_Y 0
 
 
 // global varibales
 
 bool gbFullscreen =false;
 bool gbIsLightingEnabled = false;
 Display *gpDisplay =NULL;
 XVisualInfo *gpXVisualInfo = NULL;
 Colormap gColormap;
 Window gWindow;
 int giWindowWidth=800;
 int giWindowHeight=600;
 
 GLXContext gGLXContext;
 GLUquadric *quadric = NULL;
 
 GLboolean gbPyramid = GL_TRUE;
 GLboolean gbSphere = GL_FALSE;
 GLboolean gbCube = GL_FALSE;

 float angleCube = 0.0f;
 float anglePyramid = 0.0f;
 float angleSphere = 0.0f;
 
 // Arrays required for lighting.
GLfloat light0_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light0_diffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light0_specular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light0_position[] = { 2.0f,1.0f,1.0f,0.0f };

GLfloat light1_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light1_diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light1_specular[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light1_position[] = { -2.0f,1.0f,1.0f,0.0f };

GLfloat material_ambient[] = {0.0f,0.0f,0.0f,0.0f};
GLfloat material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat material_specular[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat material_shinyness[] = {50.0f};
 
 
 int main(void)
 {
   void CreateWindow(void);
   void ToggleFullscreen(void);
   void initialize(void);
   void display(void);
   void resize(int,int);
   void updateAngle(void);
   void uninitialize(void);
 
   int winWidth= giWindowWidth;
   int winHeight= giWindowHeight;
 
   bool bDone= false;
 
   CreateWindow();
 
   initialize();
 
   XEvent event;
   KeySym keysym;
 
   while(bDone == false)
   {
     while (XPending(gpDisplay))
      {
        XNextEvent (gpDisplay,&event);
 
        switch (event.type)
         {
          case MapNotify:
           break;
          case KeyPress:
           keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
 
           char ascii[26];
           XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);
 
      switch (ascii[0])
        {
          case 'f':
          case 'F':
                 if(gbFullscreen==false)
                 {
                    ToggleFullscreen();
                    gbFullscreen=true;
                 }
 
                 else
                 {
                     ToggleFullscreen();
                     gbFullscreen=false;
                 }
 
             break;
          case 'l':
          case 'L':
             if (gbIsLightingEnabled == false)
             {
               glEnable(GL_LIGHTING);
               gbIsLightingEnabled = true;
             }
             else
             {
               glDisable(GL_LIGHTING);
               gbIsLightingEnabled = false;
             }
             break;

          case 'p':
          case 'P':
             gbPyramid = GL_TRUE;
             gbCube = GL_FALSE;
             gbSphere = GL_FALSE;
             break;
       
          case 'c':
          case 'C':
             gbCube = GL_TRUE;
             gbPyramid = GL_FALSE;
             gbSphere = GL_FALSE;
             break;
             
          case 's':
          case 'S':
             gbSphere = GL_TRUE;
             gbPyramid = GL_FALSE;
             gbCube = GL_FALSE;
             break;       
     
          default:
             break;
        }
 
      switch(keysym)
        {
             case XK_Escape:
               bDone= true;
               break;
             default:
               break;
        }
          break;
 
         case ButtonPress:
           switch (event.xbutton.button)
            {
             case 1:
               break;
             case 2:
               break;
             case 3:
               break;
             default:
               break;
           }
           break;
 
         case MotionNotify:
           break;
 
         case ConfigureNotify:
             winWidth=event.xconfigure.width;
             winHeight=event.xconfigure.height;
             resize(winWidth,winHeight);
           break;
         case Expose:
           break;
         case DestroyNotify:
           break;
         case 33:
             bDone=true;
           break;
         default:
           break;
        }
     }
 
     updateAngle();
 
     display();
 
   }
 
   uninitialize();
   return(0);
 }
 
 void CreateWindow(void)
 {
   void uninitialize(void);
 
   XSetWindowAttributes windowAttributes;
   int defaultScreen;
   int defaultDepth;
   int styleMask;
 
   static int frameBufferAttributes[]=
   {
      //GLX_X_RENDERABLE,True,
      //GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
      GLX_RENDER_TYPE,GLX_RGBA_BIT,
     // GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
      GLX_RED_SIZE,8,
      GLX_GREEN_SIZE,8,
      GLX_BLUE_SIZE,8,
      GLX_ALPHA_SIZE,8,
      GLX_DEPTH_SIZE,24,
      GLX_DOUBLEBUFFER,True,
      None
   };
 
   gpDisplay= XOpenDisplay(NULL);
 
   if(gpDisplay == NULL)
   {
     printf(" Error: Unable to open X display.\n Exiting now....\n");
     uninitialize();
     exit(1);
   }
 
   defaultScreen= XDefaultScreen(gpDisplay);
 
   gpXVisualInfo= glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
 
   windowAttributes.border_pixel=0;
   windowAttributes.background_pixmap=0;
   windowAttributes.colormap= XCreateColormap(gpDisplay,
                                              RootWindow(gpDisplay,gpXVisualInfo->screen),
                                              gpXVisualInfo->visual,
                                              AllocNone
                                     );
   gColormap= windowAttributes.colormap;
 
   windowAttributes.background_pixel= BlackPixel(gpDisplay,defaultScreen);
 
   windowAttributes.event_mask= ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;
 
   styleMask= CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;
 
   gWindow= XCreateWindow(gpDisplay,
                         RootWindow(gpDisplay,  gpXVisualInfo->screen),
                         0,
                         0,
                         giWindowWidth,
                         giWindowHeight,
                         0,
                         gpXVisualInfo->depth,
                         InputOutput,
                         gpXVisualInfo->visual,
                         styleMask,
                         &windowAttributes
                       );
 
     if(!gWindow)
     {
       printf("Error: Failed to create main window.\n Exiting now \n");
       uninitialize();
       exit(1);
     }
 
     XStoreName(gpDisplay,gWindow,"Two Lights");
 
     Atom windowManagerDelete= XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
     XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
 
     XMapWindow(gpDisplay,gWindow);
 }
 
 void ToggleFullscreen(void)
   {
     Atom wm_state;
     Atom fullscreen;
     XEvent xev={0};
 
     wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
     memset(&xev,0,sizeof(xev));
 
     xev.type=ClientMessage;
 
     xev.xclient.window=gWindow;
     xev.xclient.message_type=wm_state;
     xev.xclient.format=32;
     xev.xclient.data.l[0]=gbFullscreen?0:1;
 
     fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
     xev.xclient.data.l[1]=fullscreen;
     XSendEvent(gpDisplay,
               RootWindow(gpDisplay,gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev
             );
   }
 
 
 void initialize(void)
 {
   void resize(int,int);
   void uninitialize(void);
 
   gGLXContext= glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
 
   glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
 
   glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
   
   glClearDepth(1.0f);
   glEnable(GL_DEPTH_TEST);
   glDepthFunc(GL_LEQUAL);
 
   glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
   glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular);
   glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
   glEnable(GL_LIGHT0);
 
   glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
   glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
   glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular);
   glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
   glEnable(GL_LIGHT1);
 
   glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
   glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
   glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
   glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
 
   resize(giWindowWidth,giWindowHeight);
 
 }

 
 void display(void)
 {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
  
  
    if (gbPyramid == GL_TRUE)
    {
      //glTranslatef(-1.5f, 0.0f, -6.0f);
      glTranslatef(0.0f, 0.0f, -6.0f);
      glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);
  
      // Pyramid
      glBegin(GL_TRIANGLES);
  
      // front face
      //glColor3f(1.0f, 0.0f, 0.0f); // red
      glNormal3f(0.0f, 0.447214f, 0.894427f);
  
      glVertex3f(0.0f, 1.0f, 0.0f);   // Apex
  
                      //glColor3f(0.0f, 1.0f, 0.0f);    // green
      glVertex3f(-1.0f, -1.0f, 1.0f); // left-corner of front face
  
                      //glColor3f(0.0f, 0.0f, 1.0f);    // blue
      glVertex3f(1.0f, -1.0f, 1.0f);   //right corner of front face
  
      //right face
                       //glColor3f(1.0f, 0.0f, 0.0f); // red
      glNormal3f(0.894427f, 0.447214f, 0.0f);
  
      glVertex3f(0.0f, 1.0f, 0.0f);     // apex
  
                        //glColor3f(0.0f, 0.0f, 1.0f);  // blue
      glVertex3f(1.0f, -1.0f, 1.0f);    //left corner of the right face
  
                        //glColor3f(0.0f, 1.0f, 0.0f);  //green
      glVertex3f(1.0f, -1.0f, -1.0f);  // right corner of the right face
  
        //Back face
  
                       //glColor3f(1.0f, 0.0f, 0.0f);//red
      glNormal3f(0.0f, 0.447214f, -0.894427f);
  
      glVertex3f(0.0f, 1.0f, 0.0f); // apex
  
                      //glColor3f(0.0f, 1.0f, 0.0f);// green
      glVertex3f(1.0f, -1.0f, -1.0f); //left corner of the back face
  
                      //glColor3f(0.0f, 0.0f, 1.0f);// blue
      glVertex3f(-1.0f, -1.0f, -1.0f); //right corner of the back face
  
        
       //left face
                       //glColor3f(1.0f, 0.0f, 0.0f);//red
  
      glNormal3f(-0.894427f, 0.447214f, 0.0f);
  
      glVertex3f(0.0f, 1.0f, 0.0f); // apex
  
                      //	glColor3f(0.0f, 0.0f, 1.0f);// blue
      glVertex3f(-1.0f, -1.0f, -1.0f); //left corner of the back face
  
                       //glColor3f(0.0f, 1.0f, 0.0f);// green
      glVertex3f(-1.0f, -1.0f, 1.0f); //right corner of the back face
  
      glEnd();
  
    }
  
  
    //glMatrixMode(GL_MODELVIEW);
    //glLoadIdentity();
       
    else if (gbCube == GL_TRUE)
    {
  
      //glTranslatef(1.5f, 0.0f, -6.0f);
      glTranslatef(0.0f, 0.0f, -6.0f);
      glScalef(0.75f, 0.75f, 0.75f);
      glRotatef(angleCube, 0.0f, 1.0f, 0.0f);
  
      // Cube
      glBegin(GL_QUADS);
  
      // Top face
      //glColor3f(1.0f, 0.0f, 0.0f); // red
  
      glNormal3f(0.0f, 1.0f, 0.0f);
      glVertex3f(1.0f, 1.0f, -1.0f);   // Right-top corner of top face 
      glVertex3f(-1.0f, 1.0f, -1.0f); // left-top corner of the top face
      glVertex3f(-1.0f, 1.0f, 1.0f);   //left bottom corner of front face
      glVertex3f(1.0f, 1.0f, 1.0f); //right bottom corner of front face
  
                      //Bottom face
                      //glColor3f(0.0f, 1.0f, 0.0f); // Green
      glNormal3f(0.0f, -1.0f, 0.0f);
      glVertex3f(1.0f, -1.0f, -1.0f);   // Right-top corner of bottom face 
      glVertex3f(-1.0f, -1.0f, -1.0f); // left-top corner of the bottom face
      glVertex3f(-1.0f, -1.0f, 1.0f);   //left bottom corner of bottom face
      glVertex3f(1.0f, -1.0f, 1.0f); //right bottom corner of bottom face
  
                       //Front face
                       //glColor3f(0.0f, 0.0f, 1.0f); // Blue
      glNormal3f(0.0f, 0.0f, 1.0f);
      glVertex3f(1.0f, 1.0f, 1.0f);   // Right-top corner of front face 
      glVertex3f(-1.0f, 1.0f, 1.0f); // left-top corner of the front face
      glVertex3f(-1.0f, -1.0f, 1.0f);   //left bottom corner of front face
      glVertex3f(1.0f, -1.0f, 1.0f); //right bottom corner of front face
  
  
                       //Back face
                       //glColor3f(0.0f, 1.0f, 1.0f); // Cyan
  
      glNormal3f(0.0f, 0.0f, -1.0f);
      glVertex3f(1.0f, 1.0f, -1.0f);   // Right-top corner of back face 
      glVertex3f(-1.0f, 1.0f, -1.0f); // left-top corner of the back face
      glVertex3f(-1.0f, -1.0f, -1.0f);   //left bottom corner of back face
      glVertex3f(1.0f, -1.0f, -1.0f); //right bottom corner of back face
  
                      //Right face
                      //glColor3f(1.0f, 0.0f, 1.0f); // Magenta
  
      glNormal3f(1.0f, 0.0f, 0.0f);
      glVertex3f(1.0f, 1.0f, -1.0f);   // Right-top corner of right face 
      glVertex3f(1.0f, 1.0f, 1.0f); // left-top corner of the right face
      glVertex3f(1.0f, -1.0f, 1.0f);   //left bottom corner of right face
      glVertex3f(1.0f, -1.0f, -1.0f); //right bottom corner of right face
  
                      //Left face
                      //glColor3f(1.0f, 1.0f, 0.0f); // Yellow
  
      glNormal3f(-1.0f, 0.0f, 1.0f);
      glVertex3f(-1.0f, 1.0f, 1.0f);   // Right-top corner of left face 
      glVertex3f(-1.0f, 1.0f, -1.0f); // left-top corner of the left face
      glVertex3f(-1.0f, -1.0f, -1.0f);   //left bottom corner of left face
      glVertex3f(-1.0f, -1.0f, 1.0f); //right bottom corner of left face
  
  
      glEnd();
  
    }
  
  
    else if (gbSphere ==  GL_TRUE)
    {
      glTranslatef(0.0f, 0.0f, -3.0f);
      glRotatef(angleSphere, 0.0f, 1.0f, 0.0f);
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      
      quadric = gluNewQuadric();
  
      gluSphere(quadric, 0.75, 30, 30);
  
    }
  
       glXSwapBuffers(gpDisplay,gWindow);
 }
 
 void updateAngle()
 {
  angleCube = angleCube + 0.4f;
	if (angleCube >= 360.0f)
		angleCube = 0.0f;
	anglePyramid = anglePyramid + 0.4f;
	if (anglePyramid >= 360.0f)
		anglePyramid = 0.0f;
	angleSphere = angleSphere + 0.4f;
	if (angleSphere >= 360.0f)
		angleSphere = 0.0f;
 }
 
 
 
 void resize(int width,int height)
 {
   if(height==0)
     height=1;
   glViewport (VIEWPORT_BOTTOMLEFT_X,VIEWPORT_BOTTOMLETF_Y,(GLsizei)width,(GLsizei) height);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(FOY_ANGLE, (GLfloat)width / (GLfloat)height, ZNEAR, ZFAR);
 
 }

 
 void uninitialize(void)
  {
 
    GLXContext currentGLXContext;
    currentGLXContext = glXGetCurrentContext();
 
    if(currentGLXContext !=NULL && currentGLXContext == gGLXContext )
    {
      glXMakeCurrent(gpDisplay,0,0);
    }
 
    if(gGLXContext)
    {
      glXDestroyContext(gpDisplay,gGLXContext);
    }
 
    if(gWindow)
    {
      XDestroyWindow(gpDisplay,gWindow);
    }
 
    if(gColormap)
    {
      XFreeColormap(gpDisplay,gColormap);
    }
 
    if(gpXVisualInfo)
    {
      free(gpXVisualInfo);
      gpXVisualInfo=NULL;
    }
 
    if(gpDisplay)
    {
      XCloseDisplay(gpDisplay);
      gpDisplay=NULL;
    }
 }