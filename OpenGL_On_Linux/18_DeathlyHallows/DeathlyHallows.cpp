
 // XOpenGL Double Buffer 2D Rotation
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

#define PI 3.1415926535898

using namespace std;

// global varibales
bool gbFullscreen =false;
Display *gpDisplay =NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

struct Point
{
	GLfloat xAxis;
	GLfloat yAxis;
};

struct Color
{
	GLfloat red;
	GLfloat green;
	GLfloat blue;
};

int main(void)
{
  void CreateWindow(void);
  void ToggleFullscreen(void);
  void initialize(void);
  void display(void);
  void resize(int,int);
  void uninitialize(void);

  int winWidth= giWindowWidth;
  int winHeight= giWindowHeight;

  bool bDone= false;

  CreateWindow();

  initialize();

  XEvent event;
  KeySym keysym;

  while(bDone == false)
  {
    while (XPending(gpDisplay))
     {
       XNextEvent (gpDisplay,&event);

       switch (event.type)
        {
         case MapNotify:
          break;
         case KeyPress:
          keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

          char ascii[26];
          XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);

          switch (ascii[0])
          {
            case 'f':
            case 'F':
                if(gbFullscreen==false)
                {
                   ToggleFullscreen();
                   gbFullscreen=true;
                }

                else
                {
                    ToggleFullscreen();
                    gbFullscreen=false;
                }

            break;
              default:
            break;
          }

          switch(keysym)
          {
            case XK_Escape:
              bDone= true;
              break;
            default:
              break;
          }
         break;

        case ButtonPress:
          switch (event.xbutton.button)
           {
            case 1:
              break;
            case 2:
              break;
            case 3:
              break;
            default:
              break;
          }
          break;

        case MotionNotify:
          break;

        case ConfigureNotify:
            winWidth=event.xconfigure.width;
            winHeight=event.xconfigure.height;
            resize(winWidth,winHeight);
          break;
        case Expose:
          break;
        case DestroyNotify:
          break;
        case 33:
            bDone=true;
          break;
        default:
          break;
       }
    }

    display();

  }

  uninitialize();
  return(0);
}

void CreateWindow(void)
{
  void uninitialize(void);

  XSetWindowAttributes windowAttributes;
  int defaultScreen;
  int defaultDepth;
  int styleMask;

  static int frameBufferAttributes[]=
  {
     //GLX_X_RENDERABLE,True,
     //GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
     GLX_RENDER_TYPE,GLX_RGBA_BIT,
    // GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
     GLX_RED_SIZE,8,
     GLX_GREEN_SIZE,8,
     GLX_BLUE_SIZE,8,
     GLX_ALPHA_SIZE,8,
     GLX_DEPTH_SIZE,24,
     GLX_DOUBLEBUFFER,True,
     None
  };

  gpDisplay= XOpenDisplay(NULL);

  if(gpDisplay == NULL)
  {
    printf(" Error: Unable to open X display.\n Exiting now....\n");
    uninitialize();
    exit(1);
  }

  defaultScreen= XDefaultScreen(gpDisplay);

  gpXVisualInfo= glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

  windowAttributes.border_pixel=0;
  windowAttributes.background_pixmap=0;
  windowAttributes.colormap= XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay,gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone
                                    );
  gColormap= windowAttributes.colormap;

  windowAttributes.background_pixel= BlackPixel(gpDisplay,defaultScreen);

  windowAttributes.event_mask= ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

  styleMask= CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

  gWindow= XCreateWindow(gpDisplay,
                        RootWindow(gpDisplay,  gpXVisualInfo->screen),
                        0,
                        0,
                        giWindowWidth,
                        giWindowHeight,
                        0,
                        gpXVisualInfo->depth,
                        InputOutput,
                        gpXVisualInfo->visual,
                        styleMask,
                        &windowAttributes
                      );

    if(!gWindow)
    {
      printf("Error: Failed to create main window.\n Exiting now \n");
      uninitialize();
      exit(1);
    }

    XStoreName(gpDisplay,gWindow,"Deathly Hallows");

    Atom windowManagerDelete= XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
  {
    Atom wm_state;
    Atom fullscreen;
    XEvent xev={0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type=ClientMessage;

    xev.xclient.window=gWindow;
    xev.xclient.message_type=wm_state;
    xev.xclient.format=32;
    xev.xclient.data.l[0]=gbFullscreen?0:1;

    fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1]=fullscreen;
    XSendEvent(gpDisplay,
              RootWindow(gpDisplay,gpXVisualInfo->screen),
              False,
              StructureNotifyMask,
              &xev
            );
  }


void initialize(void)
{
  void resize(int,int);

  gGLXContext= glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

  glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);
  
  glDepthFunc(GL_LEQUAL);
  
  glClearColor(0.0f,0.0f,0.0f,0.0f);  

  resize(giWindowWidth,giWindowHeight);

}

void display(void)
{

  void drawLine(Point, Point, Color, GLfloat);
	void drawTriangle(Point, Point, Point, Color, GLfloat);
	void drawCircle(GLfloat radius, Point incenter,Color color);

	Point getIncenterOfTriangle(Point, Point, Point);
	GLfloat getRadiusOfIncircle(Point, Point, Point);
	GLint circle_points = 1000000;
	GLfloat angle;
	GLfloat sideOfTriangle = 0.9f;
	GLfloat radius = getRadiusOfIncircle(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) },
		Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) });

	Point incenter = getIncenterOfTriangle(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) },
		Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) });

  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
 glTranslatef(0.0f, 0.0f, -3.0f);

	drawCircle(radius,incenter ,Color{ 1.0f,1.0f,1.0f });
	drawTriangle(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) },
		Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Color{ 1.0f,1.0f,1.0f },
		1.0f);
	drawLine(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) }, Point{ 0.0f, -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) }, Color{ 1.0f,1.0f,1.0f }, 1.0f);

  glXSwapBuffers(gpDisplay,gWindow);
}


GLfloat getSideOfTriangle(Point vertex1, Point vertex2)
{
	GLfloat sideOfTriangle = 0.0f;

	return sqrt(((vertex2.xAxis - vertex1.xAxis)*(vertex2.xAxis - vertex1.xAxis) + (vertex2.yAxis - vertex1.yAxis)*(vertex2.yAxis - vertex1.yAxis)));
}


Point getIncenterOfTriangle(Point vertexA, Point vertexB, Point vertexC)
{
	GLfloat perimeter = 0.0f;

	GLfloat sideA = getSideOfTriangle(vertexB, vertexC);
	GLfloat sideB = getSideOfTriangle(vertexA, vertexC);
	GLfloat sideC = getSideOfTriangle(vertexA, vertexB);

	perimeter = sideA + sideB + sideC;

	GLfloat x_Axis_Incenter = ((sideA*vertexA.xAxis) + (sideB*vertexB.xAxis) + (sideC*vertexC.xAxis)) / perimeter;
	GLfloat y_Axis_Incenter = ((sideA*vertexA.yAxis) + (sideB*vertexB.yAxis) + (sideC*vertexC.yAxis)) / perimeter;

	return Point{ x_Axis_Incenter,y_Axis_Incenter };
}

GLfloat getRadiusOfIncircle(Point vertexA, Point vertexB, Point vertexC)
{
	GLfloat radiusOfincircle = 0.0f;
	GLfloat areaOfTriangle = 0.0f;
	GLfloat semiperimeterOfTriangle = 0.0f;

	GLfloat sideA = getSideOfTriangle(vertexB, vertexC);
	GLfloat sideB = getSideOfTriangle(vertexA, vertexC);
	GLfloat sideC = getSideOfTriangle(vertexA, vertexB);

	semiperimeterOfTriangle = (sideA + sideB + sideC) / 2;
	areaOfTriangle = sqrt(semiperimeterOfTriangle*(semiperimeterOfTriangle - sideA)*(semiperimeterOfTriangle - sideB)*(semiperimeterOfTriangle - sideC));
	radiusOfincircle = areaOfTriangle / semiperimeterOfTriangle;

	return radiusOfincircle;
}



void drawCircle(GLfloat radius, Point incenter, Color color)
{
	GLint circle_points = 1000000;
	GLfloat angle;

	glBegin(GL_POINTS);

	glColor3f(color.red, color.green, color.blue);

	for (int i = 0.0f; i<circle_points; i++)
	{
		angle = 2 * PI * i / circle_points;
		glVertex3f(cos(angle)*radius + incenter.xAxis,
			sin(angle)*radius + incenter.yAxis, 0.0f);
	}

	glEnd();

}

void drawTriangle(Point top, Point left, Point right, Color color, GLfloat width)
{
	glLineWidth(width);
	glBegin(GL_LINE_LOOP);

	glColor3f(color.red, color.green, color.blue);

	glVertex3f(top.xAxis, top.yAxis, 0.0f);
	glVertex3f(left.xAxis, left.yAxis, 0.0f);
	glVertex3f(right.xAxis, right.yAxis, 0.0f);

	glEnd();
}

void drawLine(Point xCoordinate, Point yCoordinate, Color color, GLfloat width)
{
	glLineWidth(width);
	glBegin(GL_LINES);

	glColor3f(color.red, color.green, color.blue);
	glVertex3f(xCoordinate.xAxis, xCoordinate.yAxis, 0.0f);
	glVertex3f(yCoordinate.xAxis, yCoordinate.yAxis, 0.0f);

	glEnd();
}

void resize(int width,int height)
{
  if(height==0)
    height=1;
  glViewport (0,0,(GLsizei)width,(GLsizei) height);
  glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
  gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}


void uninitialize(void)
 {
   GLXContext currentGLXContext;
   currentGLXContext = glXGetCurrentContext();

   if(currentGLXContext !=NULL && currentGLXContext == gGLXContext )
   {
     glXMakeCurrent(gpDisplay,0,0);
   }

   if(gGLXContext)
   {
     glXDestroyContext(gpDisplay,gGLXContext);
   }

   if(gWindow)
   {
     XDestroyWindow(gpDisplay,gWindow);
   }

   if(gColormap)
   {
     XFreeColormap(gpDisplay,gColormap);
   }

   if(gpXVisualInfo)
   {
     free(gpXVisualInfo);
     gpXVisualInfo=NULL;
   }

   if(gpDisplay)
   {
     XCloseDisplay(gpDisplay);
     gpDisplay=NULL;
   }
}
