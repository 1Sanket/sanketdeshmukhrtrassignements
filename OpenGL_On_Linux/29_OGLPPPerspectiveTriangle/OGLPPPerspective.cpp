
/// GLXContextWithFBConfigs

//headers
#include <iostream>
#include <stdio.h> //for printf()
#include <stdlib.h> //for exit()
#include <memory.h> //for memset()

//headers for XServer
#include <X11/Xlib.h> //analogous to windows.h
#include <X11/Xutil.h> //for visuals
#include <X11/XKBlib.h> //XkbKeycodeToKeysym()
#include <X11/keysym.h> //for 'Keysym'

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h> //for 'glx' functions

#include "vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variable declarations
FILE *gpFile = NULL;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext; //parallel to HGLRC

bool gbFullscreen = false;

using namespace vmath;

// Shader related variables.

enum 
{
   VDG_ATTRIBUTE_VERTEX =0,
   VDG_ATTRIBUTE_COLOR,
   VDG_ATTRIBUTE_NORMAL,
   VDG_ATTRIBUTE_TEXTURE0,
};

mat4 gPerspectiveProjectionMatrix;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Triangle;
GLuint gVbo_Triangle_Position;
GLuint gMVPUnitform;

//entry-point function
int main(int argc, char *argv[])
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void resize(int,int);
	void display(void);
	void uninitialize(void);
	
	//code
	// create log file
	gpFile=fopen("Log.txt", "w");
	if (gpFile==NULL)
	{
		printf("Log File Can Not Be Created. Exiting Now ...\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}
	
	// create the window
	CreateWindow();
	
	//initialize()
	initialize();
	
	//Message Loop

	//variable declarations
	XEvent event; //parallel to 'MSG' structure
	KeySym keySym;
	int winWidth;
	int winHeight;
	bool bDone=false;
	
	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event); //parallel to GetMessage()
			switch(event.type) //parallel to 'iMsg'
			{
				case MapNotify: //parallel to WM_CREATE
					break;
				case KeyPress: //parallel to WM_KEYDOWN
					keySym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keySym)
					{
						case XK_Escape:
							bDone=true;
							break;
						case XK_F:
						case XK_f:
							if(gbFullscreen==false)
							{
								ToggleFullscreen();
								gbFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbFullscreen=false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1: //Left Button
							break;
						case 2: //Middle Button
							break;
						case 3: //Right Button
							break;
						default: 
							break;
					}
					break;
				case MotionNotify: //parallel to WM_MOUSEMOVE
					break;
				case ConfigureNotify: //parallel to WM_SIZE
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose: //parallel to WM_PAINT
					break;
				case DestroyNotify:
					break;
				case 33: //close button, system menu -> close
					bDone=true;
					break;
				default:
					break;
			}
		}
		
		display();
	}
	
	uninitialize();
	return(0);
}



void CreateWindow(void)
{
    void uninitialize(void);

    XSetWindowAttributes winAttribs;
    GLXFBConfig *pGLXFBConfigs=NULL;
    GLXFBConfig  bestGLXFBConfig;
    XVisualInfo *pTempXVisualInfo =NULL;
    int iNumFBConfigs=0;
    int styleMask;
    int i;

    static int frameBufferAttributes[]=
    {
        GLX_X_RENDERABLE,True,
        GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
        GLX_RENDER_TYPE,GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
        GLX_RED_SIZE,8,
        GLX_GREEN_SIZE,8,
        GLX_BLUE_SIZE,8,
        GLX_ALPHA_SIZE,8,
        GLX_DEPTH_SIZE,24,
        GLX_STENCIL_SIZE,8,
        GLX_DOUBLEBUFFER,True,
        // GLX_SAMPLE_BUFFERS,1,
        //GLX_SAMPLES,4,
        None
    };

    gpDisplay=XOpenDisplay(NULL);
    
    if(gpDisplay== NULL)
    {
        printf("Error: Unable to obtain X display.\n");
        uninitialize();
        exit(1);
    }

    pGLXFBConfigs=glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);

    if(pGLXFBConfigs==NULL)
    {
        printf("Error : Failed to get valid framebuffer config.Exiting now......\n");
        uninitialize();
        exit(1);
    }

    printf("%d matching FB Configs found.\n",iNumFBConfigs);

    int  bestFramebufferconfig=-1;
    int  worstFramebufferConfig=-1;
    int  bestNumberOfSamples=-1;
    int  worstNumberOfSamples=999;

    for(i=0;i<iNumFBConfigs;i++)
    {
        pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
        
        if(pTempXVisualInfo)
        {
            int sampleBuffer,samples;

            glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
            glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
            printf("Matching framebuffer config = %d : Visual id = 0x%lu : SAMPLE_BUFFER =%d :SAMPLES =%d\n",i,pTempXVisualInfo->visualid,sampleBuffer,samples);

            if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
            {
                bestFramebufferconfig=i;
                bestNumberOfSamples=samples;
            }

            if(worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
            {
                worstFramebufferConfig=i;
                worstNumberOfSamples=samples;
            }

        }

        XFree(pTempXVisualInfo);
    }

    bestGLXFBConfig=pGLXFBConfigs[bestFramebufferconfig];

    gGLXFBConfig=bestGLXFBConfig;

    XFree(pGLXFBConfigs);

    gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
    printf("Chosen visual ID=0x%lu\n",gpXVisualInfo->visualid);

    winAttribs.border_pixel=0;
    winAttribs.background_pixmap=0;
    winAttribs.colormap= XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
    winAttribs.event_mask=StructureNotifyMask|KeyPressMask|ButtonPressMask|ExposureMask|VisibilityChangeMask|PointerMotionMask;
    
    styleMask= CWBorderPixel|CWEventMask|CWColormap;
    gColormap=winAttribs.colormap;

    gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,WIN_WIDTH,WIN_HEIGHT,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);

    if(!gWindow)
    {
        printf("Failure in window creation.\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay,gWindow,"OpenGL Programmable Pipeline Perspective Triangle");
    Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);

}

void ToggleFullscreen(void)
{
    Atom wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

    XEvent event;
    memset(&event,0,sizeof(XEvent));

    event.type=ClientMessage;
    event.xclient.window=gWindow;
    event.xclient.message_type=wm_state;
    event.xclient.format=32;
    event.xclient.data.l[0]=gbFullscreen?0:1;

    Atom fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    event.xclient.data.l[1]=fullscreen;

    XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&event);

}


void initialize(void)
{
    void uninitialize(void);
    void resize(int,int);


    glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

    GLint attribs[] ={GLX_CONTEXT_MAJOR_VERSION_ARB,3,
                      GLX_CONTEXT_MINOR_VERSION_ARB,1,
                      GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
                       0};
    
    gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

    if(!gGLXContext)
    {
        GLint attribs[]={
            GLX_CONTEXT_MAJOR_VERSION_ARB,1,
            GLX_CONTEXT_MINOR_VERSION_ARB,0,
            0
        };
    
        printf("Failed to create GLX 4.5 context.Hence using old style GLX context\n");
        gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
    }

    else
    {
        printf("OpenGL context 4.5 is created.\n");
    }

    if(!glXIsDirect(gpDisplay,gGLXContext))
    {
        printf("Indirect GLX Rendering context obtained\n");
    }

    else
    {
        printf("Direct GLX Rendering context obtained\n");
    }

    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

    if(glewInit() != GLEW_OK)
	{
        fprintf(gpFile, "Failed to init. Exiting application \n");
        
        uninitialize();
	
	}

     else
    {
          fprintf(gpFile, "glew init successful.\n");
    }

    /// **** Vertex shader **** ///
    
    // Create shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // Provide source code to shader
    const GLchar *vertexShaderSourceCode = 
    "#version 400 core" \
    "\n" \
    "in vec4 vPosition;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
    "gl_Position = u_mvp_matrix * vPosition;" \
    "}";

    glShaderSource(gVertexShaderObject,1,(const GLchar**) &vertexShaderSourceCode,NULL);

    // Compile shader 
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength =0;
    GLint iShaderCompileStatus =0;
    char *szInfoLog = NULL;

    glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
    
    if(iShaderCompileStatus == GL_FALSE)
    {
      glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

       if(iInfoLogLength > 0)
       {
          szInfoLog = (char *) malloc(iInfoLogLength);
           
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"Vertex Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
       }

    }

    /// *** Fragment Shader *** ///

    // Create shader 

    gFragmentShaderObject=  glCreateShader(GL_FRAGMENT_SHADER);

    // provide source code to shader 
    const GLchar *fragmentShaderSourceCode =
    "#version 400 core" \
    "\n" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "FragColor = vec4(1.0,1.0,1.0,1.0);" \
    "}" ;

    glShaderSource(gFragmentShaderObject,1, (const GLchar **) &fragmentShaderSourceCode,NULL);

    // Compile shader

    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);

    if (iShaderCompileStatus == GL_FALSE)
    {
       glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
         
          if(iInfoLogLength >0)
          {
             szInfoLog = (char*) malloc(iInfoLogLength);

            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"Fragment Shader Compilation Log : %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
          }
    }

    /// *** Shader Program *** ///

    // Create
    gShaderProgramObject = glCreateProgram();

    // Attach vertex shader to shader program.

    glAttachShader(gShaderProgramObject,gVertexShaderObject);

    // Attach fragment shader to shader program.

    glAttachShader(gShaderProgramObject,gFragmentShaderObject);

    // Pre-link binding of shader program object with vertex shader position attribute.

    glBindAttribLocation(gShaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

    
    // Link shader

    glLinkProgram(gShaderProgramObject);
    
    GLint iShaderProgramLinkStatus = 0;

    glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

    if(iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *) malloc (iInfoLogLength);
            
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"Shader Program Link Log :  %s \n",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }

    }

    // get MVP uniform location
    gMVPUnitform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

    const GLfloat triangleVertices []=
    {
        0.0f,1.0f,0.0f,
        -1.0f,-1.0f,0.0f,
        1.0f,-1.0f,0.0f
    };

    glGenVertexArrays(1,&gVao_Triangle);
    glBindVertexArray(gVao_Triangle);

    glGenBuffers(1,&gVbo_Triangle_Position);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_Triangle_Position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(triangleVertices),triangleVertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);

    glShadeModel(GL_SMOOTH);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

    glEnable(GL_CULL_FACE);

    glClearColor(0.0f,0.0f,1.0f,0.0f);

    // set orthographic projection matrix to identity matrix.

    gPerspectiveProjectionMatrix = mat4 :: identity();

    resize(WIN_WIDTH,WIN_HEIGHT);
}




void resize(int width,int height)
{
    if(height==0)
        height=1;
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    gPerspectiveProjectionMatrix = perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Start using OpenGL Program object.

    glUseProgram(gShaderProgramObject);

    // OpenGL drawing 
    // Set modelview and modelviewprojection matrices to identity

    mat4 modelViewMatrix = mat4:: identity();
    mat4 modelViewProjectionMatrix = mat4::identity();

    // Translate model view matrix.

    modelViewMatrix = translate(0.0f,0.0f,-6.0f);

    // multiply modelview and perspective matrix to get modelview projection matrix 

    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix ;

    // pass modelviewProjection matrix to the vertex shader in 'u_mvp_matrix' shader variable
    // whose position value we already calculated by using glGetUniformLocation()
    
    glUniformMatrix4fv(gMVPUnitform,1,GL_FALSE,modelViewProjectionMatrix);

    // *** Bind Vao ***//

    glBindVertexArray(gVao_Triangle);

    glDrawArrays(GL_TRIANGLES,0,3);

    // unbind vao

    glBindVertexArray(0);

    glUseProgram(0);

    glXSwapBuffers(gpDisplay,gWindow);

}

void uninitialize(void)
{

    // Destroy Vao 
    if (gVao_Triangle)
    {
        glDeleteVertexArrays(1,&gVao_Triangle);
        gVao_Triangle=0;
    }

    // Destroy Vbo 
    if (gVbo_Triangle_Position)
    {
        glDeleteVertexArrays(1,&gVbo_Triangle_Position);
        gVbo_Triangle_Position=0;
    }

    // detach vertex shader from shader program object 

    glDetachShader(gShaderProgramObject,gVertexShaderObject);

    // detach fragment shader from shader program object

    glDetachShader(gShaderProgramObject,gFragmentShaderObject);

    // delete vertex shader object

    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject =0;

    // delete fragment shader object

    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject =0;

    // delete shader program object

    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject =0;

    // unlink shader program
    glUseProgram(0);

    GLXContext currentContext=glXGetCurrentContext();

    if(currentContext!=NULL && currentContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,gGLXContext);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo=NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay=NULL;
    }

    if(gpFile)
    {
        fprintf(gpFile,"Log file is successfully closed.\n");
        fclose(gpFile);
        gpFile=NULL;
    }
}
