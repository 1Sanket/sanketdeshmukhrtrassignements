
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

// namespaces
using namespace std;

// global variable declarations

bool gbFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

int main(void)
{
  // function prototypes

  void CreateWindow(void);
  void ToggleFullscreen(void);
  void uninitialize();

  // variable declarations
  int winWidth = giWindowWidth;
  int winHeight= giWindowHeight;

  // code

  CreateWindow();

  // Message Loop
  XEvent event;
  KeySym keysym;

  while (1)
  {
      XNextEvent(gpDisplay,&event);
      switch (event.type)
      {
         case MapNotify:
            break;
         case KeyPress:
            keysym =XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
            char ascii[26];
            XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);

            switch (ascii[0])
            {
              case 'f':
              case 'F':
                  if(gbFullscreen==false)
                  {
                     ToggleFullscreen();
                     gbFullscreen=true;
                  }

                  else
                  {
                      ToggleFullscreen();
                      gbFullscreen=false;
                  }

              break;
                default:
              break;
            }

            switch(keysym)
            {
                case XK_Escape:
                    uninitialize();
                    exit(0);
                default:
                 break;
            }
         break;
        case ButtonPress:
            switch(event.xbutton.button)
            {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                default:
                    break;
            }
            break;
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth=event.xconfigure.width;
            winHeight=event.xconfigure.height;
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            uninitialize();
            exit(0);
        default:
            break;
      }
  }

  uninitialize();
  return (0);
}


void CreateWindow(void)
{
    //function prototypes
    void uninitialize(void);

    // variable declarations

    XSetWindowAttributes windowAttributes;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    gpDisplay = XOpenDisplay(NULL);

    if(gpDisplay==NULL)
    {
      printf("Error : Unable to open X display.\n Exiting Now .....\n");
      uninitialize();
      exit(1);
    }

    defaultScreen= XDefaultScreen(gpDisplay);
    defaultDepth= DefaultDepth(gpDisplay,defaultScreen);

    gpXVisualInfo =(XVisualInfo *)malloc (sizeof(XVisualInfo));

    if(gpXVisualInfo==NULL)
    {
      printf("Error : Unable to allocate memory for visual info.\n Exiti now\n" );
      uninitialize();
      exit(1);
    }

    XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);

    if(gpXVisualInfo== NULL)
    {
      printf("Error : Unable to get a visual \n Exiting now ..\n");
      uninitialize();
      exit(1);
    }

    windowAttributes.border_pixel=0;
    windowAttributes.background_pixmap=0;
    windowAttributes.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);

    gColormap = windowAttributes.colormap;

    windowAttributes.background_pixel= BlackPixel(gpDisplay,defaultScreen);

    windowAttributes.event_mask= ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

    styleMask= CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

    gWindow= XCreateWindow(gpDisplay,
                          RootWindow(gpDisplay,gpXVisualInfo->screen),
                          0,
                          0,
                          giWindowWidth,
                          giWindowHeight,
                          0,
                          gpXVisualInfo->depth,
                          InputOutput,
                          gpXVisualInfo->visual,
                          styleMask,
                          &windowAttributes
                        );

      if(!gWindow)
      {
        printf("Error: Failed to create main window .\n Exiting now....\n");
        uninitialize();
        exit(1);
      }

      XStoreName(gpDisplay,gWindow,"First  XWindow");

      Atom windowManagerDelete= XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
      XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

      XMapWindow(gpDisplay,gWindow);

}


void ToggleFullscreen(void)
{
  Atom wm_state;
  Atom fullscreen;
  XEvent xev={0};

  wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
  memset(&xev,0,sizeof(xev));

  xev.type=ClientMessage;
  xev.xclient.window=gWindow;
  xev.xclient.message_type=wm_state;
  xev.xclient.format=32;
  xev.xclient.data.l[0]=gbFullscreen?0:1;

  fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
  xev.xclient.data.l[1]=fullscreen;

  XSendEvent(gpDisplay,
            RootWindow(gpDisplay,gpXVisualInfo->screen),
            False,
            StructureNotifyMask,
            &xev
          );
}

void uninitialize(void)
{
  if(gWindow)
  {
    XDestroyWindow(gpDisplay,gWindow);
  }

  if(gColormap)
  {
    XFreeColormap(gpDisplay,gColormap);
  }

  if(gpXVisualInfo)
  {
      free(gpXVisualInfo);
      gpXVisualInfo=NULL;
  }

  if(gpDisplay)
  {
    XCloseDisplay(gpDisplay);
    gpDisplay=NULL;
  }
}
