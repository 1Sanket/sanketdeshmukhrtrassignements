
 // XOpenGL MonkeyHead Mesh Loading 
 #include<iostream>
 #include<stdio.h>
 #include<stdlib.h>
 #include<memory.h>
 //#include<math.h>
 
 #include<X11/Xlib.h>
 #include<X11/Xutil.h>
 #include<X11/XKBlib.h>
 #include<X11/keysym.h>
 
 #include<GL/gl.h>
 #include<GL/glx.h>
 #include<GL/glu.h>
 
 // Symbolic Constants

 #define FOY_ANGLE 45    // Field of view in y direction
 #define ZNEAR 0.1
 #define ZFAR 200.0
 
 #define VIEWPORT_BOTTOMLEFT_X 0 
 #define VIEWPORT_BOTTOMLETF_Y 0
 
 
 // global varibales
 
 bool gbFullscreen =false;
 bool gbIsLightingEnabled = false;
 Display *gpDisplay =NULL;
 XVisualInfo *gpXVisualInfo = NULL;
 Colormap gColormap;
 Window gWindow;
 int giWindowWidth=800;
 int giWindowHeight=600;
 
 GLXContext gGLXContext;
 GLUquadric *quadric = NULL;
 
 GLboolean gbIs_X_AxisRotationOfLight = GL_TRUE;
 GLboolean gbIs_Y_AxisRotationOfLight = GL_FALSE;
 GLboolean gbIs_Z_AxisRotationOfLight = GL_FALSE;
 
 float lightRotationAngle = 0.0f;

 // Arrays required for lighting.
GLfloat light0_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light0_position[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light0_diffuse[] = { 1.0f,1.0f,1.0f,1.0f }; // White light.
GLfloat light0_specular[] = { 1.0f,1.0f,1.0f,1.0f };

GLfloat light_model_ambient[] = { 0.2f,0.2f,0.2f,0.0f };
GLfloat light_model_local_viewer[] = { 0.0f };


 int main(void)
 {
   void CreateWindow(void);
   void ToggleFullscreen(void);
   void initialize(void);
   void display(void);
   void updateAngle(void);
   void resize(int,int);
   void uninitialize(void);
 
   int winWidth= giWindowWidth;
   int winHeight= giWindowHeight;
 
   bool bDone= false;
 
   CreateWindow();
 
   initialize();
 
   XEvent event;
   KeySym keysym;
 
   while(bDone == false)
   {
     while (XPending(gpDisplay))
      {
        XNextEvent (gpDisplay,&event);
 
        switch (event.type)
         {
          case MapNotify:
           break;
          case KeyPress:
           keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
 
           char ascii[26];
           XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);
 
      switch (ascii[0])
        {
          case 'f':
          case 'F':
                 if(gbFullscreen==false)
                 {
                    ToggleFullscreen();
                    gbFullscreen=true;
                 }
 
                 else
                 {
                     ToggleFullscreen();
                     gbFullscreen=false;
                 }
 
             break;

          case 'l':
          case 'L':
             if (gbIsLightingEnabled == false)
             {
               glEnable(GL_LIGHTING);
               gbIsLightingEnabled = true;
             }
             else
             {
               glDisable(GL_LIGHTING);
               gbIsLightingEnabled = false;
             }
             break;    

          case 'x': 
          case 'X':
             
             gbIs_X_AxisRotationOfLight = GL_TRUE;
             gbIs_Y_AxisRotationOfLight = GL_FALSE;
             gbIs_Z_AxisRotationOfLight = GL_FALSE;
       
             break;
       
          case 'y':
          case 'Y':
             
             gbIs_X_AxisRotationOfLight = GL_FALSE;
             gbIs_Y_AxisRotationOfLight = GL_TRUE;
             gbIs_Z_AxisRotationOfLight = GL_FALSE;
       
             break;
       
          case 'z':
          case 'Z':
             
             gbIs_X_AxisRotationOfLight = GL_FALSE;
             gbIs_Y_AxisRotationOfLight = GL_FALSE;
             gbIs_Z_AxisRotationOfLight = GL_TRUE;
       
             break;
     
          default:
             break;
        }
 
      switch(keysym)
        {
             case XK_Escape:
               bDone= true;
               break;
             default:
               break;
        }
          break;
 
         case ButtonPress:
           switch (event.xbutton.button)
            {
             case 1:
               break;
             case 2:
               break;
             case 3:
               break;
             default:
               break;
           }
           break;
 
         case MotionNotify:
           break;
 
         case ConfigureNotify:
             winWidth=event.xconfigure.width;
             winHeight=event.xconfigure.height;
             resize(winWidth,winHeight);
           break;
         case Expose:
           break;
         case DestroyNotify:
           break;
         case 33:
             bDone=true;
           break;
         default:
           break;
        }
     }
 
     updateAngle();

     display();
 
   }
 
   uninitialize();
   return(0);
 }
 
 void CreateWindow(void)
 {
   void uninitialize(void);
 
   XSetWindowAttributes windowAttributes;
   int defaultScreen;
   int defaultDepth;
   int styleMask;
 
   static int frameBufferAttributes[]=
   {
      //GLX_X_RENDERABLE,True,
      //GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
      GLX_RENDER_TYPE,GLX_RGBA_BIT,
     // GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
      GLX_RED_SIZE,8,
      GLX_GREEN_SIZE,8,
      GLX_BLUE_SIZE,8,
      GLX_ALPHA_SIZE,8,
      GLX_DEPTH_SIZE,24,
      GLX_DOUBLEBUFFER,True,
      None
   };
 
   gpDisplay= XOpenDisplay(NULL);
 
   if(gpDisplay == NULL)
   {
     printf(" Error: Unable to open X display.\n Exiting now....\n");
     uninitialize();
     exit(1);
   }
 
   defaultScreen= XDefaultScreen(gpDisplay);
 
   gpXVisualInfo= glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
 
   windowAttributes.border_pixel=0;
   windowAttributes.background_pixmap=0;
   windowAttributes.colormap= XCreateColormap(gpDisplay,
                                              RootWindow(gpDisplay,gpXVisualInfo->screen),
                                              gpXVisualInfo->visual,
                                              AllocNone
                                     );
   gColormap= windowAttributes.colormap;
 
   windowAttributes.background_pixel= BlackPixel(gpDisplay,defaultScreen);
 
   windowAttributes.event_mask= ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;
 
   styleMask= CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;
 
   gWindow= XCreateWindow(gpDisplay,
                         RootWindow(gpDisplay,  gpXVisualInfo->screen),
                         0,
                         0,
                         giWindowWidth,
                         giWindowHeight,
                         0,
                         gpXVisualInfo->depth,
                         InputOutput,
                         gpXVisualInfo->visual,
                         styleMask,
                         &windowAttributes
                       );
 
     if(!gWindow)
     {
       printf("Error: Failed to create main window.\n Exiting now \n");
       uninitialize();
       exit(1);
     }
 
     XStoreName(gpDisplay,gWindow,"Material Properties");
 
     Atom windowManagerDelete= XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
     XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
 
     XMapWindow(gpDisplay,gWindow);
 }
 
 void ToggleFullscreen(void)
   {
     Atom wm_state;
     Atom fullscreen;
     XEvent xev={0};
 
     wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
     memset(&xev,0,sizeof(xev));
 
     xev.type=ClientMessage;
 
     xev.xclient.window=gWindow;
     xev.xclient.message_type=wm_state;
     xev.xclient.format=32;
     xev.xclient.data.l[0]=gbFullscreen?0:1;
 
     fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
     xev.xclient.data.l[1]=fullscreen;
     XSendEvent(gpDisplay,
               RootWindow(gpDisplay,gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev
             );
   }
 
 
 void initialize(void)
 {
   void resize(int,int);
   void uninitialize(void);
 
   gGLXContext= glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
 
   glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
 
     glClearColor(0.25f, 0.25f, 0.25f, 0.0f);    // Dark Gray background.
   
     glClearDepth(1.0f);
     glEnable(GL_DEPTH_TEST);
     glDepthFunc(GL_LEQUAL);
   
     glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
     glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
     glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular);
     glEnable(GL_LIGHT0);
   
     glEnable(GL_AUTO_NORMAL);
     glEnable(GL_NORMALIZE);
   
     glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
     glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

   resize(giWindowWidth,giWindowHeight);
 
 }

 
 void display(void)
 {
  void drawSphere(void);
  
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
  
    glPushMatrix();
  
    gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
  
    glPushMatrix();
  
    if (gbIs_X_AxisRotationOfLight)
    {
      light0_position[1] = lightRotationAngle;
      light0_position[0] = 0.0f;
      
  
      glRotatef(lightRotationAngle, 1.0f, 0.0f, 0.0f);
      
    }
  
    else if (gbIs_Y_AxisRotationOfLight)
    {
      light0_position[0] = lightRotationAngle;
      light0_position[1] = 0.0f;
  
  
      glRotatef(lightRotationAngle, 0.0f, 1.0f, 0.0f);
      
    }
  
    else if (gbIs_Z_AxisRotationOfLight)
    {
      light0_position[0] = lightRotationAngle;
      light0_position[1] = 0.0f;
  
      glRotatef(lightRotationAngle, 0.0f, 0.0f, 1.0f);
      
    }
  
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
  
    glPopMatrix();
  
    glPushMatrix();
  
    glTranslatef(-0.8f, 1.0f, -3.0f);
    
    /// Emerald
  
    GLfloat material_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
    GLfloat material_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
    GLfloat material_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
    GLfloat material_shinyness[] = { 0.6f*128.0f };
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
  
    /// Jade
  
    material_ambient[0] = 0.135f;
    material_ambient[1] = 0.2225f;
    material_ambient[2] = 0.1575f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.54f;
    material_diffuse[1] = 0.89f;
    material_diffuse[2] = 0.63f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.316228f;
    material_specular[1] = 0.316228f;
    material_specular[2] = 0.316228f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.1f* 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Obsidian
  
    material_ambient[0] = 0.05375f;
    material_ambient[1] = 0.05f;
    material_ambient[2] = 0.06625f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.18275f;
    material_diffuse[1] = 0.17f;
    material_diffuse[2] = 0.22525f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.332741f;
    material_specular[1] = 0.328634f;
    material_specular[2] = 0.346435f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.3f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Pearl
  
    material_ambient[0] = 0.25f;
    material_ambient[1] = 0.20725f;
    material_ambient[2] = 0.20725f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 1.0f;
    material_diffuse[1] = 0.829f;
    material_diffuse[2] = 0.829f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.296648f;
    material_specular[1] = 0.296648f;
    material_specular[2] = 0.296648f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.088f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Ruby
  
    material_ambient[0] = 0.1745f;
    material_ambient[1] = 0.01175f;
    material_ambient[2] = 0.01175f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.61424f;
    material_diffuse[1] = 0.04136f;
    material_diffuse[2] = 0.04136f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.727811f;
    material_specular[1] = 0.626959f;
    material_specular[2] = 0.626959f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.6f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Turquoise
  
    material_ambient[0] = 0.1f;
    material_ambient[1] = 0.18725f;
    material_ambient[2] = 0.1745f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.396f;
    material_diffuse[1] = 0.74151f;
    material_diffuse[2] = 0.69102f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.297254f;
    material_specular[1] = 0.30829f;
    material_specular[2] = 0.306678f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.1f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
  
    /// Brass
  
    glTranslatef(0.4f, 2.0f, 0.0f);
  
    material_ambient[0] = 0.329412f;
    material_ambient[1] = 0.223529f;
    material_ambient[2] = 0.027451f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.780392f;
    material_diffuse[1] = 0.568627f;
    material_diffuse[2] = 0.113725f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.992157f;
    material_specular[1] = 0.941176f;
    material_specular[2] = 0.807843f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.21794872f * 128.0f;
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Bronze
  
    material_ambient[0] = 0.2125f;
    material_ambient[1] = 0.1275f;
    material_ambient[2] = 0.054f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.714f;
    material_diffuse[1] = 0.4284f;
    material_diffuse[2] = 0.18144f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.393548f;
    material_specular[1] = 0.271906f;
    material_specular[2] = 0.166721f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.2f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Chrome
  
    material_ambient[0] = 0.25f;
    material_ambient[1] = 0.25f;
    material_ambient[2] = 0.25f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.4f;
    material_diffuse[1] = 0.4f;
    material_diffuse[2] = 0.4f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.774597f;
    material_specular[1] = 0.774597f;
    material_specular[2] = 0.774597f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.6f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Copper
  
    material_ambient[0] = 0.19125f;
    material_ambient[1] = 0.0735f;
    material_ambient[2] = 0.0225f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.7038f;
    material_diffuse[1] = 0.27048f;
    material_diffuse[2] = 0.0828f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.256777f;
    material_specular[1] = 0.137622f;
    material_specular[2] = 0.086014f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.6f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
  
    /// Gold
  
    material_ambient[0] = 0.24725f;
    material_ambient[1] = 0.1995f;
    material_ambient[2] = 0.0745f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.75164f;
    material_diffuse[1] = 0.60648f;
    material_diffuse[2] = 0.22648f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.628281f;
    material_specular[1] = 0.555802f;
    material_specular[2] = 0.366065f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.4f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
  
    /// Silver
  
    material_ambient[0] = 0.19225f;
    material_ambient[1] = 0.19225f;
    material_ambient[2] = 0.19225f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.50754f;
    material_diffuse[1] = 0.50754f;
    material_diffuse[2] = 0.50754f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.508273f;
    material_specular[1] = 0.508273f;
    material_specular[2] = 0.508273f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.4f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    glTranslatef(0.4f, 2.0f, 0.0f);
  
    /// Black
  
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.01f;
    material_diffuse[1] = 0.01f;
    material_diffuse[2] = 0.01f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.50f;
    material_specular[1] = 0.50f;
    material_specular[2] = 0.50f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.25f * 128.0f;
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Cyan
  
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.1f;
    material_ambient[2] = 0.06f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.0f;
    material_diffuse[1] = 0.50980392f;
    material_diffuse[2] = 0.50980392f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.50196078f;
    material_specular[1] = 0.50196078f;
    material_specular[2] = 0.50196078f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.25f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Green
  
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.1f;
    material_diffuse[1] = 0.35f;
    material_diffuse[2] = 0.1f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.45f;
    material_specular[1] = 0.55f;
    material_specular[2] = 0.45f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.25f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Red
  
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.5f;
    material_diffuse[1] = 0.0f;
    material_diffuse[2] = 0.0f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.7f;
    material_specular[1] = 0.6f;
    material_specular[2] = 0.6f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.25f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// White
  
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.55f;
    material_diffuse[1] = 0.55f;
    material_diffuse[2] = 0.55f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.70f;
    material_specular[1] = 0.70f;
    material_specular[2] = 0.70f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.25f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
    
    /// Yellow Plastic
  
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.5f;
    material_diffuse[1] = 0.5f;
    material_diffuse[2] = 0.0f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.60f;
    material_specular[1] = 0.60f;
    material_specular[2] = 0.50f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.25f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
  
    glTranslatef(0.4f, 2.0f, 0.0f);
  
    /// Black
  
    material_ambient[0] = 0.02f;
    material_ambient[1] = 0.02f;
    material_ambient[2] = 0.02f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.01f;
    material_diffuse[1] = 0.01f;
    material_diffuse[2] = 0.01f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.4f;
    material_specular[1] = 0.4f;
    material_specular[2] = 0.4f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.078125f * 128.0f;
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
  
    /// Cyan
  
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.05f;
    material_ambient[2] = 0.05f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.4f;
    material_diffuse[1] = 0.5f;
    material_diffuse[2] = 0.5f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.04f;
    material_specular[1] = 0.7f;
    material_specular[2] = 0.7f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.078125f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Green
  
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.05f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.4f;
    material_diffuse[1] = 0.5f;
    material_diffuse[2] = 0.4f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.04f;
    material_specular[1] = 0.7f;
    material_specular[2] = 0.04f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.078125f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Red
  
    material_ambient[0] = 0.05f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.5f;
    material_diffuse[1] = 0.4f;
    material_diffuse[2] = 0.4f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.7f;
    material_specular[1] = 0.04f;
    material_specular[2] = 0.04f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.078125f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// White
  
    material_ambient[0] = 0.05f;
    material_ambient[1] = 0.05f;
    material_ambient[2] = 0.05f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.5f;
    material_diffuse[1] = 0.5f;
    material_diffuse[2] = 0.5f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.7f;
    material_specular[1] = 0.7f;
    material_specular[2] = 0.7f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.078125f * 128.0f;
  
    //glTranslatef(0.3f, 0.0f, 0.0f);
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    ///
  
    /// Yellow Rubber
  
    material_ambient[0] = 0.05f;
    material_ambient[1] = 0.05f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
  
    material_diffuse[0] = 0.5f;
    material_diffuse[1] = 0.5f;
    material_diffuse[2] = 0.4f;
    material_diffuse[3] = 1.0f;
  
    material_specular[0] = 0.7f;
    material_specular[1] = 0.7f;
    material_specular[2] = 0.04f;
    material_specular[3] = 1.0f;
  
    material_shinyness[0] = 0.078125f * 128.0f;
  
    glTranslatef(0.0f, -0.4f, 0.0f);
  
    glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);
  
    drawSphere();
  
    glPopMatrix();
  
  
    glXSwapBuffers(gpDisplay,gWindow);
 }

 void drawSphere(void)
 {
   glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
 
   quadric = gluNewQuadric();
 
   gluSphere(quadric, 0.1, 50, 50); 
 }

 void updateAngle()
 {
   lightRotationAngle = lightRotationAngle + 0.5f;
   if (lightRotationAngle >= 360.0f)
     lightRotationAngle = 0.0f;
 
 }
 
 void resize(int width,int height)
 {
   if(height==0)
     height=1;
   glViewport (VIEWPORT_BOTTOMLEFT_X,VIEWPORT_BOTTOMLETF_Y,(GLsizei)width,(GLsizei) height);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(FOY_ANGLE, (GLfloat)width / (GLfloat)height, ZNEAR, ZFAR);
 
 }

 
 void uninitialize(void)
  {
 
    GLXContext currentGLXContext;
    currentGLXContext = glXGetCurrentContext();
 
    if(currentGLXContext !=NULL && currentGLXContext == gGLXContext )
    {
      glXMakeCurrent(gpDisplay,0,0);
    }
 
    if(gGLXContext)
    {
      glXDestroyContext(gpDisplay,gGLXContext);
    }
 
    if(gWindow)
    {
      XDestroyWindow(gpDisplay,gWindow);
    }
 
    if(gColormap)
    {
      XFreeColormap(gpDisplay,gColormap);
    }
 
    if(gpXVisualInfo)
    {
      free(gpXVisualInfo);
      gpXVisualInfo=NULL;
    }
 
    if(gpDisplay)
    {
      XCloseDisplay(gpDisplay);
      gpDisplay=NULL;
    }
 }