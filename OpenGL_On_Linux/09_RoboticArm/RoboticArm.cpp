
 // XOpenGL Double Buffer Robotic Arm with log file.
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

using namespace std;

// global varibales
bool gbFullscreen =false;
Display *gpDisplay =NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
int shoulder = 0;
int elbow = 0;
GLUquadric *quadric = NULL;
FILE *gpLogfile = NULL;


GLXContext gGLXContext;

int main(void)
{
  void CreateWindow(void);
  void ToggleFullscreen(void);
  void initialize(void);
  void display(void);
  void resize(int,int);
  void uninitialize(void);

  int winWidth= giWindowWidth;
  int winHeight= giWindowHeight;

  bool bDone= false;

  gpLogfile=fopen("log.txt","w");
  
  if(gpLogfile == NULL)
  {
    printf("Unable to create log file....Exiting....");
    exit(0);
  }

  fputs("\n Logs for '3DRotationWithLogFile' \n", gpLogfile);
  fputs("\n ******************************************** \n", gpLogfile);
  fputs("\n *****************In main******************** \n", gpLogfile);  

  CreateWindow();

  initialize();

  XEvent event;
  KeySym keysym;

  while(bDone == false)
  {
    while (XPending(gpDisplay))
     {
       XNextEvent (gpDisplay,&event);

       switch (event.type)
        {
         case MapNotify:
          break;
         case KeyPress:
          keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

          char ascii[26];
          XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);

          switch (ascii[0])
          {
            case 'f':
            case 'F':
                if(gbFullscreen==false)
                {
                   ToggleFullscreen();
                   gbFullscreen=true;
                }

                else
                {
                    ToggleFullscreen();
                    gbFullscreen=false;
                }

            break;

            case 'E':
            elbow = (elbow + 3) % 360;
            break;
      
            case 'e':
            elbow = (elbow - 3) % 360;
            break;
      
            case 'S':
            shoulder = (shoulder + 3) % 360;
            break;
      
            case 's':
            shoulder = (shoulder - 3) % 360;
            break;
              default:
            break;
          }

          switch(keysym)
          {
            case XK_Escape:
              bDone= true;
              break;
            default:
              break;
          }
         break;

        case ButtonPress:
          switch (event.xbutton.button)
           {
            case 1:
              break;
            case 2:
              break;
            case 3:
              break;
            default:
              break;
          }
          break;

        case MotionNotify:
          break;

        case ConfigureNotify:
            winWidth=event.xconfigure.width;
            winHeight=event.xconfigure.height;
            resize(winWidth,winHeight);
          break;
        case Expose:
          break;
        case DestroyNotify:
          break;
        case 33:
            bDone=true;
          break;
        default:
          break;
       }
    }

    display();
    
  }

  uninitialize();
  return(0);
}

void CreateWindow(void)
{
  void uninitialize(void);

  XSetWindowAttributes windowAttributes;
  int defaultScreen;
  int defaultDepth;
  int styleMask;

  static int frameBufferAttributes[]=
  {
     //GLX_X_RENDERABLE,True,
     //GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
     GLX_RENDER_TYPE,GLX_RGBA_BIT,
    // GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
     GLX_RED_SIZE,8,
     GLX_GREEN_SIZE,8,
     GLX_BLUE_SIZE,8,
     GLX_ALPHA_SIZE,8,
     GLX_DEPTH_SIZE,24,
     GLX_DOUBLEBUFFER,True,
     None
  };

  
  fputs("\n *****************Entering CreateWindow()******************** \n", gpLogfile);  

  gpDisplay= XOpenDisplay(NULL);

  if(gpDisplay == NULL)
  {
    printf(" Error: Unable to open X display.\n Exiting now....\n");
    uninitialize();
    exit(1);
  }

  fputs("\n *****************Calling 'XDefaultScreen()' ******************** \n", gpLogfile);  

  defaultScreen= XDefaultScreen(gpDisplay);
  
  fputs("\n *****************Calling 'glXChooseVisual()' ******************** \n", gpLogfile);  

  gpXVisualInfo= glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

  windowAttributes.border_pixel=0;
  windowAttributes.background_pixmap=0;

  fputs("\n *****************Calling 'XCreateColormap()' ******************** \n", gpLogfile);  
  
  windowAttributes.colormap= XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay,gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone
                                    );
  gColormap= windowAttributes.colormap;

  windowAttributes.background_pixel= BlackPixel(gpDisplay,defaultScreen);

  windowAttributes.event_mask= ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

  styleMask= CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

  fputs("\n *****************Calling 'XCreateWindow()' ******************** \n", gpLogfile);  

  gWindow= XCreateWindow(gpDisplay,
                        RootWindow(gpDisplay,  gpXVisualInfo->screen),
                        0,
                        0,
                        giWindowWidth,
                        giWindowHeight,
                        0,
                        gpXVisualInfo->depth,
                        InputOutput,
                        gpXVisualInfo->visual,
                        styleMask,
                        &windowAttributes
                      );

    if(!gWindow)
    {
      printf("Error: Failed to create main window.\n Exiting now \n");
      uninitialize();
      exit(1);
    }

    fputs("\n *****************Calling 'XStoreName()' ******************** \n", gpLogfile);  

    XStoreName(gpDisplay,gWindow,"Robotic Arm");

    fputs("\n *****************Calling 'XInternAtom()' ******************** \n", gpLogfile);  

    Atom windowManagerDelete= XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

    fputs("\n *****************Calling 'XSetWMProtocols()' ******************** \n", gpLogfile);  

    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    fputs("\n *****************Calling 'XMapWindow()' ******************** \n", gpLogfile);  

    XMapWindow(gpDisplay,gWindow);

    fputs("\n *****************Exiting CreateWindow()******************** \n", gpLogfile);  
    fputs("\n *********************************************************** \n", gpLogfile);  
}

void ToggleFullscreen(void)
  {
    Atom wm_state;
    Atom fullscreen;
    XEvent xev={0};

    fputs("\n *****************Entering ToggleFullscreen()******************** \n", gpLogfile);  

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type=ClientMessage;

    xev.xclient.window=gWindow;
    xev.xclient.message_type=wm_state;
    xev.xclient.format=32;
    xev.xclient.data.l[0]=gbFullscreen?0:1;

    fputs("\n *****************Calling 'XInternAtom()' ******************** \n", gpLogfile);  

    fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1]=fullscreen;

    fputs("\n *****************Calling 'XSendEvent()' ********************* \n", gpLogfile);  

    XSendEvent(gpDisplay,
              RootWindow(gpDisplay,gpXVisualInfo->screen),
              False,
              StructureNotifyMask,
              &xev
            );

    fputs("\n *****************Existing ToggleFullscreen()******************** \n", gpLogfile);  
    fputs("\n **************************************************************** \n", gpLogfile);  
  }


void initialize(void)
{
  void resize(int,int);

  fputs("\n *****************Entering initialize()******************** \n", gpLogfile);    

  fputs("\n *****************Calling 'glXCreateContext()' ********************* \n", gpLogfile);  
  gGLXContext= glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

  fputs("\n *****************Calling 'glXMakeCurrent()' ********************* \n", gpLogfile);  
  glXMakeCurrent(gpDisplay,gWindow,gGLXContext);


  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);
  
  glDepthFunc(GL_LEQUAL);
  
  glClearColor(0.0f,0.0f,0.0f,0.0f);  

  resize(giWindowWidth,giWindowHeight);

  fputs("\n *****************Exiting initialize()******************** \n", gpLogfile); 
  fputs("\n **************************************************************** \n", gpLogfile); 

}

void display(void)
{

  
  fputs("\n *****************Entering display()******************** \n", gpLogfile);    

  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
    glTranslatef(0.0f, 0.0f, -12.0f);
  
    glPushMatrix();
  
    glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
    glTranslatef(1.0f, 0.0f, 0.0f);
    glPushMatrix();
    glScalef(2.0f, 0.5f, 1.0f);
    quadric = gluNewQuadric();
    glColor3f(0.5f, 0.35f, 0.05f);
    gluSphere(quadric, 0.5, 10, 10);
    glPopMatrix();
  
    glTranslatef(1.0f, 0.0f, 0.0f);
    glRotatef((GLfloat)elbow, 0.0f, 0.0f, 1.0f);
    glTranslatef(1.0f, 0.0f, 0.0f);
    glPushMatrix();
  
    glScalef(2.0f, 0.5f, 1.0f);
  
    quadric = gluNewQuadric();
    glColor3f(0.5f, 0.35f, 0.05f);
    gluSphere(quadric, 0.5, 10, 10);
    glPopMatrix();
    glPopMatrix();
  

  //glFlush();

  fputs("\n *****************Calling 'glXSwapBuffers()' ********************* \n", gpLogfile);  

  glXSwapBuffers(gpDisplay,gWindow);

  fputs("\n *****************Exiting display()******************** \n", gpLogfile); 
  fputs("\n **************************************************************** \n", gpLogfile); 

}

void resize(int width,int height)
{

  
  fputs("\n *****************Entering resize()******************** \n", gpLogfile);    

  if(height==0)
    height=1;
  glViewport (0,0,(GLsizei)width,(GLsizei) height);
  glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
  gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  fputs("\n *****************Exiting resize()******************** \n", gpLogfile); 
  fputs("\n **************************************************************** \n", gpLogfile); 

}


void uninitialize(void)
{
  
  fputs("\n *****************Entering uninitialize()******************** \n", gpLogfile);    

   GLXContext currentGLXContext;

   fputs("\n *****************Calling 'glXGetCurrentContext()' ********************* \n", gpLogfile);  

   currentGLXContext = glXGetCurrentContext();

   if(currentGLXContext !=NULL && currentGLXContext == gGLXContext )
   {
     fputs("\n *****************Calling 'glXMakeCurrent()' ********************* \n", gpLogfile);  
     glXMakeCurrent(gpDisplay,0,0);
   }

   if(gGLXContext)
   {
     fputs("\n *****************Calling 'glXDestroyContext()' ********************* \n", gpLogfile);  
     glXDestroyContext(gpDisplay,gGLXContext);
   }

   if(gWindow)
   {
    fputs("\n *****************Calling 'XDestroyWindow()' ********************* \n", gpLogfile);  
     XDestroyWindow(gpDisplay,gWindow);
   }

   if(gColormap)
   {
     fputs("\n *****************Calling 'XFreeColormap()' ********************* \n", gpLogfile);  
     XFreeColormap(gpDisplay,gColormap);
   }

   if(gpXVisualInfo)
   {
     fputs("\n *****************Calling 'free() for 'gpXVisualInfo' ********************* \n", gpLogfile);  
     free(gpXVisualInfo);
     gpXVisualInfo=NULL;
   }

   if(gpDisplay)
   {
     fputs("\n *****************Calling 'XCloseDisplay()' ********************* \n", gpLogfile);  
     XCloseDisplay(gpDisplay);
     gpDisplay=NULL;
   }

   if(gpLogfile)
   {
      fputs("\n *****************Calling 'fclose() for gpLogfile' ********************* \n", gpLogfile);  
      fclose(gpLogfile);
      gpLogfile=NULL;
   }

}
