
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

using namespace std;

bool gbFullScreen = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;

Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;


GLXContext gGLContext;

char ascii[32] = {'\0'};


void CreateWindow(void);
void ToggleFullScreen(void);
void uninitialize(void);

void initialize(void);
void display(void);
void resize(int width, int height, int iUpperLeftXOfWindow, int iUpperLeftYOfWindow );


int main()
{
 XEvent event;
 KeySym keysym;
 bool bDone = false;
 
 
CreateWindow();

initialize();
 while(bDone == false)
 {		
   while(XPending(gpDisplay))
   {
	XNextEvent(gpDisplay , &event);
	switch(event.type)
	{
		case MapNotify:
		  
			break;
		case MotionNotify:
		  
			break;
		case ConfigureNotify:
            giWindowWidth  = event.xconfigure.width;
		    giWindowHeight = event.xconfigure.height;	
			resize(giWindowWidth, giWindowHeight,0,0);
			
			break;
		case Expose:
		   //WM_PAINT	
			break;
	      case KeyPress:
		//WM_KEYDOWN
		keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);		
	       if(keysym == XK_Escape)
		      {
			
			      bDone = true;
		      }
	      else
	        {
		          XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);
	    	        switch(ascii[0])
		          {
	
		          	case 'F':
			          case 'f':
			
		          		if(gbFullScreen == false)
				            {
		
	 				            ToggleFullScreen();
					            gbFullScreen = true;		
				            }
				          else 
				            {
					            ToggleFullScreen();
					            gbFullScreen = false;
                    }
            break;

				case '0':

                  resize(giWindowWidth/2, giWindowHeight,0,0);
				
                break;
      
                case '1':
                
						        resize(giWindowWidth/2, giWindowHeight,giWindowWidth/2,0);
			          break;
			    case '2':
			
						    resize(giWindowWidth, giWindowHeight/2,0,giWindowHeight/2);
			          break;
				case '3':
			
						resize(giWindowWidth, giWindowHeight/2,0,0);
			        break;
			    case '4':
						resize(giWindowWidth/2, giWindowHeight/2,0,0);
			        break;
			
				case '5':
						resize(giWindowWidth/2, giWindowHeight/2,giWindowWidth/2,giWindowHeight/2);
			break;
			
			case '6':

						resize(giWindowWidth/2, giWindowHeight/2,0,0);
			break;
			case '7':

	        	resize(giWindowWidth/2, giWindowHeight/2,giWindowWidth/2,0);
			break;
			
	          }
               }
		break;
	   case ButtonPress:
	  switch(event.xbutton.button)
	  {
				case 1:
		
		
					break;
				case 2:
		
		
					break;
				case 3:
		
		
				break;
	}
	break;
		
		case DestroyNotify:
			break;
		case 33:
			uninitialize();
			exit(0);
		break;
		
		default:
		break;

	}

    }
   	
   display();
   //rotate();

  }
return 0;
	
}

void CreateWindow()
{
  XSetWindowAttributes winAttribs;
  int DefaultScreen;
  int DefaultDepth;
  int styleMask;

  static  int frameBufferAttributes[]=
  {
	GLX_RGBA,
	GLX_RED_SIZE,8,
	GLX_GREEN_SIZE,8,
	GLX_BLUE_SIZE,8,
	GLX_DOUBLEBUFFER,True,
	
	None
		
  };
 
 /*STEP 1*/
  gpDisplay = XOpenDisplay(NULL);
  if(gpDisplay == NULL)
  {
	printf("\nUnable to open Display!!!\n");
	uninitialize();
	exit(1);
  }

  /*STEP 2*/
  DefaultScreen = XDefaultScreen(gpDisplay);
  
  
  /*STEP 3*/
  DefaultDepth = DefaultDepth(gpDisplay,DefaultScreen);
  
  /*STEP 4*/
  gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
  if(gpXVisualInfo == NULL)
  {
	printf("\nError while allocating memory to XVisualInfo.\Exiting\n");
	uninitialize();
 	exit(1);
  }  

  
  gpXVisualInfo = glXChooseVisual(gpDisplay,DefaultScreen,frameBufferAttributes);
 
  /*SETTING WinAttributes--->Similar like filling WNDCLASSEX struct*/
  winAttribs.border_pixel = 0;
  winAttribs.background_pixmap = 0;
  winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
  gColormap = winAttribs.colormap;
  winAttribs.background_pixel = BlackPixel(gpDisplay,DefaultScreen);
  winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
  
  styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

  gWindow = XCreateWindow(gpDisplay,
			  RootWindow(gpDisplay,gpXVisualInfo->screen),
			  0,
			  0,
			  giWindowWidth,
			  giWindowHeight,
		  	  0,
			  gpXVisualInfo->depth,
			  InputOutput,
			  gpXVisualInfo->visual,
			  styleMask,
			  &winAttribs);
  if(!gWindow)
  {
	printf("\n Error while creating the window\nExiting now\n")	;
	uninitialize();
	exit(0);
  }

 XStoreName(gpDisplay,gWindow,"XLIB Perspective Triangle");

 Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
 
 XMapWindow(gpDisplay,gWindow);	

}


void ToggleFullScreen()
{
  Atom wm_state;
  Atom fullscreen;
  XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
  
  xev.type = ClientMessage;
  xev.xclient.window = gWindow;
  xev.xclient.message_type = wm_state;
  xev.xclient.format=32;
  xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;
  
  fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",false);
  xev.xclient.data.l[1]=fullscreen;
  
  XSendEvent(gpDisplay,
	     RootWindow(gpDisplay,gpXVisualInfo->screen),
	     False,
	     StructureNotifyMask,  
	     &xev);
 
  
}

void initialize()
{
  gGLContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
 
  glXMakeCurrent(gpDisplay,gWindow,gGLContext);
  
  glClearColor(0.0f,0.0f,0.0f,0.0f);
  
	glClearDepth(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);

  resize(giWindowWidth, giWindowHeight,0,0);
 
}

void resize(int width, int height, int iUpperLeftXOfWindow, int iUpperLeftYOfWindow )
{

	if(height == 0)
	{
		height = 1;
	}	                         
                              

	glViewport((GLsizei)iUpperLeftXOfWindow,(GLsizei)iUpperLeftYOfWindow,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f);

}


void DrawColoredTriangle()
{
  
	glBegin(GL_TRIANGLES);

		glColor3f(0.50f,0.0f,0.0f);
		glVertex3f(0.0f, 1.0,0.0f);

		glColor3f(0.0f,0.8f,0.0f);
		glVertex3f(- 1.0,- 1.0,0.0f);

		glColor3f(0.0f,0.0f,0.6f);
		glVertex3f( 1.0,- 1.0,0.0f);

		
	glEnd();
	return;
}


void display()
{
	 
  glClear(GL_COLOR_BUFFER_BIT);
  
    glMatrixMode(GL_MODELVIEW);
    
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-3.0f);
    
    DrawColoredTriangle();
    
  glXSwapBuffers(gpDisplay,gWindow);
    return;
}

void uninitialize()
{
 GLXContext currentGLXContext ;
 
 currentGLXContext =glXGetCurrentContext();

 if((currentGLXContext != NULL) && (currentGLXContext == gGLContext))
 {
	glXMakeCurrent(gpDisplay,0,0);
 }

 if(gGLContext)
 {
   glXDestroyContext(gpDisplay,gGLContext);
 } 
  if(gWindow)
  {
 	XDestroyWindow(gpDisplay,gWindow);
  }
  if(gColormap)
  {
	XFreeColormap(gpDisplay,gColormap);
  }
  if(gpXVisualInfo != NULL)
  {
	free(gpXVisualInfo);
	gpXVisualInfo = NULL;
  }
  if(gpDisplay != NULL)
  {
	XCloseDisplay(gpDisplay);
	gpDisplay = NULL;
  }

}


