
 // XOpenGL MonkeyHead Mesh Loading 
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
//#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

//#define PI 3.1415926535898
#include<vector>

//using namespace std;

// Symbolic Constants 
#define TRUE 1
#define FALSE 0

#define BUFFER_SIZE 256
#define S_EQUAL 0 // Return value of strcmp when strings are equal

// #define WIN_INIT_X 100
// #define WIN_INIT_Y 100
// #define WIN_WIDTH 800
// #define WIN_HEIGHT 600  

#define NR_POINT_COORDS 3 // Number of point coordinates 
#define NR_TEXTURE_COORDS 2 // Number of texture coordinates 
#define NR_NORMAL_COORDS 3 //  Number of normal coordinates 
#define NR_FACE_TOKENS 3 // Number of entries in face data+
#define FOY_ANGLE 45    // Field of view in y direction
#define ZNEAR 0.1
#define ZFAR 200.0

#define VIEWPORT_BOTTOMLEFT_X 0 
#define VIEWPORT_BOTTOMLETF_Y 0

#define MONKEYHEAD_X_TRANSLATE 0.0f 
#define MONKEYHEAD_Y_TRANSLATE -0.0f
#define MONKEYHEAD_Z_TRANSLATE  -5.0f

#define MONKEYHEAD_X_SCALE_FACTOR 1.5f 
#define MONKEYHEAD_Y_SCALE_FACTOR 1.5f
#define MONKEYHEAD_Z_SCALE_FACTOR 1.5f

#define START_ANGLE_POS 0.0f  //Marks beginning angle position of rotation
#define END_ANGLE_POS 360.0f  //Marks terminating angle position of rotation
#define MONKEYHEAD_ANGLE_INCREMENT 1.0f   // Increment angle for monkeyhead


// global varibales

bool gbFullscreen =false;
bool gbIsLightingEnabled = false;
Display *gpDisplay =NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLfloat gRotate;

GLXContext gGLXContext;

// Vector of vector of floats to hold vertex data
std::vector< std::vector<float> > g_vertices;

// Vector of vector of floats to hold texture data
std::vector< std::vector<float> > g_texture;

// Vector of vector of floats to hold normal data
std::vector< std::vector<float> > g_normals;

// Vector of vector of int to hold index data in g_vertices
std::vector< std::vector<int> > g_face_tri,g_face_texture,g_face_normals;

// Handle to a mesh file 
FILE *g_fp_meshfile=NULL;

// Handle to log file 
FILE *g_fp_logfile=NULL;

// Hold line in a file
char line[BUFFER_SIZE];

// Arrays required for lighting.
GLfloat light_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 1.0f,0.0f,0.0f,0.0f };

GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shinyness[] = { 50.f };


int main(void)
{
  void CreateWindow(void);
  void ToggleFullscreen(void);
  void initialize(void);
  void display(void);
  void resize(int,int);
  void uninitialize(void);
  void update(void);

  int winWidth= giWindowWidth;
  int winHeight= giWindowHeight;

  bool bDone= false;

  CreateWindow();

  initialize();

  XEvent event;
  KeySym keysym;

  while(bDone == false)
  {
    while (XPending(gpDisplay))
     {
       XNextEvent (gpDisplay,&event);

       switch (event.type)
        {
         case MapNotify:
          break;
         case KeyPress:
          keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);

          char ascii[26];
          XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);

          switch (ascii[0])
          {
            case 'f':
            case 'F':
                if(gbFullscreen==false)
                {
                   ToggleFullscreen();
                   gbFullscreen=true;
                }

                else
                {
                    ToggleFullscreen();
                    gbFullscreen=false;
                }

            break;
            case 'l':
            case  'L':
            if (gbIsLightingEnabled == false)
            {
              glEnable(GL_LIGHTING);
              gbIsLightingEnabled = true;
            }
            else
            {
              glDisable(GL_LIGHTING);
              gbIsLightingEnabled = false;
            }
            break;
    
              default:
            break;
          }

          switch(keysym)
          {
            case XK_Escape:
              bDone= true;
              break;
            default:
              break;
          }
         break;

        case ButtonPress:
          switch (event.xbutton.button)
           {
            case 1:
              break;
            case 2:
              break;
            case 3:
              break;
            default:
              break;
          }
          break;

        case MotionNotify:
          break;

        case ConfigureNotify:
            winWidth=event.xconfigure.width;
            winHeight=event.xconfigure.height;
            resize(winWidth,winHeight);
          break;
        case Expose:
          break;
        case DestroyNotify:
          break;
        case 33:
            bDone=true;
          break;
        default:
          break;
       }
    }

    // Update state variables

    update();

    display();

  }

  uninitialize();
  return(0);
}

void CreateWindow(void)
{
  void uninitialize(void);

  XSetWindowAttributes windowAttributes;
  int defaultScreen;
  int defaultDepth;
  int styleMask;

  static int frameBufferAttributes[]=
  {
     //GLX_X_RENDERABLE,True,
     //GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
     GLX_RENDER_TYPE,GLX_RGBA_BIT,
    // GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
     GLX_RED_SIZE,8,
     GLX_GREEN_SIZE,8,
     GLX_BLUE_SIZE,8,
     GLX_ALPHA_SIZE,8,
     GLX_DEPTH_SIZE,24,
     GLX_DOUBLEBUFFER,True,
     None
  };

  gpDisplay= XOpenDisplay(NULL);

  if(gpDisplay == NULL)
  {
    printf(" Error: Unable to open X display.\n Exiting now....\n");
    uninitialize();
    exit(1);
  }

  defaultScreen= XDefaultScreen(gpDisplay);

  gpXVisualInfo= glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

  windowAttributes.border_pixel=0;
  windowAttributes.background_pixmap=0;
  windowAttributes.colormap= XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay,gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone
                                    );
  gColormap= windowAttributes.colormap;

  windowAttributes.background_pixel= BlackPixel(gpDisplay,defaultScreen);

  windowAttributes.event_mask= ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

  styleMask= CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

  gWindow= XCreateWindow(gpDisplay,
                        RootWindow(gpDisplay,  gpXVisualInfo->screen),
                        0,
                        0,
                        giWindowWidth,
                        giWindowHeight,
                        0,
                        gpXVisualInfo->depth,
                        InputOutput,
                        gpXVisualInfo->visual,
                        styleMask,
                        &windowAttributes
                      );

    if(!gWindow)
    {
      printf("Error: Failed to create main window.\n Exiting now \n");
      uninitialize();
      exit(1);
    }

    XStoreName(gpDisplay,gWindow,"MonkeyHead Mesh Loading");

    Atom windowManagerDelete= XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
  {
    Atom wm_state;
    Atom fullscreen;
    XEvent xev={0};

    wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type=ClientMessage;

    xev.xclient.window=gWindow;
    xev.xclient.message_type=wm_state;
    xev.xclient.format=32;
    xev.xclient.data.l[0]=gbFullscreen?0:1;

    fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1]=fullscreen;
    XSendEvent(gpDisplay,
              RootWindow(gpDisplay,gpXVisualInfo->screen),
              False,
              StructureNotifyMask,
              &xev
            );
  }


void initialize(void)
{
  void resize(int,int);
  void uninitialize(void);
  void LoadMeshData(void);

  g_fp_logfile=fopen("MONKEYHEADLOADER.LOG","w");
  if(!g_fp_logfile)
  uninitialize();

  gGLXContext= glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

  glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);
  
  glDepthFunc(GL_LEQUAL);
  
  glClearColor(0.0f,0.0f,0.0f,0.0f);  
  glShadeModel(GL_SMOOTH);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

  glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	glEnable(GL_LIGHT0);

  LoadMeshData();

  resize(giWindowWidth,giWindowHeight);

}

void update(void)
{
  gRotate=gRotate+MONKEYHEAD_ANGLE_INCREMENT;

  if(gRotate>=END_ANGLE_POS)
  {
     gRotate=START_ANGLE_POS;
  }
}

void display(void)
{
  void uninitialize(void);
  
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glTranslatef(MONKEYHEAD_X_TRANSLATE,MONKEYHEAD_Y_TRANSLATE,MONKEYHEAD_Z_TRANSLATE);
  glRotatef(gRotate,0.0f,1.0f,0.0f);
  glScalef(MONKEYHEAD_X_SCALE_FACTOR,MONKEYHEAD_Y_SCALE_FACTOR,MONKEYHEAD_Z_SCALE_FACTOR);


glFrontFace(GL_CCW);

glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	
for(int i=0;i!=g_face_tri.size();++i)
{
  glBegin(GL_TRIANGLES);

  for (int j=0;j!=g_face_tri[i].size();j++)
  {
     int ni = g_face_normals[i][j]-1;
     glNormal3f(g_normals[ni][0],g_normals[ni][1],g_normals[ni][2]);

     int vi=g_face_tri[i][j]-1;
     glVertex3f(g_vertices[vi][0],g_vertices[vi][1],g_vertices[vi][2]);
  }
  glEnd();
}

  glXSwapBuffers(gpDisplay,gWindow);
}




void resize(int width,int height)
{
  if(height==0)
    height=1;
  glViewport (VIEWPORT_BOTTOMLEFT_X,VIEWPORT_BOTTOMLETF_Y,(GLsizei)width,(GLsizei) height);
  glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
  gluPerspective(FOY_ANGLE, (GLfloat)width / (GLfloat)height, ZNEAR, ZFAR);

}

void LoadMeshData()
{
  void uninitialize(void);
  
    // Open mesh file , name of mesh file can be parametaried
  
    g_fp_meshfile = fopen("MonkeyHeadMeshFile.OBJ", "r");
  
    if (!g_fp_meshfile)
      uninitialize();
  
    // Seperator strings 
    // String holding space seperator for strtok
  
    //char *sep_space = " ";
    char sep_space[] = " ";

    // String holding forward slash seperator for strtok
    //char *sep_fslash = "/";
    char sep_fslash[] = "/";

    // Token pointers 
    // Character pointer for holding first word in a line
  
    char *first_token = NULL;
  
    // Character pointer for holding next word seperated by specified seperator to strtok 
  
    char *token = NULL;
  
  
    // Array of character pointers to hold strings of face entries 
    // Face entries can be variable . In some files they are three and in some files they are four
  
    char *face_tokens[NR_FACE_TOKENS];
  
    // Number of non-null tokens in the above vector
  
    int nr_tokens;
  
    // Character pointer for holding string associated with vertex index
  
    char *token_vertex_index = NULL;
  
    // Character pointer for holding string associated with texture index
  
    char *token_texture_index = NULL;
  
    // Character pointer for holding string associated with normal index
  
    char *token_normal_index = NULL;
  
  
    // While there is line in a file 
  
    while (fgets(line,BUFFER_SIZE,g_fp_meshfile)!= NULL)
    {
      // Bind line to a seperator and get first token
  
      first_token = strtok(line, sep_space);
  
      // If first token indicates vertex data 
  
      if (strcmp(first_token,"v")== S_EQUAL)
      {
        // Create vector of NR_POINT_COORDS number of floats to hold coordinates 
  
        std::vector<float> vec_point_coord(NR_POINT_COORDS);
  
        //Do following NR_POINT_COORDS time
        // S1 :Get next token
        // S2 :Feed it ot atof to get floating point number out of it
        // S3 :Add floating point number generated to vector .
        // End of loop
        // S4 : At the end of loop vector is constructed , add it to global vector of vector of floats , named g_vertices
  
        for(int i=0; i != NR_POINT_COORDS;i++ )
        {
          vec_point_coord[i] = atof(strtok(NULL, sep_space));  // S1,S2,S3
        }
  
        g_vertices.push_back(vec_point_coord);
      }
  
      // If first token indicates texture data
  
      else if (strcmp(first_token,"vt")== S_EQUAL)
      {
        std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);
  
        // Do following NR_TEXTURE_COORDS time
        // S1: Get next token 
        //S2 :Feed it to atof to get floating point number out of it 
        //S3:Add the floating point number generated to vector 
        // End of loop
        // S4 : At the end of the loop ,vector is constructed , add it to global vector of vector of floats ,named g_texture
  
        for (int i=0; i!= NR_TEXTURE_COORDS; i++)
        {
          vec_texture_coord[i] = atof(strtok(NULL, sep_space));
        }
  
        g_texture.push_back(vec_texture_coord);
      }
  
      // If first token indicates normal data
  
      else if (strcmp(first_token,"vn")==S_EQUAL)
      {
        std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);
  
        // Do following NR_NORMAL_COORDS time
        // S1: Get the token
        // S2 :Feed it to atof to get floating point number out of it
        // S3 : Add the floating point number generated to vector .
        // End of loop
        // S4 : At the end of loop vector is constructed , add it to global vector of vector of floats , named g_normals
  
        for (int i=0;i!=NR_NORMAL_COORDS;i++)
        {
          vec_normal_coord[i] = atof(strtok(NULL, sep_space));
        }
  
        g_normals.push_back(vec_normal_coord);
      }
  
      // If first token indicates face data
  
      else if (strcmp(first_token, "f")== S_EQUAL)
      {
        // Define three vectors of integers with length 3 to hold indices of triangles positional coordinates , texture coordinates and normal coordinates 
        // in g_vertices, g_textures and g_normals resp
  
        std::vector <int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);
  
        // Initiate all  char pointers in face_token to NULL
  
        memset((void*)face_tokens, 0, NR_FACE_TOKENS);
  
        // Extract three fields of information in face_tokens and increment nr_tokens accordingly
  
        nr_tokens = 0;
  
        while (token=strtok(NULL,sep_space))
        {
          if (strlen(token) < 3)
            break;
          face_tokens[nr_tokens] = token;
          nr_tokens++;
        }
  
        // Every face data entry is going to have minimum three fields therefore , construct a triangle out of it with 
        // S1: triangle coordinate data and 
        // S2 : Texture coordinate index data
        // S3 :normal coordinate index data
        // S4 : Put that data in triangle_vertex_indices,texture_vertex_indices,normal_vertex_indices .Vectors will be constructed at the end of the loop
  
        for (int i=0; i!=NR_FACE_TOKENS; i++)
        {
          token_vertex_index = strtok(face_tokens[i], sep_fslash); //S1
          token_texture_index = strtok(NULL, sep_fslash); // S2
          token_normal_index = strtok(NULL, sep_fslash); // S3
  
          triangle_vertex_indices[i] = atoi(token_vertex_index);
          texture_vertex_indices[i] = atoi(token_texture_index);
          normal_vertex_indices[i] = atoi(token_normal_index);
        }
  
        // Add constructed vectors to global face vectors
  
        g_face_tri.push_back(triangle_vertex_indices);
        g_face_texture.push_back(texture_vertex_indices);
        g_face_normals.push_back(normal_vertex_indices);
      }
  
      // Initialize line buffer to NULL
  
      memset((void*)line, (int)'\0', BUFFER_SIZE);
  
    }
  
  
    // Close meshfile and make file pointer NULL
  
    fclose(g_fp_meshfile);
    g_fp_meshfile = NULL;
  
    // Log vertex , texture and  face data in log file
  
    //fprintf(g_fp_logfile, "g_vertices: %llu g_texture:%llu g_normals: %llu g_face_tri : %llu\n", g_vertices.size(), g_texture.size(), g_normals.size(), g_face_tri.size());  
    fprintf(g_fp_logfile, "g_vertices: %lu g_texture:%lu g_normals: %lu g_face_tri : %lu\n", g_vertices.size(), g_texture.size(), g_normals.size(), g_face_tri.size());  

}

void uninitialize(void)
 {

  fclose(g_fp_logfile);
  g_fp_logfile=NULL;

   GLXContext currentGLXContext;
   currentGLXContext = glXGetCurrentContext();

   if(currentGLXContext !=NULL && currentGLXContext == gGLXContext )
   {
     glXMakeCurrent(gpDisplay,0,0);
   }

   if(gGLXContext)
   {
     glXDestroyContext(gpDisplay,gGLXContext);
   }

   if(gWindow)
   {
     XDestroyWindow(gpDisplay,gWindow);
   }

   if(gColormap)
   {
     XFreeColormap(gpDisplay,gColormap);
   }

   if(gpXVisualInfo)
   {
     free(gpXVisualInfo);
     gpXVisualInfo=NULL;
   }

   if(gpDisplay)
   {
     XCloseDisplay(gpDisplay);
     gpDisplay=NULL;
   }
}
