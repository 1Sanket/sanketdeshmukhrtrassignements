
// Union sample

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

// namespaces
using namespace std;

union valueUnion
{
    int integerValue;
    float floatValue;
    char stringValue[20];
} v;

int main()
{
    printf("Enter integer value to store in union.....");
    scanf("%d",&v.integerValue);
    printf("\n Value is stored in union is %d",v.integerValue);
    printf("\n******************\n");
    printf("Enter float value to store in union.....");
    scanf("%f",&v.floatValue);
    printf("\n Value is stored in union is %f",v.floatValue);
    printf("\n******************\n");
    printf("Enter string value to store in union.....");
    scanf("%s",v.stringValue);
    printf("\n Value is stored in union is %s",v.stringValue);
    printf("\n******************\n");
    getchar();
    return(0);
}