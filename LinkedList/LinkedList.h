

struct Node
{
	int data;
	Node* nextNode;
};

bool createNode( int data);


// for adding node at the end of the list
void insertEnd(int data);

void insertBegining( int data);

Node *findData( int data);

bool deleteNode( Node *ptrDelete);

void deleteLinkedList();

void display();
