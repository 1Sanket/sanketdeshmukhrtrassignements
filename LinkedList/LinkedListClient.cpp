#include <iostream>
#include <conio.h>
using namespace std;
#include "LinkedList.h"

int main()
{
	int data;
	// Create Node.
	cout << "\n Enter data to create first node";
	cin >> data;

	if (createNode(data))
	{
		cout << "Node created successfully";
	}

	else
	{
		cout << "Unable to create node";
	}

	cout << "\n Enter data to insert at the beginning of the list";
	cin >> data;

	insertBegining(data);

	cout << "\n Enter data to insert at the end of the list";
	cin >> data;

	insertEnd(data);

	cout << "\n Enter data to search in the list";
	cin >> data;

	Node* result = findData(data);

	cout << " \n ";
	if (result == NULL)
	{
		cout << "\n Data not found";
	}

	else
	{
		cout << "\n Data found .....";
		cout << "\n" << result->data ;
	}

	cout << "\n Displaying list ....."	;
	display();


	cout << "\n Deleting data found in above search operation";
	if (deleteNode(result))
	{
		cout << "\n ";
		cout << "Node deleted successfully";
	}

	else
	{
		cout << "\n";
		cout << "Node not found";
	}

	cout << "\n Displaying list .....";

	display();


	cout << "\n Deleting list .....";

	deleteLinkedList();
	
	getchar();

	getchar();

	return(0);
}