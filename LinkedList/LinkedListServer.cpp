
#include <iostream>
using namespace std;
#include "LinkedList.h"

Node *head;

// For creating first node.
bool createNode(int data)
{
	head = (Node *) malloc(sizeof(Node));
	if (head == NULL)
	{
		return false;
	}

	else
	{
		head->data = data;
		head->nextNode = NULL;
		return true;
	}
}


// for adding node at the end of the list
void insertEnd(int data)
{
	Node *newNode = new Node;
	newNode->data = data;
	newNode->nextNode = NULL;

	Node *currentNode = head;

	while (currentNode)
	{
		if (currentNode->nextNode == NULL)
		{
			currentNode->nextNode = newNode;
			return;
		}

		currentNode = currentNode->nextNode;
	}
}

void insertBegining(int data)
{
	Node *newNode = new Node;
	newNode->data = data;
	newNode->nextNode = head;
	head = newNode;
}

Node *findData(int data)
{
	Node *currentNode = head;

	while (currentNode)
	{
		if(currentNode->data== data)
		{
			return currentNode;
		}
		currentNode = currentNode->nextNode;
	}

	return NULL;
/*
	cout << "Node :" << data << "not found in the list. \n";*/

}

bool deleteNode(Node *ptrDelete)
{
	Node *currentNode = head;

	if (ptrDelete == head)
	{
		head = currentNode->nextNode;
		delete ptrDelete;
		return true;
	}

	while (currentNode)
	{
		if(currentNode->nextNode==ptrDelete)
		{
			currentNode->nextNode = ptrDelete->nextNode;
			delete ptrDelete;
			return true;
		}

		currentNode = currentNode->nextNode;
	}

	return false;
}


void deleteLinkedList()
{
	Node *tempNode;
	while (head)
	{
		tempNode = head;
		head = tempNode->nextNode;
		delete tempNode;
	}
}

void display()
{
	//int outputList[50] ;
	//int i = 0;
	Node *list = head;
	while(list)
	{
	 
     //outputList[i]= list->data;-
		cout << "\n";
		cout << list->data << " ";
		cout << "\n";
		list = list->nextNode;
	//	i++;
	}

//	return outputList;
}