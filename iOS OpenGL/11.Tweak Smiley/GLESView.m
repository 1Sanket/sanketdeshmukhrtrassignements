
// Twek Smiley.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h> 

#import "GLESView.h"

#import "vmath.h"

enum
{
	VDG_ATTRIBUTE_VERTEX=0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};


@implementation GLESView
{
	EAGLContext *eaglContext;

	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;

	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;

    	
	GLuint Vao_Smiley;
	GLuint Vbo_Smiley_Position;
	GLuint Vbo_Smiley_Texture;

	GLuint smiley_Texture;	

	GLuint white_Texture;
	GLubyte whiteColor[4] = {255,255,255,255};

	GLuint mvpUniform;

	vmath::mat4 perspectiveProjectionMatrix;

	GLuint texture_sampler_uniform;

	int numberOfSingleTaps =0;
} 

-(id) initWithFrame:(CGRect) frame
{
	self = [super initWithFrame:frame];

	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys : 
																					[NSNumber numberWithBool:FALSE ],
																					kEAGLDrawablePropertyRetainedBacking,
																					kEAGLColorFormatRGBA8,
																					kEAGLDrawablePropertyColorFormat,
																					nil
																					];
		eaglContext =[[EAGLContext alloc]initWithAPI : kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];

			return(nil);
		}

		[EAGLContext setCurrentContext:eaglContext];

		glGenFramebuffers(1,&defaultFramebuffer);
		glGenRenderbuffers(1,&colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);

		GLint backingWidth;
		GLint backingHeight;

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,,&backingWidth);

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,,&backingHeight);

		glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed to create complete frame buffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));

			glDeleteFramebuffers(1,&defaultFramebuffer);
			glDeleteRenderbuffers(1,&colorRenderbuffer);
			glDeleteRenderbuffers(1,&depthRenderbuffer);

			return(nil);
		}

		printf("Renderer is %s | GL Version:%s | GLSL Version:%s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));

		
		// hard coded initializations

		isAnimating = NO;
		animationFrameInterval=60;  // default since iOS 8.2

        
		// Vertex Shader //

		vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
		"#version 300 es" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexture0_Coord;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec2 out_texture0_coord;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_texture0_coord = vTexture0_Coord ;" \
		"}";

		glShaderSource(vertexShaderObject,1,(const GLchar**) &vertexShaderSourceCode,NULL);

		glCompileShader(vertexShaderObject);
        GLint iInfoLogLength =0;
        GLint iShaderCompiledStatus =0;
		char *szInfoLog = NULL;

		glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

		if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength>0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Vertex Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

        
		// Fragment Shader 

		iInfoLogLength =0;
		iShaderCompiledStatus =0;
		szInfoLog = NULL;

		fragmentShaderObject= glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode= 
		"#version 300 es" \
		"\n" \
		"precision highp float;" \
		"in vec2 out_texture0_coord;" \
		"uniform sampler2D u_texture0_sampler;"\
		"out vec4 FragColor;" \
		"void main(void)"\
		"{"\
		"vec3 tex = vec3(texture(u_texture0_sampler,out_texture0_coord));"\
		"FragColor = vec4(tex,1.0f);"\
		"}";

		glShaderSource(fragmentShaderObject,1,(const GLchar **)&fragmentShaderSourceCode,NULL);
        
		glCompileShader(fragmentShaderObject);
		glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

        if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength >0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Fragment Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

        
		// Shader Program

		shaderProgramObject = glCreateProgram();

		glAttachShader(shaderProgramObject,vertexShaderObject);

		glAttachShader(shaderProgramObject,fragmentShaderObject);

		
		// pre-link binding of shader program object with vertex shader position attribute

		glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

        glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");
		
		//link shader
		glLinkProgram(shaderProgramObject);

		GLint iShaderProgramLinkStatus =0;

		glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

        
		if(iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			
			if(iInfoLogLength >0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
					printf("Shader Program Link Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

         
		// get MVP uniform location

		mvpUniform = glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		texture_sampler_uniform = glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");

		// load textures

		smiley_Texture = [self loadTextureFromBMPFile:@"Smiley" : @"bmp"];

		white_Texture = [self loadWhiteColorTexture];

	// Square

	const GLfloat squareVertices []=
    {
		 1.0f, 1.0f, 1.0f,

    
		 -1.0f, 1.0f, 1.0f,

    
		-1.0f, -1.0f, 1.0f,

    
		 1.0f, -1.0f, 1.0f
    };


    // Square Vao

    glGenVertexArrays(1,&Vao_Smiley);
    glBindVertexArray(Vao_Smiley);

    // Square Position Vbo 

    glGenBuffers(1,& Vbo_Smiley_Position);
    glBindBuffer(GL_ARRAY_BUFFER,Vbo_Smiley_Position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(squareVertices),squareVertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind square position Vbo
      
    
	const GLfloat SquareTexcoords[]=
    {
        1.0f, 0.0f,
    
        0.0f, 0.0f,
    
        0.0f, 1.0f,
    
        1.0f, 1.0f
    };

	// Square Texture Vbo 

    glGenBuffers(1,& Vbo_Smiley_Texture);
    glBindBuffer(GL_ARRAY_BUFFER,Vbo_Smiley_Texture);
    glBufferData(GL_ARRAY_BUFFER,sizeof(SquareTexcoords),SquareTexcoords,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0,2,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind square position Vbo
    glBindVertexArray(0); // Unbind Square vao
    
    glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glEnable(GL_CULL_FACE);

	glEnable(GL_TEXTURE_2D);

		// clear color

		glClearColor(0.0f,0.0f,1.0f,1.0f);

		perspectiveProjectionMatrix = vmath::mat4::identity();

		// Gesture recognition

		// Tap gesture code

        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)]; // callback is given by @selector

		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];


		UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)]; // callback is given by @selector

		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];


		// To allow to differentiate between single tap and double tap

		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];

		
		// swipe Gesture

		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];

		[self addGestureRecognizer:swipeGestureRecognizer];

		//long press gesture

		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];

		[self addGestureRecognizer:longPressGestureRecognizer];

	}

	return(self);

}


-(GLuint) loadTextureFromBMPFile:(NSString *) texFileName : (NSString *) extension
{
	//NSBundle *mainBundle = [NSBundle mainBundle];
	//NSString *appDirName = [mainBundle bundlePath];
	//NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	
	//NSString *textureFileNameWithPath=[NSString stringWithFormat: @"%@/%s",parentDirPath,texFileName];
	NSString *textureFileNameWithPath = [[NSBundle mainBundle] pathForResource : texFileName ofType:extension];

	UIImage *bmpImage = [[UIImage alloc]initWithContentsOfFile: textureFileNameWithPath];

	if(!bmpImage)
	{
		NSLog(@"Cannot find %@",textureFileNameWithPath);
		return(0);
	}

	//CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];

	CGImageRef cgImage = bmpImage.CGImage;

	int w= (int) CGImageGetWidth(cgImage);
	int h= (int) CGImageGetHeight(cgImage);

	CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
	void* pixels = (void *)CFDataGetBytePtr(imageData);

	GLuint bmpTexture;
	glGenTextures(1,&bmpTexture);

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glBindTexture(GL_TEXTURE_2D,bmpTexture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,
	0,
	GL_RGBA,
	w,
	h,
	0,
	GL_RGBA,
	GL_UNSIGNED_BYTE,
	pixels
	);

	glGenerateMipmap(GL_TEXTURE_2D);

	CFRelease(imageData);
	return(bmpTexture);
}


-(GLuint) loadWhiteColorTexture
{
	GLuint bmpTexture;
	glGenTextures(1,&bmpTexture);

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glBindTexture(GL_TEXTURE_2D,bmpTexture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,
	0,
	GL_RGBA,
	1,
	1,
	0,
	GL_RGBA,
	GL_UNSIGNED_BYTE,
	whiteColor
	);

	return(bmpTexture);
}

+(Class)layerClass
{
	return([CAEAGLLayer class]);
}


-(void) drawView:(id) sender
{

      GLfloat quadTexture[8];
    
            if (numberOfSingleTaps == 1)   // Bottom left one fourth
            {

                quadTexture[0] = 0.5f;
                quadTexture[1] = 0.5f;
                quadTexture[2] = 0.0f;
                quadTexture[3] = 0.5f;
                quadTexture[4] = 0.0f;
                quadTexture[5] = 1.0f;
                quadTexture[6] = 0.5f;
                quadTexture[7] = 1.0f;
                
            }
    
            //else if (giKeyPressed ==2)   // Full smiley
            //{
    
            //	quadTexture[0] = 1.0f;
            //	quadTexture[1] = 1.0f;
            //	quadTexture[2] = 0.0f;
            //	quadTexture[3] = 1.0f;
            //	quadTexture[4] = 0.0f;
            //	quadTexture[5] = 0.0f;
            //	quadTexture[6] = 1.0f;
            //	quadTexture[7] = 0.0f;
            //}
    
            else if (numberOfSingleTaps == 3) // four full smileys in square 
            {
    
                quadTexture[0] = 2.0f;
                quadTexture[1] = 0.0f;
                quadTexture[2] = 0.0f;
                quadTexture[3] = 0.0f;
                quadTexture[4] = 0.0f;
                quadTexture[5] = 2.0f;
                quadTexture[6] = 2.0f;
                quadTexture[7] = 2.0f;
            }
    
            else if (numberOfSingleTaps == 4) // only yellow colour in square 
            {
    
                quadTexture[0] = 0.5f;
                quadTexture[1] = 0.5f;
                quadTexture[2] = 0.5f;
                quadTexture[3] = 0.5f;
                quadTexture[4] = 0.5f;
                quadTexture[5] = 0.5f;
                quadTexture[6] = 0.5f;
                quadTexture[7] = 0.5f;
            }
    
            else   // for 2 key press of full smiley and default case , full image coordinates will be passed.
            {
    
                quadTexture[0] = 1.0f;
                quadTexture[1] = 0.0f;
                quadTexture[2] = 0.0f;
                quadTexture[3] = 0.0f;
                quadTexture[4] = 0.0f;
                quadTexture[5] = 1.0f;
                quadTexture[6] = 1.0f;
                quadTexture[7] = 1.0f;
                
            }


	[EAGLContext setCurrentContext:eaglContext];

	glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	vmath::mat4 modelViewMatrix = vmath::mat4::identity();
	vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
	//vmath::mat4 rotationMatrix = vmath::mat4::identity(); 

	modelViewMatrix = vmath::translate(-1.5f,0.0f,-6.0f);

	//rotationMatrix = vmath::rotate(anglePyramid,0.0f,1.0f,0.0f);

	//modelViewMatrix = modelViewMatrix * rotationMatrix ;

    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		// Bind with texture

	glActiveTexture(GL_TEXTURE0);

    if (numberOfSingleTaps ==1 || numberOfSingleTaps ==2 || numberOfSingleTaps ==3 || numberOfSingleTaps ==4)
    {
        glBindTexture(GL_TEXTURE_2D, smiley_Texture);
    }

    else
    {
        glBindTexture(GL_TEXTURE_2D, white_Texture);
    }

    glUniform1i(texture_sampler_uniform ,0);

	glBindVertexArray(Vao_Smiley);

	glDrawArrays(GL_TRIANGLE_FAN,0,4); // draw Square

	glBindVertexArray(0);
	
	glUseProgram(0);

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

	[eaglContext presentRenderbuffer:GL_RENDERBUFFER];

	//[self updateAngle];

}

// -(void) updateAngle
// {
// 	anglePyramid = anglePyramid + 0.7f;
// 	angleCube = angleCube - 0.7f;
// 	if (anglePyramid >= 360.0f)
// 		anglePyramid = 0.0f;
// 	if (angleCube <= 0.0f)
// 		angleCube = 360.0f;
// }

-(void)layoutSubviews
{
	GLint width;
	GLint height;

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

	[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);

	glGenRenderbuffers(1,&depthRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

	glViewport(0,0,width,height);

    perspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Failed to create complete frame buffer object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
	}

	[self drawView:nil];
}

-(void) startAnimation
{
	if(!isAnimating)
	{
		displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];

		[displayLink setPreferredFramesPerSecond : animationFrameInterval];

		[displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

		isAnimating = YES;
	}
}

-(void) stopAnimation
{
	if(isAnimating)
	{
		[displayLink invalidate];

		displayLink = nil;

		isAnimating = NO;
	}
}

-(BOOL) acceptsFirstResponder
{
	return(YES);
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
{

}

-(void) onSingleTap:(UITapGestureRecognizer *)gr
{
    if(numberOfSingleTaps >= 4 )
	  {
		numberOfSingleTaps =0;
	  }
	else
	  {
	  	numberOfSingleTaps ++;
	  }
}

-(void) onDoubleTap:(UITapGestureRecognizer *) gr
{
	
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
	[self release];

	exit(0);
}

-(void) onLongPress :(UILongPressGestureRecognizer *)gr
{

}

-(void) dealloc
{
 
    if(Vao_Smiley)
	{
		glDeleteVertexArrays(1,&Vao_Smiley);
		Vao_Smiley =0;
	}

	if(Vbo_Smiley_Position)
	{
		glDeleteBuffers(1,&Vbo_Smiley_Position);
		Vbo_Smiley_Position =0;
	}

	if(smiley_Texture)
	{
		glDeleteTextures(1,&smiley_Texture);
		smiley_Texture =0;
	}

	if(white_Texture)
	{
		glDeleteTextures(1,&white_Texture);
		white_Texture =0;
	}

	if(Vbo_Smiley_Texture)
	{
		glDeleteBuffers(1,&Vbo_Smiley_Texture);
		Vbo_Smiley_Texture =0;
	}

	glDetachShader(shaderProgramObject,vertexShaderObject);

	glDetachShader(shaderProgramObject,fragmentShaderObject);

	glDeleteShader(vertexShaderObject);
	vertexShaderObject =0;

	glDeleteShader(fragmentShaderObject);
	fragmentShaderObject=0;

	glDeleteProgram(shaderProgramObject);
	shaderProgramObject=0;


	if(depthRenderbuffer)
	{
		glDeleteRenderbuffers(1,&depthRenderbuffer);
		depthRenderbuffer=0;
	}

	if(colorRenderbuffer)
	{
		glDeleteRenderbuffers(1,&colorRenderbuffer);
		colorRenderbuffer=0;
	}

	if(defaultFramebuffer)
	{
		glDeleteRenderbuffers(1,&defaultFramebuffer);
		defaultFramebuffer=0;
	}

	if([EAGLContext currentContext]==eaglContext)
	{
		[EAGLContext setCurrentContext:nil];
	}

	[eaglContext release];
	eaglContext = nil;

	[super dealloc];
}

@end