
// Window

#import "MyView.h"

@implementation MyView
{
	NSString *centralText; 
} 

-(id) initWithFrame:(CGRect) frameRect
{
	self = [super initWithFrame:frameRect];

	if(self)
	{
		// initialization code

		// set scene's background color

		[self setBackgroundColor:[UIColor whiteColor]];

		centralText = @"Hello World !!!!!";

		// Gesture recognition

		// Tap gesture code

        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)]; // callback is given by @selector

		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];


		UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)]; // callback is given by @selector

		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];


		// To allow to differentiate between single tap and double tap

		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];

		
		// swipe Gesture

		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];

		[self addGestureRecognizer:swipeGestureRecognizer];

		//long press gesture

		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];

		[self addGestureRecognizer:longPressGestureRecognizer];

	}

	return(self);
}


-(void) drawRect:(CGRect)rect
{
	// black background
	UIColor *fillColor =[UIColor blackColor];

	[fillColor set];

	UIRectFill(rect);

	// dictionary with kvc

	NSDictionary *dictionaryForTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
																							[UIFont fontWithName:@"Helvetica" size:24],
																							 NSFontAttributeName,
																							 [UIColor greenColor],
																							 NSForegroundColorAttributeName,
																							 nil];

	CGSize textSize = [centralText sizeWithAttributes:dictionaryForTextAttributes];

	CGPoint point;

	point.x = (rect.size.width/2) - (textSize.width/2) ;
	point.y = (rect.size.height/2) - (textSize.height/2) + 12;

	[centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];

}


// To become first responder

-(BOOL) acceptsFirstResponder
{
    return(YES);
}

-(void) touchesBegan :(NSSet *)touches withEvent:(UIEvent *) event
{
	/*
	 centralText = @"'touchesBegan ' event occured";
	 [self setNeedsDisplay];
	*/
}

-(void) onSingleTap:(UITapGestureRecognizer *)gr
{
	centralText = @" onSingleTap event occured";
	[self setNeedsDisplay];
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
	centralText = @" onDoubleTap event occured";
	[self setNeedsDisplay];
}

-(void) onSwipe:(UISwipeGestureRecognizer *)gr
{
	[self release];
	exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
	centralText = @" onLongPress event occured";
	[self setNeedsDisplay];
}


-(void)dealloc
{
	[super dealloc];
}

@end

