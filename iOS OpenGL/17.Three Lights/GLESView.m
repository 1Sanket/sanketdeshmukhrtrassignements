
// Three Lights.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h> 

#import "GLESView.h"

#import "vmath.h"

enum
{
	VDG_ATTRIBUTE_VERTEX=0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};


@implementation GLESView
{
	EAGLContext *eaglContext;

	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;

	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;

	// For per vertex light 

GLuint perVertex_VertexShaderObject;
GLuint perVertex_FragmentShaderObject;
GLuint perVertexShaderProgramObject;

// For per fragment light

GLuint perFragment_VertexShaderObject;
GLuint perFragment_FragmentShaderObject;
GLuint perFragmentShaderProgramObject;

GLuint perVertex_ModelMatrixUniform,perVertex_ViewMatrixUniform,perVertex_ProjectionMatrixUniform;
GLuint perVertex_LaUniform,perVertex_LdUniform, perVertex_LsUniform,perVertex_LightPositionUniform;
GLuint perVertex_KaUniform,perVertex_KdUniform, perVertex_KsUniform, perVertex_MaterialShininessUniform;

GLuint perFragment_ModelMatrixUniform,perFragment_ViewMatrixUniform,perFragment_ProjectionMatrixUniform;
GLuint perFragment_LaUniform,perFragment_LdUniform, perFragment_LsUniform,perFragment_LightPositionUniform;
GLuint perFragment_KaUniform,perFragment_KdUniform, perFragment_KsUniform,perFragment_MaterialShininessUniform;

GLuint perVertex_doubleTapUniform;
GLuint perFragment_doubleTapUniform;

	
	GLuint vao_Sphere;
		
	GLuint vbo_Sphere_Position;
	GLuint vbo_Sphere_Normal;
	GLuint vbo_Sphere_Elements;

    GLfloat lightAmbient[] = {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};
	GLfloat lightDiffuse[] = {1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,1.0f};
	GLfloat lightSpecular[] = {1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,1.0f};
	GLfloat lightPosition[] = {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0,0.0,0.0};

	GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
	GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
	GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
	GLfloat materialShininess = 50.0f;

    	
		// float light_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
    	
		// float light_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
    	
		// float light_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
    	
		// float light_position[] = {100.0f, 100.0f, 100.0f, 1.0f};	
		    	
		// float material_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
    	
		// float material_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
    	
		// float material_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
    	
		// float material_shininess = 50.0f;
		

		//static GLfloat angleCube =0.0f;
		//bool isAnimated = false;
		int doubleTap= 0;

		bool isPerVertexLighting = true ;
		bool isPerFragmentLighting ;

		float angleRedLight = 0.0f;
		float angleBlueLight = 0.0f;
		float angleGreenLight = 0.0f;

	//GLuint LdUniform,KdUniform,LightPositionUniform;
	

	vmath::mat4 perspectiveProjectionMatrix;
} 

-(id) initWithFrame:(CGRect) frame
{
	self = [super initWithFrame:frame];

	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys : 
																					[NSNumber numberWithBool:FALSE ],
																					kEAGLDrawablePropertyRetainedBacking,
																					kEAGLColorFormatRGBA8,
																					kEAGLDrawablePropertyColorFormat,
																					nil
																					];
		eaglContext =[[EAGLContext alloc]initWithAPI : kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];

			return(nil);
		}

		[EAGLContext setCurrentContext:eaglContext];

		glGenFramebuffers(1,&defaultFramebuffer);
		glGenRenderbuffers(1,&colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);

		GLint backingWidth;
		GLint backingHeight;

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,,&backingWidth);

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,,&backingHeight);

		glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed to create complete frame buffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));

			glDeleteFramebuffers(1,&defaultFramebuffer);
			glDeleteRenderbuffers(1,&colorRenderbuffer);
			glDeleteRenderbuffers(1,&depthRenderbuffer);

			return(nil);
		}

		printf("Renderer is %s | GL Version:%s | GLSL Version:%s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));

		
		// hard coded initializations

		isAnimating = NO;
		animationFrameInterval=60;  // default since iOS 8.2

        
		// Vertex Shader //

		// Vertex Shader 

	perVertex_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *perVertex_VertexShaderSourceCode=
		"#version 300 es" \
		"\n" \
		"in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix ;" \
        "uniform mat4 u_view_matrix ;" \
        "uniform mat4 u_projection_matrix ;" \
        "uniform int isDoubleTap;" \
        "uniform vec3 u_La[3] ;" \
        "uniform vec3 u_Ld[3] ;" \
        "uniform vec3 u_Ls[3] ;" \
        "uniform vec4 u_light_position[3] ;" \
        "uniform vec3 u_Ka ;" \
        "uniform vec3 u_Kd ;" \
        "uniform vec3 u_Ks ;" \
        "uniform float u_material_shininess ;" \
        "out vec3 phong_ads_color;" \
        "void main(void)" \
        "{" \
        "vec3 color = vec3(1.0,1.0,1.0);"\
        "if (isDoubleTap == 1)"\
        "{"\
        "vec3 ambient[3];"\
        "vec3 diffuse[3];"\
        "vec3 specular[3];"\
        "for(int i=0;i<3;i++)"\
        "{"\
        "vec4 eye_coordinates = u_view_matrix *u_model_matrix* vPosition;"\
        "vec3 transformed_normals = normalize(mat3 (u_view_matrix * u_model_matrix) * vNormal );"\
        "vec3 light_direction = normalize (vec3 (u_light_position[i]) - eye_coordinates.xyz);"\
        "float tn_dot_ld = max(dot (transformed_normals ,light_direction), 0.0);"\
        "ambient[i] = u_La[i] * u_Ka;"\
        "diffuse[i] = u_Ld[i] * u_Kd * tn_dot_ld ;"\
        "vec3 reflection_vector = reflect(-light_direction,transformed_normals);"\
        "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"\
        "specular[i] = u_Ls[i] * u_Ks * pow (max(dot (reflection_vector,viewer_vector),0.0),u_material_shininess);"\
        "}"\
        "color = ambient[0] + ambient[1] + ambient[2] + diffuse[0] + diffuse[1] + diffuse[2] + specular[0] + specular[1] + specular[2];"\
        "}"\
        "else"\
        "{"\
        "color = vec3(1.0,1.0,1.0);"\
        "}"\
        "phong_ads_color = color ;"\
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;" \
        "}";

	glShaderSource(perVertex_VertexShaderObject,1,(const GLchar **) &perVertex_VertexShaderSourceCode,NULL);

	glCompileShader(perVertex_VertexShaderObject);
	
	GLint iInfoLogLength =0;
	GLint iShaderCompiledStatus =0;
	char *szInfoLog = NULL;

	glGetShaderiv(perVertex_VertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

		if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(perVertex_VertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength>0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(perVertex_VertexShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Vertex Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

        
		// Fragment Shader 

		iInfoLogLength =0;
		iShaderCompiledStatus =0;
		szInfoLog = NULL;

	perVertex_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *perVertex_FragmentShaderSourceCode =
			"#version 300 es" \
			"\n" \
			"precision highp float;" \
            "in vec3 phong_ads_color;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
			"FragColor = vec4(phong_ads_color,1.0) ;" \
			"}" ;

	glShaderSource(perVertex_FragmentShaderObject,1, (const GLchar **)&perVertex_FragmentShaderSourceCode,NULL);

	glCompileShader(perVertex_FragmentShaderObject);

	glGetShaderiv(perVertex_FragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);


        if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(perVertex_FragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength >0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(perVertex_FragmentShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Fragment Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}



       	// Shader Program

    perVertexShaderProgramObject = glCreateProgram();

	// Attach Shaders 

	glAttachShader (perVertexShaderProgramObject,perVertex_VertexShaderObject);

	glAttachShader (perVertexShaderProgramObject,perVertex_FragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute

	glBindAttribLocation(perVertexShaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

	glBindAttribLocation(perVertexShaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");

	// link Shader

	glLinkProgram(perVertexShaderProgramObject);

	GLint iShaderProgramLinkStatus=0;

	glGetProgramiv(perVertexShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

        
		if(iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(perVertexShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			
			if(iInfoLogLength >0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(perVertexShaderProgramObject,iInfoLogLength,&written,szInfoLog);
					printf("Shader Program Link Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

         
		 

		// get MVP uniform location

	 	perVertex_doubleTapUniform = glGetUniformLocation(perVertexShaderProgramObject,"isDoubleTap");
	 	perVertex_ModelMatrixUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_model_matrix");
        perVertex_ViewMatrixUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_view_matrix");
        perVertex_ProjectionMatrixUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_projection_matrix");
        perVertex_LaUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_La");
        perVertex_LdUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_Ld");
        perVertex_LsUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_Ls");
        perVertex_LightPositionUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_light_position");
        perVertex_KaUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_Ka");
        perVertex_KdUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_Kd");
        perVertex_KsUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_Ks");
        perVertex_MaterialShininessUniform = glGetUniformLocation(perVertexShaderProgramObject, "u_material_shininess");


            // per fragment Vertex Shader 

	perFragment_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *perFragment_VertexShaderSourceCode=
			"#version 300 es" \
			"\n" \
			"in vec4 vPosition;" \
            "in vec3 vNormal;" \
            "uniform mat4 u_model_matrix ;" \
            "uniform mat4 u_view_matrix ;" \
            "uniform mat4 u_projection_matrix ;" \
            "uniform vec4 u_light_position[3];"\
            "uniform int isDoubleTap;" \
            "out vec3 transformed_normals;" \
            "out vec3 light_direction[3];" \
            "out vec3 viewer_vector;" \
            "void main(void)" \
            "{" \
            "if (isDoubleTap == 1)"\
            "{"\
            "for(int i=0;i<3;i++)"\
            "{"\
            "vec4 eye_coordinates = u_view_matrix *u_model_matrix* vPosition;"\
            "transformed_normals = mat3 (u_view_matrix * u_model_matrix) * vNormal;"\
            "light_direction[i] = vec3 (u_light_position[i]) - eye_coordinates.xyz;"\
            "viewer_vector = -eye_coordinates.xyz;"\
            "}"\
            "}"\
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;" \
            "}";

	glShaderSource(perFragment_VertexShaderObject,1,(const GLchar **) &perFragment_VertexShaderSourceCode,NULL);

	glCompileShader(perFragment_VertexShaderObject);
	
    iInfoLogLength =0;
    iShaderCompiledStatus =0;
	*szInfoLog = NULL;

	glGetShaderiv(perFragment_VertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(perFragment_VertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength >0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(perFragment_VertexShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Fragment Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}
    

		// Fragment Shader

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	perFragment_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *perFragment_FragmentShaderSourceCode =
			"#version 300 es"\
			"\n" \
			"precision highp float;" \
            "in vec3 transformed_normals;" \
        	"in vec3 light_direction[3];" \
        	"in vec3 viewer_vector;" \
        	"out vec4 FragColor;" \
        	"uniform vec3 u_La[3];"\
        	"uniform vec3 u_Ld[3];"\
        	"uniform vec3 u_Ls[3];"\
        	"uniform vec3 u_Ka;"\
        	"uniform vec3 u_Kd;"\
        	"uniform vec3 u_Ks;"\
        	"uniform float u_material_shininess;"\
        	"uniform int isDoubleTap;"\
        	"void main(void)" \
        	"{" \
        	"vec3 phong_ads_color;"\
        	"if(isDoubleTap == 1)"\
        	"{"\
        	"vec3 ambient[3];"\
        	"vec3 diffuse[3];"\
        	"vec3 specular[3];"\
        	"for(int i=0;i<3;i++)"\
        	"{"\
        	"vec3 normalized_transformed_normals=normalize(transformed_normals);"\
        	"vec3 normalized_light_direction = normalize(light_direction[i]);"\
        	"vec3 normalized_viewer_vector = normalize(viewer_vector);"\
        	"ambient[i] = u_La[i] * u_Ka ;"\
        	"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"\
        	"diffuse[i] = u_Ld[i] * u_Kd * tn_dot_ld; "\
        	"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\
        	"specular[i] = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess); "\
        	"}"\
        	"phong_ads_color = ambient[0] +ambient[1] +ambient[2] + diffuse[0] + diffuse[1] + diffuse[2] + specular[0] + specular[1] + specular[2];  "\
        	"}"\
        	"else"\
        	"{"\
        	"phong_ads_color = vec3(1.0,1.0,1.0);"\
        	"}"\
        	"FragColor = vec4(phong_ads_color,1.0);"\
        	"}";

	glShaderSource(perFragment_FragmentShaderObject,1, (const GLchar **)&perFragment_FragmentShaderSourceCode,NULL);

	glCompileShader(perFragment_FragmentShaderObject);

	glGetShaderiv(perFragment_FragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(perFragment_FragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength >0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(perFragment_FragmentShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Fragment Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

    
	// Shader Program

    perFragmentShaderProgramObject = glCreateProgram();

	// Attach Shaders 

	glAttachShader (perFragmentShaderProgramObject,perFragment_VertexShaderObject);

	glAttachShader (perFragmentShaderProgramObject,perFragment_FragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute

	glBindAttribLocation(perFragmentShaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

	glBindAttribLocation(perFragmentShaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");

	// link Shader

	glLinkProgram(perFragmentShaderProgramObject);

	GLint iShaderProgramLinkStatus=0;

	glGetProgramiv(perFragmentShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);


		if(iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(perFragmentShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			
			if(iInfoLogLength >0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(perFragmentShaderProgramObject,iInfoLogLength,&written,szInfoLog);
					printf("Shader Program Link Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}


			// get uniform location 


	    perFragment_ModelMatrixUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_model_matrix");
        perFragment_ViewMatrixUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_view_matrix");
        perFragment_ProjectionMatrixUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_projection_matrix");
        perFragment_doubleTapUniform = glGetUniformLocation(perFragmentShaderProgramObject,"isDoubleTap");
        perFragment_LaUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_La");
        perFragment_LdUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_Ld");
        perFragment_LsUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_Ls");
        perFragment_LightPositionUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_light_position");
        perFragment_KaUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_Ka");
        perFragment_KdUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_Kd");
        perFragment_KsUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_Ks");
        perFragment_MaterialShininessUniform = glGetUniformLocation(perFragmentShaderProgramObject, "u_material_shininess");



	//Sphere sphere = new Sphere();
    float sphere_vertices[] = new float[1146];
    float sphere_normals[] = new float[1146];
    float sphere_textures[] = new float[764];
    short sphere_elements[] = new short[2280];

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

    numVertices = getNumberOfSphereVertices();
    numElements = getNumberOfSphereElements();


    // Sphere Vao

    glGenVertexArrays(1,&vao_Sphere);
    glBindVertexArray(vao_Sphere);

    // Sphere Position Vbo 

    glGenBuffers(1,& vbo_Sphere_Position);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Sphere_Position);
    glBufferData(GL_ARRAY_BUFFER,sphere_vertices.length * 4,sphere_vertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind sphere position Vbo
    

	// Sphere Normal Vbo 

    glGenBuffers(1,& vbo_Sphere_Normal);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Sphere_Normal);
    glBufferData(GL_ARRAY_BUFFER,sphere_normals.length * 4,sphere_normals,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind sphere normal Vbo

    
	// Element Vbo

	glGenBuffers(1,& vbo_Sphere_Elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_Sphere_Elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sphere_elements.length*2,sphere_elements,GL_STATIC_DRAW);


    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0); // unbind sphere element Vbo


    glBindVertexArray(0); // Unbind Square vao
			

        glEnable(GL_DEPTH_TEST);

		glDepthFunc(GL_LEQUAL);

		glEnable(GL_CULL_FACE);

		// clear color

		glClearColor(0.0f,0.0f,1.0f,1.0f);

		perspectiveProjectionMatrix = vmath::mat4::identity();

		// Gesture recognition

		// Tap gesture code

        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)]; // callback is given by @selector

		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];


		UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)]; // callback is given by @selector

		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];


		// To allow to differentiate between single tap and double tap

		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];

		
		// swipe Gesture

		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];

		[self addGestureRecognizer:swipeGestureRecognizer];

		//long press gesture

		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];

		[self addGestureRecognizer:longPressGestureRecognizer];

	}

	return(self);

}


+(Class)layerClass
{
	return([CAEAGLLayer class]);
}


-(void) drawView:(id) sender
{
	[EAGLContext setCurrentContext:eaglContext];

	glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);


    if (isPerVertexLighting == true)
    {

     glUseProgram(perVertexShaderProgramObject);

    if (doubleTap == 1)
    {
        glUniform1i(perVertex_doubleTapUniform, 1);

        lightPosition[0] = 0.0f ;
        lightPosition[1] = 30 * cos(angleRedLight);
        lightPosition[2] = 30 * sin(angleRedLight);
        lightPosition[3] = 0.0f;
        lightPosition[4] = 30* cos(-angleGreenLight);
        lightPosition[5] = 0.0f;
        lightPosition[6] = 30 * sin(-angleGreenLight);
        lightPosition[7] = 0.0f;
        lightPosition[8] = 30 * sin(-angleGreenLight);
        lightPosition[9] = 30 * cos(angleBlueLight);
        lightPosition[10] = 30 * sin(angleBlueLight);
        lightPosition[11] = 0.0f;

        // Light's properties

        glUniform3fv(perVertex_LaUniform,3,light_ambient);
        glUniform3fv(perVertex_LdUniform, 3,light_diffuse);
        glUniform3fv(perVertex_LsUniform, 3, light_specular);
        glUniform4fv(perVertex_LightPositionUniform, 3,light_position);
        

        // Material's properties
        glUniform3fv(perVertex_KaUniform, 1,material_ambient);
        glUniform3fv(perVertex_KdUniform,1,material_diffuse);
        glUniform3fv(perVertex_KsUniform, 1, material_specular);
        glUniform1f(perVertex_MaterialShininessUniform, material_shininess);
        
    }

    else
    {
        glUniform1i(perVertex_doubleTapUniform, 0);
    }

  
    // OpenGL drawing 
    // Set modelview and modelviewprojection matrices to identity

    vmath::mat4 modelMatrix= vmath::mat4:: identity();
    vmath::mat4 viewMatrix = vmath::mat4 :: identity();

    // Translate model view matrix.

    modelMatrix = vmath::translate(0.0f,0.0f,-3.0f);

    glUniformMatrix4fv(perVertex_ModelMatrixUniform, 1, GL_FALSE, modelMatrix);
    
    glUniformMatrix4fv(perVertex_ViewMatrixUniform, 1, GL_FALSE, viewMatrix);
    
    glUniformMatrix4fv(perVertex_ProjectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);    

    		// Sphere Code 

	// *** bind vao ***
	glBindVertexArray(vao_Sphere);
        
                // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Elements);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT,0);
        
                // *** unbind vao ***
    glBindVertexArray(0);
        

    glUseProgram(0);
}

else if (isPerFragmentLighting == true)
{

glUseProgram(perFragmentShaderProgramObject);

if (doubleTap == 1)
{
    glUniform1i(perFragment_doubleTapUniform, 1);

	    lightPosition[0] = 0.0f ;
        lightPosition[1] = 30 * cos(angleRedLight);
        lightPosition[2] = 30 * sin(angleRedLight);
        lightPosition[3] = 0.0f;
        lightPosition[4] = 30* cos(-angleGreenLight);
        lightPosition[5] = 0.0f;
        lightPosition[6] = 30 * sin(-angleGreenLight);
        lightPosition[7] = 0.0f;
        lightPosition[8] = 30 * sin(-angleGreenLight);
        lightPosition[9] = 30 * cos(angleBlueLight);
        lightPosition[10] = 30 * sin(angleBlueLight);
        lightPosition[11] = 0.0f;

    // Light's properties

    glUniform3fv(perFragment_LaUniform,3,light_ambient);
    glUniform3fv(perFragment_LdUniform, 3,light_diffuse);
    glUniform3fv(perFragment_LsUniform, 3, light_specular);
    glUniform4fv(perFragment_LightPositionUniform, 3,light_position);
    
    // Material's properties
    glUniform3fv(perFragment_KaUniform, 1,material_ambient);
    glUniform3fv(perFragment_KdUniform,1,material_diffuse);
    glUniform3fv(perFragment_KsUniform, 1, material_specular);
    glUniform1f(perFragment_MaterialShininessUniform, material_shininess);
    
}

else
{
    glUniform1i(perFragment_doubleTapUniform, 0);
}


// OpenGL drawing 
// Set modelview and modelviewprojection matrices to identity

vmath::mat4 modelMatrix= vmath::mat4:: identity();
vmath::mat4 viewMatrix = vmath::mat4 :: identity();

    // Translate model view matrix.

modelMatrix = vmath::translate(0.0f,0.0f,-3.0f);

glUniformMatrix4fv(perFragment_ModelMatrixUniform, 1, GL_FALSE, modelMatrix);

glUniformMatrix4fv(perFragment_ViewMatrixUniform, 1, GL_FALSE, viewMatrix);

glUniformMatrix4fv(perFragment_ProjectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);    

        // Sphere Code 

// *** bind vao ***
glBindVertexArray(vao_Sphere);
        
                // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Elements);
glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT,0);
    
            // *** unbind vao ***
glBindVertexArray(0);
    

glUseProgram(0);

}

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

	[eaglContext presentRenderbuffer:GL_RENDERBUFFER];

	[self updateAngle];

}

 -(void) updateAngle
 {
	angleRedLight = angleRedLight + 0.05f;
    if (angleRedLight >= 360.0f)
        angleRedLight = 0.0f;

    angleGreenLight = angleGreenLight + 0.05f;
    if (angleGreenLight >= 360.0f)
        angleGreenLight = 0.0f;

    angleBlueLight = angleBlueLight + 0.05f;
    if (angleBlueLight >= 360.0f)
        angleBlueLight = 0.0f;
 }

-(void)layoutSubviews
{
	GLint width;
	GLint height;

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

	[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);

	glGenRenderbuffers(1,&depthRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

	glViewport(0,0,width,height);

    perspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Failed to create complete frame buffer object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
	}

	[self drawView:nil];
}

-(void) startAnimation
{
	if(!isAnimating)
	{
		displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];

		[displayLink setPreferredFramesPerSecond : animationFrameInterval];

		[displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

		isAnimating = YES;
	}
}

-(void) stopAnimation
{
	if(isAnimating)
	{
		[displayLink invalidate];

		displayLink = nil;

		isAnimating = NO;
	}
}

-(BOOL) acceptsFirstResponder
{
	return(YES);
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
{

}

-(void) onSingleTap:(UITapGestureRecognizer *)gr
{
	if(isPerVertexLighting == false)
    {
        isPerVertexLighting = true;
        isPerFragmentLighting = false;
    }

	else
	{
		isPerVertexLighting = false;
        isPerFragmentLighting = true;
	}

//   if(isAnimating)
//     isAnimating = NO;
//   else
//     isAnimating = YES;	
}

-(void) onDoubleTap:(UITapGestureRecognizer *) gr
{
		doubleTap ++;
		if(doubleTap > 1)
		  doubleTap =0;

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
	[self release];

	exit(0);
}

-(void) onLongPress :(UILongPressGestureRecognizer *)gr
{

}

-(void) dealloc
{
 
    if(vao_Sphere)
	{
		glDeleteVertexArrays(1,&vao_Sphere);
		vao_Sphere =0;
	}

	if(vbo_Sphere_Position)
	{
		glDeleteBuffers(1,&vbo_Sphere_Position);
		vbo_Sphere_Position =0;
	}

	if(vbo_Sphere_Normal)
	{
		glDeleteBuffers(1,&vbo_Sphere_Normal);
		vbo_Sphere_Normal =0;
	}

	if(vbo_Sphere_Elements)
	{
		glDeleteBuffers(1,&vbo_Sphere_Elements);
		vbo_Sphere_Elements =0;
	}

	glDetachShader(perVertexShaderProgramObject,perVertex_VertexShaderObject);

	glDetachShader(perVertexShaderProgramObject,perVertex_FragmentShaderObject);

    glDetachShader(perFragmentShaderProgramObject,  perFragment_VertexShaderObject);

	glDetachShader(perFragmentShaderProgramObject ,perFragment_FragmentShaderObject);

	glDeleteShader(perVertex_VertexShaderObject);
	perVertex_VertexShaderObject=0;

	glDeleteShader(perVertex_FragmentShaderObject);
	perVertex_FragmentShaderObject =0;

    glDeleteShader(perFragment_VertexShaderObject);
	perFragment_VertexShaderObject=0;

	glDeleteShader(perFragment_FragmentShaderObject);
	perFragment_FragmentShaderObject =0;

	glDeleteProgram(perVertexShaderProgramObject);
	perVertexShaderProgramObject =0;

    glDeleteProgram(perFragmentShaderProgramObject);
	perFragmentShaderProgramObject =0;


	if(depthRenderbuffer)
	{
		glDeleteRenderbuffers(1,&depthRenderbuffer);
		depthRenderbuffer=0;
	}

	if(colorRenderbuffer)
	{
		glDeleteRenderbuffers(1,&colorRenderbuffer);
		colorRenderbuffer=0;
	}

	if(defaultFramebuffer)
	{
		glDeleteRenderbuffers(1,&defaultFramebuffer);
		defaultFramebuffer=0;
	}

	if([EAGLContext currentContext]==eaglContext)
	{
		[EAGLContext setCurrentContext:nil];
	}

	[eaglContext release];
	eaglContext = nil;

	[super dealloc];
}

@end