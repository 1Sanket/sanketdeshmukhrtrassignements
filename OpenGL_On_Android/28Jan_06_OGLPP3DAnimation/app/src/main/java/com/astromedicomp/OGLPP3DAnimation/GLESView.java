
package com.astromedicomp.OGLPP3DAnimation;

import android.content.Context;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;  // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;  // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES31;  // for OpenGLES 3.1
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

// for vbo
import java.nio.ByteBuffer;   // nio for non blocking I/O
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

// for matrix math
import android.opengl.Matrix;

// A view for OpenGLES3 graphics which also receives touch events.
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vao_pyramid = new int[1];
	private int[] vao_cube = new int[1];
	private int[] vbo_pyramid_position = new int [1];
	private int[] vbo_pyramid_color = new int[1];
	private int[] vbo_cube_position= new int[1];
	private int[] vbo_cube_color = new int[1];
	private int mvpUniform;

	private float perspectiveProjectionMatrix[] = new float[16]; //4X4 matrix. 
	private float anglePyramid=0.0f;
	private float angleCube =0.0f;

	public GLESView (Context drawingContext)
	{
	   super(drawingContext);

	   context = drawingContext;

	   // Set EGLContext to current supported version of OpenGL-ES

	    setEGLContextClientVersion(3);

	   // Set Renderer for drawing on the GLSurfaceView

	   setRenderer(this);

	   // Render the view only when there is a change in the drawing data.

	   setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	   gestureDetector = new GestureDetector(context,this,null,false);

	   gestureDetector.setOnDoubleTapListener(this);

	}

	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
	  // OpenGL-ES version check.
      String glesVersion =gl.glGetString(GL10.GL_VERSION);
      System.out.println("VDG: OpenGL-ES Version = " + glesVersion);
      
	  //GLSL version
	  String glslVersion=gl.glGetString(GLES31.GL_SHADING_LANGUAGE_VERSION);
	  System.out.println("VDG: GLSL Version = " +glslVersion);

	  initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
	   draw();
	} 

	// Handling 'onTouchEvent' is most important because it triggers all gesture and tap events.
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
	   int eventAction = e.getAction();
	   
	   if(!gestureDetector.onTouchEvent(e))
	   {
	     super.onTouchEvent(e);
	   } 

	   return(true);
	}

	@Override 
	public boolean onDoubleTap(MotionEvent e)
	{
        return(true);
	}

	@Override 
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	@Override 
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	   return(true);
	}

	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
	   
	}

	@Override 
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override 
	public void onShowPress(MotionEvent e)
	{
	  
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// Set the background frame color.
		
        System.out.println("VDG :"+"In initialize");

		// ******************************************************
		// Vertex Shader
		// ******************************************************

		// Create Shader

		vertexShaderObject= GLES31.glCreateShader(GLES31.GL_VERTEX_SHADER);

		// vertex shader source code

		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;"+
			"in vec4 vColor;" +
			"out vec4 outColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			"outColor=vColor;"+
			"}"
		);


		// provide source code to shader
		GLES31.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// compile shader and do error checking

	   GLES31.glCompileShader(vertexShaderObject);
	   int[] iShaderCompiledStatus = new int[1];
	   int[]iInfoLogLength= new int[1];
	   String szInfoLog=null;

	   GLES31.glGetShaderiv(vertexShaderObject,GLES31.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

	   if(iShaderCompiledStatus[0]==GLES31.GL_FALSE)
	   {
	      GLES31.glGetShaderiv(vertexShaderObject,GLES31.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES31.glGetShaderInfoLog(vertexShaderObject);
			System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }

	   // ********************************
	   // Fragment Shader
	   // ********************************

	   // Create Shader

		fragmentShaderObject= GLES31.glCreateShader(GLES31.GL_FRAGMENT_SHADER);

		// fragment shader source code

		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;"+
			"in vec4 outColor;" +
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = outColor;"+
			"}"
		);

		
		// provide source code to shader
		GLES31.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		// compile shader and do error checking

	   GLES31.glCompileShader(fragmentShaderObject);
	   iShaderCompiledStatus[0] = 0;
	   iInfoLogLength[0]= 0;
	   szInfoLog=null;

	   GLES31.glGetShaderiv(fragmentShaderObject,GLES31.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

	   if(iShaderCompiledStatus[0]==GLES31.GL_FALSE)
	   {
	      GLES31.glGetShaderiv(fragmentShaderObject,GLES31.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES31.glGetShaderInfoLog(fragmentShaderObject);
			System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }


	   // Create Shader Program object

	   shaderProgramObject= GLES31.glCreateProgram();

	   // Attach vertex shader to shader Program

	   GLES31.glAttachShader(shaderProgramObject,vertexShaderObject);

	   // Attach fragment shader to shader Program

	   GLES31.glAttachShader(shaderProgramObject,fragmentShaderObject);

	   // pre-link binding of shader program object with vertex shader attributes

	   GLES31.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	   
	   // Color 
	   
	   GLES31.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_COLOR, "vColor");

	   // link the two shaders together to shader Program object.

	   GLES31.glLinkProgram(shaderProgramObject);

	   int[] iShaderProgramLinkStatus = new int[1];

	   iInfoLogLength[0]= 0;
	   szInfoLog=null;

	   GLES31.glGetProgramiv(shaderProgramObject,GLES31.GL_LINK_STATUS,iShaderProgramLinkStatus,0);

	   if(iShaderProgramLinkStatus[0]==GLES31.GL_FALSE)
	   {
	      GLES31.glGetProgramiv(shaderProgramObject,GLES31.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES31.glGetProgramInfoLog(shaderProgramObject);
			System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }


	   // get MVP uniform location

	   mvpUniform= GLES31.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

	   // *** vertices , colors , shader attribs , vbo ,vao initializations *** //

	   
	   final float pyramidVertices[] = new float[]
	   {
	   	   		// front face

			0.0f, 1.0f, 0.0f,   // Apex

			-1.0f, -1.0f, 1.0f, // left-corner of front face

			1.0f, -1.0f, 1.0f,   //right corner of front face

				//right face

			0.0f, 1.0f, 0.0f,     // apex

			1.0f, -1.0f, 1.0f,    //left corner of the right face

			1.0f, -1.0f, -1.0f,  // right corner of the right face

				//Back face

			0.0f, 1.0f, 0.0f, // apex

			1.0f, -1.0f, -1.0f, //left corner of the back face

			-1.0f, -1.0f, -1.0f, //right corner of the back face

				//left face

			0.0f, 1.0f, 0.0f, // apex

			-1.0f, -1.0f, -1.0f, //left corner of the back face

			-1.0f, -1.0f, 1.0f, //right corner of the back face

		};
	   
	   // Vao Pyramid

	   GLES31.glGenVertexArrays(1,vao_pyramid,0);
	   GLES31.glBindVertexArray(vao_pyramid[0]);

	   // vbo position
	   
	   GLES31.glGenBuffers(1,vbo_pyramid_position,0);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_pyramid_position[0]);

	   ByteBuffer byteBuffer_pyaramidVertices=ByteBuffer.allocateDirect(pyramidVertices.length * 4);
	   byteBuffer_pyaramidVertices.order(ByteOrder.nativeOrder());
	   FloatBuffer pyaramidVerticesBuffer = byteBuffer_pyaramidVertices.asFloatBuffer();
	   pyaramidVerticesBuffer.put(pyramidVertices);
	   pyaramidVerticesBuffer.position(0);

	   GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,pyramidVertices.length * 4,pyaramidVerticesBuffer,GLES31.GL_STATIC_DRAW);
	   GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES31.GL_FLOAT,false,0,0);

	   GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);   // unbind Vbo position
	   
	   // color values.
	   
	   final float pyramidColorCoordinates[] = new float[]
	   {
	   	   // front face
	
		1.0f, 0.0f, 0.0f, // red
	
			// Apex

		0.0f, 1.0f, 0.0f,    // green
	
			// left-corner of front face

		0.0f, 0.0f, 1.0f,    // blue
	
			//right corner of front face

			//right face

		1.0f, 0.0f, 0.0f, // red
			// apex

		0.0f, 0.0f, 1.0f,  // blue

			//left corner of the right face

		0.0f, 1.0f, 0.0f,  //green
	
			// right corner of the right face

			//Back face

		1.0f, 0.0f, 0.0f, //red
	
			// apex

		0.0f, 1.0f, 0.0f,// green
	
			//left corner of the back face

		0.0f, 0.0f, 1.0f, // blue
	
			//right corner of the back face

			//left face
	 
		1.0f, 0.0f, 0.0f,//red
	   
			// apex

		0.0f, 0.0f, 1.0f,  // blue
   
			//left corner of the back face

		0.0f, 1.0f, 0.0f  // green


	  };
	   
	   // Color Vbo
	   
	   GLES31.glGenBuffers(1,vbo_pyramid_color,0);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_pyramid_color[0]);

	   ByteBuffer byteBuffer_pyramidColor=ByteBuffer.allocateDirect(pyramidColorCoordinates.length * 4);
	   byteBuffer_pyramidColor.order(ByteOrder.nativeOrder());
	   FloatBuffer pyramidColorBuffer = byteBuffer_pyramidColor.asFloatBuffer();
	   pyramidColorBuffer.put(pyramidColorCoordinates);
	   pyramidColorBuffer.position(0);

	   GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,pyramidColorCoordinates.length * 4,pyramidColorBuffer,GLES31.GL_STATIC_DRAW);
	   GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_COLOR,3,GLES31.GL_FLOAT,false,0,0);

	   GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_COLOR);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);  // Unbind color Vbo
	   
	   GLES31.glBindVertexArray(0);   // unbind triangle Vao
	   
	   
	   final float cubeVertices[] = new float[]
	   {
	   	    // Top face

	1.0f, 1.0f, -1.0f,   // Right-top corner of top face 
	-1.0f, 1.0f, -1.0f, // left-top corner of the top face
	-1.0f, 1.0f, 1.0f,   //left bottom corner of front face
	1.0f, 1.0f, 1.0f, //right bottom corner of front face

        //Bottom face

	1.0f, -1.0f, -1.0f,   // Right-top corner of bottom face 
	-1.0f, -1.0f, -1.0f, // left-top corner of the bottom face
	-1.0f, -1.0f, 1.0f,   //left bottom corner of bottom face
	1.0f, -1.0f, 1.0f, //right bottom corner of bottom face

		//Front face

	1.0f, 1.0f, 1.0f,   // Right-top corner of front face 
	-1.0f, 1.0f, 1.0f, // left-top corner of the front face
	-1.0f, -1.0f, 1.0f,   //left bottom corner of front face
	1.0f, -1.0f, 1.0f, //right bottom corner of front face


		//Back face
	
	1.0f, 1.0f, -1.0f,  // Right-top corner of back face 
	-1.0f, 1.0f, -1.0f, // left-top corner of the back face
	-1.0f, -1.0f, -1.0f,   //left bottom corner of back face
	1.0f, -1.0f, -1.0f, //right bottom corner of back face

		//Right face
	
	1.0f, 1.0f, -1.0f,   // Right-top corner of right face 
	1.0f, 1.0f, 1.0f, // left-top corner of the right face
	1.0f, -1.0f, 1.0f,   //left bottom corner of right face
	1.0f, -1.0f, -1.0f, //right bottom corner of right face

		//Left face

	-1.0f, 1.0f, 1.0f,   // Right-top corner of left face 
	-1.0f, 1.0f, -1.0f, // left-top corner of the left face
	-1.0f, -1.0f, -1.0f,   //left bottom corner of left face
	-1.0f, -1.0f, 1.0f //right bottom corner of left face

	};

		
	final float cubeColorCoordinates[] =
	{
		// Top face
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red

	     //Bottom face
	0.0f, 1.0f, 0.0f, // Green
	0.0f, 1.0f, 0.0f, // Green
	0.0f, 1.0f, 0.0f, // Green
	0.0f, 1.0f, 0.0f, // Green

		//Front face
	0.0f, 0.0f, 1.0f, // Blue
	0.0f, 0.0f, 1.0f, // Blue
	0.0f, 0.0f, 1.0f, // Blue
	0.0f, 0.0f, 1.0f, // Blue

		//Back face
	0.0f, 1.0f, 1.0f, // Cyan
	0.0f, 1.0f, 1.0f, // Cyan
	0.0f, 1.0f, 1.0f, // Cyan
	0.0f, 1.0f, 1.0f, // Cyan

		//Right face
	1.0f, 0.0f, 1.0f, // Magenta
	1.0f, 0.0f, 1.0f, // Magenta
	1.0f, 0.0f, 1.0f, // Magenta
	1.0f, 0.0f, 1.0f, // Magenta

		//Left face
	1.0f, 1.0f, 0.0f, // Yellow
	1.0f, 1.0f, 0.0f,// Yellow
	1.0f, 1.0f, 0.0f, // Yellow
	1.0f, 1.0f, 0.0f // Yellow

  };
	   
	   // Vao square

	   GLES31.glGenVertexArrays(1,vao_cube,0);
	   GLES31.glBindVertexArray(vao_cube[0]);

	   // Position Vbo
	   
	   GLES31.glGenBuffers(1,vbo_cube_position,0);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_cube_position[0]);

	   ByteBuffer byteBuffer_cubeVertices=ByteBuffer.allocateDirect(cubeVertices.length * 4);
	   byteBuffer_cubeVertices.order(ByteOrder.nativeOrder());
	   FloatBuffer cubeVerticesBuffer = byteBuffer_cubeVertices.asFloatBuffer();
	   cubeVerticesBuffer.put(cubeVertices);
	   cubeVerticesBuffer.position(0);

	   GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,cubeVertices.length * 4,cubeVerticesBuffer,GLES31.GL_STATIC_DRAW);
	   GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES31.GL_FLOAT,false,0,0);

	   GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);
	   
	   // Color Vbo
	   
	   GLES31.glGenBuffers(1,vbo_cube_color,0);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_cube_color[0]);

	   ByteBuffer byteBuffer_cubeColor=ByteBuffer.allocateDirect(cubeColorCoordinates.length * 4);
	   byteBuffer_cubeColor.order(ByteOrder.nativeOrder());
	   FloatBuffer cubeColorBuffer = byteBuffer_cubeColor.asFloatBuffer();
	   cubeColorBuffer.put(cubeColorCoordinates);
	   cubeColorBuffer.position(0);

	   GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,cubeColorCoordinates.length * 4,cubeColorBuffer,GLES31.GL_STATIC_DRAW);
	   GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_COLOR,3,GLES31.GL_FLOAT,false,0,0);

	   GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_COLOR);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);  // Unbind color Vbo
	   
	   GLES31.glBindVertexArray(0); // Unbind Cube Vao

	   // Enable depth testing
	   GLES31.glEnable(GLES31.GL_DEPTH_TEST);

	   GLES31.glDepthFunc(GLES31.GL_LEQUAL);

	   //GLES31.glEnable(GLES31.GL_CULL_FACE);

	   GLES31.glClearColor(0.0f,0.0f,0.0f,0.0f);

	   Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width,int height)
	{
		GLES31.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,((float)width/(float)height),0.1f,100.0f);
	}

	public void draw()
	{
		GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT|GLES31.GL_DEPTH_BUFFER_BIT);

		GLES31.glUseProgram(shaderProgramObject);

		
		float modelViewProjectionMatrix[]= new float[16];
		float modelViewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];

		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		// translate modelViewMatrix

		Matrix.translateM(modelViewMatrix,0,-1.5f,0.0f,-6.0f);
		
		// specify the rotationMatrix
		
		Matrix.rotateM(rotationMatrix,0,anglePyramid,0.0f,1.0f,0.0f);
		
		
		// Multiply the modelview and rotationMatrix

		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		
		// Multiply the modelview and projection matrix to get modelViewProjectionMatrix

		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
	

		GLES31.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		GLES31.glBindVertexArray(vao_pyramid[0]);

		GLES31.glDrawArrays(GLES31.GL_TRIANGLES,0,12);
	
		GLES31.glBindVertexArray(0);
		
		
		// Draw Cube
		
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);
		
		Matrix.translateM(modelViewMatrix,0,1.5f,0.0f,-6.0f);
		
		// Scale 
		
		Matrix.scaleM(modelViewMatrix,0,0.75f,0.75f,0.75f);
		
		// specify the rotationMatrix
		
		
		Matrix.rotateM(rotationMatrix,0,angleCube,1.0f,0.0f,0.0f);
		Matrix.rotateM(rotationMatrix,0,angleCube,0.0f,1.0f,0.0f);
		Matrix.rotateM(rotationMatrix,0,angleCube,0.0f,0.0f,1.0f);
		//Matrix.rotateM(rotationMatrix,0,modelViewMatrix,0,angleCube,1.0f,1.0f,1.0f);
		
		// Multiply the modelview and rotationMatrix

		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		// Multiply the modelview and projection matrix to get modelViewProjectionMatrix

		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES31.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		GLES31.glBindVertexArray(vao_cube[0]);

		GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,0,4);
		GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,4,4);
		GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,8,4);
		GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,12,4);
		GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,16,4);
		GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,20,4);
	
		GLES31.glBindVertexArray(0);
		
		GLES31.glUseProgram(0);

		updateAngle();
		
		requestRender();

	}
	
	void updateAngle()
	{
		anglePyramid = anglePyramid + 0.7f;
		angleCube = angleCube - 0.7f;
		if (anglePyramid >= 360.0f)
			anglePyramid = 0.0f;
		if (angleCube <= 0.0f)
			angleCube = 360.0f;
	}


	void uninitialize()
	{
		
		if(vao_pyramid[0]!=0)
		{
		  GLES31.glDeleteVertexArrays(1,vao_pyramid,0);
		  vao_pyramid[0]=0;
		}
		
		if(vao_cube[0]!=0)
		{
		  GLES31.glDeleteVertexArrays(1,vao_cube,0);
		  vao_cube[0]=0;
		}

		if(vbo_pyramid_position[0]!=0)
		{
			GLES31.glDeleteBuffers(1,vbo_pyramid_position,0);
			vbo_pyramid_position[0]=0;
		}
		
		if(vbo_pyramid_color[0]!=0)
		{
			GLES31.glDeleteBuffers(1,vbo_pyramid_color,0);
			vbo_pyramid_color[0]=0;
		}
		
		if(vbo_cube_position[0]!=0)
		{
			GLES31.glDeleteBuffers(1,vbo_cube_position,0);
			vbo_cube_position[0]=0;
		}
		
		if(vbo_cube_color[0]!=0)
		{
			GLES31.glDeleteBuffers(1,vbo_cube_color,0);
			vbo_cube_color[0]=0;
		}

		if(shaderProgramObject!=0)
		{
		   if(vertexShaderObject !=0)
		   {
		     GLES31.glDetachShader(shaderProgramObject,vertexShaderObject);
			 GLES31.glDeleteShader(vertexShaderObject);
			 vertexShaderObject=0;
		   }

		   if(fragmentShaderObject !=0)
		   {
		      GLES31.glDetachShader(shaderProgramObject,fragmentShaderObject);
			  GLES31.glDeleteShader(fragmentShaderObject);
			  fragmentShaderObject=0;
		   }
		}

		if(shaderProgramObject!=0)
		{
		   GLES31.glDeleteProgram(shaderProgramObject);
		   shaderProgramObject=0;
		}
	}

}