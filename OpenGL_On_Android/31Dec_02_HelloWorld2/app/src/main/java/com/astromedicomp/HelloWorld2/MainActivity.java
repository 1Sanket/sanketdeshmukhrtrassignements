package com.astromedicomp.HelloWorld2;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;

public class MainActivity extends Activity {

    private MyView myView; 

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
		super.onCreate(savedInstanceState);
		
		// To remove the ActionBar.
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// To make fullscreen.
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // To set orientation to Landscape.		
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		// To set the background color.
		this.getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));
		
		myView = new MyView(this);
		setContentView(myView);
		
        //setContentView(R.layout.activity_main);
    }
}
