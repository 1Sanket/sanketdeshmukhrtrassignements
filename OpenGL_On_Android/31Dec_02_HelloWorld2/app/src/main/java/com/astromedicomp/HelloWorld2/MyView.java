package com.astromedicomp.HelloWorld2;

import android.content.Context;
import android.graphics.Color;
import android.widget.TextView; 
import android.view.Gravity;

public class MyView extends TextView
{
	MyView (Context context)
	{
		super(context);
		
		setTextColor(Color.rgb(0,255,0));
		setText("Hello World !!!!!");
		setTextSize(60);
		setGravity(Gravity.CENTER);
	}
}
