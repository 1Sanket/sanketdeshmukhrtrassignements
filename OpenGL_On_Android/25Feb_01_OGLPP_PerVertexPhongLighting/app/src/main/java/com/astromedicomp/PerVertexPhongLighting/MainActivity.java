
package com.astromedicomp.PerVertexPhongLighting;

// default supplied packages by android SDK
import android.app.Activity;
import android.os.Bundle;

// Later added packages
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;

public class MainActivity extends Activity 

{
    
	private GLESView glesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        
		// To remove the Action Bar.

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// To make fullscreen.

		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// To force activity window orientation to landscape.

		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		glesView = new GLESView(this);

		// Set view as content view of the activity.

		setContentView(glesView);

    }

    @Override 
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}
}

