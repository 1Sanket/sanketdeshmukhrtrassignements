
package com.astromedicomp.OGLPPCheckerboard;

import android.content.Context;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;  // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;  // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES31;  // for OpenGLES 3.1
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

// for vbo
import java.nio.ByteBuffer;   // nio for non blocking I/O
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.Buffer;

// for matrix math
import android.opengl.Matrix;

import android.graphics.BitmapFactory;  // texture factory
import android.graphics.Bitmap;     // for PNG image
import android.opengl.GLUtils; // for texImage2D()

// A view for OpenGLES3 graphics which also receives touch events.
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vao_Checkerboard = new int[1];
	private int[] vbo_Checkerboard_position = new int [1];
	private int[] vbo_Checkerboard_texture = new int[1];
	
	private int mvpUniform;
	private int texture0_sampler_uniform;

	private int[] texture_Checkerboard = new int[1];

	private byte checkImage[]= new byte[16384] ;  //64 * 64 * 4

	private float perspectiveProjectionMatrix[] = new float[16]; //4X4 matrix.

	public GLESView (Context drawingContext)
	{
	   super(drawingContext);

	   context = drawingContext;

	   // Set EGLContext to current supported version of OpenGL-ES

	    setEGLContextClientVersion(3);

	   // Set Renderer for drawing on the GLSurfaceView

	   setRenderer(this);

	   // Render the view only when there is a change in the drawing data.

	   setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	   gestureDetector = new GestureDetector(context,this,null,false);

	   gestureDetector.setOnDoubleTapListener(this);

	}

	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
	  // OpenGL-ES version check.
      String glesVersion =gl.glGetString(GL10.GL_VERSION);
      System.out.println("VDG: OpenGL-ES Version = " + glesVersion);
      
	  //GLSL version
	  String glslVersion=gl.glGetString(GLES31.GL_SHADING_LANGUAGE_VERSION);
	  System.out.println("VDG: GLSL Version = " +glslVersion);

	  initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
	   draw();
	} 

	// Handling 'onTouchEvent' is most important because it triggers all gesture and tap events.
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
	   int eventAction = e.getAction();
	   
	   if(!gestureDetector.onTouchEvent(e))
	   {
	     super.onTouchEvent(e);
	   } 

	   return(true);
	}

	@Override 
	public boolean onDoubleTap(MotionEvent e)
	{
        return(true);
	}

	@Override 
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	@Override 
	public boolean onSingleTapConfirmed(MotionEvent e)
	{  
		  return(true);
	}

	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
	   
	}

	@Override 
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override 
	public void onShowPress(MotionEvent e)
	{
	  
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// Set the background frame color.
		
        System.out.println("VDG :"+"In initialize");

		// ******************************************************
		// Vertex Shader
		// ******************************************************

		// Create Shader

		vertexShaderObject= GLES31.glCreateShader(GLES31.GL_VERTEX_SHADER);

		// vertex shader source code

		final String vertexShaderSourceCode = String.format
		(
			"#version 310 es" +
			"\n" +
			"in vec4 vPosition;"+
			"in vec2 vTexture0_Coord;" +
			"out vec2 out_texture0_coord;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			 "out_texture0_coord= vTexture0_Coord;"+
			"}"
		);


		// provide source code to shader
		GLES31.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// compile shader and do error checking

	   GLES31.glCompileShader(vertexShaderObject);
	   int[] iShaderCompiledStatus = new int[1];
	   int[]iInfoLogLength= new int[1];
	   String szInfoLog=null;

	   GLES31.glGetShaderiv(vertexShaderObject,GLES31.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

	   if(iShaderCompiledStatus[0]==GLES31.GL_FALSE)
	   {
	      GLES31.glGetShaderiv(vertexShaderObject,GLES31.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES31.glGetShaderInfoLog(vertexShaderObject);
			System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }

	   // ********************************
	   // Fragment Shader
	   // ********************************

	   // Create Shader

		fragmentShaderObject= GLES31.glCreateShader(GLES31.GL_FRAGMENT_SHADER);

		// fragment shader source code

		final String fragmentShaderSourceCode = String.format
		(
			"#version 310 es" +
			"\n" +
			"precision highp float;"+
			"in vec2 out_texture0_coord;" +
			"uniform highp sampler2D u_texture0_sampler;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = texture(u_texture0_sampler, out_texture0_coord);"+
			"}"
		);

		
		// provide source code to shader
		GLES31.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		// compile shader and do error checking

	   GLES31.glCompileShader(fragmentShaderObject);
	   iShaderCompiledStatus[0] = 0;
	   iInfoLogLength[0]= 0;
	   szInfoLog=null;

	   GLES31.glGetShaderiv(fragmentShaderObject,GLES31.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

	   if(iShaderCompiledStatus[0]==GLES31.GL_FALSE)
	   {
	      GLES31.glGetShaderiv(fragmentShaderObject,GLES31.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES31.glGetShaderInfoLog(fragmentShaderObject);
			System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }


	   // Create Shader Program object

	   shaderProgramObject= GLES31.glCreateProgram();

	   // Attach vertex shader to shader Program

	   GLES31.glAttachShader(shaderProgramObject,vertexShaderObject);

	   // Attach fragment shader to shader Program

	   GLES31.glAttachShader(shaderProgramObject,fragmentShaderObject);

	   // pre-link binding of shader program object with vertex shader attributes

	   GLES31.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	   
	   // Color 
	   
	   GLES31.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	   // link the two shaders together to shader Program object.

	   GLES31.glLinkProgram(shaderProgramObject);

	   int[] iShaderProgramLinkStatus = new int[1];

	   iInfoLogLength[0]= 0;
	   szInfoLog=null;

	   GLES31.glGetProgramiv(shaderProgramObject,GLES31.GL_LINK_STATUS,iShaderProgramLinkStatus,0);

	   if(iShaderProgramLinkStatus[0]==GLES31.GL_FALSE)
	   {
	      GLES31.glGetProgramiv(shaderProgramObject,GLES31.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES31.glGetProgramInfoLog(shaderProgramObject);
			System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }


	   // get MVP uniform location

	   mvpUniform= GLES31.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

	   // get texture sampler uniform location

	   texture0_sampler_uniform = GLES31.glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");

       // load Textures

	   texture_Checkerboard[0]= loadGLTexture();

	
	   // Vao Checkerboard

	   GLES31.glGenVertexArrays(1,vao_Checkerboard,0);
	   GLES31.glBindVertexArray(vao_Checkerboard[0]);

	   // vbo position
	   
	   GLES31.glGenBuffers(1,vbo_Checkerboard_position,0);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_Checkerboard_position[0]);

	   GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,3 * 8 * 4,null,GLES31.GL_DYNAMIC_DRAW);
	   GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES31.GL_FLOAT,false,0,0);

	   GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);   // unbind Vbo position
	   
	   // Texture Vbo
	   
	   GLES31.glGenBuffers(1,vbo_Checkerboard_texture,0);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_Checkerboard_texture[0]);

	   GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,2 * 8 * 4,null,GLES31.GL_DYNAMIC_DRAW);

	   GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0,2,GLES31.GL_FLOAT,false,0,0);

	   GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);
	   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);  // Unbind color Vbo
	   
	   GLES31.glBindVertexArray(0);   // unbind triangle Vao
	   
	   // Enable depth testing
	   GLES31.glEnable(GLES31.GL_DEPTH_TEST);

	   GLES31.glDepthFunc(GLES31.GL_LEQUAL);

	   GLES31.glEnable(GLES31.GL_CULL_FACE);

	   GLES31.glClearColor(0.0f,0.0f,0.0f,0.0f);

	   Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width,int height)
	{
		GLES31.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,((float)width/(float)height),0.1f,100.0f);
	}

	private int loadGLTexture()
	{

	   int i,j;
	   float c;
	   int k=0;
	   boolean b;

		for (i=0;i<64;i++)
			{
			for (j=0;j<64;j++)
				{
					b = (((i & 0x8)==0) ^ ((j & 0x8)==0)) ;

					if(b == true)
                    {
					   c= 1 * 255;
					}
					else
					{
					   c= 0 * 255;
					}
									
					checkImage [k] = (byte) c;
					checkImage [k + 1]=(byte)c;
					checkImage [k + 2] =(byte)c;
					checkImage [k + 3] =(byte)255;
                     
					k =k +4; // because we are filling 4 values to array in inner for loop
				}
			}

        System.out.println("VDG : One dimensional array values");

		for (int f = 0; f<k ;f++)
		{
		  System.out.println("VDG :" + checkImage [f]);
		}


	   ByteBuffer byteBuffer=ByteBuffer.allocateDirect(checkImage.length * 4);
	   byteBuffer.order(ByteOrder.nativeOrder());
	   byteBuffer.put(checkImage);     // fill the byteBuffer with the checkImage
	   byteBuffer.position(0);
	   
	   int[] texture = new int[1] ;

	  //create texture object to apply to model

	  GLES31.glGenTextures(1,texture,0);

	  // indicate that pixel rows are tightly packed (defaults to stride of 4 which is kind of only good for RGBA or float data types)

	  GLES31.glPixelStorei(GLES31.GL_UNPACK_ALIGNMENT,1);

	  // Bind with the texture


	  GLES31.glBindTexture(GLES31.GL_TEXTURE_2D,texture[0]);

      // set up filter and wrap modes for this texture object

	  GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D,GLES31.GL_TEXTURE_MAG_FILTER,GLES31.GL_LINEAR);

	  GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D,GLES31.GL_TEXTURE_MIN_FILTER,GLES31.GL_LINEAR_MIPMAP_LINEAR);

	  // load the bitmap into the bound texture

	  //GLUtils.texImage2D(GLES31.GL_TEXTURE_2D,0,bitmap,0);

	  GLES31.glTexImage2D(GLES31.GL_TEXTURE_2D,0,GLES31.GL_RGBA,64,64,0,GLES31.GL_RGBA,GLES31.GL_UNSIGNED_BYTE,byteBuffer);

	  // generate mipmap

	  GLES31.glGenerateMipmap(GLES31.GL_TEXTURE_2D);

	  return(texture[0]);
	}

	
	public void draw()
	{

     final float squareVertices[] =
		{

            0.0f, 1.0f, 0.0f,
    
            -2.0f, 1.0f, 0.0f,
    
            -2.0f, -1.0f, 0.0f,
    
            0.0f, -1.0f, 0.0f,
    
    
            2.414210f, 1.0f, -1.41421f,
    
            1.0f, 1.0f, 0.0f,
    
            1.0f, -1.0f, 0.0f,
    
            2.41421f, -1.0f, -1.41421f

    };


    final float squareTexcoords[] =
    {

        0.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,
		1.0f,0.0f,

		////// for tilted square
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,
		1.0f,0.0f
    
    };

		GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT|GLES31.GL_DEPTH_BUFFER_BIT);

		GLES31.glUseProgram(shaderProgramObject);

		
		float modelViewProjectionMatrix[]= new float[16];
		float modelViewMatrix[] = new float[16];

		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		// translate modelViewMatrix

		Matrix.translateM(modelViewMatrix,0,0.0f,0.0f,-5.0f);
		
		// Multiply the modelview and projection matrix to get modelViewProjectionMatrix

		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
	

		GLES31.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		GLES31.glBindVertexArray(vao_Checkerboard[0]);

		// Bind with Pyramid texture

		GLES31.glActiveTexture(GLES31.GL_TEXTURE0);

		
		GLES31.glBindTexture(GLES31.GL_TEXTURE_2D,texture_Checkerboard[0]);

		// Enable 0th sampler

		GLES31.glUniform1i(texture0_sampler_uniform,0);

		// Texture buffer

		GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_Checkerboard_texture[0]);

		ByteBuffer byteBuffer=ByteBuffer.allocateDirect(squareTexcoords.length * 4);
	    byteBuffer.order(ByteOrder.nativeOrder());
	    FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
	    floatBuffer.put(squareTexcoords);
	    floatBuffer.position(0);
	     
	    GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,squareTexcoords.length * 4,floatBuffer,GLES31.GL_DYNAMIC_DRAW);

		GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);

		// position buffer

		GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_Checkerboard_position[0]);

	    byteBuffer=ByteBuffer.allocateDirect(squareVertices.length * 4);
	    byteBuffer.order(ByteOrder.nativeOrder());
	    floatBuffer = byteBuffer.asFloatBuffer();
	    floatBuffer.put(squareVertices);
	    floatBuffer.position(0);
	     
	    GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,squareVertices.length * 4,floatBuffer,GLES31.GL_DYNAMIC_DRAW);

		GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);

		GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,0,4);
		GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,4,4); 
	
		GLES31.glBindVertexArray(0);

		GLES31.glUseProgram(0);
		
		requestRender();

	}


	void uninitialize()
	{
		
		if(vao_Checkerboard[0]!=0)
		{
		  GLES31.glDeleteVertexArrays(1,vao_Checkerboard,0);
		  vao_Checkerboard[0]=0;
		}
		

		if(vbo_Checkerboard_position[0]!=0)
		{
			GLES31.glDeleteBuffers(1,vbo_Checkerboard_position,0);
			vbo_Checkerboard_position[0]=0;
		}
		
		if(vbo_Checkerboard_texture[0]!=0)
		{
			GLES31.glDeleteBuffers(1,vbo_Checkerboard_texture,0);
			vbo_Checkerboard_texture[0]=0;
		}

		if(shaderProgramObject!=0)
		{
		   if(vertexShaderObject !=0)
		   {
		     GLES31.glDetachShader(shaderProgramObject,vertexShaderObject);
			 GLES31.glDeleteShader(vertexShaderObject);
			 vertexShaderObject=0;
		   }

		   if(fragmentShaderObject !=0)
		   {
		      GLES31.glDetachShader(shaderProgramObject,fragmentShaderObject);
			  GLES31.glDeleteShader(fragmentShaderObject);
			  fragmentShaderObject=0;
		   }
		}

		if(shaderProgramObject!=0)
		{
		   GLES31.glDeleteProgram(shaderProgramObject);
		   shaderProgramObject=0;
		}
	}

}