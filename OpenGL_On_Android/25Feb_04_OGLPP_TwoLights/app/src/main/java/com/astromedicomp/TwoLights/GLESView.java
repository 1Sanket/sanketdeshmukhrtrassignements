
package com.astromedicomp.TwoLights;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;  // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;  // for EGLConfig needed as param type EGLConfig

import android.opengl.GLES31;  // for OpenGLES 3.1
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

// for vbo
import java.nio.ByteBuffer;   // nio for non blocking I/O
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

// for matrix math
import android.opengl.Matrix;

// A view for OpenGLES3 graphics which also receives touch events.
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {
    private final Context context;

    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_pyramid = new int[1];
    private int[] vbo_pyramid_position = new int [1];
    private int[] vbo_pyramid_normal = new int [1];

    private float light0_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
    private float light0_diffuse[] = {1.0f, 0.0f, 0.0f, 1.0f};
    private float light0_specular[] = {1.0f, 0.0f, 0.0f, 1.0f};
    private float light0_position[] = {-100.0f, 100.0f, 100.0f, 1.0f};

    private float light1_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
    private float light1_diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
    private float light1_specular[] = { 0.0f,0.0f,1.0f,0.0f };
    private float light1_position[] = { 100.0f,100.0f,100.0f,1.0f };

    private float material_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
    private float material_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
    private float material_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
    private float material_shininess = 50.0f;

    private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    private int la0Uniform, ld0Uniform, ls0Uniform, light0PositionUniform;
    private int la1Uniform, ld1Uniform, ls1Uniform, light1PositionUniform;
    private int kaUniform, kdUniform, ksUniform, materialShininessUniform;

    //private int lightPropertiesUniform;

    private float perspectiveProjectionMatrix[] = new float[16]; //4X4 matrix

    private float anglePyramid=0.0f;

    /*LightProperties Lights[]= new LightProperties[]
            {
                    new LightProperties(light0_ambient,light0_diffuse,light0_specular,light0_position),
                    new LightProperties(light1_ambient,light1_diffuse,light1_specular,light1_position)
            } ;*/

    public GLESView(Context drawingContext) {
        super(drawingContext);

        context = drawingContext;

        // Set EGLContext to current supported version of OpenGL-ES

        setEGLContextClientVersion(3);

        // Set Renderer for drawing on the GLSurfaceView

        setRenderer(this);

        // Render the view only when there is a change in the drawing data.

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);

        gestureDetector.setOnDoubleTapListener(this);

    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        // OpenGL-ES version check.
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VDG: OpenGL-ES Version = " + glesVersion);

        //GLSL version
        String glslVersion = gl.glGetString(GLES31.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VDG: GLSL Version = " + glslVersion);

        initialize(gl);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) {
        draw();
    }

    // Handling 'onTouchEvent' is most important because it triggers all gesture and tap events.
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        int eventAction = e.getAction();

        if (!gestureDetector.onTouchEvent(e)) {
            super.onTouchEvent(e);
        }

        return (true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent e)
    {

        return (true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {

        return (true);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return (true);
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        uninitialize();
        System.exit(0);
        return (true);
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return (true);
    }

    private void initialize(GL10 gl) {
        // Set the background frame color.

        System.out.println("VDG :" + "In initialize");

        // ******************************************************
        // Vertex Shader
        // ******************************************************

        // Create Shader

        vertexShaderObject = GLES31.glCreateShader(GLES31.GL_VERTEX_SHADER);

        // vertex shader source code

        final String vertexShaderSourceCode = String.format
                (
                        "#version 310 es" +
                                "\n" +
                                "in vec4 vPosition;" +
                                "in vec3 vNormal;" +
                                "uniform mat4 u_model_matrix;" +
                                "uniform mat4 u_view_matrix;" +
                                "uniform mat4 u_projection_matrix;" +
                                "uniform vec3 u_La0;" +
                                "uniform vec3 u_Ld0;" +
                                "uniform vec3 u_Ls0;" +
                                "uniform vec4 u_light0_position;" +
                                "uniform vec3 u_La1;" +
                                "uniform vec3 u_Ld1;" +
                                "uniform vec3 u_Ls1;" +
                                "uniform vec4 u_light1_position;" +
                                "uniform vec3 u_Ka;" +
                                "uniform vec3 u_Kd;" +
                                "uniform vec3 u_Ks;" +
                                "uniform float u_material_shininess;" +
                                "out vec3 phong_ads_color;" +
                                "void main(void)" +
                                "{" +
                                "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition ;" +
                                "vec3 tnorm = normalize(mat3 (u_view_matrix * u_model_matrix) * vNormal );" +
                                "vec3 light0_direction = normalize(vec3 (u_light0_position) - eyeCoordinates.xyz );" +
                                "float tn_dot_ld0 = max(dot(tnorm,light0_direction),0.0) ;" +
                                "vec3 reflection_vector0 = reflect(-light0_direction,tnorm) ;" +
                                "vec3 light1_direction = normalize(vec3 (u_light1_position) - eyeCoordinates.xyz );" +
                                "float tn_dot_ld1 = max(dot(tnorm,light1_direction),0.0) ;" +
                                "vec3 reflection_vector1 = reflect(-light1_direction,tnorm) ;" +
                                "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);"+
                                "vec3 ambient0 = u_La0 * u_Ka ;" +
                                "vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0 ;" +
                                "vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0,viewer_vector),0.0),u_material_shininess) ;" +
                                "vec3 ambient1 = u_La1 * u_Ka ;" +
                                "vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1 ;" +
                                "vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1,viewer_vector),0.0),u_material_shininess) ;" +
                                "phong_ads_color = ambient0 + diffuse0 + specular0 + ambient1 + diffuse1 + specular1;"+
                                "gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;" +
                                "}"
                );


        // provide source code to shader
        GLES31.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        // compile shader and do error checking

        GLES31.glCompileShader(vertexShaderObject);
        int[] iShaderCompiledStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);

        if (iShaderCompiledStatus[0] == GLES31.GL_FALSE) {
            GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

            if (iInfoLogLength[0] > 0) {
                szInfoLog = GLES31.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VDG: Vertex Shader Compilation Log = " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // ********************************
        // Fragment Shader
        // ********************************

        // Create Shader

        fragmentShaderObject = GLES31.glCreateShader(GLES31.GL_FRAGMENT_SHADER);

        // fragment shader source code

        final String fragmentShaderSourceCode = String.format
                (
                        "#version 310 es" +
                                "\n" +
                                "precision highp float;" +
                                "in vec3 phong_ads_color;" +
                                "out vec4 FragColor;" +
                                "void main(void)" +
                                "{" +
                                "FragColor = vec4(phong_ads_color,1.0);" +
                                "}"
                );


        // provide source code to shader
        GLES31.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

        // compile shader and do error checking

        GLES31.glCompileShader(fragmentShaderObject);
        iShaderCompiledStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);

        if (iShaderCompiledStatus[0] == GLES31.GL_FALSE) {
            GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

            if (iInfoLogLength[0] > 0) {
                szInfoLog = GLES31.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VDG: Fragment Shader Compilation Log = " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }


        // Create Shader Program object

        shaderProgramObject = GLES31.glCreateProgram();

        // Attach vertex shader to shader Program

        GLES31.glAttachShader(shaderProgramObject, vertexShaderObject);

        // Attach fragment shader to shader Program

        GLES31.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // pre-link binding of shader program object with vertex shader attributes

        GLES31.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

        // Normal

        GLES31.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

        // link the two shaders together to shader Program object.

        GLES31.glLinkProgram(shaderProgramObject);

        int[] iShaderProgramLinkStatus = new int[1];

        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);

        if (iShaderProgramLinkStatus[0] == GLES31.GL_FALSE) {
            GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

            if (iInfoLogLength[0] > 0) {
                szInfoLog = GLES31.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("VDG: Shader Program Link Log = " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }


        // get uniform locations

        modelMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_model_matrix");

        viewMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_view_matrix");

        projectionMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

        la0Uniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_La0");

        ld0Uniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ld0");

        ls0Uniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ls0");

        light0PositionUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_light0_position");

        la1Uniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_La1");

        ld1Uniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ld1");

        ls1Uniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ls1");

        light1PositionUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_light1_position");

        //lightPropertiesUniform = GLES31.glGetUniformLocation(shaderProgramObject, "Lights");

        kaUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ka");

        kdUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Kd");

        ksUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ks");

        materialShininessUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_material_shininess");


        final float pyramidVertices[] = new float[]
                {
                        // front face

                        0.0f, 1.0f, 0.0f,   // Apex

                        -1.0f, -1.0f, 1.0f, // left-corner of front face

                        1.0f, -1.0f, 1.0f,   //right corner of front face

                        //right face

                        0.0f, 1.0f, 0.0f,     // apex

                        1.0f, -1.0f, 1.0f,    //left corner of the right face

                        1.0f, -1.0f, -1.0f,  // right corner of the right face

                        //Back face

                        0.0f, 1.0f, 0.0f, // apex

                        1.0f, -1.0f, -1.0f, //left corner of the back face

                        -1.0f, -1.0f, -1.0f, //right corner of the back face

                        //left face

                        0.0f, 1.0f, 0.0f, // apex

                        -1.0f, -1.0f, -1.0f, //left corner of the back face

                        -1.0f, -1.0f, 1.0f, //right corner of the back face

                };

        // Vao Pyramid

        GLES31.glGenVertexArrays(1,vao_pyramid,0);
        GLES31.glBindVertexArray(vao_pyramid[0]);

        // vbo position

        GLES31.glGenBuffers(1,vbo_pyramid_position,0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_pyramid_position[0]);

        ByteBuffer byteBuffer_pyaramidVertices=ByteBuffer.allocateDirect(pyramidVertices.length * 4);
        byteBuffer_pyaramidVertices.order(ByteOrder.nativeOrder());
        FloatBuffer pyaramidVerticesBuffer = byteBuffer_pyaramidVertices.asFloatBuffer();
        pyaramidVerticesBuffer.put(pyramidVertices);
        pyaramidVerticesBuffer.position(0);

        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,pyramidVertices.length * 4,pyaramidVerticesBuffer,GLES31.GL_STATIC_DRAW);
        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES31.GL_FLOAT,false,0,0);

        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);   // unbind Vbo position

        // normal values.

        final float pyramidNormalCoordinates[] = new float[]
                {
                        0.0f, 0.447214f, 0.894427f,
                        0.0f, 0.447214f, 0.894427f,
                        0.0f, 0.447214f, 0.894427f,

                        0.894427f, 0.447214f, 0.0f,
                        0.894427f, 0.447214f, 0.0f,
                        0.894427f, 0.447214f, 0.0f,

                        0.0f, 0.447214f, -0.894427f,
                        0.0f, 0.447214f, -0.894427f,
                        0.0f, 0.447214f, -0.894427f,

                        -0.894427f, 0.447214f, 0.0f,
                        -0.894427f, 0.447214f, 0.0f,
                        -0.894427f, 0.447214f, 0.0f
                };

        // Normal Vbo

        GLES31.glGenBuffers(1,vbo_pyramid_normal,0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_pyramid_normal[0]);

        ByteBuffer byteBuffer_pyramidNormal=ByteBuffer.allocateDirect(pyramidNormalCoordinates.length * 4);
        byteBuffer_pyramidNormal.order(ByteOrder.nativeOrder());
        FloatBuffer pyramidNormalBuffer = byteBuffer_pyramidNormal.asFloatBuffer();
        pyramidNormalBuffer.put(pyramidNormalCoordinates);
        pyramidNormalBuffer.position(0);

        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,pyramidNormalCoordinates.length * 4,pyramidNormalBuffer,GLES31.GL_STATIC_DRAW);
        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,3,GLES31.GL_FLOAT,false,0,0);

        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);  // Unbind color Vbo

        GLES31.glBindVertexArray(0);   // unbind Pyramid Vao

        // Enable depth testing
        GLES31.glEnable(GLES31.GL_DEPTH_TEST);

        GLES31.glDepthFunc(GLES31.GL_LEQUAL);

        GLES31.glEnable(GLES31.GL_CULL_FACE);

        GLES31.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

    private void resize(int width, int height) {
        GLES31.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, ((float) width / (float) height), 0.1f, 100.0f);
    }

    public void draw() {
        GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT | GLES31.GL_DEPTH_BUFFER_BIT);

        GLES31.glUseProgram(shaderProgramObject);

            // set light properties

            //pass light properties array here

            GLES31.glUniform3fv(la0Uniform,1,light0_ambient,0 );
            GLES31.glUniform3fv(ld0Uniform, 1 , light0_diffuse,0);
            GLES31.glUniform3fv(ls0Uniform, 1 , light0_specular,0);
            GLES31.glUniform4fv(light0PositionUniform, 1, light0_position, 0);

            GLES31.glUniform3fv(la1Uniform,1,light1_ambient,0 );
            GLES31.glUniform3fv(ld1Uniform, 1 , light1_diffuse,0);
            GLES31.glUniform3fv(ls1Uniform, 1 , light1_specular,0);
            GLES31.glUniform4fv(light1PositionUniform, 1, light1_position, 0);

            // set material properties

            GLES31.glUniform3fv(kaUniform,1,material_ambient,0);
            GLES31.glUniform3fv(kdUniform,1,material_diffuse,0);
            GLES31.glUniform3fv(ksUniform,1,material_specular,0);
            GLES31.glUniform1f(materialShininessUniform,material_shininess);


        float modelMatrix[] = new float[16];
        float viewMatrix[] = new float[16];
        float rotationMatrix[] = new float[16];

        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(viewMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        // translate modelViewMatrix

        Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -4.5f);

        Matrix.rotateM(rotationMatrix,0,anglePyramid,0.0f,1.0f,0.0f);

        // Multiply the modelview and rotationMatrix

        Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,rotationMatrix,0);

        GLES31.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);

        GLES31.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);

        GLES31.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);

		// Draw pyramid.

        // bind vao
        GLES31.glBindVertexArray(vao_pyramid[0]);


        GLES31.glDrawArrays(GLES31.GL_TRIANGLES,0,12);

        // unbind vao
        GLES31.glBindVertexArray(0);

        GLES31.glUseProgram(0);

        updateAngle();

        requestRender();

    }

    void updateAngle()
    {
        anglePyramid = anglePyramid + 1.0f;

        if (anglePyramid >= 360.0f)
            anglePyramid = 0.0f;
    }

    void uninitialize() {

        // destroy vao
        if(vao_pyramid[0]!=0)
        {
            GLES31.glDeleteVertexArrays(1,vao_pyramid,0);
            vao_pyramid[0]=0;
        }


        if(vbo_pyramid_position[0]!=0)
        {
            GLES31.glDeleteBuffers(1,vbo_pyramid_position,0);
            vbo_pyramid_position[0]=0;
        }

        if(vbo_pyramid_normal[0]!=0)
        {
            GLES31.glDeleteBuffers(1,vbo_pyramid_normal,0);
            vbo_pyramid_normal[0]=0;
        }

        if (shaderProgramObject != 0) {
            if (vertexShaderObject != 0) {
                GLES31.glDetachShader(shaderProgramObject, vertexShaderObject);
                GLES31.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }

            if (fragmentShaderObject != 0) {
                GLES31.glDetachShader(shaderProgramObject, fragmentShaderObject);
                GLES31.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        if (shaderProgramObject != 0) {
            GLES31.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }

}