package com.astromedicomp.HelloWorld1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.graphics.Color;
import android.widget.TextView;
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		
		// To remove the ActionBar.
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// To make fullscreen.
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // To set orientation to Landscape.		
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		// To set the background color.
		this.getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));
		
		// To set the properties of TextView.
		
		TextView myTextView = new TextView(this);
		myTextView.setText("Hello World !!!!!");
		myTextView.setTextSize(60);
		myTextView.setTextColor(Color.GREEN);
		myTextView.setGravity(Gravity.CENTER);
		
		setContentView(myTextView);
        //setContentView(R.layout.activity_main);
    }
}
