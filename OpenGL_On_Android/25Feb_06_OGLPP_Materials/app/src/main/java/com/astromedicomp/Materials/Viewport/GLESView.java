
package com.astromedicomp.Materials;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;  // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;  // for EGLConfig needed as param type EGLConfig

import android.opengl.GLES31;  // for OpenGLES 3.1
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.lang.Math;

// for vbo
import java.nio.ByteBuffer;   // nio for non blocking I/O
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

// for matrix math
import android.opengl.Matrix;

// A view for OpenGLES3 graphics which also receives touch events.
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;

    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int numElements;
    private int numVertices;

    private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

    private float light0_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f};
    private float light0_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};  // White light
    private float light0_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
    private float light0_position[] = {0.0f, 0.0f, 0.0f, 0.0f};

    private float material_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f};
    private float material_diffuse[] = {0.0f, 0.0f, 0.0f, 0.0f};
    private float material_specular[] = {0.0f, 0.0f, 0.0f, 0.0f};
    private float material_shininess = 0.0f;

    private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    private int la0Uniform, ld0Uniform, ls0Uniform, light0PositionUniform;
    //private int la1Uniform, ld1Uniform, ls1Uniform, light1PositionUniform;
    //private int la2Uniform, ld2Uniform, ls2Uniform, light2PositionUniform;
    private int kaUniform, kdUniform, ksUniform, materialShininessUniform;

    private int doubleTapUniform;
    private int singleTapUniform;

    private float perspectiveProjectionMatrix[] = new float[16]; //4X4 matrix

    private int doubleTap; // For lighting
    private int singleTap; // For rotation axis

    private float lightRotationAngle = 0.0f;

    private int giWidth,giHeight;

    public GLESView(Context drawingContext) {
        super(drawingContext);

        context = drawingContext;

        // Set EGLContext to current supported version of OpenGL-ES

        setEGLContextClientVersion(3);

        // Set Renderer for drawing on the GLSurfaceView

        setRenderer(this);

        // Render the view only when there is a change in the drawing data.

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);

        gestureDetector.setOnDoubleTapListener(this);

    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        // OpenGL-ES version check.
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VDG: OpenGL-ES Version = " + glesVersion);

        //GLSL version
        String glslVersion = gl.glGetString(GLES31.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VDG: GLSL Version = " + glslVersion);

        initialize(gl);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) {
        draw();
    }

    // Handling 'onTouchEvent' is most important because it triggers all gesture and tap events.
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        int eventAction = e.getAction();

        if (!gestureDetector.onTouchEvent(e)) {
            super.onTouchEvent(e);
        }

        return (true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        doubleTap++;
        if (doubleTap > 1)
            doubleTap = 0;
        return (true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        singleTap++;
        if (singleTap > 2)
            singleTap = 0;
        return (true);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return (true);
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        uninitialize();
        System.exit(0);
        return (true);
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return (true);
    }

    private void initialize(GL10 gl) {
        // Set the background frame color.

        System.out.println("VDG :" + "In initialize");

        // ******************************************************
        // Vertex Shader
        // ******************************************************

        // Create Shader

        vertexShaderObject = GLES31.glCreateShader(GLES31.GL_VERTEX_SHADER);

        // vertex shader source code

        final String vertexShaderSourceCode = String.format(
        "#version 310 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_model_matrix ;" +
        "uniform mat4 u_view_matrix ;" +
        "uniform mat4 u_projection_matrix ;" +
        "uniform vec4 u_light0_position;"+
        "uniform mediump int u_double_tap;"+
        "out vec3 transformed_normals;" +
        "out vec3 light0_direction;" +
        "out vec3 viewer_vector;" +
        "void main(void)" +
                "{" +
                "if (u_double_tap == 1)" +
                "{" +
                "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition ;" +
                "transformed_normals = mat3 (u_view_matrix * u_model_matrix) * vNormal ;" +
                "light0_direction = vec3 (u_light0_position) - eyeCoordinates.xyz ;" +
                "viewer_vector = -eyeCoordinates.xyz;"+
                "}" +
                "gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;" +
                "}"
        );

        // provide source code to shader
        GLES31.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

        // compile shader and do error checking

        GLES31.glCompileShader(vertexShaderObject);
        int[] iShaderCompiledStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);

        if (iShaderCompiledStatus[0] == GLES31.GL_FALSE) {
            GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

            if (iInfoLogLength[0] > 0) {
                szInfoLog = GLES31.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VDG: Vertex Shader Compilation Log = " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // ********************************
        // Fragment Shader
        // ********************************

        // Create Shader

        fragmentShaderObject = GLES31.glCreateShader(GLES31.GL_FRAGMENT_SHADER);

        // fragment shader source code

        final String fragmentShaderSourceCode = String.format(
                "#version 310 es" +
                        "\n" +
                        "precision highp float;" +
                        "in vec3 transformed_normals;"+
                        "in vec3 light0_direction;"+
                        "in vec3 viewer_vector;"+
                        "out vec4 FragColor;" +
                        "uniform vec3 u_La0;"+
                        "uniform vec3 u_Ld0;"+
                        "uniform vec3 u_Ls0;"+
                        "uniform vec3 u_Ka;"+
                        "uniform vec3 u_Kd;"+
                        "uniform vec3 u_Ks;"+
                        "uniform float u_material_shininess;"+
                        "uniform int u_double_tap;"+
                        "void main(void)" +
                        "{" +
                        "vec3 phong_ads_color;"+
                        "if(u_double_tap == 1)"+
                        "{"+
                        "vec3 normalized_transformed_normal = normalize(transformed_normals) ;"+
                        "vec3 normalized_light_direction = normalize(light0_direction);"+
                        "vec3 normalized_viewer_vector = normalize(viewer_vector);"+
                        "vec3 ambient = u_La0 * u_Ka;"+
                        "float tn_dot_ld = max(dot(normalized_transformed_normal,normalized_light_direction),0.0);"+
                        "vec3 diffuse = u_Ld0 * u_Kd * tn_dot_ld;"+
                        "vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normal);"+
                        "vec3 specular = u_Ls0 * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"+
                        "phong_ads_color = ambient + diffuse + specular ;"+
                        "}"+
                        "else"+
                        "{"+
                        "phong_ads_color = vec3(1.0,1.0,1.0) ;"+
                        "}"+
                        "FragColor = vec4(phong_ads_color,1.0);" +
                        "}"
        );

        // provide source code to shader
        GLES31.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

        // compile shader and do error checking

        GLES31.glCompileShader(fragmentShaderObject);
        iShaderCompiledStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);

        if (iShaderCompiledStatus[0] == GLES31.GL_FALSE) {
            GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

            if (iInfoLogLength[0] > 0) {
                szInfoLog = GLES31.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VDG: Fragment Shader Compilation Log = " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }


        // Create Shader Program object

        shaderProgramObject = GLES31.glCreateProgram();

        // Attach vertex shader to shader Program

        GLES31.glAttachShader(shaderProgramObject, vertexShaderObject);

        // Attach fragment shader to shader Program

        GLES31.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // pre-link binding of shader program object with vertex shader attributes

        GLES31.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

        // Color

        GLES31.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

        // link the two shaders together to shader Program object.

        GLES31.glLinkProgram(shaderProgramObject);

        int[] iShaderProgramLinkStatus = new int[1];

        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);

        if (iShaderProgramLinkStatus[0] == GLES31.GL_FALSE) {
            GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

            if (iInfoLogLength[0] > 0) {
                szInfoLog = GLES31.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("VDG: Shader Program Link Log = " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }


        // get uniform locations

        modelMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_model_matrix");

        viewMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_view_matrix");

        projectionMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

        doubleTapUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_double_tap");

        singleTapUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_single_tap");

        la0Uniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_La0");

        ld0Uniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ld0");

        ls0Uniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ls0");

        light0PositionUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_light0_position");

        kaUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ka");

        kdUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Kd");

        ksUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ks");

        materialShininessUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_material_shininess");


        Sphere sphere = new Sphere();
        float sphere_vertices[] = new float[1146];
        float sphere_normals[] = new float[1146];
        float sphere_textures[] = new float[764];
        short sphere_elements[] = new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES31.glGenVertexArrays(1, vao_sphere, 0);
        GLES31.glBindVertexArray(vao_sphere[0]);

        // position vbo
        GLES31.glGenBuffers(1, vbo_sphere_position, 0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, vbo_sphere_position[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);

        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,
                sphere_vertices.length * 4,
                verticesBuffer,
                GLES31.GL_STATIC_DRAW);

        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
                3,
                GLES31.GL_FLOAT,
                false, 0, 0);

        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);

        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, 0);

        // normal vbo
        GLES31.glGenBuffers(1, vbo_sphere_normal, 0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);

        byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);

        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,
                sphere_normals.length * 4,
                verticesBuffer,
                GLES31.GL_STATIC_DRAW);

        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
                3,
                GLES31.GL_FLOAT,
                false, 0, 0);

        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);

        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, 0);

        // element vbo
        GLES31.glGenBuffers(1, vbo_sphere_element, 0);
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);

        byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);

        GLES31.glBufferData(GLES31.GL_ELEMENT_ARRAY_BUFFER,
                sphere_elements.length * 2,
                elementsBuffer,
                GLES31.GL_STATIC_DRAW);

        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, 0);

        GLES31.glBindVertexArray(0);

        // Enable depth testing
        GLES31.glEnable(GLES31.GL_DEPTH_TEST);

        GLES31.glDepthFunc(GLES31.GL_LEQUAL);

        GLES31.glEnable(GLES31.GL_CULL_FACE);

        GLES31.glClearColor(0.5f, 0.5f, 0.5f, 0.0f);

        doubleTap = 0;

        singleTap = 0;

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

    private void resize(int width, int height) {
        giWidth = width;
        giHeight=height;

        GLES31.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float) (width/4) / (float) (height/6), 0.1f, 100.0f);
    }

    public void draw() {

        GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT | GLES31.GL_DEPTH_BUFFER_BIT);

        GLES31.glUseProgram(shaderProgramObject);

        if (doubleTap == 1)
        {

            GLES31.glUniform1i(doubleTapUniform, 1);

            GLES31.glUniform1i(singleTapUniform,singleTap );

            // set light properties

            // update rotation as per single tap value

            if (singleTap ==0 ) {

                // X axis rotation
                light0_position[0] = 0.0f;
                light0_position[1] = 20 * (float) java.lang.Math.cos(lightRotationAngle);
                light0_position[2] = 20 * (float) java.lang.Math.sin(lightRotationAngle);
            }

            else if (singleTap ==1 ) {

                // Y axis rotation
                light0_position[0] = 20 * (float) java.lang.Math.cos(- lightRotationAngle);
                light0_position[1] = 0.0f;
                light0_position[2] = 20 * (float) java.lang.Math.sin(- lightRotationAngle);
            }

            else if (singleTap ==2 ) {

                // Z axis rotation
                light0_position[0] = 20 * (float) java.lang.Math.cos(lightRotationAngle);
                light0_position[1] = 20 * (float) java.lang.Math.sin(lightRotationAngle);
                light0_position[2] = 0.0f;
            }

            GLES31.glUniform3fv(la0Uniform,1,light0_ambient,0 );
            GLES31.glUniform3fv(ld0Uniform, 1 , light0_diffuse,0);
            GLES31.glUniform3fv(ls0Uniform, 1 , light0_specular,0);
            GLES31.glUniform4fv(light0PositionUniform, 1, light0_position, 0);

        }

        else
        {
            GLES31.glUniform1i(doubleTapUniform, 0);
        }


        float modelMatrix[] = new float[16];
        float viewMatrix[] = new float[16];

        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(viewMatrix, 0);

/// Emerald

        material_ambient[0] = 0.0215f;
        material_ambient[1] = 0.1745f;
        material_ambient[2] = 0.0215f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.07568f;
        material_diffuse[1] = 0.61424f;
        material_diffuse[2] = 0.07568f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.633f;
        material_specular[1] = 0.727811f;
        material_specular[2] = 0.633f;
        material_specular[3] = 1.0f;

        material_shininess = 0.6f*128.0f;

        GLES31.glViewport(0, giHeight * 5 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        /// Jade

        material_ambient[0] = 0.135f;
        material_ambient[1] = 0.2225f;
        material_ambient[2] = 0.1575f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.54f;
        material_diffuse[1] = 0.89f;
        material_diffuse[2] = 0.63f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.316228f;
        material_specular[1] = 0.316228f;
        material_specular[2] = 0.316228f;
        material_specular[3] = 1.0f;

        material_shininess = 0.1f* 128.0f;

        GLES31.glViewport(0, giHeight * 4 / 6, giWidth / 4, giHeight / 6);
        drawSphere();

        /// Obsidian

        material_ambient[0] = 0.05375f;
        material_ambient[1] = 0.05f;
        material_ambient[2] = 0.06625f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.18275f;
        material_diffuse[1] = 0.17f;
        material_diffuse[2] = 0.22525f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.332741f;
        material_specular[1] = 0.328634f;
        material_specular[2] = 0.346435f;
        material_specular[3] = 1.0f;

        material_shininess = 0.3f * 128.0f;

        GLES31.glViewport(0, giHeight * 3 / 6, giWidth / 4, giHeight / 6);
        drawSphere();

        /// Pearl

        material_ambient[0] = 0.25f;
        material_ambient[1] = 0.20725f;
        material_ambient[2] = 0.20725f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 1.0f;
        material_diffuse[1] = 0.829f;
        material_diffuse[2] = 0.829f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.296648f;
        material_specular[1] = 0.296648f;
        material_specular[2] = 0.296648f;
        material_specular[3] = 1.0f;

        material_shininess = 0.088f * 128.0f;

        GLES31.glViewport(0, giHeight * 2 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        ///

        /// Ruby

        material_ambient[0] = 0.1745f;
        material_ambient[1] = 0.01175f;
        material_ambient[2] = 0.01175f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.61424f;
        material_diffuse[1] = 0.04136f;
        material_diffuse[2] = 0.04136f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.727811f;
        material_specular[1] = 0.626959f;
        material_specular[2] = 0.626959f;
        material_specular[3] = 1.0f;

        material_shininess = 0.6f * 128.0f;

        GLES31.glViewport(0, giHeight * 1 / 6, giWidth / 4, giHeight / 6);
        drawSphere();

        ///

        /// Turquoise

        material_ambient[0] = 0.1f;
        material_ambient[1] = 0.18725f;
        material_ambient[2] = 0.1745f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.396f;
        material_diffuse[1] = 0.74151f;
        material_diffuse[2] = 0.69102f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.297254f;
        material_specular[1] = 0.30829f;
        material_specular[2] = 0.306678f;
        material_specular[3] = 1.0f;

        material_shininess = 0.1f * 128.0f;

        GLES31.glViewport(0, giHeight * 0 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        /// Brass

        material_ambient[0] = 0.329412f;
        material_ambient[1] = 0.223529f;
        material_ambient[2] = 0.027451f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.780392f;
        material_diffuse[1] = 0.568627f;
        material_diffuse[2] = 0.113725f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.992157f;
        material_specular[1] = 0.941176f;
        material_specular[2] = 0.807843f;
        material_specular[3] = 1.0f;

        material_shininess = 0.21794872f * 128.0f;

        GLES31.glViewport(giWidth / 4, giHeight * 5 / 6, giWidth / 4, giHeight / 6);
        drawSphere();

        ///

        /// Bronze

        material_ambient[0] = 0.2125f;
        material_ambient[1] = 0.1275f;
        material_ambient[2] = 0.054f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.714f;
        material_diffuse[1] = 0.4284f;
        material_diffuse[2] = 0.18144f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.393548f;
        material_specular[1] = 0.271906f;
        material_specular[2] = 0.166721f;
        material_specular[3] = 1.0f;

        material_shininess = 0.2f * 128.0f;

        GLES31.glViewport(giWidth / 4, giHeight * 4 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        ///

        /// Chrome

        material_ambient[0] = 0.25f;
        material_ambient[1] = 0.25f;
        material_ambient[2] = 0.25f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.4f;
        material_diffuse[1] = 0.4f;
        material_diffuse[2] = 0.4f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.774597f;
        material_specular[1] = 0.774597f;
        material_specular[2] = 0.774597f;
        material_specular[3] = 1.0f;

        material_shininess = 0.6f * 128.0f;

        GLES31.glViewport(giWidth / 4, giHeight * 3 / 6, giWidth / 4, giHeight / 6);
        drawSphere();

        ///

        /// Copper

        material_ambient[0] = 0.19125f;
        material_ambient[1] = 0.0735f;
        material_ambient[2] = 0.0225f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.7038f;
        material_diffuse[1] = 0.27048f;
        material_diffuse[2] = 0.0828f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.256777f;
        material_specular[1] = 0.137622f;
        material_specular[2] = 0.086014f;
        material_specular[3] = 1.0f;

        material_shininess = 0.6f * 128.0f;

        GLES31.glViewport(giWidth / 4, giHeight * 2 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        ///


        /// Gold

        material_ambient[0] = 0.24725f;
        material_ambient[1] = 0.1995f;
        material_ambient[2] = 0.0745f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.75164f;
        material_diffuse[1] = 0.60648f;
        material_diffuse[2] = 0.22648f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.628281f;
        material_specular[1] = 0.555802f;
        material_specular[2] = 0.366065f;
        material_specular[3] = 1.0f;

        material_shininess = 0.4f * 128.0f;

        GLES31.glViewport(giWidth / 4, giHeight * 1 / 6, giWidth / 4, giHeight / 6);
        drawSphere();

        ///


        /// Silver

        material_ambient[0] = 0.19225f;
        material_ambient[1] = 0.19225f;
        material_ambient[2] = 0.19225f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.50754f;
        material_diffuse[1] = 0.50754f;
        material_diffuse[2] = 0.50754f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.508273f;
        material_specular[1] = 0.508273f;
        material_specular[2] = 0.508273f;
        material_specular[3] = 1.0f;

        material_shininess = 0.4f * 128.0f;

        GLES31.glViewport(giWidth / 4, giHeight * 0 / 6, giWidth / 4, giHeight / 6);
        drawSphere();

        ///
        /// Black

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.01f;
        material_diffuse[1] = 0.01f;
        material_diffuse[2] = 0.01f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.50f;
        material_specular[1] = 0.50f;
        material_specular[2] = 0.50f;
        material_specular[3] = 1.0f;

        material_shininess = 0.25f * 128.0f;

        GLES31.glViewport(giWidth / 2, giHeight * 5 / 6, giWidth / 4, giHeight / 6);
        drawSphere();

        ///

        /// Cyan

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.1f;
        material_ambient[2] = 0.06f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.0f;
        material_diffuse[1] = 0.50980392f;
        material_diffuse[2] = 0.50980392f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.50196078f;
        material_specular[1] = 0.50196078f;
        material_specular[2] = 0.50196078f;
        material_specular[3] = 1.0f;

        material_shininess = 0.25f * 128.0f;

        GLES31.glViewport(giWidth / 2, giHeight * 4 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        ///

        /// Green

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.1f;
        material_diffuse[1] = 0.35f;
        material_diffuse[2] = 0.1f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.45f;
        material_specular[1] = 0.55f;
        material_specular[2] = 0.45f;
        material_specular[3] = 1.0f;

        material_shininess = 0.25f * 128.0f;

        GLES31.glViewport(giWidth / 2, giHeight * 3 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        ///

        /// Red

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.5f;
        material_diffuse[1] = 0.0f;
        material_diffuse[2] = 0.0f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.7f;
        material_specular[1] = 0.6f;
        material_specular[2] = 0.6f;
        material_specular[3] = 1.0f;

        material_shininess = 0.25f * 128.0f;

        GLES31.glViewport(giWidth / 2, giHeight * 2 / 6, giWidth / 4, giHeight / 6);
        drawSphere();

        ///

        /// White

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.55f;
        material_diffuse[1] = 0.55f;
        material_diffuse[2] = 0.55f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.70f;
        material_specular[1] = 0.70f;
        material_specular[2] = 0.70f;
        material_specular[3] = 1.0f;

        material_shininess = 0.25f * 128.0f;

        GLES31.glViewport(giWidth / 2, giHeight * 1 / 6, giWidth / 4, giHeight / 6);
        drawSphere();


        ///

        /// Yellow Plastic

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.5f;
        material_diffuse[1] = 0.5f;
        material_diffuse[2] = 0.0f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.60f;
        material_specular[1] = 0.60f;
        material_specular[2] = 0.50f;
        material_specular[3] = 1.0f;

        material_shininess = 0.25f * 128.0f;


        ////
        GLES31.glViewport(giWidth / 2, giHeight * 0 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        ///

        /// Black

        material_ambient[0] = 0.02f;
        material_ambient[1] = 0.02f;
        material_ambient[2] = 0.02f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.01f;
        material_diffuse[1] = 0.01f;
        material_diffuse[2] = 0.01f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.4f;
        material_specular[1] = 0.4f;
        material_specular[2] = 0.4f;
        material_specular[3] = 1.0f;

        material_shininess = 0.078125f * 128.0f;

        GLES31.glViewport((int)(giWidth / 1.4), giHeight * 5 / 6, giWidth / 4, giHeight / 6);
        //GLES31.glViewport(giWidth , giHeight * 5 / 6, giWidth / 4, giHeight / 6);


        drawSphere();

        ///


        /// Cyan

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.05f;
        material_ambient[2] = 0.05f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.4f;
        material_diffuse[1] = 0.5f;
        material_diffuse[2] = 0.5f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.04f;
        material_specular[1] = 0.7f;
        material_specular[2] = 0.7f;
        material_specular[3] = 1.0f;

        material_shininess = 0.078125f * 128.0f;

        GLES31.glViewport((int)(giWidth / 1.4), giHeight * 4 / 6, giWidth / 4, giHeight / 6);
        //GLES31.glViewport(giWidth , giHeight * 4 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        ///

        /// Green

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.05f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.4f;
        material_diffuse[1] = 0.5f;
        material_diffuse[2] = 0.4f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.04f;
        material_specular[1] = 0.7f;
        material_specular[2] = 0.04f;
        material_specular[3] = 1.0f;

        material_shininess = 0.078125f * 128.0f;

        GLES31.glViewport((int)(giWidth / 1.4), giHeight * 3 / 6, giWidth / 4, giHeight / 6);
        //GLES31.glViewport(giWidth , giHeight * 4 / 6, giWidth / 4, giHeight / 6);


        drawSphere();

        ///

        /// Red

        material_ambient[0] = 0.05f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.5f;
        material_diffuse[1] = 0.4f;
        material_diffuse[2] = 0.4f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.7f;
        material_specular[1] = 0.04f;
        material_specular[2] = 0.04f;
        material_specular[3] = 1.0f;

        material_shininess = 0.078125f * 128.0f;

        GLES31.glViewport((int)(giWidth / 1.4), giHeight * 2 / 6, giWidth / 4, giHeight / 6);
        //GLES31.glViewport(giWidth , giHeight * 2 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        ///

        /// White

        material_ambient[0] = 0.05f;
        material_ambient[1] = 0.05f;
        material_ambient[2] = 0.05f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.5f;
        material_diffuse[1] = 0.5f;
        material_diffuse[2] = 0.5f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.7f;
        material_specular[1] = 0.7f;
        material_specular[2] = 0.7f;
        material_specular[3] = 1.0f;

        material_shininess = 0.078125f * 128.0f;

        GLES31.glViewport((int)(giWidth / 1.4), giHeight * 1 / 6, giWidth / 4, giHeight / 6);
        //GLES31.glViewport(giWidth , giHeight * 1 / 6, giWidth / 4, giHeight / 6);

        drawSphere();

        ///

        /// Yellow Rubber

        material_ambient[0] = 0.05f;
        material_ambient[1] = 0.05f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;

        material_diffuse[0] = 0.5f;
        material_diffuse[1] = 0.5f;
        material_diffuse[2] = 0.4f;
        material_diffuse[3] = 1.0f;

        material_specular[0] = 0.7f;
        material_specular[1] = 0.7f;
        material_specular[2] = 0.04f;
        material_specular[3] = 1.0f;

        material_shininess = 0.078125f * 128.0f;

        //GLES31.glViewport(giWidth - 1,giHeight * 0 / 6,giWidth / 4,giHeight / 6);
        GLES31.glViewport((int)(giWidth /1.4) ,giHeight * 0 / 6,giWidth / 4,giHeight / 6);
        drawSphere();

        // Translate

        //translationMatrix = translate(0.0f, 0.0f, -2.0f);

        //modelMatrix = modelMatrix * translationMatrix;
        Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -2.0f);

        GLES31.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);

        GLES31.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);

        GLES31.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);

        GLES31.glUseProgram(0);

        updateAngle();

        requestRender();

    }

    void drawSphere()
    {

        GLES31.glUniform3fv(kaUniform,1,material_ambient,0);
        GLES31.glUniform3fv(kdUniform,1,material_diffuse,0);
        GLES31.glUniform3fv(ksUniform,1,material_specular,0);
        GLES31.glUniform1f(materialShininessUniform, material_shininess);

        // bind vao
        GLES31.glBindVertexArray(vao_sphere[0]);

        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES31.glDrawElements(GLES31.GL_TRIANGLES, numElements, GLES31.GL_UNSIGNED_SHORT, 0);

        // unbind vao
        GLES31.glBindVertexArray(0);

    }

    void updateAngle()
    {
        if (lightRotationAngle > 360.0f)
            lightRotationAngle = 360.0f - lightRotationAngle;

        lightRotationAngle = lightRotationAngle + 0.06f;
    }

    void uninitialize() {

        // destroy vao
        if (vao_sphere[0] != 0) {
            GLES31.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0] = 0;
        }

        // destroy position vbo
        if (vbo_sphere_position[0] != 0) {
            GLES31.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0] = 0;
        }

        // destroy normal vbo
        if (vbo_sphere_normal[0] != 0) {
            GLES31.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0] = 0;
        }

        // destroy element vbo
        if (vbo_sphere_element[0] != 0) {
            GLES31.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0] = 0;
        }

        if (shaderProgramObject != 0) {
            if (vertexShaderObject != 0) {
                GLES31.glDetachShader(shaderProgramObject, vertexShaderObject);
                GLES31.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }

            if (fragmentShaderObject != 0) {
                GLES31.glDetachShader(shaderProgramObject, fragmentShaderObject);
                GLES31.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        if (shaderProgramObject != 0) {
            GLES31.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }

}