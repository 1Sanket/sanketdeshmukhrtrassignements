//global variables
var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros=
{
VDG_ATTRIBUTE_POSITION:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;


var perspectiveProjectionMatrix;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var ldUniform,laUniform,lsUniform,lightPositionUniform;

var kaUniform, kdUniform ,ksUniform,materialShininessUniform;

var LKeyPressedUniform;


var requestAmimationFrame = 
window.requestAmimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

//onlaod function

var light_ambient = [0.0,0.0,0.0];
var light_diffuse = [1.0,1.0,1.0];
var light_specular = [1.0,1.0,1.0];
var light_position = [100.0,100.0,100.0,1.0];

var material_ambient =[[0.0215,0.1745,0.0215],
                             [0.135,0.2225,0.1575],
                             [0.05375,0.05,0.06625],
                             [0.25,0.20725,0.20725],
                             [0.1745,0.01175,0.01175],
                             [0.1,0.18725,0.1745],
                             [0.329412,0.223529,0.027451],
                             [0.2125,0.1275,0.054],
                             [0.25,0.25,0.25],
                             [0.19125,0.0735,0.0225],
                             [0.24725,0.1995,0.0745],
                             [0.19225,0.19225,0.19225],
                             [0.0,0.0,0.0],
                             [0.0,0.1,0.06],
                             [0.0,0.0,0.0],
                             [0.0,0.0,0.0],
                             [0.0,0.0,0.0],
                             [0.0,0.0,0.0],
                             [0.02,0.02,0.02],
                             [0.0,0.05,0.05],
                             [0.0,0.05,0.0],
                             [0.05,0.0,0.0],
                             [0.05,0.05,0.05],
                             [0.05,0.05,0.0]];
var material_diffuse = [[0.07568,0.61424,0.07568],
                            [0.54,0.89,0.63],
                            [0.18275,0.17,0.22525],
                            [1.0,0.829,0.829],
                            [0.61424,0.04136,0.04136],
                            [0.396,0.74151,0.69102],
                            [0.780392,0.568627,0.113725],
                            [0.714,0.4284,0.18144],
                            [0.4,0.4,0.4],
                            [0.7038,0.27048,0.0828],
                            [0.75164,0.60648,0.22648],
                            [0.50754,0.50754,0.50754],
                            [0.01,0.01,0.01],
                            [0.0,0.50980392,0.50980392],
                            [0.0,0.35,0.1],
                            [0.5,0.0,0.0],
                            [0.55,0.55,0.55],
                            [0.5,0.5,0.0],
                            [0.01,0.01,0.01],
                            [0.4,0.5,0.5],
                            [0.4,0.5,0.4],
                            [0.5,0.4,0.4],
                            [0.5,0.5,0.5],
                            [0.5,0.5,0.4]];
var material_specular = [[0.633,0.7278,0.633],
                                [0.316228,0.316228,0.316228],
                                [0.332741,0.328634,0.346435],
                                [0.296648,0.296648,0.296648],
                                [0.727811,0.626959,0.626959],
                                [0.297254,0.30829,0.306678],
                                [0.992157,0.941176,0.807843],
                                [0.393548,0.271906,0.166721],
                                [0.774597,0.774597,0.774597],
                                [0.256777,0.137622,0.086014],
                                [0.628281,0.555802,0.366065],
                                [0.508273,0.508273,0.508273],
                                [0.50,0.50,0.50],
                                [0.50196078,0.50196078,0.50196078],
                                [0.45,0.55,0.45],
                                [0.7,0.6,0.6],
                                [0.70,0.70,0.70],
                                [0.60,0.60,0.50],
                                [0.4,0.4,0.4],
                                [0.04,0.7,0.7],
                                [0.04,0.7,0.04],
                                [0.7,0.04,0.04],
                                [0.7,0.7,0.7],
                                [0.7,0.7,0.04]];
var material_shininess= [0.6 * 128,
                            0.1 * 128,
                            0.3 * 128,
                            0.088 * 128,
                            0.6 * 128,
                            0.1 * 128,
                            0.21794872 * 128,
                            0.2 * 128,
                            0.6 * 128,
                            0.1 * 128,
                            0.4 * 128,
                            0.4 * 128,
                            0.25 * 128,
                            0.25 * 128,
                            0.25 * 128,
                            0.25 * 128,
                            0.25 * 128,
                            0.25 * 128,
                            0.078125 * 128,
                            0.078125 * 128,
                            0.078125 * 128,
                            0.078125 * 128,
                            0.078125 * 128,
                            0.078125 * 128];


var bLKeyPressed = false;
var bXKeyPressed = true;
var bYKeyPressed = false;
var bZKeyPressed = false;
var angle_sphere= 0.0;

function main() {
    canvas = document.getElementById("AMC");

    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");

    console.log("Canvas Width is: "+canvas.width+"  Canvas Height is: "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;
    
    window.addEventListener("keydown",keyDown,false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",resize,false);

    init();
    resize();
    draw();
}


function toggleFullScreen() {
    
    var fullscreen_element = document.fullscreenElement ||
    document.webkitFullscreenElement||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    if(fullscreen_element == null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
         bFullscreen=true;
    }
    else
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
         bFullscreen=false;

    }
}

function init() {
    gl = canvas.getContext("webgl2");
    if(gl == null)
    {
        console.log("Failed to get rendering context for WebGL");
        return;
    }
    console.log(gl);
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;


    //vertex shader
    var vertexShaderSourceCode =
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normals;"+
    "out vec3 light_direction;"+
    "out vec3 viewer_vector;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;"+
    "transformed_normals = mat3(u_view_matrix * u_model_matrix)*vNormal;"+
    "light_direction = vec3(u_light_position)- eyeCoordinates.xyz;"+
    "viewer_vector = normalize(-eyeCoordinates.xyz);"+
    "}"+
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec3 transformed_normals;"+
    "in vec3 light_direction;"+
    "in vec3 viewer_vector;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "uniform int u_LKeyPressed;"+
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "vec3 phong_ads_color;"+
    "if( u_LKeyPressed == 1)"+
    "{"+
    "vec3 normalized_transformed_normals = normalize(transformed_normals);"+
    "vec3 normalized_light_direction = normalize(light_direction);"+
    "vec3 normalized_viewer_vector = normalize(viewer_vector);"+
    "vec3 ambient = u_La * u_Ka;"+
    "float tn_dot_ld= max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"+
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
    "vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"+
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"+
    "phong_ads_color = ambient + diffuse + specular;"+
    "}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0,1.0,1.0);"+
    "}"+
    "FragColor =vec4(phong_ads_color,1.0);"+
    "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_POSITION,"vPosition");
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    //linking
    gl.linkProgram(shaderProgramObject);
    if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //uniform binding
    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_view_matrix");

    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
   
    LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
    
    ldUniform = gl.getUniformLocation(shaderProgramObject,"u_Ld");
    lsUniform = gl.getUniformLocation(shaderProgramObject,"u_Ls");
    laUniform = gl.getUniformLocation(shaderProgramObject,"u_La");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject,"u_light_position");

    kdUniform = gl.getUniformLocation(shaderProgramObject,"u_Kd");
    kaUniform = gl.getUniformLocation(shaderProgramObject,"u_Ka");
    ksUniform = gl.getUniformLocation(shaderProgramObject,"u_Ks");
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject,"u_material_shininess");
    
    sphere=new Mesh();
    makeSphere(sphere,2.0,50,50);
  
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.CULL_FACE);


    gl.clearColor(0.5, 0.5, 0.5, 1.0); // Gray

    perspectiveProjectionMatrix = mat4.create();
}   

function resize() {

    if(bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        
    }
    else
    {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0,0, canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
   }


function degreesToRadian(degrees) {
    return ((degrees)*(Math.PI/180.0));
}


function draw() {

    gl.clear(gl.COLOR_BUFFER_BIT |gl.DEPTH_BUFFER_BIT);

    var angle_Radian  =  degreesToRadian(angle_sphere);
    var count = 0;
    for (var i = 0; i < 4; i++)
    {
        for (var j = 5; j >= 0; j--)
        {   
            gl.viewport(0 +(i * canvas.width/4),0 + (j* canvas.height/6), canvas.width/4,canvas.height/6);
            mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width/4)/parseFloat(canvas.height/6),0.1,100.0);
            
            gl.useProgram(shaderProgramObject);
            
            if(bLKeyPressed == true)
            {
                gl.uniform1i(LKeyPressedUniform,1);

                gl.uniform3fv(ldUniform,light_diffuse);
                gl.uniform3fv(laUniform,light_ambient);
                gl.uniform3fv(lsUniform,light_specular);

                if(bXKeyPressed)
                {
                    //x-axis
                    light_position[0] = 0.0; 
                    light_position[1] = 100.0 * Math.cos(angle_Radian); 
                    light_position[2] = 100.0 * Math.sin(angle_Radian); 
                }
                else if(bYKeyPressed)
                {
                    //y-axis
                    light_position[0] = 100.0 * Math.cos(-angle_Radian);    // -ve for left to right light rotation.
                    light_position[1] = 0.0;
                    light_position[2] = 100.0 * Math.sin(-angle_Radian);   // -ve for left to right light rotation.
                }
                else if(bZKeyPressed)
                {
                    //z-axis
                    light_position[0] = 100.0 * Math.cos(angle_Radian); 
                    light_position[1] = 100.0 * Math.sin(angle_Radian);
                    light_position[2] = 0.0; 
                }

                gl.uniform4fv(lightPositionUniform,light_position);

                gl.uniform3fv(kaUniform,material_ambient[count]);
                gl.uniform3fv(ksUniform,material_specular[count]);
                gl.uniform3fv(kdUniform,material_diffuse[count]);
                gl.uniform1f(materialShininessUniform,material_shininess[count]);
                
            }
            else
            {
                gl.uniform1i(LKeyPressedUniform,0);
            }


            modelMatrix= mat4.create();
            viewMatrix = mat4.create();
            mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-6.0]);

           
            gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
            gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
            gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);

            sphere.draw();
            gl.useProgram(null);
            count = count + 1;
        }
    }
    //update
    update();
    requestAmimationFrame(draw,canvas);
}

function update()
{
    angle_sphere = angle_sphere + 2.0;
    if(angle_sphere >=360.0)
            angle_sphere -= 360.0;

}

function uninitialize(argument) {
   

    if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject = null;
        }

        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}



function keyDown(event) {
    
    switch(event.key)
    {
        case 'f':
        case 'F':
            toggleFullScreen();
            break;
        case 'l':
        case 'L':
                if(bLKeyPressed==false)
                    bLKeyPressed=true;
                else
                    bLKeyPressed=false;
            break;
        case 'x':
        case 'X':
                bXKeyPressed = true;
                bYKeyPressed = false;
                bZKeyPressed = false;
            break;
        case 'y':
        case 'Y':
                bXKeyPressed = false;
                bYKeyPressed = true;
                bZKeyPressed = false;
            break;
        case 'z':
        case 'Z':
                bXKeyPressed = false;
                bYKeyPressed = false;
                bZKeyPressed = true;
            break;
    }

    switch(event.keyCode)
    {
        case 27:
            uninitialize();
            window.close();
        break;
    }
}

function mouseDown()
{
    //code
}