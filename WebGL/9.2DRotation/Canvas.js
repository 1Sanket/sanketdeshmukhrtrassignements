
// Two colored shapes


var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
  VDG_ATTRIBUTE_VERTEX:0,
  VDG_ATTRIBUTE_COLOR:1,
  VDG_ATTRIBUTE_NORMAL:2,
  VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Triangle;
var vbo_Triangle_Position;
var vbo_Triangle_Color;
var vao_Square;
var vbo_Square_Position;
var mvpUniform;

var angleSquare = 0.0;
var angleTriangle= 0.0;

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame ||
                           window.webkitCancelAnimationFrame ||
                           window.webkitCancelRequestAnimationFrame ||
                           window.mozCancelRequestAnimationFrame ||
                           window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame ||
                           window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame ||
                           window.msCancelAnimationFrame ;


// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    // print canvas width and height on console
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

     // register keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL

    init();

    resize();

    draw();
}


function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();

     bFullscreen = true;
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();

     bFullscreen = false;
}

}

function init()
{
    // Get WebGL 2.0 context. 

    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
       console.log("Failed to get WebGL rendering context");
       return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex Shader 

    var vertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec4 vColor;" +
    "out vec4 outColor;"+
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition; " +
    "outColor=vColor;"+
    "}" ;

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 

    var fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec4 outColor;" +
    "out vec4 FragColor;" +
    "void main()" +
    "{" +
    "FragColor = outColor;"+
    "}" ;

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program

    shaderProgramObject = gl.createProgram();

    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);

    // pre-link bindind of shader program object with vertex shader attributes 

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_COLOR,"vColor");

    // linking 

    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get mvp uniform location

    mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

   // Triangle vertices

    var triangleVertices = new Float32Array([
        0.0,1.0,0.0,
        -1.0,-1.0,0.0,
        1.0,-1.0,0.0
    ]);

    vao_Triangle = gl.createVertexArray();
    gl.bindVertexArray(vao_Triangle);
    
    vbo_Triangle_Position= gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Triangle_Position);
    gl.bufferData(gl.ARRAY_BUFFER,triangleVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

     
    var colorCoordinates = new Float32Array([
    
        1.0,0.0,0.0, // red 
        0.0,1.0,0.0, // Green
        0.0,0.0,1.0 // Blue
    ]);

    vbo_Triangle_Color= gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Triangle_Color);
    gl.bufferData(gl.ARRAY_BUFFER,colorCoordinates,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    gl.bindVertexArray(null);

    
    // Square

    var squareVertices = new Float32Array([
        -1.0,1.0,0.0,
        -1.0,-1.0,0.0,
         1.0,-1.0,0.0,
        1.0,1.0,0.0
    ]);

    vao_Square = gl.createVertexArray();
    gl.bindVertexArray(vao_Square);
    
    vbo_Square_Position= gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Square_Position);
    gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    gl.vertexAttrib3f(WebGLMacros.VDG_ATTRIBUTE_COLOR,0.258824, 0.258824, 0.435294);

    gl.bindVertexArray(null);

    gl.clearColor(0.0,0.0,0.0,1.0);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    if(bFullscreen == true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;        
    }

    gl.viewport(0,0,canvas.width,canvas.height);

    // orthographic projection = left , right ,bottom ,top , near ,far

     //if (canvas.width <= canvas.height)
      //  mat4.ortho(perspectiveProjectionMatrix,-100.0,100.0,(-100.0 * (canvas.height/canvas.width)),(100.0 *(canvas.height/canvas.width)),-100.0,100.0);
    // else
    //    mat4.ortho(perspectiveProjectionMatrix,-100.0,100.0,(-100.0 * (canvas.width/canvas.height)),(100.0 *(canvas.width/canvas.height)),-100.0,100.0);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width/canvas.height),0.1,100.0);
    
}


function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);
    
    var modelViewMatrix= mat4.create();
    var modelViewProjectionMatrix= mat4.create();
    var rotationMatrix = mat4.create();

    mat4.translate(modelViewMatrix,modelViewMatrix,[-1.5,0.0,-4.0]);

    mat4.rotateY(rotationMatrix,rotationMatrix,degreeToRadian(angleTriangle));
    
    // Multiply modelview and rotation matrix

    mat4.multiply(modelViewMatrix,modelViewMatrix,rotationMatrix);

    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindVertexArray(vao_Triangle);

    gl.drawArrays(gl.TRIANGLES,0,3);

    gl.bindVertexArray(null);

    // modelViewMatrix = mat4.identity();
    // modelViewProjectionMatrix = mat4.identity();

    mat4.identity(modelViewMatrix);
    mat4.identity(modelViewProjectionMatrix);
    mat4.identity(rotationMatrix);

    // Translate model view matrix.

    mat4.translate(modelViewMatrix,modelViewMatrix,[1.5,0.0,-4.0]);

    mat4.rotateX(rotationMatrix,rotationMatrix,degreeToRadian(angleSquare));
    
    // Multiply modelview and rotation matrix

    mat4.multiply(modelViewMatrix,modelViewMatrix,rotationMatrix);

    // multiply modelview and perspective matrix to get modelview projection matrix 

    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);

    // pass modelviewProjection matrix to the vertex shader in 'u_mvp_matrix' shader variable
    // whose position value we already calculated by using glGetUniformLocation()
    
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindVertexArray(vao_Square);
    
    gl.drawArrays(gl.TRIANGLE_FAN,0,4);
    
    gl.bindVertexArray(null);

    gl.useProgram(null);


    updateAngle();

    // Animation loop
    requestAnimationFrame(draw,canvas);
}

function updateAngle()
{
    angleTriangle = angleTriangle + 0.7;
	angleSquare = angleSquare - 0.7;
	if (angleTriangle >= 360.0)
		angleTriangle = 0.0;
	if (angleSquare <= 0.0)
		angleSquare = 360.0;
}

function degreeToRadian(degrees)
{
    return (degrees * (Math.PI/180.0));
}

function keyDown(event)
{
    switch(event.keyCode)
       {
           case 27: // escape
                   uninitialize();
                   window.close();
                   break;
           
           case 70 : // for 'F' or 'f'
                   togglefullscreen();
                   break;
	}
}

function mouseDown()
{
    
}

function uninitialize()
{
    if(vao_Triangle)
    {
        gl.deleteVertexArray(vao_Triangle);
        vao_Triangle = null;
    }

    if(vbo_Triangle_Position)
    {
        gl.deleteBuffer(vbo_Triangle_Position);
        vbo_Triangle_Position = null;
    }

    if(vbo_Triangle_Color)
    {
        gl.deleteBuffer(vbo_Triangle_Color);
        vbo_Triangle_Color = null;
    }

    if(vao_Square)
    {
        gl.deleteVertexArray(vao_Square);
        vao_Square = null;
    }

    if(vbo_Square_Position)
    {
        gl.deleteBuffer(vbo_Square_Position);
        vbo_Square_Position = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
        gl.detachShader(shaderProgramObject,fragmentShaderObject);
        gl.deleteShader(fragmentShaderObject);
        fragmentShaderObject = null;
        }

        if(vertexShaderObject)
        {
        gl.detachShader(shaderProgramObject,vertexShaderObject);
        gl.deleteShader(vertexShaderObject);
        vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}