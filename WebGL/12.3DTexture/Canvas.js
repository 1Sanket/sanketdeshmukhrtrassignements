
// Two colored shapes


var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
  VDG_ATTRIBUTE_VERTEX:0,
  VDG_ATTRIBUTE_COLOR:1,
  VDG_ATTRIBUTE_NORMAL:2,
  VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Cube;
var vbo_Cube_Position;
var vbo_Cube_Texture;

var vao_Pyramid;
var vbo_Pyramid_Position;
var vbo_Pyramid_Texture;

var cube_Texture=0;
var pyramid_Texture =0;

var uniform_texture0_sampler;

var mvpUniform;

var perspectiveProjectionMatrix;

var anglePyramid =0.0;
var angleCube =0.0;


var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame ||
                           window.webkitCancelAnimationFrame ||
                           window.webkitCancelRequestAnimationFrame ||
                           window.mozCancelRequestAnimationFrame ||
                           window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame ||
                           window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame ||
                           window.msCancelAnimationFrame ;


// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    // print canvas width and height on console
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

     // register keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL

    init();

    resize();

    draw();
}


function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();

     bFullscreen = true;
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();

     bFullscreen = false;
}

}

function init()
{
    // Get WebGL 2.0 context. 

    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
       console.log("Failed to get WebGL rendering context");
       return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex Shader 

    var vertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec2 vTexture0_Coord;" +
    "out vec2 out_texture0_coord;"+
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition; " +
    "out_texture0_coord = vTexture0_Coord;"+
    "}" ;


    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 

    var fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec2 out_texture0_coord;" +
    "uniform highp sampler2D u_texture0_sampler;"+
    "out vec4 FragColor;" +
    "void main()" +
    "{" +
    "FragColor = texture(u_texture0_sampler,out_texture0_coord);"+
    "}" ;

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program

    shaderProgramObject = gl.createProgram();

    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);

    // pre-link bindind of shader program object with vertex shader attributes 

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

    // linking 

    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }


    // get mvp uniform location

    mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

    uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject,"u_texture0_sampler");


    var pyramidVertices=new Float32Array([
        0.0, 1.0, 0.0,    // front-top
        -1.0, -1.0, 1.0,  // front-left
        1.0, -1.0, 1.0,   // front-right
        
        0.0, 1.0, 0.0,    // right-top
        1.0, -1.0, 1.0,   // right-left
        1.0, -1.0, -1.0,  // right-right
        
        0.0, 1.0, 0.0,    // back-top
        1.0, -1.0, -1.0,  // back-left
        -1.0, -1.0, -1.0, // back-right
        
        0.0, 1.0, 0.0,    // left-top
        -1.0, -1.0, -1.0, // left-left
        -1.0, -1.0, 1.0   // left-right
       ]);


    var pyramidTexcoords=new Float32Array([
         0.5, 1.0, // front-top
         0.0, 0.0, // front-left
         1.0, 0.0, // front-right
         
         0.5, 1.0, // right-top
         1.0, 0.0, // right-left
         0.0, 0.0, // right-right
         
         0.5, 1.0, // back-top
         1.0, 0.0, // back-left
         0.0, 0.0, // back-right
         
         0.5, 1.0, // left-top
         0.0, 0.0, // left-left
         1.0, 0.0, // left-right
         ]);


    var cubeVertices=new Float32Array([
     // top surface
     1.0, 1.0,-1.0,  // top-right of top
     -1.0, 1.0,-1.0, // top-left of top
     -1.0, 1.0, 1.0, // bottom-left of top
     1.0, 1.0, 1.0,  // bottom-right of top
     
     // bottom surface
     1.0,-1.0, 1.0,  // top-right of bottom
     -1.0,-1.0, 1.0, // top-left of bottom
     -1.0,-1.0,-1.0, // bottom-left of bottom
     1.0,-1.0,-1.0,  // bottom-right of bottom
     
     // front surface
     1.0, 1.0, 1.0,  // top-right of front
     -1.0, 1.0, 1.0, // top-left of front
     -1.0,-1.0, 1.0, // bottom-left of front
     1.0,-1.0, 1.0,  // bottom-right of front
     
     // back surface
     1.0,-1.0,-1.0,  // top-right of back
     -1.0,-1.0,-1.0, // top-left of back
     -1.0, 1.0,-1.0, // bottom-left of back
     1.0, 1.0,-1.0,  // bottom-right of back
     
     // left surface
     -1.0, 1.0, 1.0, // top-right of left
     -1.0, 1.0,-1.0, // top-left of left
     -1.0,-1.0,-1.0, // bottom-left of left
     -1.0,-1.0, 1.0, // bottom-right of left
     
     // right surface
     1.0, 1.0,-1.0,  // top-right of right
     1.0, 1.0, 1.0,  // top-left of right
     1.0,-1.0, 1.0,  // bottom-left of right
     1.0,-1.0,-1.0,  // bottom-right of right
     ]);


//  convertt all 1s And -1s to -0.75 or +0.75

for(var i=0;i<72;i++)
{
if(cubeVertices[i]<0.0)
cubeVertices[i]=cubeVertices[i]+0.25;
else if(cubeVertices[i]>0.0)
cubeVertices[i]=cubeVertices[i]-0.25;
else
cubeVertices[i]=cubeVertices[i]; 
}


var cubeTexcoords=new Float32Array([
      0.0,0.0,
      1.0,0.0,
      1.0,1.0,
      0.0,1.0,
      
      0.0,0.0,
      1.0,0.0,
      1.0,1.0,
      0.0,1.0,
      
      0.0,0.0,
      1.0,0.0,
      1.0,1.0,
      0.0,1.0,
      
      0.0,0.0,
      1.0,0.0,
      1.0,1.0,
      0.0,1.0,
      
      0.0,0.0,
      1.0,0.0,
      1.0,1.0,
      0.0,1.0,
      
      0.0,0.0,
      1.0,0.0,
      1.0,1.0,
      0.0,1.0,
      ]);


    //Load pyramid Texture
    
        pyramid_Texture = gl.createTexture();
        pyramid_Texture.image = new Image();
        pyramid_Texture.image.src="stone.png";
        pyramid_Texture.image.onload = function ()
        {
          gl.bindTexture(gl.TEXTURE_2D,pyramid_Texture);
          gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
          gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,pyramid_Texture.image);
          gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
          gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
          gl.bindTexture(gl.TEXTURE_2D,null);
    
        }

    // Load Cube Texture

        cube_Texture = gl.createTexture();
        cube_Texture.image = new Image();
        cube_Texture.image.src="vijay_kundali.png";
        cube_Texture.image.onload = function ()
        {
          gl.bindTexture(gl.TEXTURE_2D,cube_Texture);
          gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
          gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,cube_Texture.image);
          gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
          gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
          gl.bindTexture(gl.TEXTURE_2D,null);
    
        }
    
    // Pyramid

    vao_Pyramid=gl.createVertexArray();
    gl.bindVertexArray(vao_Pyramid);
    
    vbo_Pyramid_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Pyramid_Position);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    vbo_Pyramid_Texture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Pyramid_Texture);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidTexcoords,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
                           2, // 2 is for S and T co-ordinates in our pyramidTexcoords array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    gl.bindVertexArray(null);

   
    // Cube Code

    vao_Cube=gl.createVertexArray();
    gl.bindVertexArray(vao_Cube);
    
    vbo_Cube_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Cube_Position);
    gl.bufferData(gl.ARRAY_BUFFER,cubeVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    vbo_Cube_Texture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Cube_Texture);
    gl.bufferData(gl.ARRAY_BUFFER,cubeTexcoords,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
                           2, // 2 is for S and T co-ordinates in our pyramidTexcoords array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    gl.bindVertexArray(null);

    //gl.clearDepth(1.0);
    
    gl.enable(gl.DEPTH_TEST);
    
    gl.enable(gl.CULL_FACE);
    

    gl.clearColor(0.0,0.0,0.0,1.0);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    if(bFullscreen == true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;        
    }

    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width/canvas.height),0.1,100.0);
    
}


function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);
    
    var modelViewMatrix= mat4.create();
    var modelViewProjectionMatrix= mat4.create();

    mat4.translate(modelViewMatrix,modelViewMatrix,[-1.5,0.0,-5.0]);

    mat4.rotateY(modelViewMatrix,modelViewMatrix,degreeToRadian(anglePyramid));

    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);

    // Bind with texture

    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindTexture(gl.TEXTURE_2D,pyramid_Texture);

    gl.uniform1i(uniform_texture0_sampler, 0);
    
    gl.bindVertexArray(vao_Pyramid);

    gl.drawArrays(gl.TRIANGLES,0,12);

    gl.bindVertexArray(null);

    mat4.identity(modelViewMatrix);
    mat4.identity(modelViewProjectionMatrix);
    
    mat4.translate(modelViewMatrix,modelViewMatrix,[1.5,0.0,-5.0]);

    mat4.rotateX(modelViewMatrix,modelViewMatrix,degreeToRadian(angleCube));
    mat4.rotateY(modelViewMatrix,modelViewMatrix,degreeToRadian(angleCube));
    mat4.rotateZ(modelViewMatrix,modelViewMatrix,degreeToRadian(angleCube));

    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    
        // Bind with texture
    
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);


    //bind with texture

    gl.bindTexture(gl.TEXTURE_2D,cube_Texture);
    
    gl.uniform1i(uniform_texture0_sampler, 0);

    gl.bindVertexArray(vao_Cube);
    
        gl.drawArrays(gl.TRIANGLE_FAN,0,4);
        gl.drawArrays(gl.TRIANGLE_FAN,4,4);
        gl.drawArrays(gl.TRIANGLE_FAN,8,4);
        gl.drawArrays(gl.TRIANGLE_FAN,12,4);
        gl.drawArrays(gl.TRIANGLE_FAN,16,4);
        gl.drawArrays(gl.TRIANGLE_FAN,20,4);
    
    gl.bindVertexArray(null);

    gl.useProgram(null);

    updateAngle();

    // Animation loop
    requestAnimationFrame(draw,canvas);
}

function updateAngle()
{
    anglePyramid = anglePyramid + 2.0;
    if(anglePyramid >= 360.0) 
      anglePyramid = anglePyramid - 360.0;

      angleCube = angleCube + 2.0;
      if(angleCube >= 360.0) 
        angleCube = angleCube - 360.0;

}

function degreeToRadian(degrees)
{
    return (degrees * (Math.PI/180));
}

function keyDown(event)
{
    switch(event.keyCode)
       {
           case 27: // escape
                   uninitialize();
                   window.close();
                   break;
           
           case 70 : // for 'F' or 'f'
                   togglefullscreen();
                   break;
	}
}

function mouseDown()
{
    
}

function uninitialize()
{

    if(pyramid_Texture)
    {
        gl.deleteTexture(pyramid_Texture);
        pyramid_Texture =0;
    }

    if(cube_Texture)
    {
        gl.deleteTexture(cube_Texture);
        cube_Texture =0;
    }

    if(vao_Pyramid)
    {
        gl.deleteVertexArray(vao_Pyramid);
        vao_Pyramid = null;
    }

    if(vbo_Pyramid_Position)
    {
        gl.deleteBuffer(vbo_Pyramid_Position);
        vbo_Pyramid_Position = null;
    }

    if(vbo_Pyramid_Texture)
    {
        gl.deleteBuffer(vbo_Pyramid_Texture);
        vbo_Pyramid_Texture = null;
    }


    if(vao_Cube)
    {
        gl.deleteVertexArray(vao_Cube);
        vao_Cube = null;
    }

    if(vbo_Cube_Position)
    {
        gl.deleteBuffer(vbo_Cube_Position);
        vbo_Cube_Position = null;
    }

    if(vbo_Cube_Texture)
    {
        gl.deleteBuffer(vbo_Cube_Texture);
        vbo_Cube_Texture = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
        gl.detachShader(shaderProgramObject,fragmentShaderObject);
        gl.deleteShader(fragmentShaderObject);
        fragmentShaderObject = null;
        }

        if(vertexShaderObject)
        {
        gl.detachShader(shaderProgramObject,vertexShaderObject);
        gl.deleteShader(vertexShaderObject);
        vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}