
// Two colored shapes


var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
  VDG_ATTRIBUTE_VERTEX:0,
  VDG_ATTRIBUTE_COLOR:1,
  VDG_ATTRIBUTE_NORMAL:2,
  VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Smiley;
var vbo_Smiley_Position;
var vbo_Smiley_Texture;

var checkerboard_Texture=0;
var uniform_texture0_sampler;

var mvpUniform;

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame ||
                           window.webkitCancelAnimationFrame ||
                           window.webkitCancelRequestAnimationFrame ||
                           window.mozCancelRequestAnimationFrame ||
                           window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame ||
                           window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame ||
                           window.msCancelAnimationFrame ;


// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    // print canvas width and height on console
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

     // register keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL

    init();

    resize();

    draw();
}


function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();

     bFullscreen = true;
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();

     bFullscreen = false;
}

}

function init()
{
    // Get WebGL 2.0 context. 

    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
       console.log("Failed to get WebGL rendering context");
       return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex Shader 

    var vertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec2 vTexture0_Coord;" +
    "out vec2 out_texture0_coord;"+
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition; " +
    "out_texture0_coord = vTexture0_Coord;"+
    "}" ;


    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 

    var fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec2 out_texture0_coord;" +
    "uniform highp sampler2D u_texture0_sampler;"+
    "out vec4 FragColor;" +
    "void main()" +
    "{" +
    "FragColor = texture(u_texture0_sampler,out_texture0_coord);"+
    "}" ;

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program

    shaderProgramObject = gl.createProgram();

    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);

    // pre-link bindind of shader program object with vertex shader attributes 

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

    // linking 

    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }


    // get mvp uniform location

    mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

    uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject,"u_texture0_sampler");

   // Square vertices

    var squareVertices = new Float32Array([
        1.0, 1.0, 0.0,
        
            
            -1.0, 1.0, 0.0,
        
            
            -1.0, -1.0, 0.0,
        
            
            1.0, -1.0, 0.0
    ]);

     
    var squareTextureCoordinates = new Float32Array([
    
        1.0, 0.0,
        
            0.0, 0.0,
        
            0.0, 1.0,
        
            1.0, 1.0
    
    ]);


    checkerboard_Texture=loadGLTexture();
    
    vao_Smiley=gl.createVertexArray();
    gl.bindVertexArray(vao_Smiley);
    
    vbo_Smiley_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Smiley_Position);
    gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    vbo_Smiley_Texture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Smiley_Texture);
    //gl.bufferData(gl.ARRAY_BUFFER,squareTextureCoordinates,gl.STATIC_DRAW);
    gl.bufferData(gl.ARRAY_BUFFER,0,gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
                           2, // 2 is for S and T co-ordinates in our pyramidTexcoords array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    gl.bindVertexArray(null);

    //gl.clearDepth(1.0);
    
    gl.enable(gl.DEPTH_TEST);
    
    //gl.depthFunc(gl.LEQUAL);
    
    //gl.hint(gl.PERSPECTIVE_CORRECTION_HINT,gl.NICEST);
    

    gl.clearColor(0.0,0.0,0.0,1.0);

    perspectiveProjectionMatrix = mat4.create();
}

function loadGLTexture()
{
	var i = 0, j = 0, c = 0;
	
	
	var TEX_WIDTH  =  64;
	var TEX_HEIGHT =  64;
	var DEPTH = 4;
	var checker_board_texture_array = new Uint8Array(TEX_HEIGHT*TEX_WIDTH*DEPTH);
	
	for (i = 0; i < TEX_HEIGHT; i++)
	{
		for (j = 0; j < TEX_WIDTH; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

			checker_board_texture_array[0 + DEPTH*(i + TEX_HEIGHT*(j))] = c;
			checker_board_texture_array[1 + DEPTH*(i + TEX_HEIGHT*(j))] = c;
			checker_board_texture_array[2 + DEPTH*(i + TEX_HEIGHT*(j))] = c;
			checker_board_texture_array[3 + DEPTH*(i + TEX_HEIGHT*(j))] = 255;
		}
	}
	
	var texture=gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_ALIGNMENT, true);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, TEX_WIDTH, TEX_HEIGHT, 0, gl.RGBA, gl.UNSIGNED_BYTE,checker_board_texture_array);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.bindTexture(gl.TEXTURE_2D, null);
	
	return texture;
}

function resize()
{
    if(bFullscreen == true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;        
    }

    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width/canvas.height),0.1,100.0);
    
}


function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    var squareVertices = new Float32Array
	([
    

            0.0, 1.0, 0.0,
    
            -2.0, 1.0, 0.0,
    
            -2.0, -1.0, 0.0,
    
            0.0, -1.0, 0.0,
    
    
            2.414210, 1.0, -1.41421,
    
            1.0, 1.0, 0.0,
    
            1.0, -1.0, 0.0,
    
            2.41421, -1.0, -1.41421

        ]);


    var squareTexcoords =  new Float32Array
	([

        0.0,0.0,
		0.0,1.0,
		1.0,1.0,
		1.0,0.0,

		////// for tilted square
		0.0,0.0,
		0.0,1.0,
		1.0,1.0,
		1.0,0.0
    
    ]);

    gl.useProgram(shaderProgramObject);
    
    var modelViewMatrix= mat4.create();
    var modelViewProjectionMatrix= mat4.create();

    mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);

    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);

    // Bind with texture

    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);


        //Bind with texture

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,checkerboard_Texture);
        gl.uniform1i(uniform_texture0_sampler,0);
    
        // *** Bind Square Vao ***//
    
        gl.bindVertexArray(vao_Smiley);
        
        gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Smiley_Position);

        gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.DYNAMIC_DRAW);

        gl.bindBuffer(gl.ARRAY_BUFFER,null);

        gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Smiley_Texture);

        gl.bufferData(gl.ARRAY_BUFFER,squareTexcoords,gl.DYNAMIC_DRAW);

        gl.bindBuffer(gl.ARRAY_BUFFER,null);

        gl.drawArrays(gl.TRIANGLE_FAN,0,4); // draw Straight Checkerboard
        gl.drawArrays(gl.TRIANGLE_FAN,4,4); // draw Tilted Checkerboard
    
        // unbind vao
    
        gl.bindVertexArray(null);
    
        gl.useProgram(null);

    // gl.bindTexture(gl.TEXTURE_2D,checkerboard_Texture);

    // gl.uniform1i(uniform_texture0_sampler, 0);
    
    // gl.bindVertexArray(vao_Smiley);

    // gl.drawArrays(gl.TRIANGLE_FAN,0,4);

    // gl.bindVertexArray(null);
    
    // gl.useProgram(null);

    // Animation loop
    requestAnimationFrame(draw,canvas);
}


function keyDown(event)
{
    switch(event.keyCode)
       {
           case 27: // escape
                   uninitialize();
                   window.close();
                   break;
           
           case 70 : // for 'F' or 'f'
                   togglefullscreen();
                   break;
	}
}

function mouseDown()
{
    
}

function uninitialize()
{
    if(vao_Smiley)
    {
        gl.deleteVertexArray(vao_Smiley);
        vao_Smiley = null;
    }

    if(vbo_Smiley_Position)
    {
        gl.deleteBuffer(vbo_Smiley_Position);
        vbo_Smiley_Position = null;
    }

    if(vbo_Smiley_Texture)
    {
        gl.deleteBuffer(vbo_Smiley_Texture);
        vbo_Smiley_Texture = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
        gl.detachShader(shaderProgramObject,fragmentShaderObject);
        gl.deleteShader(fragmentShaderObject);
        fragmentShaderObject = null;
        }

        if(vertexShaderObject)
        {
        gl.detachShader(shaderProgramObject,vertexShaderObject);
        gl.deleteShader(vertexShaderObject);
        vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}