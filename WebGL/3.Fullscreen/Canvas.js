
var canvas = null;
var context = null;

// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    // print canvas width and height on console
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    // get 2D context
    context=canvas.getContext("2d");
    if(!context)
        console.log("Obtaining 2D Context Failed\n");
    else
        console.log("Obtaining 2D Context Succeeded\n");
    
    // fill canvas with black ccolor
    context.fillStyle="black"; 
    context.fillRect(0,0,canvas.width,canvas.height);

    drawText("Hello World !!!!!");

     // register keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
}

function drawText(text)
{
      // centrally place  the text
    context.textAlign="center"; // center horizontally
    context.textBaseline="middle"; // center vertically

    // set text font
    context.font="48px sans-serif";
    
    // set text color
    context.fillStyle="white"; 
    
    // display the text in the  center
    context.fillText(text,canvas.width/2,canvas.height/2);
}

function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();
}

}

function keyDown(event)
{
    switch(event.keyCode)
       {
           case 70 : // for 'F' or 'f'
                   togglefullscreen();

                // repaint 

                drawText("Hello World !!!!!");
                break;
	}
}

function mouseDown()
{
    
}
