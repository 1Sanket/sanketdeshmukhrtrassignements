
// Two colored shapes


var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
  VDG_ATTRIBUTE_VERTEX:0,
  VDG_ATTRIBUTE_COLOR:1,
  VDG_ATTRIBUTE_NORMAL:2,
  VDG_ATTRIBUTE_TEXTURE0:3,
};


// For per vertex light

var perVertex_VertexShaderObject;
var perVertex_FragmentShaderObject;
var perVertex_ShaderProgramObject;

// For per fragment light

var perFragment_VertexShaderObject;
var perFragment_FragmentShaderObject;
var perFragment_ShaderProgramObject;


var light_ambient = [0.0,0.0,0.0];
var light_diffuse = [1.0,1.0,1.0];
var light_specular = [1.0,1.0,1.0];
var light_position = [100.0,100.0,100.0,1.0];

var material_ambient = [0.0,0.0,0.0];
var material_diffuse = [1.0,1.0,1.0];
var material_specular = [1.0,1.0,1.0];
var material_shininess = 50.0 ;

var bPerVertexLighting = true ;
var bPerFragmentLighting ;
var isVKeyPressed = false;
var isFKeyPressed = false;

var sphere = null;

// For per vertex shader 

var perVertex_modelMatrixUniform,perVertex_viewMatrixUniform,perVertex_projectionMatrixUniform;
var perVertex_laUniform,perVertex_ldUniform,perVertex_lsUniform,perVertex_lightPositionUniform;
var perVertex_kaUniform,perVertex_kdUniform,perVertex_ksUniform,perVertex_materialShininessUniform;
var perVertex_LKeyPressedUniform;

// For per fragment shader

var perFragment_modelMatrixUniform,perFragment_viewMatrixUniform,perFragment_projectionMatrixUniform;
var perFragment_laUniform,perFragment_ldUniform,perFragment_lsUniform,perFragment_lightPositionUniform;
var perFragment_kaUniform,perFragment_kdUniform,perFragment_ksUniform,perFragment_materialShininessUniform;
var perFragment_LKeyPressedUniform;

var perspectiveProjectionMatrix;

var bLKeyPressed = false;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame ||
                           window.webkitCancelAnimationFrame ||
                           window.webkitCancelRequestAnimationFrame ||
                           window.mozCancelRequestAnimationFrame ||
                           window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame ||
                           window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame ||
                           window.msCancelAnimationFrame ;


// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    // print canvas width and height on console
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

     // register keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL

    init();

    resize();

    draw();
}


function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();

     bFullscreen = true;
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();

     bFullscreen = false;
}

}

function init()
{
    // Get WebGL 2.0 context. 

    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
       console.log("Failed to get WebGL rendering context");
       return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //Per vertex vertex Shader 

    var perVertex_vertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "out vec3 phong_ads_color;"+
    "void main(void)" +
    "{" +
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition ;"+
    "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
    "vec3 light_direction = normalize(vec3(u_light_position)-eyeCoordinates.xyz) ;"+
    "float tn_dot_ld = max(dot(transformed_normals,light_direction),0.0) ;"+
    "vec3 ambient = u_La * u_Ka ;"+
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld ;"+
    "vec3 reflection_vector = reflect(-light_direction,transformed_normals) ;"+
    "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);"+
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,viewer_vector),0.0),u_material_shininess) ;"+
    "phong_ads_color = ambient + diffuse + specular;"+
    "}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0,1.0,1.0);"+
    "}"+
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition; " +
    "}" ;


    perVertex_VertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(perVertex_VertexShaderObject,perVertex_vertexShaderSourceCode);
    gl.compileShader(perVertex_VertexShaderObject);

    if (gl.getShaderParameter(perVertex_VertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(perVertex_VertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // per vertex fragment shader 

    var perVertex_fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec3 phong_ads_color;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "FragColor = vec4(phong_ads_color,1.0);"+
    "}" ;

    perVertex_FragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(perVertex_FragmentShaderObject,perVertex_fragmentShaderSourceCode);
    gl.compileShader(perVertex_FragmentShaderObject);

    if(gl.getShaderParameter(perVertex_FragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(perVertex_FragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program

    perVertex_ShaderProgramObject = gl.createProgram();

    gl.attachShader(perVertex_ShaderProgramObject,perVertex_VertexShaderObject);
    gl.attachShader(perVertex_ShaderProgramObject,perVertex_FragmentShaderObject);

    // pre-link bindind of shader program object with vertex shader attributes 

    gl.bindAttribLocation(perVertex_ShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

    gl.bindAttribLocation(perVertex_ShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    // linking 

    gl.linkProgram(perVertex_ShaderProgramObject);

    if (gl.getProgramParameter(perVertex_ShaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(perVertex_ShaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }


    // get mvp uniform location


    perVertex_modelMatrixUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_model_matrix");

    perVertex_viewMatrixUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_view_matrix");

    perVertex_projectionMatrixUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_projection_matrix");

    perVertex_LKeyPressedUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_LKeyPressed");

    perVertex_laUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_La");
    
    perVertex_ldUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Ld");

    perVertex_lsUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Ls");

    perVertex_lightPositionUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_light_position");

    perVertex_kaUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Ka");
    
    perVertex_kdUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Kd");

    perVertex_ksUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Ks");

    perVertex_materialShininessUniform = gl.getUniformLocation(perVertex_ShaderProgramObject,"u_material_shininess");

    

    //per fragment vertex Shader 

    var perFragment_vertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normals;"+
    "out vec3 light_direction;"+
    "out vec3 viewer_vector;"+
    "void main(void)" +
    "{" +
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition ;"+
    "transformed_normals = mat3(u_view_matrix * u_model_matrix) *  vNormal;"+
    "light_direction = vec3(u_light_position) - eyeCoordinates.xyz ;"+
    "viewer_vector = - eyeCoordinates.xyz ;"+
    "}"+
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition; " +
    "}" ;


    perFragment_VertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(perFragment_VertexShaderObject,perFragment_vertexShaderSourceCode);
    gl.compileShader(perFragment_VertexShaderObject);

    if (gl.getShaderParameter(perFragment_VertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(perFragment_VertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 

    var perFragment_fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec3 transformed_normals;" +
    "in vec3 light_direction;"+
    "in vec3 viewer_vector;"+
    "out vec4 FragColor;" +
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "uniform int u_LKeyPressed;"+
    "void main(void)" +
    "{" +
    "vec3 phong_ads_color;"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec3 normalized_transformed_normals = normalize(transformed_normals);"+
    "vec3 normalized_light_direction = normalize(light_direction);"+
    "vec3 normalized_viewer_vector = normalize(viewer_vector);"+
    "vec3 ambient = u_La * u_Ka;"+
    "float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"+
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
    "vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"+
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"+
    "phong_ads_color = ambient + diffuse + specular ;"+
    "}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0,1.0,1.0) ;"+
    "}"+
    "FragColor = vec4(phong_ads_color,1.0);"+
    "}" ;

    perFragment_FragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(perFragment_FragmentShaderObject,perFragment_fragmentShaderSourceCode);
    gl.compileShader(perFragment_FragmentShaderObject);

    if(gl.getShaderParameter(perFragment_FragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(perFragment_FragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program

    perFragment_ShaderProgramObject = gl.createProgram();

    gl.attachShader(perFragment_ShaderProgramObject,perFragment_VertexShaderObject);
    gl.attachShader(perFragment_ShaderProgramObject,perFragment_FragmentShaderObject);

    // pre-link bindind of shader program object with vertex shader attributes 

    gl.bindAttribLocation(perFragment_ShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(perFragment_ShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    // linking 

    gl.linkProgram(perFragment_ShaderProgramObject);

    if (gl.getProgramParameter(perFragment_ShaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(perFragment_ShaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }


    // get mvp uniform location


    perFragment_modelMatrixUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_model_matrix");
    perFragment_viewMatrixUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_view_matrix");
    perFragment_projectionMatrixUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_projection_matrix");
    perFragment_LKeyPressedUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_LKeyPressed");
    perFragment_laUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_La");
    perFragment_ldUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ld");
    perFragment_lsUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ls"); 
    perFragment_lightPositionUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_light_position");
    perFragment_kaUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ka");
    perFragment_kdUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Kd");
    perFragment_ksUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ks");
    perFragment_materialShininessUniform = gl.getUniformLocation(perFragment_ShaderProgramObject,"u_material_shininess");
    
    // Sphere 

    sphere = new Mesh();

    makeSphere(sphere,2.0,30,30);
    
    gl.enable(gl.DEPTH_TEST);

    gl.clearDepth(gl.LEQUAL);
    
    gl.enable(gl.CULL_FACE);

    gl.clearColor(0.0,0.0,0.0,1.0);  // Black color

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    if(bFullscreen == true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;        
    }

    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width/canvas.height),0.1,100.0);
    
}
 

function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if (bPerVertexLighting == true)
    {

    gl.useProgram(perVertex_ShaderProgramObject);

    if(bLKeyPressed == true )
    {
        gl.uniform1i(perVertex_LKeyPressedUniform,1);

        
        // set light properties

        gl.uniform3fv(perVertex_laUniform,light_ambient);
        gl.uniform3fv(perVertex_ldUniform,light_diffuse);
        gl.uniform3fv(perVertex_lsUniform,light_specular);
        gl.uniform4fv(perVertex_lightPositionUniform,light_position);


        // set material properties

        gl.uniform3fv(perVertex_kaUniform,material_ambient);
        gl.uniform3fv(perVertex_kdUniform,material_diffuse);
        gl.uniform3fv(perVertex_ksUniform,material_specular);
        gl.uniform1f(perVertex_materialShininessUniform,material_shininess);

    }

    else

    {
        gl.uniform1i(perVertex_LKeyPressedUniform,0);
    }
    
    
    var modelMatrix= mat4.create();
    var viewMatrix= mat4.create();

    mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-7.0]);
    
    gl.uniformMatrix4fv(perVertex_modelMatrixUniform,false,modelMatrix);

    gl.uniformMatrix4fv(perVertex_viewMatrixUniform,false,viewMatrix);

    gl.uniformMatrix4fv(perVertex_projectionMatrixUniform,false,perspectiveProjectionMatrix);

    sphere.draw();

    gl.useProgram(null);
    
    }

    else if (bPerFragmentLighting == true)
    {

    gl.useProgram(perFragment_ShaderProgramObject);

    if(bLKeyPressed == true )
    {
        gl.uniform1i(perFragment_LKeyPressedUniform,1);

        
        // set light properties

        gl.uniform3fv(perFragment_laUniform,light_ambient);
        gl.uniform3fv(perFragment_ldUniform,light_diffuse);
        gl.uniform3fv(perFragment_lsUniform,light_specular);
        gl.uniform4fv(perFragment_lightPositionUniform,light_position);


        // set material properties

        gl.uniform3fv(perFragment_kaUniform,material_ambient);
        gl.uniform3fv(perFragment_kdUniform,material_diffuse);
        gl.uniform3fv(perFragment_ksUniform,material_specular);
        gl.uniform1f(perFragment_materialShininessUniform,material_shininess);

    }

    else

    {
        gl.uniform1i(perFragment_LKeyPressedUniform,0);
    }
    
    
    var modelMatrix= mat4.create();
    var viewMatrix= mat4.create();

    mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-7.0]);
    
    gl.uniformMatrix4fv(perFragment_modelMatrixUniform,false,modelMatrix);
    gl.uniformMatrix4fv(perFragment_viewMatrixUniform,false,viewMatrix);
    gl.uniformMatrix4fv(perFragment_projectionMatrixUniform,false,perspectiveProjectionMatrix);

    sphere.draw();

    gl.useProgram(null);
    
    }
    // Animation loop
    requestAnimationFrame(draw,canvas);
}


function keyDown(event)
{
    switch(event.keyCode)
       {
           case 27: // escape
                  togglefullscreen();
                  break;
           case 81 : // for 'q' or 'Q'
                   uninitialize();
                   window.close();
                   break;
           
           case 70 : // for 'F' or 'f'
                if (isFKeyPressed == false)
                {
                    bPerFragmentLighting = true;
                    bPerVertexLighting = false;
                    isFKeyPressed = true;
                    isVKeyPressed = false;
                } 
                   break;

            case  86: // for 'V' or 'v'
                if (isVKeyPressed == false)
                {
                    bPerVertexLighting = true;
                    bPerFragmentLighting = false;
                    isVKeyPressed = true;
                    isFKeyPressed = false;
                }
                    break;

           case 76 : // for 'l' or  'L'
                   if(bLKeyPressed == false)
                      bLKeyPressed = true;
                   else
                      bLKeyPressed = false;
                   break;
	}
}

function mouseDown()
{
    
}

function uninitialize()
{

    if(sphere)
    {
        sphere.deallocate();
        sphere = null;
    }

    if(perVertex_ShaderProgramObject)
    {
        if(perVertex_FragmentShaderObject)
        {
        gl.detachShader(perVertex_ShaderProgramObject,perVertex_FragmentShaderObject);
        gl.deleteShader(perVertex_FragmentShaderObject);
        perVertex_FragmentShaderObject = null;
        }

        if(perVertex_VertexShaderObject)
        {
        gl.detachShader(perVertex_ShaderProgramObject,perVertex_VertexShaderObject);
        gl.deleteShader(perVertex_VertexShaderObject);
        perVertex_VertexShaderObject = null;
        }
        gl.deleteProgram(perVertex_ShaderProgramObject);
        perVertex_ShaderProgramObject = null;
    }

    if(perFragment_ShaderProgramObject)
    {
        if(perFragment_FragmentShaderObject)
        {
        gl.detachShader(perFragment_ShaderProgramObject,perFragment_FragmentShaderObject);
        gl.deleteShader(perFragment_FragmentShaderObject);
        perFragment_FragmentShaderObject = null;
        }

        if(perFragment_VertexShaderObject)
        {
        gl.detachShader(perFragment_ShaderProgramObject,perFragment_VertexShaderObject);
        gl.deleteShader(perFragment_VertexShaderObject);
        perFragment_VertexShaderObject = null;
        }
        gl.deleteProgram(perFragment_ShaderProgramObject);
        perFragment_ShaderProgramObject = null;
    }
}