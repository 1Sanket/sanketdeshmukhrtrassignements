
// Two Lights


var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
  VDG_ATTRIBUTE_VERTEX:0,
  VDG_ATTRIBUTE_COLOR:1,
  VDG_ATTRIBUTE_NORMAL:2,
  VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var light0_ambient = [0.0,0.0,0.0];
var light0_diffuse = [1.0,0.0,0.0];
var light0_specular = [1.0,0.0,0.0];
var light0_position = [-100.0,100.0,100.0,1.0];

var light1_ambient = [0.0,0.0,0.0];
var light1_diffuse = [0.0,0.0,1.0];
var light1_specular = [0.0,0.0,1.0];
var light1_position = [100.0,100.0,100.0,1.0];

var material_ambient = [0.0,0.0,0.0];
var material_diffuse = [1.0,1.0,1.0];
var material_specular = [1.0,1.0,1.0];
var material_shininess = 50.0 ;

var modelMatrixUniform,viewMatrixUniform,projectionMatrixUniform;
var la0Uniform,ld0Uniform,ls0Uniform,light0PositionUniform;
var la1Uniform,ld1Uniform,ls1Uniform,light1PositionUniform;
var kaUniform,kdUniform,ksUniform,materialShininessUniform;

var perspectiveProjectionMatrix;

var vao_Pyramid ;
var vbo_Pyramid_Position;
var vbo_Pyramid_Normal;

var anglePyramid= 0.0;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame ||
                           window.webkitCancelAnimationFrame ||
                           window.webkitCancelRequestAnimationFrame ||
                           window.mozCancelRequestAnimationFrame ||
                           window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame ||
                           window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame ||
                           window.msCancelAnimationFrame ;


// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    // print canvas width and height on console
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

     // register keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL

    init();

    resize();

    draw();
}


function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();

     bFullscreen = true;
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();

     bFullscreen = false;
}

}

function init()
{
    // Get WebGL 2.0 context. 

    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
       console.log("Failed to get WebGL rendering context");
       return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex Shader 

    var vertexShaderSourceCode =

    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_model_matrix;" +
    "uniform mat4 u_view_matrix;" +
    "uniform mat4 u_projection_matrix;" +
    "uniform vec3 u_La0;" +
    "uniform vec3 u_Ld0;" +
    "uniform vec3 u_Ls0;" +
    "uniform vec4 u_light0_position;" +
    "uniform vec3 u_La1;" +
    "uniform vec3 u_Ld1;" +
    "uniform vec3 u_Ls1;" +
    "uniform vec4 u_light1_position;" +
    "uniform vec3 u_Ka;" +
    "uniform vec3 u_Kd;" +
    "uniform vec3 u_Ks;" +
    "uniform float u_material_shininess;" +
    "out vec3 phong_ads_color;" +
    "void main(void)" +
    "{" +
    "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition ;" +
    "vec3 tnorm = normalize(mat3 (u_view_matrix * u_model_matrix) * vNormal );" +
    "vec3 light0_direction = normalize(vec3 (u_light0_position) - eyeCoordinates.xyz );" +
    "float tn_dot_ld0 = max(dot(tnorm,light0_direction),0.0) ;" +
    "vec3 reflection_vector0 = reflect(-light0_direction,tnorm) ;" +
    "vec3 light1_direction = normalize(vec3 (u_light1_position) - eyeCoordinates.xyz );" +
    "float tn_dot_ld1 = max(dot(tnorm,light1_direction),0.0) ;" +
    "vec3 reflection_vector1 = reflect(-light1_direction,tnorm) ;" +
    "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);"+
    "vec3 ambient0 = u_La0 * u_Ka ;" +
    "vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0 ;" +
    "vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0,viewer_vector),0.0),u_material_shininess) ;" +
    "vec3 ambient1 = u_La1 * u_Ka ;" +
    "vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1 ;" +
    "vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1,viewer_vector),0.0),u_material_shininess) ;" +
    "phong_ads_color = ambient0 + diffuse0 + specular0 + ambient1 + diffuse1 + specular1;"+
    "gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;" +
    "}";


    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 

    var fragmentShaderSourceCode =

    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec3 phong_ads_color;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "FragColor = vec4(phong_ads_color,1.0);" +
    "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program

    shaderProgramObject = gl.createProgram();

    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);

    // pre-link bindind of shader program object with vertex shader attributes 

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    // linking 

    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }


    var pyramidVertices=new Float32Array([
        0.0, 1.0, 0.0,    // front-top
        -1.0, -1.0, 1.0,  // front-left
        1.0, -1.0, 1.0,   // front-right
        
        0.0, 1.0, 0.0,    // right-top
        1.0, -1.0, 1.0,   // right-left
        1.0, -1.0, -1.0,  // right-right
        
        0.0, 1.0, 0.0,    // back-top
        1.0, -1.0, -1.0,  // back-left
        -1.0, -1.0, -1.0, // back-right
        
        0.0, 1.0, 0.0,    // left-top
        -1.0, -1.0, -1.0, // left-left
        -1.0, -1.0, 1.0   // left-right
       ]);

    
       var pyramidNormalCoordinates=new Float32Array([

               0.0, 0.447214, 0.894427,
               0.0, 0.447214, 0.894427,
               0.0, 0.447214, 0.894427,

               0.894427, 0.447214, 0.0,
               0.894427, 0.447214, 0.0,
               0.894427, 0.447214, 0.0,

               0.0, 0.447214, -0.894427,
               0.0, 0.447214, -0.894427,
               0.0, 0.447214, -0.894427,

               -0.894427, 0.447214, 0.0,
               -0.894427, 0.447214, 0.0,
               -0.894427, 0.447214, 0.0
       ]);

    vao_Pyramid=gl.createVertexArray();
    gl.bindVertexArray(vao_Pyramid);
    
    vbo_Pyramid_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Pyramid_Position);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
     
    vbo_Pyramid_Normal = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Pyramid_Normal);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidNormalCoordinates,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_NORMAL,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    gl.bindVertexArray(null);


    // get mvp uniform location


    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_model_matrix");

    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_view_matrix");

    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");

    la0Uniform = gl.getUniformLocation(shaderProgramObject,"u_La0");
    
    ld0Uniform = gl.getUniformLocation(shaderProgramObject,"u_Ld0");

    ls0Uniform = gl.getUniformLocation(shaderProgramObject,"u_Ls0");

    light0PositionUniform = gl.getUniformLocation(shaderProgramObject,"u_light0_position");

    la1Uniform = gl.getUniformLocation(shaderProgramObject,"u_La1");
    
    ld1Uniform = gl.getUniformLocation(shaderProgramObject,"u_Ld1");

    ls1Uniform = gl.getUniformLocation(shaderProgramObject,"u_Ls1");

    light1PositionUniform = gl.getUniformLocation(shaderProgramObject,"u_light1_position");

    kaUniform = gl.getUniformLocation(shaderProgramObject,"u_Ka");
    
    kdUniform = gl.getUniformLocation(shaderProgramObject,"u_Kd");

    ksUniform = gl.getUniformLocation(shaderProgramObject,"u_Ks");

    materialShininessUniform = gl.getUniformLocation(shaderProgramObject,"u_material_shininess");
    
    gl.enable(gl.DEPTH_TEST);

    gl.clearDepth(gl.LEQUAL);
    
    gl.enable(gl.CULL_FACE);

    gl.clearColor(0.0,0.0,0.0,1.0);  // Black color

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    if(bFullscreen == true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;        
    }

    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width/canvas.height),0.1,100.0);
    
}
 
function updateAngle()
{
    anglePyramid = anglePyramid + 0.7;
    
    if (anglePyramid >= 360.0)
		anglePyramid = 0.0;
}

function degreeToRadian(degrees)
{
    return (degrees * (Math.PI/180.0));
}

function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);
        
        // set light properties

        gl.uniform3fv(la0Uniform,light0_ambient);
        gl.uniform3fv(ld0Uniform,light0_diffuse);
        gl.uniform3fv(ls0Uniform,light0_specular);
        gl.uniform4fv(light0PositionUniform,light0_position);

        gl.uniform3fv(la1Uniform,light1_ambient);
        gl.uniform3fv(ld1Uniform,light1_diffuse);
        gl.uniform3fv(ls1Uniform,light1_specular);
        gl.uniform4fv(light1PositionUniform,light1_position);


        // set material properties

        gl.uniform3fv(kaUniform,material_ambient);
        gl.uniform3fv(kdUniform,material_diffuse);
        gl.uniform3fv(ksUniform,material_specular);
        gl.uniform1f(materialShininessUniform,material_shininess);
    
    
    var modelMatrix= mat4.create();
    var viewMatrix= mat4.create();
    var rotationMatrix = mat4.create();

    mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-7.0]);
    
    mat4.rotateY(rotationMatrix,rotationMatrix,degreeToRadian(anglePyramid));
    
    // Multiply modelview and rotation matrix

    mat4.multiply(modelMatrix,modelMatrix,rotationMatrix);

    //mat4.multiply(perspectiveProjectionMatrix,perspectiveProjectionMatrix,modelMatrix);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);

    gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);

    gl.bindVertexArray(vao_Pyramid);
    
    gl.drawArrays(gl.TRIANGLES,0,12);
    
    gl.bindVertexArray(null);

    gl.useProgram(null);

    updateAngle();

    // Animation loop
    requestAnimationFrame(draw,canvas);
}


function keyDown(event)
{
    switch(event.keyCode)
       {
           case 27: // escape
                   uninitialize();
                   window.close();
                   break;
           
           case 70 : // for 'F' or 'f'
                   togglefullscreen();
                   break;

	}
}

function mouseDown()
{
    
}

function uninitialize()
{

    if(vao_Pyramid)
    {
        gl.deleteVertexArray(vao_Pyramid);
        vao_Pyramid = null;
    }

    if(vbo_Pyramid_Position)
    {
        gl.deleteBuffer(vbo_Pyramid_Position);
        vbo_Pyramid_Position = null;
    }

    if(vbo_Pyramid_Normal)
    {
        gl.deleteBuffer(vbo_Pyramid_Normal);
        vbo_Pyramid_Normal = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
        gl.detachShader(shaderProgramObject,fragmentShaderObject);
        gl.deleteShader(fragmentShaderObject);
        fragmentShaderObject = null;
        }

        if(vertexShaderObject)
        {
        gl.detachShader(shaderProgramObject,vertexShaderObject);
        gl.deleteShader(vertexShaderObject);
        vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}