
// Two colored shapes


var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
  VDG_ATTRIBUTE_VERTEX:0,
  VDG_ATTRIBUTE_COLOR:1,
  VDG_ATTRIBUTE_NORMAL:2,
  VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_Pyramid;
var vbo_Pyramid_Position;
var vbo_Pyramid_Color;
var vao_Cube;
var vbo_Cube_Position;
var vbo_Cube_Color;
var mvpUniform;

var angleCube = 0.0;
var anglePyramid= 0.0;

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame ||
                           window.webkitCancelAnimationFrame ||
                           window.webkitCancelRequestAnimationFrame ||
                           window.mozCancelRequestAnimationFrame ||
                           window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame ||
                           window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame ||
                           window.msCancelAnimationFrame ;


// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    // print canvas width and height on console
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

     // register keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL

    init();

    resize();

    draw();
}


function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();

     bFullscreen = true;
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();

     bFullscreen = false;
}

}

function init()
{
    // Get WebGL 2.0 context. 

    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
       console.log("Failed to get WebGL rendering context");
       return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex Shader 

    var vertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec4 vColor;" +
    "out vec4 outColor;"+
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition; " +
    "outColor=vColor;"+
    "}" ;

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 

    var fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec4 outColor;" +
    "out vec4 FragColor;" +
    "void main()" +
    "{" +
    "FragColor = outColor;"+
    "}" ;

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program

    shaderProgramObject = gl.createProgram();

    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);

    // pre-link bindind of shader program object with vertex shader attributes 

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_COLOR,"vColor");

    // linking 

    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get mvp uniform location

    mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

   // Triangle vertices

    var pyramidVertices = new Float32Array([
        		// front face

	    0.0, 1.0, 0.0,   // Apex
    
        -1.0, -1.0, 1.0, // left-corner of front face
    
         1.0, -1.0, 1.0,   //right corner of front face
    
            //right face
    
        0.0, 1.0, 0.0,     // apex
    
        1.0, -1.0, 1.0,    //left corner of the right face
    
        1.0, -1.0, -1.0,  // right corner of the right face
    
            //Back face
    
        0.0, 1.0, 0.0, // apex
    
        1.0, -1.0, -1.0, //left corner of the back face
    
        -1.0, -1.0, -1.0, //right corner of the back face
    
            //left face
    
        0.0, 1.0, 0.0, // apex
    
        -1.0, -1.0, -1.0, //left corner of the back face
    
        -1.0, -1.0, 1.0 //right corner of the back face
    
    ]);

    vao_Pyramid = gl.createVertexArray();
    gl.bindVertexArray(vao_Pyramid);
    
    vbo_Pyramid_Position= gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Pyramid_Position);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

     
    var pyramidColorCoordinates = new Float32Array([
    
        // front face
	
	1.0, 0.0, 0.0, // red
	
		// Apex

	0.0, 1.0, 0.0,    // green
	
		// left-corner of front face

	0.0, 0.0, 1.0,    // blue
	
		//right corner of front face

        //right face

	1.0, 0.0, 0.0, // red
        // apex

	0.0, 0.0, 1.0,  // blue

		//left corner of the right face

	0.0, 1.0, 0.0,  //green
	
		// right corner of the right face

		//Back face

	1.0, 0.0, 0.0, //red
	
		// apex

	0.0, 1.0, 0.0,// green
	
		//left corner of the back face

	0.0, 0.0, 1.0, // blue
	
		//right corner of the back face

		//left face
	 
	1.0, 0.0, 0.0,//red
	   
		// apex

	0.0, 0.0, 1.0,  // blue
   
	//left corner of the back face

    0.0, 1.0, 0.0  // green
    
    ]);

    vbo_Pyramid_Color= gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Pyramid_Color);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidColorCoordinates,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    gl.bindVertexArray(null);

    
    // Square

    var cubeVertices = new Float32Array([
        		// Top face

	1.0, 1.0, -1.0,   // Right-top corner of top face 
	-1.0, 1.0, -1.0, // left-top corner of the top face
	-1.0, 1.0, 1.0,   //left bottom corner of front face
	1.0, 1.0, 1.0, //right bottom corner of front face

        //Bottom face

	1.0, -1.0, -1.0,   // Right-top corner of bottom face 
	-1.0, -1.0, -1.0, // left-top corner of the bottom face
	-1.0, -1.0, 1.0,   //left bottom corner of bottom face
	1.0, -1.0, 1.0, //right bottom corner of bottom face

		//Front face

	1.0, 1.0, 1.0,   // Right-top corner of front face 
	-1.0, 1.0, 1.0, // left-top corner of the front face
	-1.0, -1.0, 1.0,   //left bottom corner of front face
	1.0, -1.0, 1.0, //right bottom corner of front face


		//Back face
	
	1.0, 1.0, -1.0,  // Right-top corner of back face 
	-1.0, 1.0, -1.0, // left-top corner of the back face
	-1.0, -1.0, -1.0,   //left bottom corner of back face
	1.0, -1.0, -1.0, //right bottom corner of back face

		//Right face
	
	1.0, 1.0, -1.0,   // Right-top corner of right face 
	1.0, 1.0, 1.0, // left-top corner of the right face
	1.0, -1.0, 1.0,   //left bottom corner of right face
	1.0, -1.0, -1.0, //right bottom corner of right face

		//Left face

	-1.0, 1.0, 1.0,   // Right-top corner of left face 
	-1.0, 1.0, -1.0, // left-top corner of the left face
	-1.0, -1.0, -1.0,   //left bottom corner of left face
	-1.0, -1.0, 1.0 //right bottom corner of left face

    ]);

    vao_Cube = gl.createVertexArray();
    gl.bindVertexArray(vao_Cube);
    
    vbo_Cube_Position= gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Cube_Position);
    gl.bufferData(gl.ARRAY_BUFFER,cubeVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    
    var cubeColorCoordinates = new Float32Array([
		// Top face
        1.0, 0.0, 0.0, // red
        1.0, 0.0, 0.0, // red
        1.0, 0.0, 0.0, // red
        1.0, 0.0, 0.0, // red
    
             //Bottom face
        0.0, 1.0, 0.0, // Green
        0.0, 1.0, 0.0, // Green
        0.0, 1.0, 0.0, // Green
        0.0, 1.0, 0.0, // Green
    
            //Front face
        0.0, 0.0, 1.0, // Blue
        0.0, 0.0, 1.0, // Blue
        0.0, 0.0, 1.0, // Blue
        0.0, 0.0, 1.0, // Blue
    
            //Back face
        0.0, 1.0, 1.0, // Cyan
        0.0, 1.0, 1.0, // Cyan
        0.0, 1.0, 1.0, // Cyan
        0.0, 1.0, 1.0, // Cyan
    
            //Right face
        1.0, 0.0, 1.0, // Magenta
        1.0, 0.0, 1.0, // Magenta
        1.0, 0.0, 1.0, // Magenta
        1.0, 0.0, 1.0, // Magenta
    
            //Left face
        1.0, 1.0, 0.0, // Yellow
        1.0, 1.0, 0.0,// Yellow
        1.0, 1.0, 0.0, // Yellow
        1.0, 1.0, 0.0 // Yellow
    

]);


vbo_Cube_Color= gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Cube_Color);
gl.bufferData(gl.ARRAY_BUFFER,cubeColorCoordinates,gl.STATIC_DRAW);
gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
gl.bindBuffer(gl.ARRAY_BUFFER,null);

    //gl.vertexAttrib3f(WebGLMacros.VDG_ATTRIBUTE_COLOR,0.258824, 0.258824, 0.435294);

    gl.bindVertexArray(null);

    gl.clearDepth(1.0);
    
    gl.enable(gl.DEPTH_TEST);
    
    gl.depthFunc(gl.LEQUAL);
    
    //gl.hint(gl.PERSPECTIVE_CORRECTION_HINT,gl.NICEST);
    

    gl.clearColor(0.0,0.0,0.0,1.0);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    if(bFullscreen == true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;        
    }

    gl.viewport(0,0,canvas.width,canvas.height);

    // orthographic projection = left , right ,bottom ,top , near ,far

     //if (canvas.width <= canvas.height)
      //  mat4.ortho(perspectiveProjectionMatrix,-100.0,100.0,(-100.0 * (canvas.height/canvas.width)),(100.0 *(canvas.height/canvas.width)),-100.0,100.0);
    // else
    //    mat4.ortho(perspectiveProjectionMatrix,-100.0,100.0,(-100.0 * (canvas.width/canvas.height)),(100.0 *(canvas.width/canvas.height)),-100.0,100.0);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width/canvas.height),0.1,100.0);
    
}


function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);
    
    var modelViewMatrix= mat4.create();
    var modelViewProjectionMatrix= mat4.create();
    var rotationMatrix = mat4.create();
    var scaleMatrix= mat4.create();

    mat4.translate(modelViewMatrix,modelViewMatrix,[-2.0,0.0,-5.0]);

    mat4.rotateY(rotationMatrix,rotationMatrix,degreeToRadian(anglePyramid));
    
    // Multiply modelview and rotation matrix

    mat4.multiply(modelViewMatrix,modelViewMatrix,rotationMatrix);

    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindVertexArray(vao_Pyramid);

    gl.drawArrays(gl.TRIANGLES,0,12);

    gl.bindVertexArray(null);

    // modelViewMatrix = mat4.identity();
    // modelViewProjectionMatrix = mat4.identity();

    mat4.identity(modelViewMatrix);
    mat4.identity(modelViewProjectionMatrix);
    mat4.identity(rotationMatrix);

    // Translate model view matrix.

    //mat4.multiply(modelViewMatrix,modelViewMatrix,scaleMatrix);

    mat4.translate(modelViewMatrix,modelViewMatrix,[2.0,0.0,-5.0]);

    mat4.scale(modelViewMatrix,modelViewMatrix,[0.75,0.75,0.75]);

    mat4.rotateX(rotationMatrix,rotationMatrix,degreeToRadian(angleCube));
    mat4.rotateY(rotationMatrix,rotationMatrix,degreeToRadian(angleCube));
    mat4.rotateZ(rotationMatrix,rotationMatrix,degreeToRadian(angleCube));
    
    // Multiply modelview and rotation matrix

    mat4.multiply(modelViewMatrix,modelViewMatrix,rotationMatrix);

    // multiply modelview and perspective matrix to get modelview projection matrix 

    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);

    // pass modelviewProjection matrix to the vertex shader in 'u_mvp_matrix' shader variable
    // whose position value we already calculated by using glGetUniformLocation()
    
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindVertexArray(vao_Cube);
    
    gl.drawArrays(gl.TRIANGLE_FAN,0,4); // draw Cube
    gl.drawArrays(gl.TRIANGLE_FAN,4,4); 
    gl.drawArrays(gl.TRIANGLE_FAN,8,4); 
    gl.drawArrays(gl.TRIANGLE_FAN,12,4); 
    gl.drawArrays(gl.TRIANGLE_FAN,16,4); 
    gl.drawArrays(gl.TRIANGLE_FAN,20,4);  
    
    gl.bindVertexArray(null);

    gl.useProgram(null);


    updateAngle();

    // Animation loop
    requestAnimationFrame(draw,canvas);
}

function updateAngle()
{
    anglePyramid = anglePyramid + 0.7;
	angleCube = angleCube - 0.7;
	if (anglePyramid >= 360.0)
		anglePyramid = 0.0;
	if (angleCube <= 0.0)
		angleCube = 360.0;
}

function degreeToRadian(degrees)
{
    return (degrees * (Math.PI/180.0));
}

function keyDown(event)
{
    switch(event.keyCode)
       {
           case 27: // escape
                   uninitialize();
                   window.close();
                   break;
           
           case 70 : // for 'F' or 'f'
                   togglefullscreen();
                   break;
	}
}

function mouseDown()
{
    
}

function uninitialize()
{
    if(vao_Pyramid)
    {
        gl.deleteVertexArray(vao_Pyramid);
        vao_Pyramid = null;
    }

    if(vbo_Pyramid_Position)
    {
        gl.deleteBuffer(vbo_Pyramid_Position);
        vbo_Pyramid_Position = null;
    }

    if(vbo_Pyramid_Color)
    {
        gl.deleteBuffer(vbo_Pyramid_Color);
        vbo_Pyramid_Color = null;
    }

    if(vao_Cube)
    {
        gl.deleteVertexArray(vao_Cube);
        vao_Cube = null;
    }

    if(vbo_Cube_Position)
    {
        gl.deleteBuffer(vbo_Cube_Position);
        vbo_Cube_Position = null;
    }

    if(vbo_Cube_Color)
    {
        gl.deleteBuffer(vbo_Cube_Color);
        vbo_Cube_Color = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
        gl.detachShader(shaderProgramObject,fragmentShaderObject);
        gl.deleteShader(fragmentShaderObject);
        fragmentShaderObject = null;
        }

        if(vertexShaderObject)
        {
        gl.detachShader(shaderProgramObject,vertexShaderObject);
        gl.deleteShader(vertexShaderObject);
        vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}