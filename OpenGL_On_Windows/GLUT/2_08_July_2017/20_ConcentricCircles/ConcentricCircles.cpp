#include <GL\freeglut.h>
#include <math.h>

#define PI 3.1415926535898

bool bFullScreen = false;

struct Point
{
	GLfloat xAxis;
	GLfloat yAxis;
};

struct Color
{
	GLfloat red;
	GLfloat green;
	GLfloat blue;
};

int main(int argc, char** argv)
{
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Concentric Circles");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	void drawCircle(GLfloat,Color);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat radius= 0.1f;
	/*for (float i=0;i<10;i++)
	{
		radius = radius + 0.1f;
		drawCircle( radius, Color{ 1.0f,1.0f,1.0f });
	}*/
	drawCircle(radius, Color{ 1.0f,0.0f,0.0f });
	radius += 0.1f;
	drawCircle(radius, Color{ 0.0f,1.0f,0.0f });
	radius += 0.1f;
	drawCircle(radius, Color{ 0.0f,0.0f,1.0f });
	radius += 0.1f;
	drawCircle(radius, Color{ 0.0f,1.0f,1.0f });
	radius += 0.1f;
	drawCircle(radius, Color{ 1.0f,0.0f,1.0f });
	radius += 0.1f;
	drawCircle(radius, Color{ 1.0f,1.0f,0.0f });
	radius += 0.1f;
	drawCircle(radius, Color{ 1.0f,1.0f,1.0f });
	radius += 0.1f;
	drawCircle(radius, Color{ 1.0f,0.5f,0.0f });
	radius += 0.1f;
	drawCircle(radius, Color{ 0.5f,0.5f,0.5f });
	radius += 0.1f;
	drawCircle(radius, Color{ 1.0f,0.43f,0.78f });

	glutSwapBuffers();

}

void drawCircle(GLfloat radius, Color color)
{
	GLint circle_points = 1000000;
	GLfloat angle;

	glBegin(GL_POINTS);

	glColor3f(color.red, color.green, color.blue);

	for (int i = 0.0f; i<circle_points; i++)
	{
		angle = 2 * PI * i / circle_points;
		glVertex3f(cos(angle)*radius,
			sin(angle)*radius, 0.0f);
	}

	glEnd();

}



void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScreen == false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{

}