#include <GL\freeglut.h>

bool bFullScreen = false;
struct Point
{
	GLfloat xAxis;
	GLfloat yAxis;
};

struct Color
{
	GLfloat red;
	GLfloat green;
	GLfloat blue;
};

int main(int argc, char** argv)
{
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Corn Flower Rectangle");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	void drawRectangle(Point, Point, Point , Point, Color);
	glClear(GL_COLOR_BUFFER_BIT);

	drawRectangle(Point{ -1.0f,1.0f }, Point{ -1.0f,-1.0f }, Point{ 1.0f,-1.0f }, Point{ 1.0f,1.0f }, Color{ 0.258824f, 0.258824f, 0.435294f });
	
	glutSwapBuffers();

}

void drawRectangle(Point leftTop, Point leftBottom, Point rightBottom, Point rightTop, Color color)
{
	//glLineWidth(width);
	glBegin(GL_QUADS);

	glColor3f(color.red, color.green, color.blue);

	glVertex3f(leftTop.xAxis, leftTop.yAxis, 0.0f);
	glVertex3f(leftBottom.xAxis, leftBottom.yAxis, 0.0f);
	glVertex3f(rightBottom.xAxis, rightBottom.yAxis, 0.0f);
	glVertex3f(rightTop.xAxis, rightTop.yAxis, 0.0f);

	glEnd();
}

void initialize(void)
{
	glClearColor(0.0f,0.0f,0.0f,0.0f);

}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScreen == false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{

}