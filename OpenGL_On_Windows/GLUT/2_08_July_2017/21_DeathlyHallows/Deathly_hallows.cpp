#include <GL\freeglut.h>
#include <math.h>

#define PI 3.1415926535898

bool bFullScreen = false;

struct Point
{
	GLfloat xAxis;
	GLfloat yAxis;
};

struct Color
{
	GLfloat red;
	GLfloat green;
	GLfloat blue;
};

int main(int argc, char** argv)
{
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Deathly Hallows");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	void drawLine(Point, Point, Color, GLfloat);
	void drawTriangle(Point, Point, Point, Color, GLfloat);
	void drawCircle(GLfloat radius, Point incenter,Color color);

	Point getIncenterOfTriangle(Point, Point, Point);
	GLfloat getRadiusOfIncircle(Point, Point, Point);
	GLint circle_points = 1000000;
	GLfloat angle;
	GLfloat sideOfTriangle = 0.9f;
	GLfloat radius = getRadiusOfIncircle(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) },
		Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) });

	Point incenter = getIncenterOfTriangle(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) },
		Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) });

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	drawCircle(radius,incenter ,Color{ 1.0f,1.0f,1.0f });
	drawTriangle(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) },
		Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Color{ 1.0f,1.0f,1.0f },
		1.0f);
	drawLine(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) }, Point{ 0.0f, -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) }, Color{ 1.0f,1.0f,1.0f }, 1.0f);

	glutSwapBuffers();

}

GLfloat getSideOfTriangle(Point vertex1, Point vertex2)
{
	GLfloat sideOfTriangle = 0.0f;

	return sqrt(((vertex2.xAxis - vertex1.xAxis)*(vertex2.xAxis - vertex1.xAxis) + (vertex2.yAxis - vertex1.yAxis)*(vertex2.yAxis - vertex1.yAxis)));
}


Point getIncenterOfTriangle(Point vertexA, Point vertexB, Point vertexC)
{
	GLfloat perimeter = 0.0f;
	/*GLfloat sideA = getSideOfTriangle(vertexC,vertexB);
	GLfloat sideB = getSideOfTriangle(vertexC,vertexA);
	GLfloat sideC = getSideOfTriangle(vertexB,vertexA);*/

	GLfloat sideA = getSideOfTriangle(vertexB, vertexC);
	GLfloat sideB = getSideOfTriangle(vertexA, vertexC);
	GLfloat sideC = getSideOfTriangle(vertexA, vertexB);

	perimeter = sideA + sideB + sideC;

	GLfloat x_Axis_Incenter = ((sideA*vertexA.xAxis) + (sideB*vertexB.xAxis) + (sideC*vertexC.xAxis)) / perimeter;
	GLfloat y_Axis_Incenter = ((sideA*vertexA.yAxis) + (sideB*vertexB.yAxis) + (sideC*vertexC.yAxis)) / perimeter;

	return Point{ x_Axis_Incenter,y_Axis_Incenter };
}

GLfloat getRadiusOfIncircle(Point vertexA, Point vertexB, Point vertexC)
{
	GLfloat radiusOfincircle = 0.0f;
	GLfloat areaOfTriangle = 0.0f;
	GLfloat semiperimeterOfTriangle = 0.0f;
	//GLfloat sideA = getSideOfTriangle(vertexC, vertexB);
	//GLfloat sideB = getSideOfTriangle(vertexC, vertexA);
	//GLfloat sideC = getSideOfTriangle(vertexB, vertexA);

	GLfloat sideA = getSideOfTriangle(vertexB, vertexC);
	GLfloat sideB = getSideOfTriangle(vertexA, vertexC);
	GLfloat sideC = getSideOfTriangle(vertexA, vertexB);

	semiperimeterOfTriangle = (sideA + sideB + sideC) / 2;
	areaOfTriangle = sqrt(semiperimeterOfTriangle*(semiperimeterOfTriangle - sideA)*(semiperimeterOfTriangle - sideB)*(semiperimeterOfTriangle - sideC));
	radiusOfincircle = areaOfTriangle / semiperimeterOfTriangle;

	return radiusOfincircle;
}



void drawCircle(GLfloat radius, Point incenter, Color color)
{
	GLint circle_points = 1000000;
	GLfloat angle;

	glBegin(GL_POINTS);

	glColor3f(color.red, color.green, color.blue);

	for (int i = 0.0f; i<circle_points; i++)
	{
		angle = 2 * PI * i / circle_points;
		glVertex3f(cos(angle)*radius + incenter.xAxis,
			sin(angle)*radius + incenter.yAxis, 0.0f);
	}

	glEnd();

}

void drawTriangle(Point top, Point left, Point right, Color color, GLfloat width)
{
	glLineWidth(width);
	glBegin(GL_LINE_LOOP);

	glColor3f(color.red, color.green, color.blue);

	glVertex3f(top.xAxis, top.yAxis, 0.0f);
	glVertex3f(left.xAxis, left.yAxis, 0.0f);
	glVertex3f(right.xAxis, right.yAxis, 0.0f);

	glEnd();
}

void drawLine(Point xCoordinate, Point yCoordinate, Color color, GLfloat width)
{
	glLineWidth(width);
	glBegin(GL_LINES);

	glColor3f(color.red, color.green, color.blue);
	glVertex3f(xCoordinate.xAxis, xCoordinate.yAxis, 0.0f);
	glVertex3f(yCoordinate.xAxis, yCoordinate.yAxis, 0.0f);

	glEnd();
}


void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScreen == false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{

}