#include <GL\freeglut.h>
#include <math.h>

#define PI 3.1415926535898

bool bFullScreen = false;

struct Point
{
	GLfloat xAxis;
	GLfloat yAxis;
};

struct Color
{
	GLfloat red;
	GLfloat green;
	GLfloat blue;
};

int main(int argc, char** argv)
{
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Concentric Triangles");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	void drawTriangle(Point, Point, Point, Color, GLfloat);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat sideOfTriangle = 0.1f;

	Color triangleColor[] = { Color{1.0f,0.0f,0.0f},Color{0.0f,1.0f,0.0f},Color{0.0f,0.0f,1.0f},Color{1.0f,0.0f,1.0f},Color{0.0f,1.0f,1.0f},
							  Color{1.0f,0.0f,1.0f},Color{1.0f,1.0f,0.0f},
							  Color{1.0f,1.0f,1.0f},Color{1.0f,0.5f,0.0f},Color{0.5f,0.5f,0.5f},Color{1.0f,0.43f,0.78f} };

	
	for (int i=0;i<10;i++)
	{
		//drawTriangle(Point{ 0.0f,sideOfTriangle }, Point{ -sideOfTriangle / 2.0f,-(sideOfTriangle - ((GLfloat)(sqrt(3) / 2)*sideOfTriangle)) }, Point{ sideOfTriangle / 2.0f,-(sideOfTriangle - ((GLfloat)(sqrt(3) / 2)*sideOfTriangle)) }, Color{ 1.0f,0.0f,0.0f }, 1.0f);

		drawTriangle(Point{ 0.0f,(GLfloat )(sideOfTriangle / sqrt(3)) }, 
			         Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
			         Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		             triangleColor[i],
			         1.0f);
		sideOfTriangle += 0.1f;
	}

	glutSwapBuffers();

}

void drawTriangle(Point top, Point left, Point right, Color color, GLfloat width)
{
	glLineWidth(width);
	glBegin(GL_LINE_LOOP);

	glColor3f(color.red, color.green, color.blue);

	glVertex3f(top.xAxis, top.yAxis, 0.0f);
	glVertex3f(left.xAxis, left.yAxis, 0.0f);
	glVertex3f(right.xAxis, right.yAxis, 0.0f);

	glEnd();
}



void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScreen == false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{

}