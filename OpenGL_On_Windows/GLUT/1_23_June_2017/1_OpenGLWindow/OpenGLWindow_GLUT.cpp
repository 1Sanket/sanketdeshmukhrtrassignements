#include <GL\freeglut.h>

bool bFullScreen = false;

int main(int argc,char** argv)
{
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	
	glutInitWindowSize(800,600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GLUT first window");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

//	glFlush();
	glutSwapBuffers();

}

void initialize(void)
{
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

}

void keyboard(unsigned char key,int x,int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScreen==false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button,int state,int x,int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON :
		break;
	default:
		break;
	}
}

void resize(int width,int height)
{
	// glViewPort();
}

void uninitialize(void)
{

}