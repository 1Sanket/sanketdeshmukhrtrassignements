
#include <Windows.h>
#include <gl/GL.h>
#include <GL\GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbIsEscapeKeyPressed = false;
bool gbFullscreen = false;
void resize(int, int);
float gfITranslateCounter = -2.2;
float gfNTranslateCounter = 3.0;
float gfSaffronRedColorTransparency = 0.0;
float gfSaffronGreenColorTransparency = 0.0;
float gfSaffronBlueColorTransparency = 0.0;
float gfITranslateCounterUp = -2.3;
float gfGreenRedColorTransparency = 0.0;
float gfGreenGreenColorTransparency = 0.0;
float gfGreenBlueColorTransparency = 0.0;
float gfATranslateCounter = 5.2;
float gfHorizontalLineCounter = -4.8;
float gfHorizontalLineXAxisCounter = -3.8;


struct Point
{
	GLfloat xAxis;
	GLfloat yAxis;
};



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);


	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OGL_India_Animation");
	bool bDone = false;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = CreateSolidBrush(RGB(1, 1, 1));
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx
	(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL India Animation"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{

			if (gbActiveWindow == true)
			{
				if (gbIsEscapeKeyPressed == true)
					bDone = true;
				display();
			}
		}

	}

	uninitialize();
	return (int(msg.wParam));

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbIsEscapeKeyPressed = true;
			break;

		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}


void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}

void initialize(void)
{
	//void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	void drawI(void);
	void drawN(void);
	void drawD(void);
	void drawA(void);
	void drawHorizontalLine(void);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(gfITranslateCounter, 0.0f, -3.0f);

	drawI();

	gfITranslateCounter = gfITranslateCounter + 0.0001;

	if (gfITranslateCounter >= -1.7)
	{
		gfITranslateCounter = -1.7 ;
	}
	
	/*glLoadIdentity();
	glTranslatef(-1.0f, gfNTranslateCounter, -3.0f); */

		glLoadIdentity();

		glTranslatef(-1.0f, gfNTranslateCounter, -3.0f);
		//glTranslatef(-1.0f, -1.7f, -3.0f);

		drawN();

		gfNTranslateCounter = gfNTranslateCounter - 0.0001;

		if (gfNTranslateCounter <= 0.0)
		{
			gfNTranslateCounter = 0.0;
		}

		glLoadIdentity();

		glTranslatef(-0.1f, 0.0f, -3.0f);


		if (gfNTranslateCounter == 0.0)
		{
			drawD();
		}

		glLoadIdentity();

		glTranslatef(0.6f, gfITranslateCounterUp, -3.0f);

		if (gfSaffronRedColorTransparency == 1.0 && gfSaffronGreenColorTransparency == 153 && gfSaffronBlueColorTransparency == 51
			&& gfGreenRedColorTransparency == 19.0 && gfGreenGreenColorTransparency == 136.0 && gfGreenBlueColorTransparency == 8.0)
		{

			drawI();

			gfITranslateCounterUp = gfITranslateCounterUp + 0.0001;

			if (gfITranslateCounterUp >= 0.0)
			{
				gfITranslateCounterUp = 0.0;
			}

			glLoadIdentity();

			glTranslatef(gfATranslateCounter, 0.0f, -3.0f);
			drawA();

			gfATranslateCounter = gfATranslateCounter - 0.0001;

			if (gfATranslateCounter <= 1.5)
			{
				gfATranslateCounter = 1.5;
			}

			if (gfATranslateCounter == 1.5)
			{
				glLoadIdentity();

				glTranslatef(gfHorizontalLineCounter, 0.0f, -4.0f);
				//glTranslatef(3.0f, 0.0f, 0.0f);

				drawHorizontalLine();

				gfHorizontalLineCounter = gfHorizontalLineCounter + 0.0005;

				if (gfHorizontalLineCounter >= 0.53)
				{
					gfHorizontalLineCounter = 0.53;
					gfHorizontalLineXAxisCounter += 0.0005;

					if (gfHorizontalLineXAxisCounter >= 1.15)
					{
						gfHorizontalLineXAxisCounter = 1.15;
					}
				}
			}
		}
	
	SwapBuffers(ghdc);

}

void drawI()
{

	glLineWidth(5.0f);
	
	glBegin(GL_LINES);

	glColor3f((GLfloat)1.0f, (GLfloat)153 / 255, (GLfloat)51 / 255);     // orange
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(GLfloat(19 / 255), (GLfloat)136 / 255, (GLfloat)8 / 255);    //green
	glVertex3f(0.0f, -1.0f, 0.0f);

	glEnd();

}

void drawN()
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);

	glColor3f((GLfloat)1.0f, (GLfloat)153 / 255, (GLfloat)51 / 255);
	glVertex3f(-0.25f, 1.0f, 0.0f);

	glColor3f(GLfloat(19 / 255), (GLfloat)136 / 255, (GLfloat)8 / 255);
	glVertex3f(-0.25f, -1.0f, 0.0f);

	glEnd();

	glBegin(GL_LINES);

	glColor3f((GLfloat)1.0f, (GLfloat)153 / 255, (GLfloat)51 / 255);

	glVertex3f(0.25f, 1.0f, 0.0f);

	glColor3f(GLfloat(19 / 255), (GLfloat)136 / 255, (GLfloat)8 / 255);
	glVertex3f(0.25f, -1.0f, 0.0f);


	glEnd();

	glBegin(GL_LINES);

	glColor3f((GLfloat)1.0f, (GLfloat)153 / 255, (GLfloat)51 / 255);
	glVertex3f(-0.25f, 1.0f, 0.0f);

	glColor3f(GLfloat(19 / 255), (GLfloat)136 / 255, (GLfloat)8 / 255);
	glVertex3f(0.25f, -1.0f, 0.0f);

	glEnd();

}


void drawD()
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);

	gfSaffronRedColorTransparency += 0.0001;
	if (gfSaffronRedColorTransparency >= 1.0)
	{
		gfSaffronRedColorTransparency = 1.0;
	}

	
	gfSaffronGreenColorTransparency += 0.0153;
	if (gfSaffronGreenColorTransparency >= 153)
	{
		gfSaffronGreenColorTransparency = 153;
	}

	gfSaffronBlueColorTransparency += 0.0051;
	if (gfSaffronBlueColorTransparency >= 51)
	{
		gfSaffronBlueColorTransparency = 51;
	}

	gfGreenRedColorTransparency += 0.019;
	if (gfGreenRedColorTransparency >= 19.0)
	{
		gfGreenRedColorTransparency = 19.0;
	}


	gfGreenGreenColorTransparency += 0.136;
	if (gfGreenGreenColorTransparency >= 136)
	{
		gfGreenGreenColorTransparency = 136;
	}

	gfGreenBlueColorTransparency += 0.008;
	if (gfGreenBlueColorTransparency >= 8)
	{
		gfGreenBlueColorTransparency = 8;
	}

	//glColor3f((GLfloat)gfRedColorTransparency, (GLfloat)gfGreenColorTransparency / 255, (GLfloat)gfBlueColorTransparency / 255);
	glColor3f((GLfloat)gfSaffronRedColorTransparency, (GLfloat)gfSaffronGreenColorTransparency / 255, (GLfloat)gfSaffronBlueColorTransparency / 255);
	glVertex3f(-0.25f, 1.0f, 0.0f);


	glVertex3f(0.26f, 1.0f, 0.0f);

	glEnd();

	glBegin(GL_LINES);
	
	glColor3f((GLfloat)gfSaffronRedColorTransparency, (GLfloat)gfSaffronGreenColorTransparency/ 255, (GLfloat)gfSaffronBlueColorTransparency / 255);
	glVertex3f(0.25f, 1.0f, 0.0f);

	glColor3f(GLfloat(gfGreenRedColorTransparency / 255), (GLfloat)gfGreenGreenColorTransparency / 255, (GLfloat)gfGreenBlueColorTransparency / 255);
	glVertex3f(0.25f, -1.0f, 0.0f);

	glEnd();

	glBegin(GL_LINES);

	
	glVertex3f(0.26f, -1.0f, 0.0f);

	glColor3f(GLfloat(gfGreenRedColorTransparency / 255), (GLfloat)gfGreenGreenColorTransparency / 255, (GLfloat)gfGreenBlueColorTransparency / 255);
	glVertex3f(-0.25f, -1.0f, 0.0f);

	glEnd();

	glBegin(GL_LINES);

	glColor3f((GLfloat)gfSaffronRedColorTransparency, (GLfloat)gfSaffronGreenColorTransparency/ 255, (GLfloat)gfSaffronBlueColorTransparency / 255);
	glVertex3f(-0.20f, 1.0f, 0.0f);

	glColor3f(GLfloat(gfGreenRedColorTransparency / 255), (GLfloat)gfGreenGreenColorTransparency / 255, (GLfloat)gfGreenBlueColorTransparency / 255);
	glVertex3f(-0.20f, -1.0f, 0.0f);

	glEnd();

}


void drawA()
{

	glLineWidth(5.0f);
	glBegin(GL_LINES);

	glColor3f((GLfloat)1.0f, (GLfloat)153 / 255, (GLfloat)51 / 255);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(GLfloat(19 / 255), (GLfloat)136 / 255, (GLfloat)8 / 255);
	glVertex3f(-0.50f, -1.0f, 0.0f);

	glEnd();

	glBegin(GL_LINES);

	glColor3f((GLfloat)1.0f, (GLfloat)153 / 255, (GLfloat)51 / 255);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(GLfloat(19 / 255), (GLfloat)136 / 255, (GLfloat)8 / 255);
	glVertex3f(0.50f, -1.0f, 0.0f);

	glEnd();

}


void drawHorizontalLine()
{
	glLineWidth(3.2f);
	glBegin(GL_LINES);

	glColor3f((GLfloat)1.0f, (GLfloat)153 / 255, (GLfloat)51 / 255);
	glVertex3f(gfHorizontalLineXAxisCounter, 0.01f, 0.0f);
	glVertex3f(1.8f, 0.01f, 0.0f);

	glEnd();

	glBegin(GL_LINES);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(gfHorizontalLineXAxisCounter, -0.01f, 0.0f);
	glVertex3f(1.8f, -0.01f, 0.0f);

	glEnd();

	glLineWidth(3.5f);
	glBegin(GL_LINES);

	glColor3f(GLfloat(19 / 255), (GLfloat)136 / 255, (GLfloat)8 / 255);
	glVertex3f(gfHorizontalLineXAxisCounter, -0.04f, 0.0f);
	glVertex3f(1.8f, -0.04f, 0.0f);

	glEnd();

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	//glOrtho(-50.0f, 50.0f, -50.0f, 50.0f, -50.0f, 50.0f);

}

void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}



