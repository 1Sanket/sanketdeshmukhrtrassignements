#include <stdio.h>
#include <windows.h>

#include <glew.h>
#include <gl\GL.h>

#include "vmath.h"
#include "OGLPP_SmileyTexture.h"

#pragma comment(lib,"glew32.lib")
#pragma comment (lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};


// Prototype of WndProc

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpfile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof (WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
int  giKeyPressed = 0;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Smiley;
GLuint gVbo_Smiley_position;
GLuint gVbo_Smiley_texture;

GLuint gMVPUniform;

mat4 gPerspectiveProjectionMatrix;

GLuint gTexture_sampler_uniform;
GLuint gTexture_Smiley;
GLuint gTexture_White;
GLubyte checkImage[4] = {255,255,255,255};


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP_Texture");
	bool bDone = false;

	if (fopen_s(&gpfile,"Log.txt","w") !=0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created.... \n Exiting......"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpfile, "Log file is successfully opened. \n");
	}

	// Initialise members of struct WNDCLASSEX

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	
	// register class.

	RegisterClassEx(&wndClass);

	// Create window 

	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable pipeline Tweak Smiley Texture"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	// Message loop .

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{
			// rendering function.
			display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}

		uninitialize();

		return ((int)msg.wParam);
}

	LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
	{
		void resize(int, int);
		void ToggleFullscreen(void);
		void uninitialize(void);

		static WORD xMouse = NULL;
		static WORD yMouse = NULL;

		switch (iMsg)
		{
		case WM_ACTIVATE:
			 
			if (HIWORD(wParam) == 0) // if 0 , window is active 
			{
				gbActiveWindow = true;
			}

			else
			{
				gbActiveWindow = false;
			}

			break;

		case WM_ERASEBKGND:
			return (0);

		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
     
		case  WM_KEYDOWN:
			  
			switch (wParam)
			{
			case VK_ESCAPE:
				if (gbEscapeKeyIsPressed == false)
					gbEscapeKeyIsPressed = true;
				break;

			case 0x46: // for 'f' or 'F'
				if (gbFullscreen == false)
				{
					ToggleFullscreen();
					gbFullscreen = true;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = false;
				}

				break;

			case 0x31 : // 1
				giKeyPressed = 1;
				break;

			case 0x32: //2
				giKeyPressed = 2;
				break;

			case 0x33 : //3
				giKeyPressed = 3;
				break;

			case 0x34 : //4
				giKeyPressed = 4;
				break;

			default: 
				giKeyPressed = 0;
				break;
			}

			break;

		case WM_LBUTTONDOWN:
			break;

		case WM_CLOSE:
			uninitialize();
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default :
			break;
		}

		return (DefWindowProc(hwnd, iMsg, wParam, lParam));

	}

	void ToggleFullscreen(void)
	{
		//variable declarations
		MONITORINFO mi;

		//code
		if (gbFullscreen == false)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			if (dwStyle & WS_OVERLAPPEDWINDOW)
			{
				mi = { sizeof(MONITORINFO) };
				if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
				{
					SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
					SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				}
			}
			ShowCursor(FALSE);
		}

		else
		{
			//code
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}
	}



	void initialize(void)
	{
		void uninitialize(void);
		void resize(int, int);
		int LoadGLTextures(GLuint *, TCHAR[]);
		void LoadGLTextures();

		PIXELFORMATDESCRIPTOR pfd;
		int iPixelFormatIndex;

		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;

		ghdc = GetDC(ghwnd);

		iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

		if (iPixelFormatIndex == 0)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		ghrc = wglCreateContext(ghdc);
		if (ghrc == NULL)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (wglMakeCurrent(ghdc, ghrc) == false)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// GLEW initialization code for GLSL (It must be Here i.e; After creating OpenGL context but before using any OpenGL Function)

		GLenum glew_error = glewInit();

		if (glew_error != GLEW_OK)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// **** Vertex Shader ****

		gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		// provide source code to shader 

		const GLchar * vertexShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec2 vTexture0_Coord;" \
			"out vec2 out_texture0_coord ;" \
			"uniform mat4 u_mvp_matrix;" \
			"void main(void)" \
			"{" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"out_texture0_coord = vTexture0_Coord ; " \
			"}";

		glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

		// Compile Shader 

		glCompileShader(gVertexShaderObject);

		GLint iInfoLogLength = 0;
		GLint iShaderCompiledStatus = 0;
		char *szInfoLog = NULL;

		glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Vertex Shader compilation log :%s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}


		/// *** Fragment Shader *** ///

		gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec2 out_texture0_coord;" \
			"out vec4 FragColor;" \
			"uniform sampler2D u_texture0_sampler;" \
			"void main(void)" \
			"{" \
			"FragColor = texture(u_texture0_sampler,out_texture0_coord);" \
			"}";

		glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

		glCompileShader(gFragmentShaderObject);
		glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;

					glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Fragment Shader compilation log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		/// *** Shader Program *** ///

		gShaderProgramObject = glCreateProgram();

		glAttachShader(gShaderProgramObject, gVertexShaderObject);

		glAttachShader(gShaderProgramObject, gFragmentShaderObject);

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

		glLinkProgram(gShaderProgramObject);

		GLint iShaderProgramLinkStatus = 0;
		glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);

		if (iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Shader Program Link log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		// get MVP uniform location

		gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

		gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

		/// *** vertices , colors , shader attribs , vbo ,vao initializations *** /// 

		GLfloat squareVertices[] =
		{
		
		 1.0f, 1.0f, 1.0f,

		
		-1.0f, 1.0f, 1.0f,

		
		-1.0f, -1.0f, 1.0f,

		
		1.0f, -1.0f, 1.0f

		};

	
		/*const GLfloat squareTexcoords[] =
		{
			1.0f, 1.0f,
		
			0.0f, 1.0f,
		
			0.0f, 0.0f,
		
			1.0f, 0.0f
		
		};*/

		// Square code 

		glGenVertexArrays(1, &gVao_Smiley);
		glBindVertexArray(gVao_Smiley);

		// Vbo for position 
		glGenBuffers(1, &gVbo_Smiley_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Smiley_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// vbo for texture 

		glGenBuffers(1, &gVbo_Smiley_texture);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Smiley_texture);
		/*glBufferData(GL_ARRAY_BUFFER, sizeof(squareTexcoords), squareTexcoords, GL_STATIC_DRAW);*/
		glBufferData(GL_ARRAY_BUFFER,2* 4 * sizeof(GL_FLOAT), NULL, GL_DYNAMIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);

		//-----------------

		glShadeModel(GL_SMOOTH);

		glClearDepth(1.0f);

		glEnable(GL_DEPTH_TEST);

		glDepthFunc(GL_LEQUAL);

		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		glEnable(GL_CULL_FACE);

		LoadGLTextures(&gTexture_Smiley, MAKEINTRESOURCE(IDBITMAP_SMILEY));

		LoadGLTextures();  // for white colored texture

		glEnable(GL_TEXTURE_2D);

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		gPerspectiveProjectionMatrix = mat4::identity();

		resize(WIN_WIDTH, WIN_HEIGHT);
	}

	int LoadGLTextures(GLuint *texture,TCHAR imageResourceId[])
	{
		HBITMAP hBitmap;
		BITMAP bmp;
		int iStatus = FALSE;

		glGenTextures(1, texture); // 1 image

		hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

		if (hBitmap)
		{
			iStatus = TRUE;
			GetObject(hBitmap, sizeof(bmp), &bmp);
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glBindTexture(GL_TEXTURE_2D, *texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_RGB,
				bmp.bmWidth,
				bmp.bmHeight,
				0,
				GL_BGR,
				GL_UNSIGNED_BYTE,
				bmp.bmBits);

			// Create mipmaps for this texture for better image quality

			glGenerateMipmap(GL_TEXTURE_2D);

			DeleteObject(hBitmap);
		}

		return (iStatus);
	}

	void LoadGLTextures()
	{
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glGenTextures(1,&gTexture_White); // 1 image

		glBindTexture(GL_TEXTURE_2D,gTexture_White);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkImage);

		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	}

	void display(void)
	{
		GLfloat quadTexture[8];

		if (giKeyPressed == 1)   // Bottom left one fourth
		{

			quadTexture[0] = 0.5f;
			quadTexture[1] = 0.5f;
			quadTexture[2] = 0.0f;
			quadTexture[3] = 0.5f;
			quadTexture[4] = 0.0f;
			quadTexture[5] = 0.0f;
			quadTexture[6] = 0.5f;
			quadTexture[7] = 0.0f;
		}

		//else if (giKeyPressed ==2)   // Full smiley
		//{

		//	quadTexture[0] = 1.0f;
		//	quadTexture[1] = 1.0f;
		//	quadTexture[2] = 0.0f;
		//	quadTexture[3] = 1.0f;
		//	quadTexture[4] = 0.0f;
		//	quadTexture[5] = 0.0f;
		//	quadTexture[6] = 1.0f;
		//	quadTexture[7] = 0.0f;
		//}

		else if (giKeyPressed == 3) // four full smileys in square 
		{

			quadTexture[0] = 2.0f;
			quadTexture[1] = 2.0f;
			quadTexture[2] = 0.0f;
			quadTexture[3] = 2.0f;
			quadTexture[4] = 0.0f;
			quadTexture[5] = 0.0f;
			quadTexture[6] = 2.0f;
			quadTexture[7] = 0.0f;
		}

		else if (giKeyPressed == 4) // only yellow colour in square 
		{

			quadTexture[0] = 0.5f;
			quadTexture[1] = 0.5f;
			quadTexture[2] = 0.5f;
			quadTexture[3] = 0.5f;
			quadTexture[4] = 0.5f;
			quadTexture[5] = 0.5f;
			quadTexture[6] = 0.5f;
			quadTexture[7] = 0.5f;
		}

		else   // for 2 key press of full smiley and default case , full image coordinates will be passed.
		{

			quadTexture[0] = 1.0f;
			quadTexture[1] = 1.0f;
			quadTexture[2] = 0.0f;
			quadTexture[3] = 1.0f;
			quadTexture[4] = 0.0f;
			quadTexture[5] = 0.0f;
			quadTexture[6] = 1.0f;
			quadTexture[7] = 0.0f;
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);

		mat4 modelViewMatrix;
		mat4 modelViewProjectionMatrix;

		// OpenGL drawing 

		modelViewMatrix = mat4::identity();

		// Apply z axis translation to go deep into the screen by -6.0

		modelViewMatrix = translate(0.0f, 0.0f, -6.0f);

		modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

		// Pass above modelviewprojectionmatrix to the vertex shader in 'u_mvp_matrix' shader variable whose position we have already calculated by using  glGetUniformLocation()

		glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		// bind with texture 

		glActiveTexture(GL_TEXTURE0); // 0 th texture - corresponds to VDG_ATTRIBUTE_TEXTURE0

		if (giKeyPressed ==1 || giKeyPressed ==2 || giKeyPressed ==3 || giKeyPressed ==4)
		{
			glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
		}

		else
		{
			glBindTexture(GL_TEXTURE_2D, gTexture_White);
		}

		glUniform1i(gTexture_sampler_uniform, 0); // 0th sampler enable (as we have only one texture sampler in fragment shader)

		// bind vao
      
		glBindVertexArray(gVao_Smiley);

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Smiley_texture);

		glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexture), quadTexture, GL_DYNAMIC_DRAW);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4); 

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
		
		glUseProgram(0);

		SwapBuffers(ghdc);
	}


	void resize(int width, int height)
	{
		if (height == 0)
			height = 1;

		glViewport(0, 0, (GLsizei)width, (GLsizei)height);

		gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	}


	void uninitialize(void)
	{
		if (gbFullscreen == true)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}

		// Pyramid
		
		if(gVao_Smiley)
		{
			glDeleteVertexArrays(1, &gVao_Smiley);
			gVao_Smiley = 0;
		}

		if (gVbo_Smiley_position)
		{
			glDeleteBuffers(1, &gVbo_Smiley_position);
			gVbo_Smiley_position = 0;
		}

		if (gVbo_Smiley_texture)
		{
			glDeleteBuffers(1, &gVbo_Smiley_texture);
			gVbo_Smiley_texture = 0;
		}

		if (gTexture_Smiley)
		{
			glDeleteTextures(1, &gTexture_Smiley);
			gTexture_Smiley = 0;
		}

		glDetachShader(gShaderProgramObject, gVertexShaderObject);

		glDetachShader(gShaderProgramObject, gFragmentShaderObject);

		glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;

		glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);

		wglMakeCurrent(NULL, NULL);

		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

		if (gpfile)
		{
			fprintf(gpfile, "Log file is successfully closed.\n");
			fclose(gpfile);
			gpfile = NULL;
		}
	}
