#include <stdio.h>
#include <windows.h>

#include <glew.h>
#include <gl\GL.h>

#include "vmath.h"
#include "OGLPP_Texture.h"

#pragma comment(lib,"glew32.lib")
#pragma comment (lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};


// Prototype of WndProc

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpfile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof (WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Pyramid;
GLuint gVbo_Pyramid_position;
GLuint gVbo_Pyramid_texture;

GLuint gVao_Cube;
GLuint gVbo_Cube_position;
GLuint gVbo_Cube_texture;

GLuint gMVPUniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat gAnglePyramid = 0.0f;
GLfloat gAngleCube = 0.0f;

GLuint gTexture_sampler_uniform;
GLuint gTexture_Kundali;
GLuint gTexture_Stone;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void spin(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP_Texture");
	bool bDone = false;

	if (fopen_s(&gpfile,"Log.txt","w") !=0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created.... \n Exiting......"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpfile, "Log file is successfully opened. \n");
	}

	// Initialise members of struct WNDCLASSEX

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	
	// register class.

	RegisterClassEx(&wndClass);

	// Create window 

	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable pipeline Texture"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	// Message loop .

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{
			// rendering function.
			display();

			spin();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}

		uninitialize();

		return ((int)msg.wParam);
}

	LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
	{
		void resize(int, int);
		void ToggleFullscreen(void);
		void uninitialize(void);

		static WORD xMouse = NULL;
		static WORD yMouse = NULL;

		switch (iMsg)
		{
		case WM_ACTIVATE:
			 
			if (HIWORD(wParam) == 0) // if 0 , window is active 
			{
				gbActiveWindow = true;
			}

			else
			{
				gbActiveWindow = false;
			}

			break;

		case WM_ERASEBKGND:
			return (0);

		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
     
		case  WM_KEYDOWN:
			  
			switch (wParam)
			{
			case VK_ESCAPE:
				if (gbEscapeKeyIsPressed == false)
					gbEscapeKeyIsPressed = true;
				break;

			case 0x46: // for 'f' or 'F'
				if (gbFullscreen == false)
				{
					ToggleFullscreen();
					gbFullscreen = true;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = false;
				}

				break;

			default: 
				break;
			}

			break;

		case WM_LBUTTONDOWN:
			break;

		case WM_CLOSE:
			uninitialize();
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default :
			break;
		}

		return (DefWindowProc(hwnd, iMsg, wParam, lParam));

	}

	void ToggleFullscreen(void)
	{
		//variable declarations
		MONITORINFO mi;

		//code
		if (gbFullscreen == false)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			if (dwStyle & WS_OVERLAPPEDWINDOW)
			{
				mi = { sizeof(MONITORINFO) };
				if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
				{
					SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
					SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				}
			}
			ShowCursor(FALSE);
		}

		else
		{
			//code
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}
	}



	void initialize(void)
	{
		void uninitialize(void);
		void resize(int, int);
		int LoadGLTextures(GLuint *, TCHAR[]);

		PIXELFORMATDESCRIPTOR pfd;
		int iPixelFormatIndex;

		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;

		ghdc = GetDC(ghwnd);

		iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

		if (iPixelFormatIndex == 0)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		ghrc = wglCreateContext(ghdc);
		if (ghrc == NULL)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (wglMakeCurrent(ghdc, ghrc) == false)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// GLEW initialization code for GLSL (It must be Here i.e; After creating OpenGL context but before using any OpenGL Function)

		GLenum glew_error = glewInit();

		if (glew_error != GLEW_OK)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// **** Vertex Shader ****

		gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		// provide source code to shader 

		const GLchar * vertexShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec2 vTexture0_Coord;" \
			"out vec2 out_texture0_coord ;" \
			"uniform mat4 u_mvp_matrix;" \
			"void main(void)" \
			"{" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"out_texture0_coord = vTexture0_Coord ; " \
			"}";

		glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

		// Compile Shader 

		glCompileShader(gVertexShaderObject);

		GLint iInfoLogLength = 0;
		GLint iShaderCompiledStatus = 0;
		char *szInfoLog = NULL;

		glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Vertex Shader compilation log :%s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}


		/// *** Fragment Shader *** ///

		gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec2 out_texture0_coord;" \
			"out vec4 FragColor;" \
			"uniform sampler2D u_texture0_sampler;" \
			"void main(void)" \
			"{" \
			"FragColor = texture(u_texture0_sampler,out_texture0_coord);" \
			"}";

		glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

		glCompileShader(gFragmentShaderObject);
		glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;

					glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Fragment Shader compilation log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		/// *** Shader Program *** ///

		gShaderProgramObject = glCreateProgram();

		glAttachShader(gShaderProgramObject, gVertexShaderObject);

		glAttachShader(gShaderProgramObject, gFragmentShaderObject);

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

		glLinkProgram(gShaderProgramObject);

		GLint iShaderProgramLinkStatus = 0;
		glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);

		if (iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Shader Program Link log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		// get MVP uniform location

		gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

		gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

		/// *** vertices , colors , shader attribs , vbo ,vao initializations *** ///

		const GLfloat pyramidVertices[] =
		{
			0,1,0,  // front top
			-1,-1,1, // front left
			1,-1,1, // front right

			0,1,0, // right top
			1,-1,1, // right left
			1,-1,-1, //right right

			0,1,0 , // back top
			1,-1,-1, // back left
			-1,-1,-1, //back right

			0,1,0, // left top
			-1,-1,-1, // left left
			-1,-1,1 // left right

		};


		const GLfloat pyramidTexcoords[] =
		{
			0.5,1.0, // front top
			0.0,0.0, // front left
			1.0,0.0, // front right

			0.5,1.0,  // right top
			1.0,0.0, //right left
			0.0,0.0, //right right

			0.5,1.0, // back top
			1.0,0.0, // back left
			0.0,0.0, // back right

			0.5,1.0, // left top
			0.0,0.0, //  left left
			1.0,0.0  // left right 
		};


		// seperated two arrays of cube according to above mixed array 

		GLfloat cubeVertices[] =
		{
			// top surface 

			1.0f,1.0f,-1.0f, // top right 
			-1.0f,1.0f,-1.0f, // top left
			-1.0f,1.0f,1.0f, // bottom left
			1.0f,1.0f,1.0f, //bottom right


			// bottom surface 

			 1.0f,-1.0f,1.0f, // top right 
			-1.0f,-1.0f,1.0f, // top left
			-1.0f,-1.0f,-1.0f, // bottom left
			 1.0f,-1.0f,-1.0f, //bottom right


			// front surface 

			1.0f,1.0f,1.0f, // top right 
			-1.0f,1.0f,1.0f, // top left
			-1.0f,-1.0f,1.0f, // bottom left
			1.0f,-1.0f,1.0f, //bottom right


			// back surface 

			1.0f,-1.0f,-1.0f, // top right 
			-1.0f,-1.0f,-1.0f, // top left
			-1.0f,1.0f,-1.0f, // bottom left
			1.0f,1.0f,-1.0f, //bottom right


			// left surface 

			-1.0f,1.0f,1.0f, // top right 
			-1.0f,1.0f,-1.0f, // top left
			-1.0f,-1.0f,-1.0f, // bottom left
			-1.0f,-1.0f,1.0f, //bottom right

			// right surface 

			1.0f,1.0f,-1.0f, // top right 
			1.0f,1.0f,1.0f, // top left
			1.0f,-1.0f,1.0f, // bottom left
			1.0f,-1.0f,-1.0f //bottom right

		};

		// If above -1.0f or 1.0f values make cube much larger than Pyramid, then follow the code in following loop which 
		// will convert all 1s and -1s to -0.75 or 0.75

		for (int i = 0; i < 72; i++)
		{
			if (cubeVertices[i] < 0.0f)
				cubeVertices[i] = cubeVertices[i] + 0.25f;
			else if (cubeVertices[i] > 0.0f)
				cubeVertices[i] = cubeVertices[i] - 0.25f;
			else
				cubeVertices[i] = cubeVertices[i]; // no change
		}

		const GLfloat cubeTexcoords[] =
		{
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,

			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,

			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,

			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,

			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,

			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,
		};

		// Pyramid Code

		glGenVertexArrays(1, &gVao_Pyramid);
		glBindVertexArray(gVao_Pyramid);

		// vbo for position
		glGenBuffers(1, &gVbo_Pyramid_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL); // 3 for x,y,z vertices
		glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// Vbo for texture
		glGenBuffers(1, &gVbo_Pyramid_texture);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_texture);
		glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTexcoords), pyramidTexcoords, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL); // 2 for s and t of texture
		glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);


		// Cube code 

		glGenVertexArrays(1, &gVao_Cube);
		glBindVertexArray(gVao_Cube);

		// Vbo for position 
		glGenBuffers(1, &gVbo_Cube_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// vbo for texture 

		glGenBuffers(1, &gVbo_Cube_texture);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_texture);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexcoords), cubeTexcoords, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);

		//-----------------

		glShadeModel(GL_SMOOTH);

		glClearDepth(1.0f);

		glEnable(GL_DEPTH_TEST);

		glDepthFunc(GL_LEQUAL);

		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		glEnable(GL_CULL_FACE);

		LoadGLTextures(&gTexture_Kundali, MAKEINTRESOURCE(IDBITMAP_KUNDALI));
		LoadGLTextures(&gTexture_Stone, MAKEINTRESOURCE(IDBITMAP_STONE));

		glEnable(GL_TEXTURE_2D);

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		gPerspectiveProjectionMatrix = mat4::identity();

		resize(WIN_WIDTH, WIN_HEIGHT);
	}

	int LoadGLTextures(GLuint *texture,TCHAR imageResourceId[])
	{
		HBITMAP hBitmap;
		BITMAP bmp;
		int iStatus = FALSE;

		glGenTextures(1, texture); // 1 image

		hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

		if (hBitmap)
		{
			iStatus = TRUE;
			GetObject(hBitmap, sizeof(bmp), &bmp);
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glBindTexture(GL_TEXTURE_2D, *texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_RGB,
				bmp.bmWidth,
				bmp.bmHeight,
				0,
				GL_BGR,
				GL_UNSIGNED_BYTE,
				bmp.bmBits);

			// Create mipmaps for this texture for better image quality

			glGenerateMipmap(GL_TEXTURE_2D);

			DeleteObject(hBitmap);
		}

		return (iStatus);
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);

		mat4 modelViewMatrix;
		mat4 rotationMatrix;
		mat4 modelViewProjectionMatrix;

		// OpenGL drawing 

		// Pyramid code

		modelViewMatrix = mat4::identity();

		// Apply z axis translation to go deep into the screen by -6.0

		modelViewMatrix = translate(-1.5f, 0.0f, -6.0f);

		rotationMatrix = rotate(gAnglePyramid, 0.0f, 1.0f, 0.0f);

		modelViewMatrix = modelViewMatrix * rotationMatrix;

		modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

		// Pass above modelviewprojectionmatrix to the vertex shader in 'u_mvp_matrix' shader variable whose position we have already calculated by using  glGetUniformLocation()

		glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		// bind with pyramid texture 

		glActiveTexture(GL_TEXTURE0); // 0 th texture - corresponds to VDG_ATTRIBUTE_TEXTURE0
		glBindTexture(GL_TEXTURE_2D, gTexture_Stone);

		glUniform1i(gTexture_sampler_uniform, 0); // 0th sampler enable (as we have only one texture sampler in fragment shader)

		// bind vao
      
		glBindVertexArray(gVao_Pyramid);

		glDrawArrays(GL_TRIANGLES, 0, 12); // 3 (each with its x,y,z) vertices for 4 faces of pyramid.

		glBindVertexArray(0);

		// Cube code 

		modelViewMatrix = mat4::identity();

		modelViewMatrix = translate(1.5f, 0.0f, -6.0f);

		rotationMatrix = rotate(gAngleCube, gAngleCube, gAngleCube);

		modelViewMatrix = modelViewMatrix * rotationMatrix;

		modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with cube texture 

		glActiveTexture(GL_TEXTURE0);

		glBindTexture(GL_TEXTURE_2D, gTexture_Kundali);

		glUniform1i(gTexture_sampler_uniform, 0);

		// Bind vao 
		glBindVertexArray(gVao_Cube);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

		glBindVertexArray(0);

		glUseProgram(0);

		SwapBuffers(ghdc);
	}


	void resize(int width, int height)
	{
		if (height == 0)
			height = 1;

		glViewport(0, 0, (GLsizei)width, (GLsizei)height);

		gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	}

	void spin(void)
	{
		gAnglePyramid = gAnglePyramid + 0.09f;
		
		if (gAnglePyramid >= 360.0f)
			gAnglePyramid = gAnglePyramid - 360.0f;

		gAngleCube = gAngleCube + 0.09f;

		if (gAngleCube >= 360.0f)
			gAngleCube = gAngleCube - 360.0f;
	}

	void uninitialize(void)
	{
		if (gbFullscreen == true)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}

		// Pyramid
		
		if(gVao_Pyramid)
		{
			glDeleteVertexArrays(1, &gVao_Pyramid);
			gVao_Pyramid = 0;
		}

		if (gVbo_Pyramid_position)
		{
			glDeleteBuffers(1, &gVbo_Pyramid_position);
			gVbo_Pyramid_position = 0;
		}

		if (gVbo_Pyramid_texture)
		{
			glDeleteBuffers(1, &gVbo_Pyramid_texture);
			gVbo_Pyramid_texture = 0;
		}

		if (gTexture_Stone)
		{
			glDeleteTextures(1, &gTexture_Stone);
			gTexture_Stone = 0;
		}

		// Cube

		if (gVao_Cube)
		{
			glDeleteVertexArrays(1, &gVao_Cube);
			gVao_Cube = 0;
		}

		if (gVbo_Cube_position)
		{
			glDeleteBuffers(1, &gVbo_Cube_position);
			gVbo_Cube_position = 0;
		}

		if (gVbo_Cube_texture)
		{
			glDeleteBuffers(1, &gVbo_Cube_texture);
			gVbo_Cube_texture = 0;
		}

		if (gTexture_Kundali)
		{
			glDeleteTextures(1, &gTexture_Kundali);
			gTexture_Kundali = 0;
		}

		glDetachShader(gShaderProgramObject, gVertexShaderObject);

		glDetachShader(gShaderProgramObject, gFragmentShaderObject);

		glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;

		glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);

		wglMakeCurrent(NULL, NULL);

		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

		if (gpfile)
		{
			fprintf(gpfile, "Log file is successfully closed.\n");
			fclose(gpfile);
			gpfile = NULL;
		}
	}
