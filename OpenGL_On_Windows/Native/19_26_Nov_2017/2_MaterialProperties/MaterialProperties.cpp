
#include <Windows.h>
#include <gl/GL.h>
#include <GL\GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbIsEscapeKeyPressed = false;
bool gbFullscreen = false;
bool gbIsLightingEnabled = false;
GLUquadric *quadric = NULL;

GLboolean gbIs_X_AxisRotationOfLight = GL_TRUE;
GLboolean gbIs_Y_AxisRotationOfLight = GL_FALSE;
GLboolean gbIs_Z_AxisRotationOfLight = GL_FALSE;

float lightRotationAngle = 0.0f;

void resize(int, int);

// Arrays required for lighting.
GLfloat light0_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light0_position[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light0_diffuse[] = { 1.0f,1.0f,1.0f,1.0f }; // White light.
GLfloat light0_specular[] = { 1.0f,1.0f,1.0f,1.0f };

GLfloat light_model_ambient[] = { 0.2f,0.2f,0.2f,0.0f };
GLfloat light_model_local_viewer[] = { 0.0f };


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	void initialize(void);
	void uninitialize(void);
	void display(void);
	void updateAngle(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OGL_MaterialProperties");
	bool bDone = false;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = CreateSolidBrush(RGB(1, 1, 1));
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx
	(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGl fixed function pipeline : Material Properties"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{

			if (gbActiveWindow == true)
			{
				if (gbIsEscapeKeyPressed == true)
					bDone = true;
				updateAngle();
				display();
			}
		}

	}

	uninitialize();
	return (int(msg.wParam));

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbIsEscapeKeyPressed = true;
			break;

		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		case 0x4C: // for 'l' or 'L'
			if (gbIsLightingEnabled == false)
			{
				glEnable(GL_LIGHTING);
				gbIsLightingEnabled = true;
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbIsLightingEnabled = false;
			}
			break;

		case 0x58: // for 'x' or 'X'
			
			gbIs_X_AxisRotationOfLight = GL_TRUE;
			gbIs_Y_AxisRotationOfLight = GL_FALSE;
			gbIs_Z_AxisRotationOfLight = GL_FALSE;

			break;

		case 0x59: // for 'y' or 'Y'
			
			gbIs_X_AxisRotationOfLight = GL_FALSE;
			gbIs_Y_AxisRotationOfLight = GL_TRUE;
			gbIs_Z_AxisRotationOfLight = GL_FALSE;

			break;

		case 0x5A: // for 'z' or 'Z'
			
			gbIs_X_AxisRotationOfLight = GL_FALSE;
			gbIs_Y_AxisRotationOfLight = GL_FALSE;
			gbIs_Z_AxisRotationOfLight = GL_TRUE;

			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}


void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}

void initialize(void)
{

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);    // Dark Gray background.

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular);
	glEnable(GL_LIGHT0);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	void drawSphere(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();

	gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	glPushMatrix();

	if (gbIs_X_AxisRotationOfLight)
	{
		light0_position[1] = lightRotationAngle;
		light0_position[0] = 0.0f;
		

		glRotatef(lightRotationAngle, 1.0f, 0.0f, 0.0f);
		
	}

	else if (gbIs_Y_AxisRotationOfLight)
	{
		light0_position[0] = lightRotationAngle;
		light0_position[1] = 0.0f;


		glRotatef(lightRotationAngle, 0.0f, 1.0f, 0.0f);
		
	}

	else if (gbIs_Z_AxisRotationOfLight)
	{
		light0_position[0] = lightRotationAngle;
		light0_position[1] = 0.0f;

		glRotatef(lightRotationAngle, 0.0f, 0.0f, 1.0f);
		
	}

	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);

	glPopMatrix();

	glPushMatrix();

	glTranslatef(-0.8f, 1.0f, -3.0f);
	
	/// Emerald

	GLfloat material_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
	GLfloat material_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
	GLfloat material_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
	GLfloat material_shinyness[] = { 0.6f*128.0f };

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///


	/// Jade

	material_ambient[0] = 0.135f;
	material_ambient[1] = 0.2225f;
	material_ambient[2] = 0.1575f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.54f;
	material_diffuse[1] = 0.89f;
	material_diffuse[2] = 0.63f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.316228f;
	material_specular[1] = 0.316228f;
	material_specular[2] = 0.316228f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.1f* 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Obsidian

	material_ambient[0] = 0.05375f;
	material_ambient[1] = 0.05f;
	material_ambient[2] = 0.06625f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.18275f;
	material_diffuse[1] = 0.17f;
	material_diffuse[2] = 0.22525f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.332741f;
	material_specular[1] = 0.328634f;
	material_specular[2] = 0.346435f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.3f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Pearl

	material_ambient[0] = 0.25f;
	material_ambient[1] = 0.20725f;
	material_ambient[2] = 0.20725f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 1.0f;
	material_diffuse[1] = 0.829f;
	material_diffuse[2] = 0.829f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.296648f;
	material_specular[1] = 0.296648f;
	material_specular[2] = 0.296648f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.088f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Ruby

	material_ambient[0] = 0.1745f;
	material_ambient[1] = 0.01175f;
	material_ambient[2] = 0.01175f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.61424f;
	material_diffuse[1] = 0.04136f;
	material_diffuse[2] = 0.04136f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.727811f;
	material_specular[1] = 0.626959f;
	material_specular[2] = 0.626959f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.6f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Turquoise

	material_ambient[0] = 0.1f;
	material_ambient[1] = 0.18725f;
	material_ambient[2] = 0.1745f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.396f;
	material_diffuse[1] = 0.74151f;
	material_diffuse[2] = 0.69102f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.297254f;
	material_specular[1] = 0.30829f;
	material_specular[2] = 0.306678f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.1f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///


	/// Brass

	glTranslatef(0.4f, 2.0f, 0.0f);

	material_ambient[0] = 0.329412f;
	material_ambient[1] = 0.223529f;
	material_ambient[2] = 0.027451f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.780392f;
	material_diffuse[1] = 0.568627f;
	material_diffuse[2] = 0.113725f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.992157f;
	material_specular[1] = 0.941176f;
	material_specular[2] = 0.807843f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.21794872f * 128.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Bronze

	material_ambient[0] = 0.2125f;
	material_ambient[1] = 0.1275f;
	material_ambient[2] = 0.054f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.714f;
	material_diffuse[1] = 0.4284f;
	material_diffuse[2] = 0.18144f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.393548f;
	material_specular[1] = 0.271906f;
	material_specular[2] = 0.166721f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.2f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Chrome

	material_ambient[0] = 0.25f;
	material_ambient[1] = 0.25f;
	material_ambient[2] = 0.25f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.4f;
	material_diffuse[1] = 0.4f;
	material_diffuse[2] = 0.4f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.774597f;
	material_specular[1] = 0.774597f;
	material_specular[2] = 0.774597f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.6f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Copper

	material_ambient[0] = 0.19125f;
	material_ambient[1] = 0.0735f;
	material_ambient[2] = 0.0225f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.7038f;
	material_diffuse[1] = 0.27048f;
	material_diffuse[2] = 0.0828f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.256777f;
	material_specular[1] = 0.137622f;
	material_specular[2] = 0.086014f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.6f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///


	/// Gold

	material_ambient[0] = 0.24725f;
	material_ambient[1] = 0.1995f;
	material_ambient[2] = 0.0745f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.75164f;
	material_diffuse[1] = 0.60648f;
	material_diffuse[2] = 0.22648f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.628281f;
	material_specular[1] = 0.555802f;
	material_specular[2] = 0.366065f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.4f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///


	/// Silver

	material_ambient[0] = 0.19225f;
	material_ambient[1] = 0.19225f;
	material_ambient[2] = 0.19225f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.50754f;
	material_diffuse[1] = 0.50754f;
	material_diffuse[2] = 0.50754f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.508273f;
	material_specular[1] = 0.508273f;
	material_specular[2] = 0.508273f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.4f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	glTranslatef(0.4f, 2.0f, 0.0f);

	/// Black

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.01f;
	material_diffuse[1] = 0.01f;
	material_diffuse[2] = 0.01f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.50f;
	material_specular[1] = 0.50f;
	material_specular[2] = 0.50f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.25f * 128.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Cyan

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.1f;
	material_ambient[2] = 0.06f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.0f;
	material_diffuse[1] = 0.50980392f;
	material_diffuse[2] = 0.50980392f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.50196078f;
	material_specular[1] = 0.50196078f;
	material_specular[2] = 0.50196078f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.25f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Green

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.1f;
	material_diffuse[1] = 0.35f;
	material_diffuse[2] = 0.1f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.45f;
	material_specular[1] = 0.55f;
	material_specular[2] = 0.45f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.25f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Red

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.5f;
	material_diffuse[1] = 0.0f;
	material_diffuse[2] = 0.0f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.7f;
	material_specular[1] = 0.6f;
	material_specular[2] = 0.6f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.25f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// White

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.55f;
	material_diffuse[1] = 0.55f;
	material_diffuse[2] = 0.55f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.70f;
	material_specular[1] = 0.70f;
	material_specular[2] = 0.70f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.25f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///
	
	/// Yellow Plastic

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.5f;
	material_diffuse[1] = 0.5f;
	material_diffuse[2] = 0.0f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.60f;
	material_specular[1] = 0.60f;
	material_specular[2] = 0.50f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.25f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///


	glTranslatef(0.4f, 2.0f, 0.0f);

	/// Black

	material_ambient[0] = 0.02f;
	material_ambient[1] = 0.02f;
	material_ambient[2] = 0.02f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.01f;
	material_diffuse[1] = 0.01f;
	material_diffuse[2] = 0.01f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.4f;
	material_specular[1] = 0.4f;
	material_specular[2] = 0.4f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.078125f * 128.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///


	/// Cyan

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.05f;
	material_ambient[2] = 0.05f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.4f;
	material_diffuse[1] = 0.5f;
	material_diffuse[2] = 0.5f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.04f;
	material_specular[1] = 0.7f;
	material_specular[2] = 0.7f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.078125f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Green

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.05f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.4f;
	material_diffuse[1] = 0.5f;
	material_diffuse[2] = 0.4f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.04f;
	material_specular[1] = 0.7f;
	material_specular[2] = 0.04f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.078125f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Red

	material_ambient[0] = 0.05f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.5f;
	material_diffuse[1] = 0.4f;
	material_diffuse[2] = 0.4f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.7f;
	material_specular[1] = 0.04f;
	material_specular[2] = 0.04f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.078125f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// White

	material_ambient[0] = 0.05f;
	material_ambient[1] = 0.05f;
	material_ambient[2] = 0.05f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.5f;
	material_diffuse[1] = 0.5f;
	material_diffuse[2] = 0.5f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.7f;
	material_specular[1] = 0.7f;
	material_specular[2] = 0.7f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.078125f * 128.0f;

	//glTranslatef(0.3f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///

	/// Yellow Rubber

	material_ambient[0] = 0.05f;
	material_ambient[1] = 0.05f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.5f;
	material_diffuse[1] = 0.5f;
	material_diffuse[2] = 0.4f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.7f;
	material_specular[1] = 0.7f;
	material_specular[2] = 0.04f;
	material_specular[3] = 1.0f;

	material_shinyness[0] = 0.078125f * 128.0f;

	glTranslatef(0.0f, -0.4f, 0.0f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	drawSphere();

	///
	/*int sphereNumber = 0;

	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			sphereNumber++;

			glTranslatef(0.3f, 0.0f, 0.0f);

			glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
			glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
			glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness); 

			drawSphere();
		}

		glTranslatef(-1.2f, -0.4f, 0.0f);
	}*/


	glPopMatrix();

	//glPopMatrix();

	SwapBuffers(ghdc);

}

void drawSphere(void)
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	quadric = gluNewQuadric();

	gluSphere(quadric, 0.1, 50, 50); 
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void updateAngle()
{
	lightRotationAngle = lightRotationAngle + 0.5f;
	if (lightRotationAngle >= 360.0f)
		lightRotationAngle = 0.0f;

}

void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}
