#include <stdio.h>
#include <windows.h>

#include <glew.h>
#include <gl\GL.h>

#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment (lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};


// Prototype of WndProc

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpfile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof (WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Cube;
GLuint gVbo_Cube_position;
GLuint gVbo_Cube_normal;

GLuint gModelViewMatrixUniform,gProjectionMatrixUniform;
GLuint gLdUniform, gKdUniform, gLightPositionUniform;

GLuint gLKeyPressedUniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat gAngleCube = 0.0f;

bool gbAnimate;
bool gbLight;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void spin(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP_Texture");
	bool bDone = false;

	if (fopen_s(&gpfile,"Log.txt","w") !=0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created.... \n Exiting......"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpfile, "Log file is successfully opened. \n");
	}

	// Initialise members of struct WNDCLASSEX

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	
	// register class.

	RegisterClassEx(&wndClass);

	// Create window 

	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable pipeline Diffused Light"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	// Message loop .

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{
			// rendering function.
			display();

			if (gbAnimate == true)
			{
				spin();
			}

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}

		uninitialize();

		return ((int)msg.wParam);
}

	LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
	{
		void resize(int, int);
		void ToggleFullscreen(void);
		void uninitialize(void);

		static bool bIsAKeyPressed = false;
		static bool bIsLKeyPressed = false;

		switch (iMsg)
		{
		case WM_ACTIVATE:
			 
			if (HIWORD(wParam) == 0) // if 0 , window is active 
			{
				gbActiveWindow = true;
			}

			else
			{
				gbActiveWindow = false;
			}

			break;

		case WM_ERASEBKGND:
			return (0);

		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
     
		case  WM_KEYDOWN:
			  
			switch (wParam)
			{
			case VK_ESCAPE:
				if (gbEscapeKeyIsPressed == false)
					gbEscapeKeyIsPressed = true;
				break;

			case 0x46: // for 'f' or 'F'
				if (gbFullscreen == false)
				{
					ToggleFullscreen();
					gbFullscreen = true;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = false;
				}

				break;

			case 0x41: // for 'A' or 'a'
				if (bIsAKeyPressed == false)
				{
					gbAnimate = true;
					bIsAKeyPressed = true;
				}
				else
				{
					gbAnimate = false;
					bIsAKeyPressed = false;
				}

				break;

			case 0x4C: // for 'L' or 'l'
				if (bIsLKeyPressed == false)
				{
					gbLight = true;
					bIsLKeyPressed = true;
				}
				else
				{
					gbLight = false;
					bIsLKeyPressed = false;
				}

				break;

			default: 
				break;
			}

			break;

		case WM_LBUTTONDOWN:
			break;

		case WM_CLOSE:
			uninitialize();
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default :
			break;
		}

		return (DefWindowProc(hwnd, iMsg, wParam, lParam));

	}

	void ToggleFullscreen(void)
	{
		//variable declarations
		MONITORINFO mi;

		//code
		if (gbFullscreen == false)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			if (dwStyle & WS_OVERLAPPEDWINDOW)
			{
				mi = { sizeof(MONITORINFO) };
				if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
				{
					SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
					SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				}
			}
			ShowCursor(FALSE);
		}

		else
		{
			//code
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}
	}



	void initialize(void)
	{
		void uninitialize(void);
		void resize(int, int);

		PIXELFORMATDESCRIPTOR pfd;
		int iPixelFormatIndex;

		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;

		ghdc = GetDC(ghwnd);

		iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

		if (iPixelFormatIndex == 0)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		ghrc = wglCreateContext(ghdc);
		if (ghrc == NULL)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (wglMakeCurrent(ghdc, ghrc) == false)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// GLEW initialization code for GLSL (It must be Here i.e; After creating OpenGL context but before using any OpenGL Function)

		GLenum glew_error = glewInit();

		if (glew_error != GLEW_OK)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// **** Vertex Shader ****

		gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		// provide source code to shader 

		const GLchar * vertexShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;" \
			"uniform mat4 u_model_view_matrix ;" \
			"uniform mat4 u_projection_matrix ;" \
			"uniform int u_LKeyPressed;" \
			"uniform vec3 u_Ld ;" \
			"uniform vec3 u_Kd ;" \
			"uniform vec4 u_light_position ;" \
			"out vec3 diffuse_light ;" \
			"void main(void)" \
			"{" \
			"if (u_LKeyPressed == 1)"\
			"{"\
			"vec4 eyeCoordinates = u_model_view_matrix * vPosition;"\
			"vec3 tnorm = normalize(mat3 (u_model_view_matrix) * vNormal );"\
			"vec3 s = normalize (vec3 (u_light_position - eyeCoordinates ));"\
			"diffuse_light = u_Ld * u_Kd * max(dot (s,tnorm), 0.0);"\
			"}"\
			"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition ;" \
			"}";

		glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

		// Compile Shader 

		glCompileShader(gVertexShaderObject);

		GLint iInfoLogLength = 0;
		GLint iShaderCompiledStatus = 0;
		char *szInfoLog = NULL;

		glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Vertex Shader compilation log :%s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}


		/// *** Fragment Shader *** ///

		gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec3 diffuse_light;" \
			"out vec4 FragColor;" \
			"uniform int u_LKeyPressed;" \
			"void main(void)" \
			"{" \
			"vec4 color;"\
			"if (u_LKeyPressed == 1)"\
			"{"\
			"color = vec4 (diffuse_light,1.0);"\
			"}"\
			"else"\
			"{"\
			"color = vec4 (1.0,1.0,1.0,1.0);"\
			"}"\
			"FragColor = color ;" \
			"}";

		glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

		glCompileShader(gFragmentShaderObject);
		glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;

					glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Fragment Shader compilation log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		/// *** Shader Program *** ///

		gShaderProgramObject = glCreateProgram();

		glAttachShader(gShaderProgramObject, gVertexShaderObject);

		glAttachShader(gShaderProgramObject, gFragmentShaderObject);

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

		glLinkProgram(gShaderProgramObject);

		GLint iShaderProgramLinkStatus = 0;
		glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);

		if (iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Shader Program Link log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		// get uniform locations

		gModelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_view_matrix");

		gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

		gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");

		gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");

		gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");

		gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

		/// *** vertices , colors , shader attribs , vbo ,vao initializations *** ///

		GLfloat cubeVertices[] =
		{
			// top surface 

			1.0f,1.0f,-1.0f, // top right 
			-1.0f,1.0f,-1.0f, // top left
			-1.0f,1.0f,1.0f, // bottom left
			1.0f,1.0f,1.0f, //bottom right


			// bottom surface 

			 1.0f,-1.0f,1.0f, // top right 
			-1.0f,-1.0f,1.0f, // top left
			-1.0f,-1.0f,-1.0f, // bottom left
			 1.0f,-1.0f,-1.0f, //bottom right


			// front surface 

			1.0f,1.0f,1.0f, // top right 
			-1.0f,1.0f,1.0f, // top left
			-1.0f,-1.0f,1.0f, // bottom left
			1.0f,-1.0f,1.0f, //bottom right


			// back surface 

			1.0f,-1.0f,-1.0f, // top right 
			-1.0f,-1.0f,-1.0f, // top left
			-1.0f,1.0f,-1.0f, // bottom left
			1.0f,1.0f,-1.0f, //bottom right


			// left surface 

			-1.0f,1.0f,1.0f, // top right 
			-1.0f,1.0f,-1.0f, // top left
			-1.0f,-1.0f,-1.0f, // bottom left
			-1.0f,-1.0f,1.0f, //bottom right

			// right surface 

			1.0f,1.0f,-1.0f, // top right 
			1.0f,1.0f,1.0f, // top left
			1.0f,-1.0f,1.0f, // bottom left
			1.0f,-1.0f,-1.0f //bottom right

		};

		// If above -1.0f or 1.0f values make cube much larger than Pyramid, then follow the code in following loop which 
		// will convert all 1s and -1s to -0.75 or 0.75

		for (int i = 0; i < 72; i++)
		{
			if (cubeVertices[i] < 0.0f)
				cubeVertices[i] = cubeVertices[i] + 0.25f;
			else if (cubeVertices[i] > 0.0f)
				cubeVertices[i] = cubeVertices[i] - 0.25f;
			else
				cubeVertices[i] = cubeVertices[i]; // no change
		}

		const GLfloat cubeNormals[] =
		{
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,

			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,

			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,

			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f
		};

		// Cube code 

		glGenVertexArrays(1, &gVao_Cube);
		glBindVertexArray(gVao_Cube);

		// Vbo for position 
		glGenBuffers(1, &gVbo_Cube_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// vbo for normal 

		glGenBuffers(1, &gVbo_Cube_normal);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube_normal);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);

		//-----------------

		glShadeModel(GL_SMOOTH);

		glClearDepth(1.0f);

		glEnable(GL_DEPTH_TEST);

		glDepthFunc(GL_LEQUAL);

		//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		glEnable(GL_CULL_FACE);

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		gPerspectiveProjectionMatrix = mat4::identity();

		gbAnimate = false;
		gbLight = false;

		resize(WIN_WIDTH, WIN_HEIGHT);
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);

		if (gbLight == true)
		{
			glUniform1i(gLKeyPressedUniform, 1);

			glUniform3f(gLdUniform, 1.0f, 1.0f, 1.0f);
			glUniform3f(gKdUniform, 0.5f, 0.5f, 0.5f);

			float lightPosition[] = { 0.0f,0.0f,2.0f,1.0f };
			glUniform4fv(gLightPositionUniform, 1, (GLfloat *)lightPosition);
		}

		else
		{
			glUniform1i(gLKeyPressedUniform, 0);
		}

		mat4 modelMatrix= mat4:: identity();
		mat4 rotationMatrix = mat4 :: identity();
		mat4 modelViewMatrix = mat4 :: identity();

		// OpenGL drawing 

		// Apply z axis translation to go deep into the screen by -6.0

		modelMatrix = translate(0.0f, 0.0f, -5.0f);

		rotationMatrix = rotate(gAngleCube,gAngleCube,gAngleCube);

		modelViewMatrix = modelMatrix * rotationMatrix;

		// Pass above modelviewprojectionmatrix to the vertex shader in 'u_mvp_matrix' shader variable whose position we have already calculated by using  glGetUniformLocation()

		glUniformMatrix4fv(gModelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);

		glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);


		// Cube code 

		// Bind vao 
		glBindVertexArray(gVao_Cube);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

		glBindVertexArray(0);

		glUseProgram(0);

		SwapBuffers(ghdc);
	}


	void resize(int width, int height)
	{
		if (height == 0)
			height = 1;

		glViewport(0, 0, (GLsizei)width, (GLsizei)height);

		gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	}

	void spin(void)
	{

		gAngleCube = gAngleCube + 0.09f;

		if (gAngleCube >= 360.0f)
			gAngleCube = gAngleCube - 360.0f;
	}

	void uninitialize(void)
	{
		if (gbFullscreen == true)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}


		// Cube

		if (gVao_Cube)
		{
			glDeleteVertexArrays(1, &gVao_Cube);
			gVao_Cube = 0;
		}

		if (gVbo_Cube_position)
		{
			glDeleteBuffers(1, &gVbo_Cube_position);
			gVbo_Cube_position = 0;
		}

		if (gVbo_Cube_normal)
		{
			glDeleteBuffers(1, &gVbo_Cube_normal);
			gVbo_Cube_normal = 0;
		}

		glDetachShader(gShaderProgramObject, gVertexShaderObject);

		glDetachShader(gShaderProgramObject, gFragmentShaderObject);

		glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;

		glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);

		wglMakeCurrent(NULL, NULL);

		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

		if (gpfile)
		{
			fprintf(gpfile, "Log file is successfully closed.\n");
			fclose(gpfile);
			gpfile = NULL;
		}
	}
