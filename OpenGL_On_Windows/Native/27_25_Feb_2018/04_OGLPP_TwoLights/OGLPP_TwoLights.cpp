#include <stdio.h>
#include <windows.h>

#include <glew.h>
#include <gl\GL.h>

#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment (lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};


// Prototype of WndProc

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpfile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof (WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Pyramid;
GLuint gVbo_Pyramid_position;
GLuint gVbo_Pyramid_normal;

GLuint gModelMatrixUniform,gViewMatrixUniform,gProjectionMatrixUniform;
GLuint gLa0Uniform,gLd0Uniform, gLs0Uniform,gLight0PositionUniform;
GLuint gLa1Uniform, gLd1Uniform, gLs1Uniform, gLight1PositionUniform;
GLuint gKaUniform, gKdUniform, gKsUniform, gMaterialShininessUniform;

mat4 gPerspectiveProjectionMatrix;

//GLfloat lightOneAmbient[] = {0.0f,0.0f,0.0f,1.0f};
//GLfloat lightOneDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
//GLfloat lightOneSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
//GLfloat lightOnePosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat light0_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light0_diffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light0_specular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light0_position[] = { -2.0f,1.0f,2.0f,0.0f };

GLfloat light1_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light1_diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light1_specular[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light1_position[] = { 2.0f,1.0f,2.0f,0.0f };

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;

GLfloat gAnglePyramid = 0.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void spin(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP_Texture");
	bool bDone = false;

	if (fopen_s(&gpfile,"Log.txt","w") !=0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created.... \n Exiting......"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpfile, "Log file is successfully opened. \n");
	}

	// Initialise members of struct WNDCLASSEX

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	
	// register class.

	RegisterClassEx(&wndClass);

	// Create window 

	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable pipeline Two Lights Per Vertex Phong Lighting"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	// Message loop .

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{
			// rendering function.
			display();

			spin();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}

		uninitialize();

		return ((int)msg.wParam);
}

	LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
	{
		void resize(int, int);
		void ToggleFullscreen(void);
		void uninitialize(void);

		switch (iMsg)
		{
		case WM_ACTIVATE:
			 
			if (HIWORD(wParam) == 0) // if 0 , window is active 
			{
				gbActiveWindow = true;
			}

			else
			{
				gbActiveWindow = false;
			}

			break;

		case WM_ERASEBKGND:
			return (0);

		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
     
		case  WM_KEYDOWN:
			  
			switch (wParam)
			{
			case VK_ESCAPE:
				if (gbEscapeKeyIsPressed == false)
					gbEscapeKeyIsPressed = true;
				break;

			case 0x46: // for 'f' or 'F'
				if (gbFullscreen == false)
				{
					ToggleFullscreen();
					gbFullscreen = true;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = false;
				}

				break;

			default: 
				break;
			}

			break;

		case WM_LBUTTONDOWN:
			break;

		case WM_CLOSE:
			uninitialize();
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default :
			break;
		}

		return (DefWindowProc(hwnd, iMsg, wParam, lParam));

	}

	void ToggleFullscreen(void)
	{
		//variable declarations
		MONITORINFO mi;

		//code
		if (gbFullscreen == false)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			if (dwStyle & WS_OVERLAPPEDWINDOW)
			{
				mi = { sizeof(MONITORINFO) };
				if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
				{
					SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
					SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				}
			}
			ShowCursor(FALSE);
		}

		else
		{
			//code
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}
	}



	void initialize(void)
	{
		void uninitialize(void);
		void resize(int, int);

		PIXELFORMATDESCRIPTOR pfd;
		int iPixelFormatIndex;

		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;

		ghdc = GetDC(ghwnd);

		iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

		if (iPixelFormatIndex == 0)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		ghrc = wglCreateContext(ghdc);
		if (ghrc == NULL)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (wglMakeCurrent(ghdc, ghrc) == false)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// GLEW initialization code for GLSL (It must be Here i.e; After creating OpenGL context but before using any OpenGL Function)

		GLenum glew_error = glewInit();

		if (glew_error != GLEW_OK)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// **** Vertex Shader ****

		gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		// provide source code to shader 

		const GLchar * vertexShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;" \
			"uniform mat4 u_model_matrix ;" \
			"uniform mat4 u_view_matrix ;" \
			"uniform mat4 u_projection_matrix ;" \
			"uniform vec3 u_La0 ;" \
			"uniform vec3 u_Ld0 ;" \
			"uniform vec3 u_Ls0 ;" \
			"uniform vec4 u_light0_position ;" \
			"uniform vec3 u_La1 ;" \
			"uniform vec3 u_Ld1 ;" \
			"uniform vec3 u_Ls1 ;" \
			"uniform vec4 u_light1_position ;" \
			"uniform vec3 u_Ka ;" \
			"uniform vec3 u_Kd ;" \
			"uniform vec3 u_Ks ;" \
			"uniform float u_material_shininess ;" \
			"out vec3 phong_ads_color ;" \
			"void main(void)" \
			"{" \
			"vec4 eye_coordinates = u_view_matrix *u_model_matrix* vPosition;"\
			"vec3 transformed_normals = normalize(mat3 (u_view_matrix * u_model_matrix) * vNormal );"\
			"vec3 light0_direction = normalize (vec3 (u_light0_position) - eye_coordinates.xyz);"\
			"vec3 light1_direction = normalize (vec3 (u_light1_position) - eye_coordinates.xyz);"\
			"float tn_dot_ld0 = max(dot (transformed_normals ,light0_direction), 0.0);"\
			"float tn_dot_ld1 = max(dot (transformed_normals ,light1_direction), 0.0);"\
			"vec3 ambient0 = u_La0 * u_Ka;"\
			"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0 ;"\
			"vec3 ambient1 = u_La1 * u_Ka;"\
			"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1 ;"\
			"vec3 reflection_vector0 = reflect(-light0_direction,transformed_normals);"\
			"vec3 reflection_vector1 = reflect(-light1_direction,transformed_normals);"\
			"vec3 viewer_vector = normalize(-eye_coordinates.xyz);"\
			"vec3 specular0 = u_Ls0 * u_Ks * pow (max(dot (reflection_vector0,viewer_vector),0.0),u_material_shininess);"\
			"vec3 specular1 = u_Ls1 * u_Ks * pow (max(dot (reflection_vector1,viewer_vector),0.0),u_material_shininess);"\
			"phong_ads_color = ambient0 + diffuse0 + specular0 + ambient1 + diffuse1 + specular1;"\
			/*"phong_ads_color =vec3(1.0,1.0,1.0);"\*/
			/*"phong_ads_color = ambient0 + diffuse0 + specular0;"\*/
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;" \
			"}";

		glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

		// Compile Shader 

		glCompileShader(gVertexShaderObject);

		GLint iInfoLogLength = 0;
		GLint iShaderCompiledStatus = 0;
		char *szInfoLog = NULL;

		glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Vertex Shader compilation log :%s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}


		/// *** Fragment Shader *** ///

		gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec3 phong_ads_color;" \
			"out vec4 FragColor;" \
			"void main(void)" \
			"{" \
			"FragColor = vec4(phong_ads_color,1.0) ;" \
			"}";

		glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;

					glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Fragment Shader compilation log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		/// *** Shader Program *** ///

		gShaderProgramObject = glCreateProgram();

		glAttachShader(gShaderProgramObject, gVertexShaderObject);

		glAttachShader(gShaderProgramObject, gFragmentShaderObject);

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

		glLinkProgram(gShaderProgramObject);

		GLint iShaderProgramLinkStatus = 0;
		glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);

		if (iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Shader Program Link log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		// get uniform locations

		gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");

		gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");

		gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

		gLa0Uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");

		gLd0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");

		gLs0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");

		gLight0PositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light0_position");

		gLa1Uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");

		gLd1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");

		gLs1Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");

		gLight1PositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light1_position");

		gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");

		gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");

		gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");

		gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	
		// Pyramid Vertices

		const GLfloat pyramidVertices[]=
		{
			// front face

			0.0f, 1.0f, 0.0f,   // Apex

			-1.0f, -1.0f, 1.0f, // left-corner of front face

			 1.0f, -1.0f, 1.0f,   //right corner of front face

			//right face

			0.0f, 1.0f, 0.0f,     // apex

			1.0f, -1.0f, 1.0f,    //left corner of the right face

			1.0f, -1.0f, -1.0f,  // right corner of the right face

			//Back face

			0.0f, 1.0f, 0.0f, // apex

			1.0f, -1.0f, -1.0f, //left corner of the back face

			-1.0f, -1.0f, -1.0f, //right corner of the back face

			//left face

			0.0f, 1.0f, 0.0f, // apex

			-1.0f, -1.0f, -1.0f, //left corner of the back face

			-1.0f, -1.0f, 1.0f, //right corner of the back face

		};


		
		// Pyramid Normals

		const GLfloat pyramidNormals [] =
		{
		
	    0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f

		};

		// vao
		glGenVertexArrays(1, &gVao_Pyramid);
		glBindVertexArray(gVao_Pyramid);

		// position vbo
		glGenBuffers(1, &gVbo_Pyramid_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

		glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// normal vbo
		glGenBuffers(1, &gVbo_Pyramid_normal);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_normal);
		glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

		glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// unbind vao
		glBindVertexArray(0);


		//-----------------

		glShadeModel(GL_SMOOTH);

		glClearDepth(1.0f);

		glEnable(GL_DEPTH_TEST);

		glDepthFunc(GL_LEQUAL);

		//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		glEnable(GL_CULL_FACE);

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		gPerspectiveProjectionMatrix = mat4::identity();

		resize(WIN_WIDTH, WIN_HEIGHT);
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);			

		mat4 modelMatrix= mat4:: identity();
		mat4 viewMatrix = mat4 :: identity();
		mat4 rotationMatrix = mat4::identity();

		// OpenGL drawing 

		// Apply z axis translation to go deep into the screen by -6.0

		modelMatrix = translate(0.0f, 0.0f, -6.0f);

		rotationMatrix = rotate(gAnglePyramid, 0.0f, 1.0f, 0.0f);

		modelMatrix = modelMatrix * rotationMatrix;
		// Pass above modelviewprojectionmatrix to the vertex shader in 'u_mvp_matrix' shader variable whose position we have already calculated by using  glGetUniformLocation()

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

		glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		// Light's properties

		glUniform3fv(gLa0Uniform, 1, light0_ambient);
		glUniform3fv(gLd0Uniform, 1, light0_diffuse);
		glUniform3fv(gLs0Uniform, 1, light0_specular);
		glUniform4fv(gLight0PositionUniform, 1, light0_position);


		glUniform3fv(gLa1Uniform, 1, light1_ambient);
		glUniform3fv(gLd1Uniform, 1, light1_diffuse);
		glUniform3fv(gLs1Uniform, 1, light1_specular);
		glUniform4fv(gLight1PositionUniform, 1, light1_position);

		// Material's properties
		glUniform3fv(gKaUniform, 1, materialAmbient);
		glUniform3fv(gKdUniform, 1, materialDiffuse);
		glUniform3fv(gKsUniform, 1, materialSpecular);
		glUniform1f(gMaterialShininessUniform, materialShininess);

		// Pyramid Code 

		// *** bind vao ***
		glBindVertexArray(gVao_Pyramid);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glDrawArrays(GL_TRIANGLES, 0, 12);

		// *** unbind vao ***
		glBindVertexArray(0);


		glUseProgram(0);

		SwapBuffers(ghdc);
	}


	void resize(int width, int height)
	{
		if (height == 0)
			height = 1;

		glViewport(0, 0, (GLsizei)width, (GLsizei)height);

		gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	}

	void spin(void)
	{
		gAnglePyramid = gAnglePyramid + 0.09f;

		if (gAnglePyramid >= 360.0f)
			gAnglePyramid = gAnglePyramid - 360.0f;
	}

	void uninitialize(void)
	{
		if (gbFullscreen == true)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}


		// Sphere

		if (gVao_Pyramid)
		{
			glDeleteVertexArrays(1, &gVao_Pyramid);
			gVao_Pyramid = 0;
		}

		if (gVbo_Pyramid_position)
		{
			glDeleteBuffers(1, &gVbo_Pyramid_position);
			gVbo_Pyramid_position = 0;
		}

		if (gVbo_Pyramid_normal)
		{
			glDeleteBuffers(1, &gVbo_Pyramid_normal);
			gVbo_Pyramid_normal = 0;
		}

		glDetachShader(gShaderProgramObject, gVertexShaderObject);

		glDetachShader(gShaderProgramObject, gFragmentShaderObject);

		glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;

		glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);

		wglMakeCurrent(NULL, NULL);

		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

		if (gpfile)
		{
			fprintf(gpfile, "Log file is successfully closed.\n");
			fclose(gpfile);
			gpfile = NULL;
		}
	}
