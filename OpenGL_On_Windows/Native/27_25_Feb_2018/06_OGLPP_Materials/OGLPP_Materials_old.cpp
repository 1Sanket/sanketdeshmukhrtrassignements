#include <stdio.h>
#include <windows.h>

#include <glew.h>
#include <gl\GL.h>

#include "vmath.h"

#include "Sphere.h"

#pragma comment(lib,"glew32.lib")
#pragma comment (lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};


// Sphere related variables 

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint  gNumVertices;
GLuint gNumElements;

// Prototype of WndProc

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpfile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof (WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
bool gbLight;

GLboolean gbIs_X_AxisRotationOfLight = GL_TRUE;
GLboolean gbIs_Y_AxisRotationOfLight = GL_FALSE;
GLboolean gbIs_Z_AxisRotationOfLight = GL_FALSE;

float lightRotationAngle = 0.0f;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gModelMatrixUniform,gViewMatrixUniform,gProjectionMatrixUniform;
GLuint gLa0Uniform,gLd0Uniform, gLs0Uniform,gLight0PositionUniform;
GLuint gKaUniform, gKdUniform, gKsUniform, gMaterialShininessUniform;

GLuint gLKeyPressedUniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat light0_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light0_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };  // White light
GLfloat light0_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light0_position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffuse[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_specular[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_shinyness =  0.0f ;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void updateAngle(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP_Texture");
	bool bDone = false;

	if (fopen_s(&gpfile,"Log.txt","w") !=0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created.... \n Exiting......"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpfile, "Log file is successfully opened. \n");
	}

	// Initialise members of struct WNDCLASSEX

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	
	// register class.

	RegisterClassEx(&wndClass);

	// Create window 

	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable pipeline Materials"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	// Message loop .

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{
			// rendering function.
			display();

			updateAngle();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}

		uninitialize();

		return ((int)msg.wParam);
}

	LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
	{
		void resize(int, int);
		void ToggleFullscreen(void);
		void uninitialize(void);

		static bool bIsAKeyPressed = false;
		static bool bIsLKeyPressed = false;
		static bool bIsVKeyPressed = false;
		static bool bIsFKeyPressed = false;

		switch (iMsg)
		{
		case WM_ACTIVATE:
			 
			if (HIWORD(wParam) == 0) // if 0 , window is active 
			{
				gbActiveWindow = true;
			}

			else
			{
				gbActiveWindow = false;
			}

			break;

		case WM_ERASEBKGND:
			return (0);

		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
     
		case  WM_KEYDOWN:
			  
			switch (wParam)
			{
			case VK_ESCAPE: 
				if (gbEscapeKeyIsPressed == false)
					gbEscapeKeyIsPressed = true;
				break;

			case 0x46: // 'f' or 'F' 
				if (gbFullscreen == false)
				{
					ToggleFullscreen();
					gbFullscreen = true;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = false;
				}

				break;

			case 0x4C: // for 'L' or 'l'
				if (bIsLKeyPressed == false)
				{
					gbLight = true;
					bIsLKeyPressed = true;
				}
				else
				{
					gbLight = false;
					bIsLKeyPressed = false;
				}

				break;

			case 0x58: // for 'x' or 'X'

				gbIs_X_AxisRotationOfLight = GL_TRUE;
				gbIs_Y_AxisRotationOfLight = GL_FALSE;
				gbIs_Z_AxisRotationOfLight = GL_FALSE;

				break;

			case 0x59: // for 'y' or 'Y'

				gbIs_X_AxisRotationOfLight = GL_FALSE;
				gbIs_Y_AxisRotationOfLight = GL_TRUE;
				gbIs_Z_AxisRotationOfLight = GL_FALSE;

				break;

			case 0x5A: // for 'z' or 'Z'

				gbIs_X_AxisRotationOfLight = GL_FALSE;
				gbIs_Y_AxisRotationOfLight = GL_FALSE;
				gbIs_Z_AxisRotationOfLight = GL_TRUE;

				break;
	
			default: 
				break;
			}

			break;

		case WM_LBUTTONDOWN:
			break;

		case WM_CLOSE:
			uninitialize();
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default :
			break;
		}

		return (DefWindowProc(hwnd, iMsg, wParam, lParam));

	}

	void ToggleFullscreen(void)
	{
		//variable declarations
		MONITORINFO mi;

		//code
		if (gbFullscreen == false)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			if (dwStyle & WS_OVERLAPPEDWINDOW)
			{
				mi = { sizeof(MONITORINFO) };
				if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
				{
					SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
					SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				}
			}
			ShowCursor(FALSE);
		}

		else
		{
			//code
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}
	}



	void initialize(void)
	{
		void uninitialize(void);
		void resize(int, int);

		PIXELFORMATDESCRIPTOR pfd;
		int iPixelFormatIndex;

		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;

		ghdc = GetDC(ghwnd);

		iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

		if (iPixelFormatIndex == 0)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		ghrc = wglCreateContext(ghdc);
		if (ghrc == NULL)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (wglMakeCurrent(ghdc, ghrc) == false)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// GLEW initialization code for GLSL (It must be Here i.e; After creating OpenGL context but before using any OpenGL Function)

		GLenum glew_error = glewInit();

		if (glew_error != GLEW_OK)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// **** Vertex Shader ****

		gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		const GLchar * vertexShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;" \
			"uniform mat4 u_model_matrix ;" \
			"uniform mat4 u_view_matrix ;" \
			"uniform mat4 u_projection_matrix ;" \
			"uniform vec4 u_light0_position;"\
			"uniform int u_lighting_enabled;" \
			"out vec3 transformed_normals;" \
			"out vec3 light_direction;" \
			"out vec3 viewer_vector;" \
			"void main(void)" \
			"{" \
			"if (u_lighting_enabled == 1)"\
			"{"\
			"vec4 eye_coordinates = u_view_matrix *u_model_matrix* vPosition;"\
			"transformed_normals = mat3 (u_view_matrix * u_model_matrix) * vNormal;"\
			"light_direction = vec3 (u_light0_position) - eye_coordinates.xyz;"\
			"viewer_vector = -eye_coordinates.xyz;"\
			"}"\
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;" \
			"}";

		glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

		// Compile Shader 

		glCompileShader(gVertexShaderObject);

		GLint iInfoLogLength = 0;
		GLint iShaderCompiledStatus = 0;
		char *szInfoLog = NULL;

		glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Vertex Shader compilation log :%s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}


		/// *** Fragment Shader *** ///

		gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec3 transformed_normals;" \
			"in vec3 light_direction;" \
			"in vec3 viewer_vector;" \
			"out vec4 FragColor;" \
			"uniform vec3 u_La0;"\
			"uniform vec3 u_Ld0;"\
			"uniform vec3 u_Ls0;"\
			"uniform vec3 u_Ka;"\
			"uniform vec3 u_Kd;"\
			"uniform vec3 u_Ks;"\
			"uniform float u_material_shininess;"\
			"uniform int u_lighting_enabled;"\
			"void main(void)" \
			"{" \
			"vec3 phong_ads_color;"\
			"if(u_lighting_enabled == 1)"\
			"{"\
			"vec3 normalized_transformed_normals=normalize(transformed_normals);"\
			"vec3 normalized_light_direction = normalize(light_direction);"\
			"vec3 normalized_viewer_vector = normalize(viewer_vector);"\
			"vec3 ambient = u_La0 * u_Ka ;"\
			"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"\
			"vec3 diffuse = u_Ld0 * u_Kd * tn_dot_ld; "\
			"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\
			"vec3 specular = u_Ls0 * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess); "\
			"phong_ads_color = ambient + diffuse + specular;  "\
			"}"
			"else"\
			"{"\
			"phong_ads_color = vec3(1.0,1.0,1.0);"\
			"}"\
			"FragColor = vec4(phong_ads_color,1.0);"\
			"}";

		glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;

					glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Fragment Shader compilation log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		/// *** Shader Program *** ///

		gShaderProgramObject = glCreateProgram();

		glAttachShader(gShaderProgramObject, gVertexShaderObject);

		glAttachShader(gShaderProgramObject, gFragmentShaderObject);

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

		glLinkProgram(gShaderProgramObject);

		GLint iShaderProgramLinkStatus = 0;
		glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);

		if (iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Shader Program Link log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		// get uniform locations

		gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");

		gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");

		gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

		gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

		gLa0Uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");

		gLd0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");

		gLs0Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");

		gLight0PositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light0_position");

		gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");

		gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");

		gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");

		gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");


		// Get Sphere Vertices

		getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

		gNumVertices = getNumberOfSphereVertices();

		gNumElements = getNumberOfSphereElements();

		// vao
		glGenVertexArrays(1, &gVao_sphere);
		glBindVertexArray(gVao_sphere);

		// position vbo
		glGenBuffers(1, &gVbo_sphere_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

		glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// normal vbo
		glGenBuffers(1, &gVbo_sphere_normal);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
		glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

		glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// element vbo
		glGenBuffers(1, &gVbo_sphere_element);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		// unbind vao
		glBindVertexArray(0);


		//-----------------

		glShadeModel(GL_SMOOTH);

		glClearDepth(1.0f);

		glEnable(GL_DEPTH_TEST);

		glDepthFunc(GL_LEQUAL);

		//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		glEnable(GL_CULL_FACE);

		//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		glClearColor(0.25f, 0.25f, 0.25f, 0.0f);    // Dark Gray background.

		gPerspectiveProjectionMatrix = mat4::identity();

		gbLight = false;

		resize(WIN_WIDTH, WIN_HEIGHT);
	}

	void display(void)
	{
		void drawSphere(void);
		mat4 modelMatrix = mat4::identity();
		mat4 viewMatrix = mat4::identity();
		mat4 rotationMatrix = mat4::identity();
		mat4 translationMatrix = mat4::identity();
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);

		if (gbLight == true)
		{
			glUniform1i(gLKeyPressedUniform, 1); 

			if (gbIs_X_AxisRotationOfLight == GL_TRUE)
			{
				light0_position[1] =  80 * cos(lightRotationAngle);
				light0_position[2] =  80 *  sin(lightRotationAngle);
				light0_position[0] = 0.0f;
			}

			else if (gbIs_Y_AxisRotationOfLight == GL_TRUE)
			{
				light0_position[0] = 80 * cos(-lightRotationAngle);
				light0_position[2] = 80 * sin(-lightRotationAngle);
				light0_position[1] = 0.0f;
			}

			else if (gbIs_Z_AxisRotationOfLight == GL_TRUE)
			{
				light0_position[0] = 80 * cos(lightRotationAngle);
				light0_position[1] = 80 * sin(lightRotationAngle);
				light0_position[2] = 0.0f;
			}

	
			// Light's properties

			glUniform3fv(gLa0Uniform,1,light0_ambient);
			glUniform3fv(gLd0Uniform, 1,light0_diffuse);
			glUniform3fv(gLs0Uniform, 1, light0_specular);
			glUniform4fv(gLight0PositionUniform, 1,light0_position);

		}

		else
		{
	
			glUniform1i(gLKeyPressedUniform, 0);
		}

		/// Emerald

		material_ambient[0] = 0.0215f;
		material_ambient[1] = 0.1745f;
		material_ambient[2] = 0.0215f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.07568f;
		material_diffuse[1] = 0.61424f;
		material_diffuse[2] = 0.07568f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.633f;
		material_specular[1] = 0.727811f;
		material_specular[2] = 0.633f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.6f*128.0f;

		translationMatrix = translate(-5.0f, 3.8f, -15.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		/// Jade

		material_ambient[0] = 0.135f;
		material_ambient[1] = 0.2225f;
		material_ambient[2] = 0.1575f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.54f;
		material_diffuse[1] = 0.89f;
		material_diffuse[2] = 0.63f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.316228f;
		material_specular[1] = 0.316228f;
		material_specular[2] = 0.316228f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.1f* 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		/// Obsidian

		material_ambient[0] = 0.05375f;
		material_ambient[1] = 0.05f;
		material_ambient[2] = 0.06625f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.18275f;
		material_diffuse[1] = 0.17f;
		material_diffuse[2] = 0.22525f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.332741f;
		material_specular[1] = 0.328634f;
		material_specular[2] = 0.346435f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.3f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		/// Pearl

		material_ambient[0] = 0.25f;
		material_ambient[1] = 0.20725f;
		material_ambient[2] = 0.20725f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 1.0f;
		material_diffuse[1] = 0.829f;
		material_diffuse[2] = 0.829f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.296648f;
		material_specular[1] = 0.296648f;
		material_specular[2] = 0.296648f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.088f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Ruby

		material_ambient[0] = 0.1745f;
		material_ambient[1] = 0.01175f;
		material_ambient[2] = 0.01175f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.61424f;
		material_diffuse[1] = 0.04136f;
		material_diffuse[2] = 0.04136f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.727811f;
		material_specular[1] = 0.626959f;
		material_specular[2] = 0.626959f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.6f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Turquoise

		material_ambient[0] = 0.1f;
		material_ambient[1] = 0.18725f;
		material_ambient[2] = 0.1745f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.396f;
		material_diffuse[1] = 0.74151f;
		material_diffuse[2] = 0.69102f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.297254f;
		material_specular[1] = 0.30829f;
		material_specular[2] = 0.306678f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.1f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		// Start new row

		translationMatrix  = translationMatrix.identity();

		modelMatrix = modelMatrix.identity();

		translationMatrix = translate(-2.0f, 3.8f, -15.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		/// Brass

		/*translationMatrix = translationMatrix.identity();

		translationMatrix = translate(1.0f, 9.0f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);*/

		material_ambient[0] = 0.329412f;
		material_ambient[1] = 0.223529f;
		material_ambient[2] = 0.027451f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.780392f;
		material_diffuse[1] = 0.568627f;
		material_diffuse[2] = 0.113725f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.992157f;
		material_specular[1] = 0.941176f;
		material_specular[2] = 0.807843f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.21794872f * 128.0f;

		/*translationMatrix = translate(0.0f, -1.2f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);*/

		drawSphere();

		///

		/// Bronze

		material_ambient[0] = 0.2125f;
		material_ambient[1] = 0.1275f;
		material_ambient[2] = 0.054f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.714f;
		material_diffuse[1] = 0.4284f;
		material_diffuse[2] = 0.18144f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.393548f;
		material_specular[1] = 0.271906f;
		material_specular[2] = 0.166721f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.2f * 128.0f;

		glTranslatef(0.0f, -0.4f, 0.0f);

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Chrome

		material_ambient[0] = 0.25f;
		material_ambient[1] = 0.25f;
		material_ambient[2] = 0.25f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.4f;
		material_diffuse[1] = 0.4f;
		material_diffuse[2] = 0.4f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.774597f;
		material_specular[1] = 0.774597f;
		material_specular[2] = 0.774597f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.6f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Copper

		material_ambient[0] = 0.19125f;
		material_ambient[1] = 0.0735f;
		material_ambient[2] = 0.0225f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.7038f;
		material_diffuse[1] = 0.27048f;
		material_diffuse[2] = 0.0828f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.256777f;
		material_specular[1] = 0.137622f;
		material_specular[2] = 0.086014f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.6f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///


		/// Gold

		material_ambient[0] = 0.24725f;
		material_ambient[1] = 0.1995f;
		material_ambient[2] = 0.0745f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.75164f;
		material_diffuse[1] = 0.60648f;
		material_diffuse[2] = 0.22648f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.628281f;
		material_specular[1] = 0.555802f;
		material_specular[2] = 0.366065f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.4f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///


		/// Silver

		material_ambient[0] = 0.19225f;
		material_ambient[1] = 0.19225f;
		material_ambient[2] = 0.19225f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.50754f;
		material_diffuse[1] = 0.50754f;
		material_diffuse[2] = 0.50754f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.508273f;
		material_specular[1] = 0.508273f;
		material_specular[2] = 0.508273f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.4f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		// Start new row

		translationMatrix = translationMatrix.identity();

		modelMatrix = modelMatrix.identity();

		translationMatrix = translate(1.0f, 3.8f, -15.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


	/*	translationMatrix = translate(0.4f, 2.0f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);*/

		/// Black

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.0f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.01f;
		material_diffuse[1] = 0.01f;
		material_diffuse[2] = 0.01f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.50f;
		material_specular[1] = 0.50f;
		material_specular[2] = 0.50f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.25f * 128.0f;

		drawSphere();

		///

		/// Cyan

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.1f;
		material_ambient[2] = 0.06f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.0f;
		material_diffuse[1] = 0.50980392f;
		material_diffuse[2] = 0.50980392f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.50196078f;
		material_specular[1] = 0.50196078f;
		material_specular[2] = 0.50196078f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.25f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Green

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.0f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.1f;
		material_diffuse[1] = 0.35f;
		material_diffuse[2] = 0.1f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.45f;
		material_specular[1] = 0.55f;
		material_specular[2] = 0.45f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.25f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Red

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.0f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.5f;
		material_diffuse[1] = 0.0f;
		material_diffuse[2] = 0.0f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.7f;
		material_specular[1] = 0.6f;
		material_specular[2] = 0.6f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.25f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// White

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.0f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.55f;
		material_diffuse[1] = 0.55f;
		material_diffuse[2] = 0.55f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.70f;
		material_specular[1] = 0.70f;
		material_specular[2] = 0.70f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.25f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Yellow Plastic

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.0f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.5f;
		material_diffuse[1] = 0.5f;
		material_diffuse[2] = 0.0f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.60f;
		material_specular[1] = 0.60f;
		material_specular[2] = 0.50f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.25f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/*translationMatrix = translate(0.4f, 2.0f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);*/

		// Start new row

		translationMatrix = translationMatrix.identity();

		modelMatrix = modelMatrix.identity();

		translationMatrix = translate(4.0f, 3.8f, -15.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);


		/// Black

		material_ambient[0] = 0.02f;
		material_ambient[1] = 0.02f;
		material_ambient[2] = 0.02f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.01f;
		material_diffuse[1] = 0.01f;
		material_diffuse[2] = 0.01f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.4f;
		material_specular[1] = 0.4f;
		material_specular[2] = 0.4f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.078125f * 128.0f;

		drawSphere();

		///


		/// Cyan

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.05f;
		material_ambient[2] = 0.05f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.4f;
		material_diffuse[1] = 0.5f;
		material_diffuse[2] = 0.5f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.04f;
		material_specular[1] = 0.7f;
		material_specular[2] = 0.7f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.078125f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Green

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.05f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.4f;
		material_diffuse[1] = 0.5f;
		material_diffuse[2] = 0.4f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.04f;
		material_specular[1] = 0.7f;
		material_specular[2] = 0.04f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.078125f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Red

		material_ambient[0] = 0.05f;
		material_ambient[1] = 0.0f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.5f;
		material_diffuse[1] = 0.4f;
		material_diffuse[2] = 0.4f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.7f;
		material_specular[1] = 0.04f;
		material_specular[2] = 0.04f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.078125f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// White

		material_ambient[0] = 0.05f;
		material_ambient[1] = 0.05f;
		material_ambient[2] = 0.05f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.5f;
		material_diffuse[1] = 0.5f;
		material_diffuse[2] = 0.5f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.7f;
		material_specular[1] = 0.7f;
		material_specular[2] = 0.7f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.078125f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		///

		/// Yellow Rubber

		material_ambient[0] = 0.05f;
		material_ambient[1] = 0.05f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.5f;
		material_diffuse[1] = 0.5f;
		material_diffuse[2] = 0.4f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.7f;
		material_specular[1] = 0.7f;
		material_specular[2] = 0.04f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.078125f * 128.0f;

		translationMatrix = translate(0.0f, -1.5f, 0.0f);

		modelMatrix = modelMatrix * translationMatrix;

		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);

		drawSphere();

		glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);

		glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		glUseProgram(0);

		SwapBuffers(ghdc);
	}

	void drawSphere(void)
	{
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shinyness);

		glBindVertexArray(gVao_sphere);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// *** unbind vao ***
		glBindVertexArray(0);
	}

	void resize(int width, int height)
	{
		if (height == 0)
			height = 1;

		glViewport(0, 0, (GLsizei)width, (GLsizei)height);

		gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

		//gPerspectiveProjectionMatrix = perspective(45.0f, 1.5f, 0.1f, 100.0f);
	}

	void updateAngle()
	{
		if (lightRotationAngle > 360.0f)
			lightRotationAngle = 360.0f - lightRotationAngle;

		lightRotationAngle = lightRotationAngle + 0.005f;
	}

	void uninitialize(void)
	{
		if (gbFullscreen == true)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}


		// Sphere

		if (gVao_sphere)
		{
			glDeleteVertexArrays(1, &gVao_sphere);
			gVao_sphere = 0;
		}

		if (gVbo_sphere_position)
		{
			glDeleteBuffers(1, &gVbo_sphere_position);
			gVbo_sphere_position = 0;
		}

		if (gVbo_sphere_normal)
		{
			glDeleteBuffers(1, &gVbo_sphere_normal);
			gVbo_sphere_normal = 0;
		}

		if (gVbo_sphere_element)
		{
			glDeleteBuffers(1, &gVbo_sphere_element);
			gVbo_sphere_element = 0;
		}

		glDetachShader(gShaderProgramObject, gVertexShaderObject);

		glDetachShader(gShaderProgramObject, gFragmentShaderObject);

		glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;

		glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);

		wglMakeCurrent(NULL, NULL);

		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

		if (gpfile)
		{
			fprintf(gpfile, "Log file is successfully closed.\n");
			fclose(gpfile);
			gpfile = NULL;
		}
	}
