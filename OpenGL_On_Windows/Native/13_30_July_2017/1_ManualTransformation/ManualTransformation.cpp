
#include <Windows.h>
#include <gl/GL.h>
#include <GL\GLU.h>
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define PI 3.14

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbIsEscapeKeyPressed = false;
bool gbFullscreen = false;
void resize(int, int);
float angleCubeDegrees = 0.0f;
float angleCubeRadian = 0.0f;
GLfloat identityMatrix[16];
GLfloat translationMatrix[16];
GLfloat scaleMatrix[16];
GLfloat rotation_x_Matrix[16];
GLfloat rotation_y_Matrix[16];
GLfloat rotation_z_Matrix[16];


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void updateAngle(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OGL_Horizontal_Blue_Lines");
	bool bDone = false;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = CreateSolidBrush(RGB(1, 1, 1));
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx
	(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGl fixed function pipeline : Yellow Triangle on graph paper"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{

			if (gbActiveWindow == true)
			{
				if (gbIsEscapeKeyPressed == true)
					bDone = true;
				updateAngle();
				display();
				//updateAngle();
			}
		}

	}

	uninitialize();
	return (int(msg.wParam));

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		/*case WM_PAINT:
		display();
		break;*/
		//case WM_ERASEBKGND:
		//	return(0);
		//	break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbIsEscapeKeyPressed = true;
			break;

		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}


void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}

void initialize(void)
{
	//void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//identity matrix
	identityMatrix[0] = 1;
	identityMatrix[1] = 0;
	identityMatrix[2] = 0;
	identityMatrix[3] = 0;

	identityMatrix[4] = 0;
	identityMatrix[5] = 1;
	identityMatrix[6] = 0;
	identityMatrix[7] = 0;

	identityMatrix[8] = 0;
	identityMatrix[9] = 0;
	identityMatrix[10] = 1;
	identityMatrix[11] = 0;

	identityMatrix[12] = 0;
	identityMatrix[13] = 0;
	identityMatrix[14] = 0;
	identityMatrix[15] = 1;


    //Translation matrix
    translationMatrix[0] = 1;
	translationMatrix[1] = 0;
	translationMatrix[2] = 0;
	translationMatrix[3] = 0;

	translationMatrix[4] = 0;
	translationMatrix[5] = 1;
	translationMatrix[6] = 0;
	translationMatrix[7] = 0;

	translationMatrix[8] = 0;
	translationMatrix[9] = 0;
	translationMatrix[10] = 1;
	translationMatrix[11] = 0;

	translationMatrix[12] = 0;
	translationMatrix[13] = 0;
	translationMatrix[14] = -6;
	translationMatrix[15] = 1;


	//Scale matrix
	scaleMatrix[0] = 0.75;
	scaleMatrix[1] = 0;
	scaleMatrix[2] = 0;
	scaleMatrix[3] = 0;

	scaleMatrix[4] = 0;
	scaleMatrix[5] = 0.75;
	scaleMatrix[6] = 0;
	scaleMatrix[7] = 0;

	scaleMatrix[8] = 0;
	scaleMatrix[9] = 0;
	scaleMatrix[10] = 0.75;
	scaleMatrix[11] = 0;

	scaleMatrix[12] = 0;
	scaleMatrix[13] = 0;
	scaleMatrix[14] = 0;
	scaleMatrix[15] = 1;

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//X axis rotation matrix
	rotation_x_Matrix[0] = 1;
	rotation_x_Matrix[1] = 0;
	rotation_x_Matrix[2] = 0;
	rotation_x_Matrix[3] = 0;

	rotation_x_Matrix[4] = 0;
	rotation_x_Matrix[5] = cos(angleCubeRadian);
	rotation_x_Matrix[6] = sin(angleCubeRadian);
	rotation_x_Matrix[7] = 0;

	rotation_x_Matrix[8] = 0;
	rotation_x_Matrix[9] = -sin(angleCubeRadian);
	rotation_x_Matrix[10] = cos(angleCubeRadian);
	rotation_x_Matrix[11] = 0;

	rotation_x_Matrix[12] = 0;
	rotation_x_Matrix[13] = 0;
	rotation_x_Matrix[14] = 0;
	rotation_x_Matrix[15] = 1;

	//Y axis rotation matrix
	rotation_y_Matrix[0] = cos(angleCubeRadian);
	rotation_y_Matrix[1] = 0;
	rotation_y_Matrix[2] = -sin(angleCubeRadian);
	rotation_y_Matrix[3] = 0;

	rotation_y_Matrix[4] = 0;
	rotation_y_Matrix[5] = 1;
	rotation_y_Matrix[6] = 0;
	rotation_y_Matrix[7] = 0;

	rotation_y_Matrix[8] = sin(angleCubeRadian);
	rotation_y_Matrix[9] = 0;
	rotation_y_Matrix[10] = cos(angleCubeRadian);
	rotation_y_Matrix[11] = 0;

	rotation_y_Matrix[12] = 0;
	rotation_y_Matrix[13] = 0;
	rotation_y_Matrix[14] = 0;
	rotation_y_Matrix[15] = 1;

	//Z axis rotation matrix
	rotation_z_Matrix[0] = cos(angleCubeRadian);
	rotation_z_Matrix[1] = sin(angleCubeRadian);
	rotation_z_Matrix[2] = 0;
	rotation_z_Matrix[3] = 0;

	rotation_z_Matrix[4] = -sin(angleCubeRadian);
	rotation_z_Matrix[5] = cos(angleCubeRadian);
	rotation_z_Matrix[6] = 0;
	rotation_z_Matrix[7] = 0;

	rotation_z_Matrix[8] = 0;
	rotation_z_Matrix[9] = 0;
	rotation_z_Matrix[10] = 1;
	rotation_z_Matrix[11] = 0;

	rotation_z_Matrix[12] = 0;
	rotation_z_Matrix[13] = 0;
	rotation_z_Matrix[14] = 0;
	rotation_z_Matrix[15] = 1;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	
	glMatrixMode(GL_MODELVIEW);

	glLoadMatrixf(identityMatrix);

	glMultMatrixf(translationMatrix);
	glMultMatrixf(scaleMatrix);
	
	angleCubeRadian = angleCubeDegrees*(PI / 180);

	glMultMatrixf(rotation_x_Matrix);
	glMultMatrixf(rotation_y_Matrix);
	glMultMatrixf(rotation_z_Matrix);

	glBegin(GL_QUADS);

	// Top face
	glColor3f(1.0f, 0.0f, 0.0f); // red

	glVertex3f(1.0f, 1.0f, -1.0f);   // Right-top corner of top face 
	glVertex3f(-1.0f, 1.0f, -1.0f); // left-top corner of the top face
	glVertex3f(-1.0f, 1.0f, 1.0f);   //left bottom corner of front face
	glVertex3f(1.0f, 1.0f, 1.0f); //right bottom corner of front face

								  //Bottom face
	glColor3f(0.0f, 1.0f, 0.0f); // Green

	glVertex3f(1.0f, -1.0f, -1.0f);   // Right-top corner of bottom face 
	glVertex3f(-1.0f, -1.0f, -1.0f); // left-top corner of the bottom face
	glVertex3f(-1.0f, -1.0f, 1.0f);   //left bottom corner of bottom face
	glVertex3f(1.0f, -1.0f, 1.0f); //right bottom corner of bottom face

								   //Front face
	glColor3f(0.0f, 0.0f, 1.0f); // Blue

	glVertex3f(1.0f, 1.0f, 1.0f);   // Right-top corner of front face 
	glVertex3f(-1.0f, 1.0f, 1.0f); // left-top corner of the front face
	glVertex3f(-1.0f, -1.0f, 1.0f);   //left bottom corner of front face
	glVertex3f(1.0f, -1.0f, 1.0f); //right bottom corner of front face


								   //Back face
	glColor3f(0.0f, 1.0f, 1.0f); // Cyan

	glVertex3f(1.0f, 1.0f, -1.0f);   // Right-top corner of back face 
	glVertex3f(-1.0f, 1.0f, -1.0f); // left-top corner of the back face
	glVertex3f(-1.0f, -1.0f, -1.0f);   //left bottom corner of back face
	glVertex3f(1.0f, -1.0f, -1.0f); //right bottom corner of back face

									//Right face
	glColor3f(1.0f, 0.0f, 1.0f); // Magenta

	glVertex3f(1.0f, 1.0f, -1.0f);   // Right-top corner of right face 
	glVertex3f(1.0f, 1.0f, 1.0f); // left-top corner of the right face
	glVertex3f(1.0f, -1.0f, 1.0f);   //left bottom corner of right face
	glVertex3f(1.0f, -1.0f, -1.0f); //right bottom corner of right face

									//Left face
	glColor3f(1.0f, 1.0f, 0.0f); // Yellow

	glVertex3f(-1.0f, 1.0f, 1.0f);   // Right-top corner of left face 
	glVertex3f(-1.0f, 1.0f, -1.0f); // left-top corner of the left face
	glVertex3f(-1.0f, -1.0f, -1.0f);   //left bottom corner of left face
	glVertex3f(-1.0f, -1.0f, 1.0f); //right bottom corner of left face


	glEnd();
	//updateAngle();
	SwapBuffers(ghdc);

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	//glOrtho(-50.0f, 50.0f, -50.0f, 50.0f, -50.0f, 50.0f);

}

void updateAngle()
{
	angleCubeDegrees = angleCubeDegrees + 0.1f;
	if (angleCubeDegrees >= 360.0f)
		angleCubeDegrees = 0.0f;

}

void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}



