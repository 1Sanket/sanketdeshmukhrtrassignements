#include <Windows.h>
#include <gl/GL.h>
#include <GL\GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbIsEscapeKeyPressed = false;
bool gbFullscreen = false;
bool gbIsLightingEnabled = false;
void resize(int, int);
float angleCube = 0.0f;
float anglePyramid = 0.0f;


// Arrays required for lighting.
GLfloat light_ambient[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f,0.0f,1.0f,1.0f };  

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void updateAngle(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OGL_Light_PyramidNCube");
	bool bDone = false;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = CreateSolidBrush(RGB(1, 1, 1));
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx
	(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGl fixed function pipeline : Light - Pyramid and Cube"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{

			if (gbActiveWindow == true)
			{
				if (gbIsEscapeKeyPressed == true)
					bDone = true;
				updateAngle();
				display();
			}
		}

	}

	uninitialize();
	return (int(msg.wParam));

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbIsEscapeKeyPressed = true;
			break;

		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C: // for 'l' or 'L'
			if (gbIsLightingEnabled == false)
			{
				glEnable(GL_LIGHTING);
				gbIsLightingEnabled = true;
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbIsLightingEnabled = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}


void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}

void initialize(void)
{

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glEnable(GL_LIGHT0);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-1.5f, 0.0f, -6.0f);
	glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);


	glBegin(GL_TRIANGLES);

	// front face
	//glColor3f(1.0f, 0.0f, 0.0f); // red
	glNormal3f(0.0f,0.0f,1.0f);

	glVertex3f(0.0f, 1.0f, 0.0f);   // Apex

    //glColor3f(0.0f, 1.0f, 0.0f);    // green
	glVertex3f(-1.0f, -1.0f, 1.0f); // left-corner of front face

	//glColor3f(0.0f, 0.0f, 1.0f);    // blue
	glVertex3f(1.0f, -1.0f, 1.0f);   //right corner of front face

									 //right face
    //glColor3f(1.0f, 0.0f, 0.0f); // red
	glNormal3f(1.0f,0.0f,0.0f);

	glVertex3f(0.0f, 1.0f, 0.0f);     // apex

	//glColor3f(0.0f, 0.0f, 1.0f);  // blue
	glVertex3f(1.0f, -1.0f, 1.0f);    //left corner of the right face

	//glColor3f(0.0f, 1.0f, 0.0f);  //green
	glVertex3f(1.0f, -1.0f, -1.0f);  // right corner of the right face

									 //Back face

	//glColor3f(1.0f, 0.0f, 0.0f);//red
	glNormal3f(0.0f, 0.0f, -1.0f);

	glVertex3f(0.0f, 1.0f, 0.0f); // apex

	//glColor3f(0.0f, 1.0f, 0.0f);// green
	glVertex3f(1.0f, -1.0f, -1.0f); //left corner of the back face

	//glColor3f(0.0f, 0.0f, 1.0f);// blue
	glVertex3f(-1.0f, -1.0f, -1.0f); //right corner of the back face

									 //left face
	//glColor3f(1.0f, 0.0f, 0.0f);//red

	glNormal3f(-1.0f, 0.0f, 0.0f);

	glVertex3f(0.0f, 1.0f, 0.0f); // apex

//	glColor3f(0.0f, 0.0f, 1.0f);// blue
	glVertex3f(-1.0f, -1.0f, -1.0f); //left corner of the back face

	//glColor3f(0.0f, 1.0f, 0.0f);// green
	glVertex3f(-1.0f, -1.0f, 1.0f); //right corner of the back face

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 0.0f, -6.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angleCube, 1.0f, 1.0f, 1.0f);

	glBegin(GL_QUADS);

	// Top face
	//glColor3f(1.0f, 0.0f, 0.0f); // red

	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);   // Right-top corner of top face 
	glVertex3f(-1.0f, 1.0f, -1.0f); // left-top corner of the top face
	glVertex3f(-1.0f, 1.0f, 1.0f);   //left bottom corner of front face
	glVertex3f(1.0f, 1.0f, 1.0f); //right bottom corner of front face

								  //Bottom face
	//glColor3f(0.0f, 1.0f, 0.0f); // Green
	glNormal3f(0.0f, -1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);   // Right-top corner of bottom face 
	glVertex3f(-1.0f, -1.0f, -1.0f); // left-top corner of the bottom face
	glVertex3f(-1.0f, -1.0f, 1.0f);   //left bottom corner of bottom face
	glVertex3f(1.0f, -1.0f, 1.0f); //right bottom corner of bottom face

								   //Front face
	//glColor3f(0.0f, 0.0f, 1.0f); // Blue
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);   // Right-top corner of front face 
	glVertex3f(-1.0f, 1.0f, 1.0f); // left-top corner of the front face
	glVertex3f(-1.0f, -1.0f, 1.0f);   //left bottom corner of front face
	glVertex3f(1.0f, -1.0f, 1.0f); //right bottom corner of front face


								   //Back face
	//glColor3f(0.0f, 1.0f, 1.0f); // Cyan

	glNormal3f(0.0f, 0.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);   // Right-top corner of back face 
	glVertex3f(-1.0f, 1.0f, -1.0f); // left-top corner of the back face
	glVertex3f(-1.0f, -1.0f, -1.0f);   //left bottom corner of back face
	glVertex3f(1.0f, -1.0f, -1.0f); //right bottom corner of back face

									//Right face
	//glColor3f(1.0f, 0.0f, 1.0f); // Magenta

	glNormal3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);   // Right-top corner of right face 
	glVertex3f(1.0f, 1.0f, 1.0f); // left-top corner of the right face
	glVertex3f(1.0f, -1.0f, 1.0f);   //left bottom corner of right face
	glVertex3f(1.0f, -1.0f, -1.0f); //right bottom corner of right face

									//Left face
	//glColor3f(1.0f, 1.0f, 0.0f); // Yellow

	glNormal3f(-1.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);   // Right-top corner of left face 
	glVertex3f(-1.0f, 1.0f, -1.0f); // left-top corner of the left face
	glVertex3f(-1.0f, -1.0f, -1.0f);   //left bottom corner of left face
	glVertex3f(-1.0f, -1.0f, 1.0f); //right bottom corner of left face


	glEnd();
	SwapBuffers(ghdc);

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void updateAngle()
{
	angleCube = angleCube + 0.1f;
	if (angleCube >= 360.0f)
		angleCube = 0.0f;
	anglePyramid = anglePyramid + 0.1f;
	if (anglePyramid >= 360.0f)
		anglePyramid = 0.0f;
}

void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}
