#include <stdio.h>
#include <windows.h>

#include <glew.h>
#include <gl\GL.h>

#include "vmath.h"
#include "OGLPP_Texture.h"

#pragma comment(lib,"glew32.lib")
#pragma comment (lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//struct CubeVertexData
//{
//	float position[3];
//	float color[3];
//	float normal[3];
//	float texCoords[2];
//
//};


// Prototype of WndProc

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpfile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof (WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Cube;
GLuint gVbo_Cube;

mat4 gPerspectiveProjectionMatrix;

GLfloat gAngleCube = 0.0f;

GLuint gTexture_sampler_uniform;
GLuint gTexture_Stone;

GLuint La_uniform;
GLuint Ld_Uniform;
GLuint Ls_Uniform;

GLuint Ka_Uniform;
GLuint Kd_Uniform;
GLuint Ks_Uniform;
GLuint Material_shininess_uniform;

GLfloat light_Ambient[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat light_Diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_Specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_Position[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat material_Ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_Diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_Specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 128.0f;

GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLightPositionUniform;
GLuint gLKeyPressedUniform;

bool bIsLkeyPressed;
bool gbLight;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void spin(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP_Interleaved_BufferSubData");
	bool bDone = false;

	if (fopen_s(&gpfile,"Log.txt","w") !=0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created.... \n Exiting......"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpfile, "Log file is successfully opened. \n");
	}

	// Initialise members of struct WNDCLASSEX

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	
	// register class.

	RegisterClassEx(&wndClass);

	// Create window 

	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable pipeline 2D Interleaved - BufferSubData"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	// Message loop .

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{
			// rendering function.
			display();

			spin();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}

		uninitialize();

		return ((int)msg.wParam);
}

	LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
	{
		void resize(int, int);
		void ToggleFullscreen(void);
		void uninitialize(void);

		static WORD xMouse = NULL;
		static WORD yMouse = NULL;

		switch (iMsg)
		{
		case WM_ACTIVATE:
			 
			if (HIWORD(wParam) == 0) // if 0 , window is active 
			{
				gbActiveWindow = true;
			}

			else
			{
				gbActiveWindow = false;
			}

			break;

		case WM_ERASEBKGND:
			return (0);

		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
     
		case  WM_KEYDOWN:
			  
			switch (wParam)
			{
			case VK_ESCAPE:
				if (gbEscapeKeyIsPressed == false)
					gbEscapeKeyIsPressed = true;
				break;

			case 0x46: // for 'f' or 'F'
				if (gbFullscreen == false)
				{
					ToggleFullscreen();
					gbFullscreen = true;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = false;
				}

				break;

			case 0x4C:  //L or l
				if (bIsLkeyPressed == false)
				{
					gbLight = true;
					bIsLkeyPressed = true;
				}
				else
				{
					gbLight = false;
					bIsLkeyPressed = false;
				}
				break;

			default: 
				break;
			}

			break;

		case WM_LBUTTONDOWN:
			break;

		case WM_CLOSE:
			uninitialize();
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default :
			break;
		}

		return (DefWindowProc(hwnd, iMsg, wParam, lParam));

	}

	void ToggleFullscreen(void)
	{
		//variable declarations
		MONITORINFO mi;

		//code
		if (gbFullscreen == false)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			if (dwStyle & WS_OVERLAPPEDWINDOW)
			{
				mi = { sizeof(MONITORINFO) };
				if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
				{
					SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
					SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				}
			}
			ShowCursor(FALSE);
		}

		else
		{
			//code
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}
	}



	void initialize(void)
	{
		void uninitialize(void);
		void resize(int, int);
		int LoadGLTextures(GLuint *, TCHAR[]);

		PIXELFORMATDESCRIPTOR pfd;
		int iPixelFormatIndex;

		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;

		ghdc = GetDC(ghwnd);

		iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

		if (iPixelFormatIndex == 0)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		ghrc = wglCreateContext(ghdc);
		if (ghrc == NULL)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		if (wglMakeCurrent(ghdc, ghrc) == false)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// GLEW initialization code for GLSL (It must be Here i.e; After creating OpenGL context but before using any OpenGL Function)

		GLenum glew_error = glewInit();

		if (glew_error != GLEW_OK)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

		// **** Vertex Shader ****

		gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		// provide source code to shader 

		const GLchar * vertexShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec2 vTexture0_Coord;" \
			"in vec4 vColor;" \
			"in vec3 vNormal;" \
			"out vec4 out_color;"\
			"out vec2 out_texture0_coord;" \
			"uniform mat4 u_model_matrix;" \
			"uniform mat4 u_view_matrix;" \
			"uniform mat4 u_projection_matrix;" \
			"uniform int u_LKeyPressed;" \
			"out vec3 transformed_normals;" \
			"out vec3 light_direction;"\
			"out vec3 viewer_vector;"\
			"uniform vec4 u_light_position;" \
			"void main (void)" \
			"{" \
			"if(u_LKeyPressed == 1)" \
			"{" \
			"vec4 eyeCoordinates =u_view_matrix * u_model_matrix * vPosition;" \
			"transformed_normals = mat3(u_view_matrix * u_model_matrix)*vNormal;" \
			"light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
			"viewer_vector = -eyeCoordinates.xyz;" \
			"}" \
			"out_texture0_coord = vTexture0_Coord;" \
			"out_color = vColor;" \
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix *vPosition;" \
			"}";


		glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

		// Compile Shader 

		glCompileShader(gVertexShaderObject);

		GLint iInfoLogLength = 0;
		GLint iShaderCompiledStatus = 0;
		char *szInfoLog = NULL;

		glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Vertex Shader compilation log :%s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}


		/// *** Fragment Shader *** ///

		gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode =
			"#version 400 core" \
			"\n" \
			"in vec2 out_texture0_coord;"
			"in vec3 transformed_normals;"\
			"in vec3 light_direction;"\
			"in vec3 viewer_vector;"\
			"uniform vec3 u_La;" \
			"uniform vec3 u_Ld;" \
			"uniform vec3 u_Ls;" \
			"uniform vec3 u_Ka;" \
			"uniform vec3 u_Kd;" \
			"uniform vec3 u_Ks;" \
			"uniform float u_material_shininess;" \
			"in vec4 out_color;"
			"uniform int u_LKeyPressed;"\
			"uniform sampler2D u_texture0_sampler;" \
			"out vec4 FragColor;" \
			"void main (void)" \
			"{" \
			"vec4 color;" \
			"vec4 phong_ads_color;" \
			"if(u_LKeyPressed == 1)" \
			"{" \
			"vec3 normalized_transformed_normals = normalize(transformed_normals);"\
			"vec3 normalized_light_direction = normalize(light_direction);"\
			"vec3 normalized_viewer_vector = normalize(viewer_vector);"\
			"vec3 ambient = u_La * u_Ka;"\
			"float tn_dot_ld = max(dot(normalized_transformed_normals , normalized_light_direction),0.0);"\
			"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"\
			"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\
			"vec3 specular = u_Ls * u_Ks *pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"\
			"phong_ads_color = vec4(ambient + diffuse + specular,1.0);"\
			"color = phong_ads_color * texture(u_texture0_sampler,out_texture0_coord) * out_color;" \
			"}" \
			"else" \
			"{" \
			"color = texture(u_texture0_sampler,out_texture0_coord) + out_color;" \
			"}" \
			"FragColor = color;" \
			"}";

		glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

		glCompileShader(gFragmentShaderObject);
		glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if (szInfoLog != NULL)
				{
					GLsizei written;

					glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Fragment Shader compilation log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		/// *** Shader Program *** ///

		gShaderProgramObject = glCreateProgram();

		glAttachShader(gShaderProgramObject, gVertexShaderObject);

		glAttachShader(gShaderProgramObject, gFragmentShaderObject);

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_COLOR, "vColor");

		glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

		glLinkProgram(gShaderProgramObject);

		GLint iShaderProgramLinkStatus = 0;
		glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);

		if (iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpfile, "Shader Program Link log: %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		// get MVP uniform location

		gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
		gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
		gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

		Ka_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
		Kd_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
		Ks_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
		Material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

		gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
		Ld_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
		La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
		La_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

		gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
		gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

		/// *** vertices , colors , shader attribs , vbo ,vao initializations *** ///

		//cube vertices
		const GLfloat cubeVertices[] =
		{
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f
		};


		//cube colors
		const GLfloat cubeColors[] =
		{
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f
		};


		//cube normals
		const GLfloat CubeNormals[] =
		{
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			0.0f,0.0f,-1.0f,
			0.0f,0.0f,-1.0f,
			0.0f,0.0f,-1.0f,
			0.0f,0.0f,-1.0f,
			-1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f,
			0.0f,-1.0f,0.0f,
			0.0f,-1.0f,0.0f,
			0.0f,-1.0f,0.0f
		};

		//cube texture coordinates
		const GLfloat cubeTextCoords[] =
		{
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f
		};

		// Cube code 

		glGenVertexArrays(1, &gVao_Cube);
		glBindVertexArray(gVao_Cube);

		// Vbo 
		glGenBuffers(1, &gVbo_Cube);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Cube);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices) + sizeof(cubeColors) + sizeof(CubeNormals) + sizeof(cubeTextCoords), NULL, GL_STATIC_DRAW);
		GLuint offset = 0;

		glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(cubeVertices), cubeVertices);
		offset += sizeof(cubeVertices);
		glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(cubeColors), cubeColors);
		offset += sizeof(cubeColors);
		glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(CubeNormals), CubeNormals);
		offset += sizeof(CubeNormals);
		glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(cubeTextCoords), cubeTextCoords);
		offset += sizeof(cubeTextCoords);
		//vertex
		glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
		//color
		glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)sizeof(cubeVertices));
		glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);
		//normal
		glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(sizeof(cubeVertices) + sizeof(cubeColors)));
		glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
		//tex coords
		glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(sizeof(cubeVertices) + sizeof(cubeColors) + sizeof(CubeNormals)));
		glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);

		//-----------------

		glShadeModel(GL_SMOOTH);

		glClearDepth(1.0f);

		glEnable(GL_DEPTH_TEST);

		glDepthFunc(GL_LEQUAL);

		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		glEnable(GL_CULL_FACE);

		LoadGLTextures(&gTexture_Stone, MAKEINTRESOURCE(IDBITMAP_STONE));

		glEnable(GL_TEXTURE_2D);

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		gPerspectiveProjectionMatrix = mat4::identity();

		resize(WIN_WIDTH, WIN_HEIGHT);
	}

	int LoadGLTextures(GLuint *texture,TCHAR imageResourceId[])
	{
		HBITMAP hBitmap;
		BITMAP bmp;
		int iStatus = FALSE;

		glGenTextures(1, texture); // 1 image

		hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

		if (hBitmap)
		{
			iStatus = TRUE;
			GetObject(hBitmap, sizeof(bmp), &bmp);
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glBindTexture(GL_TEXTURE_2D, *texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_RGB,
				bmp.bmWidth,
				bmp.bmHeight,
				0,
				GL_BGR,
				GL_UNSIGNED_BYTE,
				bmp.bmBits);

			// Create mipmaps for this texture for better image quality

			glGenerateMipmap(GL_TEXTURE_2D);

			DeleteObject(hBitmap);
		}

		return (iStatus);
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gShaderProgramObject);

		mat4 modelMatrix = mat4::identity();
		mat4 viewMatrix = mat4::identity();
		mat4 rotationMatrix = mat4::identity();

		if (gbLight == true)
		{
			glUniform1i(gLKeyPressedUniform, 1);

			//light properties
			glUniform3fv(La_uniform, 1, light_Ambient);
			glUniform3fv(Ld_Uniform, 1, light_Diffuse);
			glUniform3fv(Ls_Uniform, 1, light_Specular);
			glUniform4fv(gLightPositionUniform, 1, light_Position);

			glUniform3fv(Ka_Uniform, 1, material_Ambient);
			glUniform3fv(Kd_Uniform, 1, material_Diffuse);
			glUniform3fv(Ks_Uniform, 1, material_Specular);
			glUniform1f(Material_shininess_uniform, material_shininess);
		}
		else
		{
			glUniform1i(gLKeyPressedUniform, 0);
		}

		// OpenGL drawing 
		// Cube code 

		modelMatrix = translate(0.0f, 0.0f, -5.0f);
		rotationMatrix = rotate(gAngleCube, gAngleCube,gAngleCube);
		modelMatrix = modelMatrix * rotationMatrix;


		glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		//Bind with cube texture 

		glActiveTexture(GL_TEXTURE0);

		glBindTexture(GL_TEXTURE_2D, gTexture_Stone);

		glUniform1i(gTexture_sampler_uniform, 0);

		// Bind vao 
		glBindVertexArray(gVao_Cube);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

		glBindVertexArray(0);

		glUseProgram(0);

		SwapBuffers(ghdc);
	}


	void resize(int width, int height)
	{
		if (height == 0)
			height = 1;

		glViewport(0, 0, (GLsizei)width, (GLsizei)height);

		gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	}

	void spin(void)
	{
		gAngleCube = gAngleCube + 0.009f;

		if (gAngleCube >= 360.0f)
			gAngleCube = gAngleCube - 360.0f;
	}

	void uninitialize(void)
	{
		if (gbFullscreen == true)
		{
			dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

			ShowCursor(TRUE);
		}


		if (gTexture_Stone)
		{
			glDeleteTextures(1, &gTexture_Stone);
			gTexture_Stone = 0;
		}

		// Cube

		if (gVao_Cube)
		{
			glDeleteVertexArrays(1, &gVao_Cube);
			gVao_Cube = 0;
		}

		if (gVbo_Cube)
		{
			glDeleteBuffers(1, &gVbo_Cube);
			gVbo_Cube = 0;
		}

		glDetachShader(gShaderProgramObject, gVertexShaderObject);

		glDetachShader(gShaderProgramObject, gFragmentShaderObject);

		glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;

		glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);

		wglMakeCurrent(NULL, NULL);

		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

		if (gpfile)
		{
			fprintf(gpfile, "Log file is successfully closed.\n");
			fclose(gpfile);
			gpfile = NULL;
		}
	}
