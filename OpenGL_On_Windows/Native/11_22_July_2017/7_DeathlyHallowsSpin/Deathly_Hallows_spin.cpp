#include <Windows.h>
#include <gl/GL.h>
#include <GL\GLU.h>
#include <math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define PI 3.1415926535898

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbIsEscapeKeyPressed = false;
bool gbFullscreen = false;
float angleTri = 0.0f;

void resize(int, int);

struct Point
{
	GLfloat xAxis;
	GLfloat yAxis;
};

struct Color
{
	GLfloat red;
	GLfloat green;
	GLfloat blue;
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void updateAngle(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Deathly Hallows Spin");
	bool bDone = false;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = CreateSolidBrush(RGB(1, 1, 1));
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx
	(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("Deathly Hallows Spin"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{

			if (gbActiveWindow == true)
			{
				if (gbIsEscapeKeyPressed == true)
					bDone = true;
				updateAngle();
				display();
			}
		}

	}

	uninitialize();
	return (int(msg.wParam));

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbIsEscapeKeyPressed = true;
			break;

		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}


void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}

void initialize(void)
{

	Point getIncenterOfTriangle(Point, Point, Point);
	GLfloat getRadiusOfIncircle(Point, Point, Point);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	void updateAngle(void);
	void drawTriangle(Point, Point, Point, Color, GLfloat);
	void drawCircle(GLfloat radius, Point incenter, Color color);
	void drawLine(Point, Point, Color, GLfloat);
	Point getIncenterOfTriangle(Point, Point, Point);
	GLfloat getRadiusOfIncircle(Point, Point, Point);
	
	GLint circle_points = 1000;
	GLfloat sideOfTriangle = 0.9f;
	
	GLfloat radius = getRadiusOfIncircle(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) },
		Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) });

	Point incenter = getIncenterOfTriangle(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) },
		Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) });
	
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glRotatef(angleTri, 0.0f, 1.0f, 0.0f);

	drawCircle(radius, incenter, Color{ 1.0f,1.0f,1.0f });
	
	drawTriangle(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) },
		Point{ -(sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Point{ (sideOfTriangle / 2.0f), -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) },
		Color{ 1.0f,1.0f,1.0f },
		1.0f);
	
	drawLine(Point{ 0.0f,(GLfloat)(sideOfTriangle / sqrt(3)) }, Point{ 0.0f, -(GLfloat)(sideOfTriangle / (2.0f * sqrt(3))) }, Color{ 1.0f,1.0f,1.0f }, 1.0f);

	SwapBuffers(ghdc);

}

void drawCircle(GLfloat radius, Point incenter, Color color)
{
	GLint circle_points = 1000;
	GLfloat angle;

	glBegin(GL_POINTS);

	glColor3f(color.red, color.green, color.blue);

	for (int i = 0.0f; i<circle_points; i++)
	{
		angle = 2 * PI * i / circle_points;
		glVertex3f(cos(angle)*radius + incenter.xAxis,
			sin(angle)*radius + incenter.yAxis, 0.0f);
	}

	glEnd();

}

void drawTriangle(Point top, Point left, Point right, Color color, GLfloat width)
{
	glLineWidth(width);
	glBegin(GL_LINE_LOOP);

	glColor3f(color.red, color.green, color.blue);

	glVertex3f(top.xAxis, top.yAxis, 0.0f);
	glVertex3f(left.xAxis, left.yAxis, 0.0f);
	glVertex3f(right.xAxis, right.yAxis, 0.0f);

	glEnd();
}


void drawLine(Point xCoordinate, Point yCoordinate, Color color, GLfloat width)
{
	glLineWidth(width);
	glBegin(GL_LINES);

	glColor3f(color.red, color.green, color.blue);
	glVertex3f(xCoordinate.xAxis, xCoordinate.yAxis, 0.0f);
	glVertex3f(yCoordinate.xAxis, yCoordinate.yAxis, 0.0f);

	glEnd();
}
void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void updateAngle()
{
	angleTri = angleTri + 0.1f;
	if (angleTri >= 360.0f)
		angleTri = 0.0f;

}


GLfloat getSideOfTriangle(Point vertex1, Point vertex2)
{
	GLfloat sideOfTriangle = 0.0f;

	return sqrt(((vertex2.xAxis - vertex1.xAxis)*(vertex2.xAxis - vertex1.xAxis) + (vertex2.yAxis - vertex1.yAxis)*(vertex2.yAxis - vertex1.yAxis)));
}


Point getIncenterOfTriangle(Point vertexA, Point vertexB, Point vertexC)
{
	GLfloat perimeter = 0.0f;

	GLfloat sideA = getSideOfTriangle(vertexB, vertexC);
	GLfloat sideB = getSideOfTriangle(vertexA, vertexC);
	GLfloat sideC = getSideOfTriangle(vertexA, vertexB);

	perimeter = sideA + sideB + sideC;

	GLfloat x_Axis_Incenter = ((sideA*vertexA.xAxis) + (sideB*vertexB.xAxis) + (sideC*vertexC.xAxis)) / perimeter;
	GLfloat y_Axis_Incenter = ((sideA*vertexA.yAxis) + (sideB*vertexB.yAxis) + (sideC*vertexC.yAxis)) / perimeter;

	return Point{ x_Axis_Incenter,y_Axis_Incenter };
}

GLfloat getRadiusOfIncircle(Point vertexA, Point vertexB, Point vertexC)
{
	GLfloat radiusOfincircle = 0.0f;
	GLfloat areaOfTriangle = 0.0f;
	GLfloat semiperimeterOfTriangle = 0.0f;

	GLfloat sideA = getSideOfTriangle(vertexB, vertexC);
	GLfloat sideB = getSideOfTriangle(vertexA, vertexC);
	GLfloat sideC = getSideOfTriangle(vertexA, vertexB);

	semiperimeterOfTriangle = (sideA + sideB + sideC) / 2;
	areaOfTriangle = sqrt(semiperimeterOfTriangle*(semiperimeterOfTriangle - sideA)*(semiperimeterOfTriangle - sideB)*(semiperimeterOfTriangle - sideC));
	radiusOfincircle = areaOfTriangle / semiperimeterOfTriangle;

	return radiusOfincircle;
}


void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}


