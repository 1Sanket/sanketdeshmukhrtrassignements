
// Windows header

#include <Windows.h>

// OpenGl headers

#include <gl/GL.h>
#include <gl/GLU.h>

// C headers  

#include <stdio.h>
#include <stdlib.h>

// C++ headers

#include <vector>

// Symbolic Constants.

#define TRUE			  1  
#define FALSE			  0

#define BUFFER_SIZE		  256    // Maximum length of string in mesh file.
#define S_EQUAL			  0      // Return value of strcmp when strings are equal.

#define WIN_INIT_X		  100    // x-coordinate of top-left point.
#define WIN_INIT_Y        100    // y-coordinate of top-left point.
#define WIN_WIDTH		  800	   // Initial width of window.
#define WIN_HEIGHT		  600    // Initial height of window.

#define VK_F			  0x46   // Virual key code of capital F key
#define VK_f			  0x60   // Virtual key code of small f key

#define NR_POINT_COORDS   3      // Number of point coordinates.
#define NR_TEXTURE_COORDS 2      // Number of texture coordinates.
#define NR_NORMAL_COORDS  3      // Number of normal coordinates.
#define NR_FACE_TOKENS    3      // Minimum number of entries in face data. 

#define FOY_ANGLE		  45     //	Field of view in Y direction.
#define ZNEAR			  0.1    // Distance from viewer to near plane of viewing volume.
#define ZFAR			  200.0  // Distance from viewer to far plane of viewing volume.

#define VIEWPORT_BOTTOMLEFT_X  0  // X-coordinate of bottom-left point of viewport rectangle.
#define VIEWPORT_BOTTOMLEFT_Y  0  // Y-coordinate of bottom-left point of viewport rectangle.

#define MONKEYHEAD_X_TRANSLATE 0.0f  // x-translation of monkey head
#define MONKEYHEAD_Y_TRANSLATE -0.0f // y-translation of monkey head
#define MONKEYHEAD_Z_TRANSLATE -5.0f // z-translation of monkey head

#define MONKEYHEAD_X_SCALE_FACTOR 1.5f //X-scale factor of monkey head
#define MONKEYHEAD_Y_SCALE_FACTOR 1.5f // Y-scale factor of monkey head
#define MONKEYHEAD_Z_SCALE_FACTOR 1.5f // Z-scale factor of monkey head

#define START_ANGLE_POS    0.0f //marks beginning angle position of rotation
#define END_ANGLE_POS      360.0f //Marks terminating angle position rotation
#define MONKEYHEAD_ANGLE_INCREMENT 0.05f  // increment angle for monkeyhead

// Import libraries

#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

// Error handling macros

#define ERRORBOX1(lpszErrorMessage,lpszCaption){MessageBox((HWND)NULL, TEXT(lpszErrorMessage), TEXT(lpszCaption), MB_ICONERROR);ExitProcess(EXIT_FAILURE);}

#define ERRORBOX2(hwnd,lpszErrorMessage,lpszCaption) {MessageBox((HWND)NULL,TEXT(lpszErrorMessage),TEXT(lpszCaption),MB_ICONERROR);DestroyWindow(hwnd);}

// Callback procedure declaration

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

// Global Defination

HWND g_hwnd = NULL;  //Handle to a window.
HDC  g_hdc = NULL;  // Handle to a device context
HGLRC g_hrc = NULL; // Handle to OpenGL rendering context

DWORD g_dwStyle = NULL; //Window Style
WINDOWPLACEMENT g_wpPrev; //Structure for holding previous window position

bool g_bActiveWindow = false; // Flag indicating whether window is active or not
bool g_bEscapeKeyIsPressed = false; // Flag indicating whether escape is pressed or not
bool g_bFullScreen = false; // Flag indicating whether window is currently fullscreen or not
bool gbIsLightingEnabled = false;

// Arrays required for lighting.
GLfloat light_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 1.0f,0.0f,0.0f,0.0f };

GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shinyness[] = { 50.f };

GLfloat g_rotate;

// Vector of vector of floats to hold the vertex data

std::vector<std::vector<float>> g_vertices;

// Vector of vector of floats to hold the texture data

std::vector <std::vector <float>> g_texture;

// vector of vector of floats to hold the normal data

std::vector<std::vector<float>> g_normals;

//vector of vector of int to hold the index data in g_vertices

std::vector <std::vector<int>> g_face_tri, g_face_tetxture, g_face_normals;

// Handle to a mesh file

FILE *g_fp_meshfile = NULL;

// Handle to a log file

FILE *g_fp_logfile = NULL;

// Hold line in a file 

char line[BUFFER_SIZE];

// Definition of window entry point function

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	// Function prototypes user defined functions called within WinMain

	void initialize(void);   // initialise OpenGL state machine 
	void update(void);   // Update state variables 
	void display(void);  // Render scene
	void uninitialize(void);  // Free/ Destroy resources

	// WinMain : Local data definitions 

	static TCHAR szAppName[] = TEXT("Mesh loading version 2");  // Name of window class
	HWND hWnd = NULL; // Handle to a window
	HBRUSH hBrush = NULL; // Handle to a background painting brush
	HCURSOR hCursor = NULL; // Handle to a cursor
	HICON hIcon = NULL; // Handle to an icon
	HICON hIconSm = NULL; // Handle to a small icon

	bool bDone = false;  // Flag indicating whether or not to exit from game loop

	WNDCLASSEX wndEx; //Structure holding window class attributes
	MSG msg;  // Structure holding message attributes

	// zero out structures 

	ZeroMemory((void*)&wndEx, sizeof(WNDCLASSEX));
	ZeroMemory((void*)&msg, sizeof(MSG));

	// Acquire following resources

	// 1. Black brush
	// 2. Cursor	
	// 3. Icon
	// 4. Small Icon

	hBrush = (HBRUSH)GetStockObject(BLACK_BRUSH);

	if (!hBrush)
		ERRORBOX1("Error in getting stock object", "GetStockObject Error");

	hCursor = LoadCursor((HINSTANCE)NULL, IDC_ARROW);

	if (!hCursor)
		ERRORBOX1("Error in loading a cursor", "LoadCursor Error");

	hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);

	if (!hIcon)
		ERRORBOX1("Error in loading icon", "LoadIcon Error");

	hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);

	if (!hIconSm)
		ERRORBOX1("Error in loading icon", "LoadIcon Error");

	// Fill window class structure

	wndEx.cbClsExtra = 0; //Extra class bytes allocation
	wndEx.cbWndExtra = 0; // Extra Window bytes allocation
	wndEx.cbSize = sizeof(WNDCLASSEX); // size of structure
	wndEx.hbrBackground = hBrush; //Handle to a background brush
	wndEx.hCursor = hCursor;  // Handle to a cursor
	wndEx.hIcon = hIcon; // Handle to an icon
	wndEx.hIconSm = hIconSm;// Handle to a small icon
	wndEx.hInstance = hInstance; // Handle to a program instance
	wndEx.lpfnWndProc = WndProc; // Address of a window procedure
	wndEx.lpszClassName = szAppName; // Name of a class
	wndEx.lpszMenuName = NULL; // Name of menu
	wndEx.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;  // Window style

	// registering window class 

	if (!RegisterClassEx(&wndEx))
		ERRORBOX1("Error in registering a class", "RegisterClassEx Error");

	// Create window in memory 

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,  // Extended window style
		szAppName,         // class name
		szAppName,        // Window caption
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Window style
		WIN_INIT_X, // X-coordinate of top left point
		WIN_INIT_Y,  // Y-coordinate of top left point
		WIN_WIDTH,  // Initial width of window
		WIN_HEIGHT,  //Initial height of window
		(HWND)NULL, // Handle to parent window. NULL : Desktop 
		(HMENU)NULL, // Handle to a menu . NULL :No menu
		hInstance,   // Handle to a program instance
		(LPVOID)NULL); // data to be sent to window callback. NULL : No data to send

	if (!hWnd)
		ERRORBOX1("Error in creating a window in memory", "CreateWindowEx Error");

	// Store handle to a window in global handle
	g_hwnd = hWnd;

	// Initialise openGL rendering context

	initialize();

	ShowWindow(hWnd, SW_SHOW); // Set specified window's show state
	SetForegroundWindow(hWnd);  // Put a thread created the specified window to a foreground
	SetFocus(hWnd);   // Set keyboard focus to specified window


	// Game loop
	while (bDone == false)
	{
		// Remark : PeekMessage is NOT a blocking function

		if(PeekMessage(&msg, // Pointer to structure for window message
			(HWND)NULL,    // Handle to a window
			0,             // First message in a queue
			0,             // Last message in a queue  (Select all messages)
			PM_REMOVE)) // Remove message from queue after processing from PeekMessage
		{
			if (msg.message == WM_QUIT)  // If current message is a quit message then exit the game loop
			{
				bDone = true;           // Causes game loop to exit
			}

			else
			{
				TranslateMessage(&msg); // Translate virtual key message into character message
				DispatchMessage(&msg);  // Dispatches message to a window procedure

			}
		}

		else                               // If there is no message on message queue then do rendering
		{
			if (g_bActiveWindow== true)    //  if window is foreground (receiving keystrokes)
			{
				if (g_bEscapeKeyIsPressed)  // if escape key is pressed
 				{
					bDone = true;           // Exit from game loop
				}

				else
				{
					// Update state variables

					update();

					// Render a scene

					display();
				}
			}
		}
	}

	// Release / Destroy resources 

	uninitialize();
	return((int)msg.wParam);  // return from WinMain

}


LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// Prototypes of functions called from WndProc

	void resize(int, int); // Handle window resize event
	void ToggleFullScreen(void); // Toggle window from fullscreen to previously saved dimension and vice versa
	void uninitialize(void);  // Release / Destroy reources

	switch (uMsg)
	{
		// Handler of an event : Actiavte or deactivate the window
		case WM_ACTIVATE:
			if (HIWORD(wParam) == 0)
			{
				g_bActiveWindow = true;
			}

			else
			{
				g_bActiveWindow = false;
			}

			break;

        // Handler of an event : Background data must be erased
		case WM_ERASEBKGND:
			return(0);

		// Handler of an event : A window has been resized
		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;

		// Handler of an event : A key has been pressed

		case WM_KEYDOWN:
			switch (wParam)  // Which key is pressed
			{
				// Handler : Escape key is pressed 
			case VK_ESCAPE:
				if (g_bEscapeKeyIsPressed == false)
				{
					g_bEscapeKeyIsPressed = true;
				}

				break;
			
			// Handler : Small or capital key is pressed
			case VK_F:
			case VK_f:
				if (g_bFullScreen == false)
				{
					ToggleFullScreen();
					g_bFullScreen = true;
				}

				else
				{
					ToggleFullScreen();
					g_bFullScreen = false;
				}

				break;

			case 0x4C: // for 'l' or 'L'
				if (gbIsLightingEnabled == false)
				{
					glEnable(GL_LIGHTING);
					gbIsLightingEnabled = true;
				}
				else
				{
					glDisable(GL_LIGHTING);
					gbIsLightingEnabled = false;
				}
				break;

				// for all other keys
			
			default:
				break;
		  }
			break;

			// handler of an event: Left mouse button has been pressed.

		case WM_LBUTTONDOWN:

			break;

			// Handler of an event : A window or application must be closed

		case WM_CLOSE:
			uninitialize();
			break;

			// Handler of an event : A window is being destroyed 
		case WM_DESTROY:
			PostQuitMessage(EXIT_SUCCESS);
			break;

		default : 
			break;
	}

	// Default procesing of events

	return (DefWindowProc(hWnd, uMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	MONITORINFO mi;    // information structure holding monitor information
	
	if (g_bFullScreen == false)
	{
		// Screen is not currently fullscreen , so toggle to full screen mode
		
		// Retrieves specified info of specified window 
		// hWnd : Which window ? GWL_STYLE : Get Window Style

		g_dwStyle = GetWindowLong(g_hwnd, GWL_STYLE);

		if (g_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			// If window is currently having WS_OVERLAPPEDWINDOW style

			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(g_hwnd,&g_wpPrev) && GetMonitorInfo(MonitorFromWindow(g_hwnd,MONITORINFOF_PRIMARY),&mi))
			{
				// If wpPrev is successfully filled with current window placement and if mi is filled with primary monitor info then  
				//  S1: Remove WS_OVERLAPPEDWINDOW style
				//  S2: Set window position by aligning left-top point's XY coordinates to monnitors left top coordinates and setting window width and height to 
				//  monitors height and width (effectively making window full screen)
                //  SWP_NOZORDER - Don't change z order
				//  SWP_FRAMESCHANGED  - Forces recalculation of non client area

				SetWindowLong(g_hwnd, GWL_STYLE, g_dwStyle & ~WS_OVERLAPPEDWINDOW); // S1
				SetWindowPos(g_hwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}

			// Make cursor disappear

			ShowCursor(FALSE);

		}
	}

	else
	{
		// Screen is  fullscreen , so toggle to previously saved dimension
		// S1: Add WS_OVERLAPPEDWINDOW to window style via SetWindowLong
		// S2: Set window placement to stored previous placement in wpPrev via SetWindowPlacement 
		// S3: Force the effects of SetWindowPlacement by call to SetWindowPos with 
		//  SWP_NOMOVE  : Dont change left top point (ie ignore third and fourth parameter )
		//  SWP_NOSIZE : Dont change dimensions of window (ie ignore fifth and sixth parameter)
		//  SWP_NOZORDER : Dont change z-order of the window and its children
		// SWP_NOOWNERZORDER :Dont changez-order of owner of the window (refered by g_hwnd)
		// SWP_FRAMESCHANGED : Force recalculation of non - client area 
		// S4: Make cursor visible

		SetWindowLong(g_hwnd, GWL_STYLE, g_dwStyle | WS_OVERLAPPEDWINDOW);  // S1
		SetWindowPlacement(g_hwnd, &g_wpPrev); // S2
		SetWindowPos(g_hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);   // S3
		ShowCursor(TRUE);  // S4
	}
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);
	void LoadMeshData(void); // Load data from mesh file and populate global vectors.

	g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "w");
	if (!g_fp_logfile)
		uninitialize();

	PIXELFORMATDESCRIPTOR pfd;  //  Information structure describing the pixel format .
	int iPixelFormatIndex = -1; //  Index to Pixel format structure in the internal tables.

	ZeroMemory((void*)&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize PIXELFORMATDESCRIPTOR with desired values of its attribute
	//pfd.nSize : Allows Microsoft to maintain multiple versions of structure.
	//pfd.nVersion:Version information
	//pfd.dwFlags
	//PFD_DRAW_TO_WINDOW:Indicates real time rendering
	//PFD_SUPPORT_OPENGL: Pixel format having this flag set will be considered for matching
	//PFD_DOUBLEBUFFER:Pixel format having this flag set will be considered for matching
	//pfd.iPixelType : Type of pixel format to be chosen 
	//pfd.cColorBit : Specifies color depth in bits
	//pfd.cRedBits : Specifies bit-depth for red color
	//pfd.cGreenBits : Specifies bit-depth for green color
	//pfd.cBlueBits : Specifies bit-depth for blue color
	//pfd.cAlphaBits : Specifies bit-depth for alpha component
	//pfd.cDepthBits : Specifies depth bits

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	g_hdc = GetDC(g_hwnd);

	// Attempt to match an appropriate pixel format supported by device to a given pixel format specification (via &pfd). If fails then release the device context
	// obtained above and make handle null

	iPixelFormatIndex = ChoosePixelFormat(g_hdc, &pfd);
	 
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(g_hwnd,g_hdc);
		g_hdc = NULL;
	}

	// Sets pixel format descriptor of the specified device context (g_hdc) to device supported pixel format specified by handle iPixelFormatIndex
	// Address of logical pixel format descriptor is third parameter . If fails release the device context and make handle null

	if (SetPixelFormat(g_hdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(g_hwnd, g_hdc);
		g_hdc = NULL;
	}

	// Creates an OpenGL rendering context out of specified device context . If fails release the device context and make handle NULL

	g_hrc = wglCreateContext(g_hdc);

	if (g_hrc == NULL)
	{
		ReleaseDC(g_hwnd, g_hdc);
		g_hdc = NULL;
	}

	// Makes specified OpenGL rendering context by g_hrc the current rendering context of calling thread. If fails then delete OpenGL rendering context 
	// release device context and make both handles NULL
	
	if (wglMakeCurrent(g_hdc,g_hrc)== false)
	{
		wglDeleteContext(g_hrc);
		g_hrc = NULL;
		ReleaseDC(g_hwnd, g_hdc);
		g_hdc = NULL;
	}

	// Specifies clear value for the color buffer = black.

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Add depth :
	// S1: Enable depth test in state machine
	// S2 :Specify the depth comparison function .GL_LEQUAL : Passes if incoming z value is less than or equal to stored z value
	// S3:Specify the clear value for depth buffer
	// S4:Select shade model , FLAT or SMOOTH
	// S5: Specify the implementation specific hint
	//GL_PERSPECTIVE_CORRECTION_HINT : Indicates quality of color and texture coordinates interpolation
	//GL_NICEST : The most correct or the highest quality

	glEnable(GL_DEPTH_TEST); // S1
	glDepthFunc(GL_LEQUAL); //S2
	glClearDepth(1.0f); //S3
	glShadeModel(GL_SMOOTH); // S4
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // S5

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shinyness);

	glEnable(GL_LIGHT0);

	// Read mesh file and load global vectors with appropriate data

	LoadMeshData();

	// Warm up call to resize

	resize(WIN_WIDTH, WIN_HEIGHT);

}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	// Set the viewport transformation.
	// AP1,AP2 : (x,y) for lower left corner of viewport rectangle
	// AP3,AP4 :width - height of viewport rectangle

	glViewport(VIEWPORT_BOTTOMLEFT_X, VIEWPORT_BOTTOMLEFT_Y, (GLsizei)width, (GLsizei)height);

	// Set the projection transformation 
    // S1 : Select projection matrix mode 
	// S2 : Load identity matrix on projection matrix stack
	// S3 : Set perspective projection using gluPerspective
	// AP1 : Field of view angle , in y-direction , in degrees 
	// AP2 : Aspect ratio 
	// AP3 - AP4 : Distance from view to near clipping plane (AP3) & to far clipping plane (AP4)

	glMatrixMode(GL_PROJECTION); // S1

	glLoadIdentity(); // S2

	gluPerspective(FOY_ANGLE, (GLfloat)width / (GLfloat)height, ZNEAR, ZFAR); // S3

}


void LoadMeshData(void)
{
	void uninitialize(void);

	// Open mesh file , name of mesh file can be parametaried

	g_fp_meshfile = fopen("MonkeyHeadMeshFile.OBJ", "r");

	if (!g_fp_meshfile)
		uninitialize();

	// Seperator strings 
	// String holding space seperator for strtok

	char *sep_space = " ";

	// String holding forward slash seperator for strtok
	char *sep_fslash = "/";

	// Token pointers 
	// Character pointer for holding first word in a line

	char *first_token = NULL;

	// Character pointer for holding next word seperated by specified seperator to strtok 

	char *token = NULL;


	// Array of character pointers to hold strings of face entries 
	// Face entries can be variable . In some files they are three and in some files they are four

	char *face_tokens[NR_FACE_TOKENS];

	// Number of non-null tokens in the above vector

	int nr_tokens;

	// Character pointer for holding string associated with vertex index

	char *token_vertex_index = NULL;

	// Character pointer for holding string associated with texture index

	char *token_texture_index = NULL;

	// Character pointer for holding string associated with normal index

	char *token_normal_index = NULL;


	// While there is line in a file 

	while (fgets(line,BUFFER_SIZE,g_fp_meshfile)!= NULL)
	{
		// Bind line to a seperator and get first token

		first_token = strtok(line, sep_space);

		// If first token indicates vertex data 

		if (strcmp(first_token,"v")== S_EQUAL)
		{
			// Create vector of NR_POINT_COORDS number of floats to hold coordinates 

			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			//Do following NR_POINT_COORDS time
			// S1 :Get next token
			// S2 :Feed it ot atof to get floating point number out of it
			// S3 :Add floating point number generated to vector .
			// End of loop
			// S4 : At the end of loop vector is constructed , add it to global vector of vector of floats , named g_vertices

			for(int i=0; i != NR_POINT_COORDS;i++ )
			{
				vec_point_coord[i] = atof(strtok(NULL, sep_space));  // S1,S2,S3
			}

			g_vertices.push_back(vec_point_coord);
		}

		// If first token indicates texture data

		else if (strcmp(first_token,"vt")== S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			// Do following NR_TEXTURE_COORDS time
			// S1: Get next token 
			//S2 :Feed it to atof to get floating point number out of it 
			//S3:Add the floating point number generated to vector 
			// End of loop
			// S4 : At the end of the loop ,vector is constructed , add it to global vector of vector of floats ,named g_texture

			for (int i=0; i!= NR_TEXTURE_COORDS; i++)
			{
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			}

			g_texture.push_back(vec_texture_coord);
		}

		// If first token indicates normal data

		else if (strcmp(first_token,"vn")==S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);

			// Do following NR_NORMAL_COORDS time
			// S1: Get the token
			// S2 :Feed it to atof to get floating point number out of it
			// S3 : Add the floating point number generated to vector .
			// End of loop
			// S4 : At the end of loop vector is constructed , add it to global vector of vector of floats , named g_normals

			for (int i=0;i!=NR_NORMAL_COORDS;i++)
			{
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			}

			g_normals.push_back(vec_normal_coord);
		}

		// If first token indicates face data

		else if (strcmp(first_token, "f")== S_EQUAL)
		{
			// Define three vectors of integers with length 3 to hold indices of triangles positional coordinates , texture coordinates and normal coordinates 
			// in g_vertices, g_textures and g_normals resp

			std::vector <int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);

			// Initiate all  char pointers in face_token to NULL

			memset((void*)face_tokens, 0, NR_FACE_TOKENS);

			// Extract three fields of information in face_tokens and increment nr_tokens accordingly

			nr_tokens = 0;

			while (token=strtok(NULL,sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			// Every face data entry is going to have minimum three fields therefore , construct a triangle out of it with 
			// S1: triangle coordinate data and 
			// S2 : Texture coordinate index data
			// S3 :normal coordinate index data
			// S4 : Put that data in triangle_vertex_indices,texture_vertex_indices,normal_vertex_indices .Vectors will be constructed at the end of the loop

			for (int i=0; i!=NR_FACE_TOKENS; i++)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash); //S1
				token_texture_index = strtok(NULL, sep_fslash); // S2
				token_normal_index = strtok(NULL, sep_fslash); // S3

				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			// Add constructed vectors to global face vectors

			g_face_tri.push_back(triangle_vertex_indices);
			g_face_tetxture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}

		// Initialize line buffer to NULL

		memset((void*)line, (int)'\0', BUFFER_SIZE);

	}


	// Close meshfile and make file pointer NULL

	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	// Log vertex , texture and  face data in log file

	fprintf(g_fp_logfile, "g_vertices: %llu g_texture:%llu g_normals: %llu g_face_tri : %llu\n", g_vertices.size(), g_texture.size(), g_normals.size(), g_face_tri.size());

}

void update(void)
{
	// Increment MonkeyHead rotation angle by MONKEYHEAD_ANGLE_INCREMENT

	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;

	//If rotation angle equals or exceeds END_ANGLE_POS then reset to START_ANGLE_POS 

	if (g_rotate>=END_ANGLE_POS)
	{
		g_rotate = START_ANGLE_POS;
	}
}


void display(void)
{
	void uninitialize(void);

	// Clear color and depth buffer by their clear values

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Select modelview matrix stack and load identity matrix into it

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLATE);

	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR);

	glFrontFace(GL_CCW);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//S1 : For every face index maintained in triangular form in g_face_tri 
	// Do following 
	// S2:Set geometry primitive to GL_TRIANGLES
	// S3 : Extract triangle from outer loop index 
	//for every point of a triangle
	//S4 :Calculate index in g_vertices in variable vi
	// S5 : calculate x,y,z coordinates of point 
	// S6 :Send to glVertex3f
	// In S4 , we have to substract g_face_tri[i][j] by one because in mesh file indexing starts from 1 and in case of arrays / vectors indexing starts from 0

	for (int i=0; i!=g_face_tri.size();++i)  // S1
	{
		glBegin(GL_TRIANGLES); // S2
		
		for (int j =0; j!= g_face_tri[i].size();j++)  // S3
		{
			int ni = g_face_normals[i][j] - 1;  // S4
			glNormal3f(g_normals[ni][0], g_normals[ni][1], g_normals[ni][2]);
			int vi = g_face_tri[i][j] - 1;  // S4
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]); // S5, S6
			
		}

		//for (int j = 0; j != g_face_normals[i].size(); j++)  // S3
		//{
		//	int ni = g_face_normals[i][j] - 1;  // S4
		//	glNormal3f(g_normals[ni][0],g_normals[ni][1],g_normals[ni][2]); 
		//}

		glEnd();
	}

	// Bring background framebuffer to foreground and make current foreground buffer available for rendering.

	SwapBuffers(g_hdc);
}

void uninitialize(void)
{
	if (g_bFullScreen == true)
	{
		g_dwStyle = GetWindowLong(g_hwnd, GWL_STYLE);
		SetWindowLong(g_hwnd, GWL_STYLE, g_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(g_hwnd, &g_wpPrev);
		SetWindowPos(g_hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	//HGLRC : NULL means calling threads current rendering context is no longer current as well as it releases the device context used by that rendering context
	//HDC : is ignored if HGLRC is passed as NULL . Therefore , HDC is also NULL 

	wglMakeCurrent((HDC)NULL, (HGLRC)NULL);

	// Delete specified OpenGL rendering context. Make  handle NULL

	wglDeleteContext(g_hrc);
	g_hrc = (HGLRC)NULL;

	// Release the device context obtained via GetDC in initialize

	ReleaseDC(g_hwnd, g_hdc);
	g_hdc = (HDC)NULL;

	// Close log file and make file pointer NULL

	fclose(g_fp_logfile);
	g_fp_logfile = NULL;

	// Sends WM_DESTROY message to specified window , making beginning of the end of game loop.
	DestroyWindow(g_hwnd);

}