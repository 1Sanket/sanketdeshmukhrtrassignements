
// OGLPP Materials

// headers

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>  // CV for core video

#import <OpenGL/gl3.h>  // for OpenGL Core profile - 3.0 and above
#import <OpenGL/gl3ext.h> //For extensions of OpenGL 3.0 and above

#import "vmath.h"

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};

bool isLightEnabled= false;

GLboolean gbIs_X_AxisRotationOfLight = GL_TRUE;
GLboolean gbIs_Y_AxisRotationOfLight = GL_FALSE;
GLboolean gbIs_Z_AxisRotationOfLight = GL_FALSE;

float lightRotationAngle = 0.0f;

// 'C' style global function declarations 

CVReturn MyDisplayLinkCallback (CVDisplayLinkRef , const CVTimeStamp *,const CVTimeStamp *
                                ,CVOptionFlags,CVOptionFlags *,void *);   // Void is for the object for which callback is done  

// global variables

FILE *gpFile = NULL;

// interface declaration

@interface AppDelegate : NSObject <NSApplicationDelegate ,NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function

int main(int argc, const char * argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];

    NSApp = [NSApplication sharedApplication];

    [NSApp setDelegate : [[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}


// interface implementation

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView; 
}

-(void) applicationDidFinishLaunching : (NSNotification *) aNotification 
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath ];
    const char * pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
    gpFile = fopen(pszLogFileNameWithPath,"w");

    if(gpFile == NULL)
    {
        printf("Cannot create Log file.\n Exiting......\n ");
        [self release];
        [NSApp terminate : self];
    }

    fprintf( gpFile,"Program is started successfully \n");

    // window 
    NSRect win_rect;
    win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

    // create window

    window = [[NSWindow alloc] initWithContentRect:win_rect
              styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
              backing:NSBackingStoreBuffered
              defer:NO ];

    [window setTitle:@"macOS  OpenGL Materials"];
    [window center] ;                         

    glView = [[GLView alloc]initWithFrame : win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];

}

-(void) applicationWillTerminate :(NSNotification *) notification 
{
   fprintf(gpFile,"Program is terminated successfully \n");

   if(gpFile)
   {
       fclose(gpFile);
       gpFile = NULL;
   }
}

-(void) windowWillClose:(NSNotification *) notification
{
    [NSApp terminate : self];
}

-(void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}

@end

@implementation GLView
{
    @private 
        CVDisplayLinkRef displayLink;

		GLuint vertexShaderObject;
		GLuint fragmentShaderObject;
		GLuint shaderProgramObject;

		GLuint vao_Sphere;
		
		GLuint vbo_Sphere_Position;
		GLuint vbo_Sphere_Normal;
		GLuint vbo_Sphere_Elements;

    	
		float light_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
    	
		float light_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
    	
		float light_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
    	
		float light_position[] = {100.0f, 100.0f, 100.0f, 1.0f};	
		    	
		float material_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f};
    	
		float material_diffuse[] = {0.0f, 0.0f, 0.0f, 0.0f};
    	
		float material_specular[] = {0.0f, 0.0f, 0.0f, 0.0f};
    	
		float material_shininess = 0.0f;

		
        int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
        int laUniform, ldUniform, lsUniform, lightPositionUniform;
        int kaUniform, kdUniform, ksUniform, materialShininessUniform;

		GLuint LKeyPressedUniform;

		vmath::mat4 perspectiveProjectionMatrix;

}

-(id) initWithFrame : (NSRect) frame
{
    self = [super initWithFrame : frame];

    if(self)
    {
        [[self window] setContentView : self];

        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // Must specify the 4.1 core profile to use OpenGL 4.1

            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core, //version 4.1

            // specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery, // If hardware renderer is not found then give error .Do not give software renderer
            NSOpenGLPFAAccelerated, // For hardware acceleration
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0    // Last zero is must .It indicates end of an array  
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
        
        if(pixelFormat == nil)
        {
            fprintf(gpFile,"No valid OpenGL Pixel Format is available.Exiting...../n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext =[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // shareContext: nil means do not share context.
        [self setPixelFormat : pixelFormat];
        [self setOpenGLContext:glContext]; // It automatically releases the older context , if present and sets the newer one.
    }

    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *) pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];

    return (kCVReturnSuccess);
}


-(void) prepareOpenGL  // overrided method. called automatically.Analogous to initialise() of windows
{
    // OpenGL Info
    fprintf(gpFile,"OpenGL Version : %s \n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext]; 
     
    GLint swapInt = 1;

    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval]; // CP for Context Parameter 


	// Vertex Shader 

	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode=
			"#version 410 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;"\
			"uniform mat4 u_model_matrix;" \
            "uniform mat4 u_view_matrix;" \
			"uniform mat4 u_projection_matrix;" \
			"uniform int u_LKeyPressed;"\
			"uniform vec4 u_light_position;"\
            "out vec3 transformed_normals;"\
            "out vec3 light_direction;"\
            "out vec3 viewer_vector;"\
			"void main(void)" \
			"{" \
			"if (u_LKeyPressed == 1)"\
			"{"\
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition ;" \
            "transformed_normals = mat3 (u_view_matrix * u_model_matrix) * vNormal ;" \
            "light_direction = vec3 (u_light_position) - eyeCoordinates.xyz ;" \
            "viewer_vector = -eyeCoordinates.xyz;"\
			"}"\
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;"\
			"}";

	glShaderSource(vertexShaderObject,1,(const GLchar **) &vertexShaderSourceCode,NULL);

	glCompileShader(vertexShaderObject);
	
	GLint iInfoLogLength =0;
	GLint iShaderCompiledStatus =0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	// Fragment Shader

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
			"#version 410"\
			"\n" \
            "uniform int u_LKeyPressed;"\
			"in vec3 transformed_normals;"\
            "in vec3 light_direction;"\
            "in vec3 viewer_vector;"\
            "out vec4 FragColor;" \
            "uniform vec3 u_La;"\
            "uniform vec3 u_Ld;"\
            "uniform vec3 u_Ls;"\
            "uniform vec3 u_Ka;"\
            "uniform vec3 u_Kd;"\
            "uniform vec3 u_Ks;"\
            "uniform float u_material_shininess;"\
			"void main(void)" \
			"{" \
            "vec3 phong_ads_color;"\
            "if(u_LKeyPressed == 1)"\
            "{"\
            "vec3 normalized_transformed_normal = normalize(transformed_normals) ;"+
            "vec3 normalized_light_direction = normalize(light_direction);"+
            "vec3 normalized_viewer_vector = normalize(viewer_vector);"+
            "vec3 ambient = u_La * u_Ka;"+
            "float tn_dot_ld = max(dot(normalized_transformed_normal,normalized_light_direction),0.0);"+
            "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
            "vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normal);"+
            "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"+
            "phong_ads_color = ambient + diffuse + specular ;"+
            "}"+
            "else"+
            "{"+
            "phong_ads_color = vec3(1.0,1.0,1.0) ;"+
             "}"+
			"FragColor = vec4(phong_ads_color,1.0) ;" \
			"}" ;

	glShaderSource(fragmentShaderObject,1, (const GLchar **)&fragmentShaderSourceCode,NULL);

	glCompileShader(fragmentShaderObject);

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	// Shader Program

	shaderProgramObject = glCreateProgram();

	// Attach Shaders 

	glAttachShader (shaderProgramObject,vertexShaderObject);

	glAttachShader (shaderProgramObject,fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute

	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");

	// link Shader

	glLinkProgram(shaderProgramObject);

	GLint iShaderProgramLinkStatus=0;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if(iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);

			if(szInfoLog !=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Shader Program Link Log:%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	// get uniform location 


	 LKeyPressedUniform = glGetUniformLocation(shaderProgramObject,"u_LKeyPressed");

	 modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");

     viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix"); 

     projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix"); 

     laUniform = glGetUniformLocation(shaderProgramObject, "u_La"); 

     ldUniform = glGetUniformLocation(shaderProgramObject, "u_Ld"); 

     lsUniform = glGetUniformLocation(shaderProgramObject, "u_Ls"); 

     lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position"); 

     kaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka"); 

     kdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd"); 

     ksUniform = glGetUniformLocation(shaderProgramObject, "u_Ks"); 

     materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess"); 

    
	//Sphere sphere = new Sphere();
    float sphere_vertices[] = new float[1146];
    float sphere_normals[] = new float[1146];
    float sphere_textures[] = new float[764];
    short sphere_elements[] = new short[2280];

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

    numVertices = getNumberOfSphereVertices();
    numElements = getNumberOfSphereElements();


    // Sphere Vao

    glGenVertexArrays(1,&vao_Sphere);
    glBindVertexArray(vao_Sphere);

    // Sphere Position Vbo 

    glGenBuffers(1,& vbo_Sphere_Position);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Sphere_Position);
    glBufferData(GL_ARRAY_BUFFER,sphere_vertices.length * 4,sphere_vertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind sphere position Vbo
    

	// Sphere Normal Vbo 

    glGenBuffers(1,& vbo_Sphere_Normal);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Sphere_Normal);
    glBufferData(GL_ARRAY_BUFFER,sphere_normals.length * 4,sphere_normals,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind sphere normal Vbo

    
	// Element Vbo

	glGenBuffers(1,& vbo_Sphere_Elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_Sphere_Elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sphere_elements.length*2,sphere_elements,GL_STATIC_DRAW);


    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0); // unbind sphere element Vbo


    glBindVertexArray(0); // Unbind Square vao

    glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

//	glEnable(GL_CULL_FACE);

    // set background color
    glClearColor(0.0f,0.0f,0.0f,0.0f);  // blue

	// set projection matrix to identity matrix

	perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink); // CGDisplays - core graphics display
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self); // self is the 6th parameter of the callback
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj]; 
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink); // This will start the thread.
}

-(void) reshape  // This is overriden method . called automatically . Analogous to WM_SIZE
{
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]); 
    NSRect rect = [self bounds];

    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
    {
        height = 1;
    }

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void) drawView
{
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	if (isLightEnabled == true)
    {
        glUniform1i(LKeyPressedUniform,1);

         if (gbIs_X_AxisRotationOfLight == GL_TRUE)
        {
            light_position[1] =  80 * cos(lightRotationAngle);
            light_position[2] =  80 *  sin(lightRotationAngle);
            light_position[0] = 0.0f;
        }

        else if (gbIs_Y_AxisRotationOfLight == GL_TRUE)
        {
            light_position[0] = 80 * cos(-lightRotationAngle);
            light_position[2] = 80 * sin(-lightRotationAngle);
            light_position[1] = 0.0f;
        }

        else if (gbIs_Z_AxisRotationOfLight == GL_TRUE)
        {
            light_position[0] = 80 * cos(lightRotationAngle);
            light_position[1] = 80 * sin(lightRotationAngle);
            light_position[2] = 0.0f;
        }

		            // set light properties

        glUniform3fv(laUniform,light_ambient);

        glUniform3fv(ldUniform,light_diffuse);

        glUniform3fv(lsUniform,light_specular);

        glUniform4fv(lightPositionUniform,light_position);
            
			
			// set material properties
    
	    //glUniform3fv(kaUniform,material_ambient);

	    //glUniform3fv(kdUniform,material_diffuse);

	    //glUniform3fv(ksUniform,material_specular);

		// glUniform1f(materialShininessUniform,material_shininess);
	}
    
    else
    {
        glUniform1i(LKeyPressedUniform,0);
    }
	
	// OGL drawing
	// set modelView & model view projection matrices to identity

	vmath::mat4 viewMatrix = vmath::mat4::identity(); 

	vmath::mat4 modelMatrix = vmath::mat4::identity(); 

	
        /// Emerald

		material_ambient[0] = 0.0215f;
		material_ambient[1] = 0.1745f;
		material_ambient[2] = 0.0215f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.07568f;
		material_diffuse[1] = 0.61424f;
		material_diffuse[2] = 0.07568f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.633f;
		material_specular[1] = 0.727811f;
		material_specular[2] = 0.633f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.6f*128.0f;

       glViewport(0,(GLsizei) gHeight*5/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
    //    gPerspectiveProjectionMatrix = perspective(45.0f,((GLfloat) gWidth/4) / ((GLfloat) gHeight/6) ,0.1f,100.0f);
       drawSphere();

// /// Jade

material_ambient[0] = 0.135f;
material_ambient[1] = 0.2225f;
material_ambient[2] = 0.1575f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.54f;
material_diffuse[1] = 0.89f;
material_diffuse[2] = 0.63f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.316228f;
material_specular[1] = 0.316228f;
material_specular[2] = 0.316228f;
material_specular[3] = 1.0f;

material_shinyness = 0.1f* 128.0f;        


glViewport(0,(GLsizei) gHeight*4/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
drawSphere();
        
//         /// Obsidian

		material_ambient[0] = 0.05375f;
		material_ambient[1] = 0.05f;
		material_ambient[2] = 0.06625f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.18275f;
		material_diffuse[1] = 0.17f;
		material_diffuse[2] = 0.22525f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.332741f;
		material_specular[1] = 0.328634f;
		material_specular[2] = 0.346435f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.3f * 128.0f;

        glViewport(0,(GLsizei) gHeight*3/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();


//         		/// Pearl

		material_ambient[0] = 0.25f;
		material_ambient[1] = 0.20725f;
		material_ambient[2] = 0.20725f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 1.0f;
		material_diffuse[1] = 0.829f;
		material_diffuse[2] = 0.829f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.296648f;
		material_specular[1] = 0.296648f;
		material_specular[2] = 0.296648f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.088f * 128.0f;

        glViewport(0,(GLsizei) gHeight*2/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();



//          		/// Ruby
 
material_ambient[0] = 0.1745f;
material_ambient[1] = 0.01175f;
material_ambient[2] = 0.01175f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.61424f;
material_diffuse[1] = 0.04136f;
material_diffuse[2] = 0.04136f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.727811f;
material_specular[1] = 0.626959f;
material_specular[2] = 0.626959f;
material_specular[3] = 1.0f;

material_shinyness = 0.6f * 128.0f;


                 glViewport(0,(GLsizei) gHeight*2/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
                 drawSphere();

        
//          		/// Turquoise

		material_ambient[0] = 0.1f;
		material_ambient[1] = 0.18725f;
		material_ambient[2] = 0.1745f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.396f;
		material_diffuse[1] = 0.74151f;
		material_diffuse[2] = 0.69102f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.297254f;
		material_specular[1] = 0.30829f;
		material_specular[2] = 0.306678f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.1f * 128.0f;


        glViewport(0,(GLsizei) gHeight*1/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

//         // Second row

//         // Brass

        material_ambient[0] = 0.329412f;
		material_ambient[1] = 0.223529f;
		material_ambient[2] = 0.027451f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.780392f;
		material_diffuse[1] = 0.568627f;
		material_diffuse[2] = 0.113725f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.992157f;
		material_specular[1] = 0.941176f;
		material_specular[2] = 0.807843f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.21794872f * 128.0f;

        glViewport(0,0,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();


//         		/// Bronze

		material_ambient[0] = 0.2125f;
		material_ambient[1] = 0.1275f;
		material_ambient[2] = 0.054f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.714f;
		material_diffuse[1] = 0.4284f;
		material_diffuse[2] = 0.18144f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.393548f;
		material_specular[1] = 0.271906f;
		material_specular[2] = 0.166721f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.2f * 128.0f;

        glViewport((GLsizei)gWidth/4,(GLsizei) gHeight*5/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

//          		/// Chrome

		material_ambient[0] = 0.25f;
		material_ambient[1] = 0.25f;
		material_ambient[2] = 0.25f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.4f;
		material_diffuse[1] = 0.4f;
		material_diffuse[2] = 0.4f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.774597f;
		material_specular[1] = 0.774597f;
		material_specular[2] = 0.774597f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.6f * 128.0f;


        glViewport((GLsizei)gWidth/4,(GLsizei) gHeight*4/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

//          		/// Copper

		material_ambient[0] = 0.19125f;
		material_ambient[1] = 0.0735f;
		material_ambient[2] = 0.0225f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.7038f;
		material_diffuse[1] = 0.27048f;
		material_diffuse[2] = 0.0828f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.256777f;
		material_specular[1] = 0.137622f;
		material_specular[2] = 0.086014f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.6f * 128.0f;

        glViewport((GLsizei)gWidth/4,(GLsizei) gHeight*3/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();


//         		/// Gold

		material_ambient[0] = 0.24725f;
		material_ambient[1] = 0.1995f;
		material_ambient[2] = 0.0745f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.75164f;
		material_diffuse[1] = 0.60648f;
		material_diffuse[2] = 0.22648f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.628281f;
		material_specular[1] = 0.555802f;
		material_specular[2] = 0.366065f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.4f * 128.0f;

        glViewport((GLsizei)gWidth/4,(GLsizei) gHeight*2/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

        // red

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;
    
        material_diffuse[0] = 0.5f;
        material_diffuse[1] = 0.0f;
        material_diffuse[2] = 0.0f;
        material_diffuse[3] = 1.0f;
    
        material_specular[0] = 0.7f;
        material_specular[1] = 0.6f;
        material_specular[2] = 0.6f;
        material_specular[3] = 1.0f;
    
        material_shinyness = 0.25f * 128.0f;
    

        glViewport((GLsizei)gWidth/4,(GLsizei) gHeight*1/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

//         // Third row 

	/// White

	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 1.0f;

	material_diffuse[0] = 0.55f;
	material_diffuse[1] = 0.55f;
	material_diffuse[2] = 0.55f;
	material_diffuse[3] = 1.0f;

	material_specular[0] = 0.70f;
	material_specular[1] = 0.70f;
	material_specular[2] = 0.70f;
	material_specular[3] = 1.0f;

	material_shinyness = 0.25f * 128.0f;


        glViewport((GLsizei)gWidth/4,(GLsizei) gHeight*1/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

/// Yellow Plastic

        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;
    
        material_diffuse[0] = 0.5f;
        material_diffuse[1] = 0.5f;
        material_diffuse[2] = 0.0f;
        material_diffuse[3] = 1.0f;
    
        material_specular[0] = 0.60f;
        material_specular[1] = 0.60f;
        material_specular[2] = 0.50f;
        material_specular[3] = 1.0f;
    
        material_shinyness = 0.25f * 128.0f;
        
        glViewport((GLsizei)gWidth/4,0,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

//         		/// Black

material_ambient[0] = 0.0f;
material_ambient[1] = 0.0f;
material_ambient[2] = 0.0f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.01f;
material_diffuse[1] = 0.01f;
material_diffuse[2] = 0.01f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.50f;
material_specular[1] = 0.50f;
material_specular[2] = 0.50f;
material_specular[3] = 1.0f;

material_shinyness = 0.25f * 128.0f;

        glViewport((GLsizei)gWidth/2,(GLsizei) gHeight*5/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();



//         		/// Cyan

material_ambient[0] = 0.0f;
material_ambient[1] = 0.1f;
material_ambient[2] = 0.06f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.0f;
material_diffuse[1] = 0.50980392f;
material_diffuse[2] = 0.50980392f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.50196078f;
material_specular[1] = 0.50196078f;
material_specular[2] = 0.50196078f;
material_specular[3] = 1.0f;

material_shinyness = 0.25f * 128.0f;

		
        glViewport((GLsizei)gWidth/2,(GLsizei) gHeight*4/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

        // 		/// Green

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.0f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.1f;
		material_diffuse[1] = 0.35f;
		material_diffuse[2] = 0.1f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.45f;
		material_specular[1] = 0.55f;
		material_specular[2] = 0.45f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.25f * 128.0f;
    

        glViewport((GLsizei)gWidth/2,(GLsizei) gHeight*3/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

        material_ambient[0] = 0.0f;
		material_ambient[1] = 0.0f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.5f;
		material_diffuse[1] = 0.0f;
		material_diffuse[2] = 0.0f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.7f;
		material_specular[1] = 0.6f;
		material_specular[2] = 0.6f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.25f * 128.0f;



        glViewport((GLsizei)gWidth/2,(GLsizei) gHeight*2/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

//         // Fourth row 

//         		/// White

material_ambient[0] = 0.0f;
material_ambient[1] = 0.0f;
material_ambient[2] = 0.0f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.55f;
material_diffuse[1] = 0.55f;
material_diffuse[2] = 0.55f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.70f;
material_specular[1] = 0.70f;
material_specular[2] = 0.70f;
material_specular[3] = 1.0f;

material_shinyness = 0.25f * 128.0f;



        glViewport((GLsizei)gWidth/2,(GLsizei) gHeight*1/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

//          		/// Yellow Plastic

material_ambient[0] = 0.0f;
material_ambient[1] = 0.0f;
material_ambient[2] = 0.0f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.5f;
material_diffuse[1] = 0.5f;
material_diffuse[2] = 0.0f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.60f;
material_specular[1] = 0.60f;
material_specular[2] = 0.50f;
material_specular[3] = 1.0f;

material_shinyness = 0.25f * 128.0f;

        glViewport((GLsizei)gWidth/2,0,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();
        
//         		/// Black

material_ambient[0] = 0.02f;
material_ambient[1] = 0.02f;
material_ambient[2] = 0.02f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.01f;
material_diffuse[1] = 0.01f;
material_diffuse[2] = 0.01f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.4f;
material_specular[1] = 0.4f;
material_specular[2] = 0.4f;
material_specular[3] = 1.0f;

material_shinyness = 0.078125f * 128.0f;
    
        glViewport((GLsizei)gWidth/1.4,(GLsizei) gHeight*5/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

        //         		/// Cyan

		material_ambient[0] = 0.0f;
		material_ambient[1] = 0.05f;
		material_ambient[2] = 0.05f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.4f;
		material_diffuse[1] = 0.5f;
		material_diffuse[2] = 0.5f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.04f;
		material_specular[1] = 0.7f;
		material_specular[2] = 0.7f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.078125f * 128.0f;

        glViewport((GLsizei)gWidth/1.4,(GLsizei) gHeight*4/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

// 		/// Green

material_ambient[0] = 0.0f;
material_ambient[1] = 0.05f;
material_ambient[2] = 0.0f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.4f;
material_diffuse[1] = 0.5f;
material_diffuse[2] = 0.4f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.04f;
material_specular[1] = 0.7f;
material_specular[2] = 0.04f;
material_specular[3] = 1.0f;

material_shinyness = 0.078125f * 128.0f;


        glViewport((GLsizei)gWidth/1.4,(GLsizei) gHeight*3/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();


//          		/// Red

		material_ambient[0] = 0.05f;
		material_ambient[1] = 0.0f;
		material_ambient[2] = 0.0f;
		material_ambient[3] = 1.0f;

		material_diffuse[0] = 0.5f;
		material_diffuse[1] = 0.4f;
		material_diffuse[2] = 0.4f;
		material_diffuse[3] = 1.0f;

		material_specular[0] = 0.7f;
		material_specular[1] = 0.04f;
		material_specular[2] = 0.04f;
		material_specular[3] = 1.0f;

		material_shinyness = 0.078125f * 128.0f;

        glViewport((GLsizei)gWidth/1.4,(GLsizei) gHeight*2/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();

//         		/// White

material_ambient[0] = 0.05f;
material_ambient[1] = 0.05f;
material_ambient[2] = 0.05f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.5f;
material_diffuse[1] = 0.5f;
material_diffuse[2] = 0.5f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.7f;
material_specular[1] = 0.7f;
material_specular[2] = 0.7f;
material_specular[3] = 1.0f;

material_shinyness = 0.078125f * 128.0f;

        glViewport((GLsizei)gWidth/1.4,(GLsizei) gHeight*1/6,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();


//          		/// Yellow Rubber

material_ambient[0] = 0.05f;
material_ambient[1] = 0.05f;
material_ambient[2] = 0.0f;
material_ambient[3] = 1.0f;

material_diffuse[0] = 0.5f;
material_diffuse[1] = 0.5f;
material_diffuse[2] = 0.4f;
material_diffuse[3] = 1.0f;

material_specular[0] = 0.7f;
material_specular[1] = 0.7f;
material_specular[2] = 0.04f;
material_specular[3] = 1.0f;

material_shinyness = 0.078125f * 128.0f;

        glViewport((GLsizei)gWidth/1.4,0,(GLsizei)gWidth/4 ,(GLsizei) gHeight/6 );
        drawSphere();
    

    //vmath::mat4 rotationMatrix = vmath::mat4::identity(); 

	
	modelMatrix = vmath::translate(0.0f,0.0f,-2.0f);

	//rotationMatrix = vmath::rotate(angleCube,angleCube,angleCube);

	//modelViewMatrix = modelMatrix * rotationMatrix ;

	glUniformMatrix4fv(modelMatrixUniform,1,GL_FALSE, modelMatrix);

    glUniformMatrix4fv(viewMatrixUniform,1,GL_FALSE, viewMatrix);

	// multiply modelView and perspective matrix to get modelViewProjectionMatrix

	//modelViewProjectionMatrix =  perspectiveProjectionMatrix * modelViewMatrix;

	// pass above modelViewProjectionMatrix to vertex shader in 'u_mvp_matrix' shader variable whose position is already calculated

	glUniformMatrix4fv(projectionMatrixUniform,1,GL_FALSE,perspectiveProjectionMatrix);

	// bind vao_Sphere


	glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	[self updateAngle];
}

-(void) drawSphere
{
    
    glUniform3fv(kaUniform, 1, material_ambient);
    glUniform3fv(kdUniform, 1, material_diffuse);
    glUniform3fv(ksUniform, 1, material_specular);
    glUniform1f(materialShininessUniform, material_shinyness);

    
    glBindVertexArray(vao_Sphere);

	// Draw Sphere

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Elements);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT); 

	glBindVertexArray(0);
}


 -(void) updateAngle
 {
 	if (lightRotationAngle > 360.0f)
        lightRotationAngle = 360.0f - lightRotationAngle;

    lightRotationAngle = lightRotationAngle + 0.05f;
 }

-(BOOL) acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    
    return (YES);
}

-(void) keyDown:(NSEvent *) theEvent
{
    
    int key = (int) [[theEvent characters]characterAtIndex:0];

    switch(key)
    {
        case 27:     // Esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
            
		case 'L':
        case 'l':
                if(isLightEnabled == false)
                   {
                       isLightEnabled = true;
                   }
                   else
                   {
                       isLightEnabled = false;
                   }
             break;
                                     
        case 'X':
        case 'x':
                gbIs_X_AxisRotationOfLight = true;
                gbIs_Y_AxisRotationOfLight = false;
                gbIs_Z_AxisRotationOfLight = false;
            break;

        
        case 'Y':
        case 'y':
        
                gbIs_X_AxisRotationOfLight = false;
                gbIs_Y_AxisRotationOfLight = true;
                gbIs_Z_AxisRotationOfLight = false;
        
            break;

        
        case 'Z':
        case 'z':
        
                gbIs_X_AxisRotationOfLight = false;
                gbIs_Y_AxisRotationOfLight = false;
                gbIs_Z_AxisRotationOfLight = true;
            break;

        default:
            break;
    }
}

-(void) mouseDown:(NSEvent *) theEvent
{

}

-(void) mouseDragged:(NSEvent *)theEvent
{

}

-(void) rightMouseDown:(NSEvent *)theEvent
{

}

-(void) dealloc
{
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

	if(vao_Sphere)
	{
		glDeleteVertexArrays(1,&vao_Sphere);
		vao_Sphere =0;
	}

	if(vbo_Sphere_Position)
	{
		glDeleteBuffers(1,&vbo_Sphere_Position);
		vbo_Sphere_Position =0;
	}

	if(vbo_Sphere_Normal)
	{
		glDeleteBuffers(1,&vbo_Sphere_Normal);
		vbo_Sphere_Normal =0;
	}

	if(vbo_Sphere_Elements)
	{
		glDeleteBuffers(1,&vbo_Sphere_Elements);
		vbo_Sphere_Elements =0;
	}

	glDetachShader(shaderProgramObject,vertexShaderObject);

	glDetachShader(shaderProgramObject,fragmentShaderObject);

	glDeleteShader(vertexShaderObject);
	vertexShaderObject=0;

	glDeleteShader(fragmentShaderObject);
	fragmentShaderObject =0;

	glDeleteProgram(shaderProgramObject);
	shaderProgramObject =0;

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback (CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result =[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];

    return(result);
}


