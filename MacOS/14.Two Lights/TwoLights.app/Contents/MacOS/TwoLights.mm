
// OGLPP Two Lights

// headers

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>  // CV for core video

#import <OpenGL/gl3.h>  // for OpenGL Core profile - 3.0 and above
#import <OpenGL/gl3ext.h> //For extensions of OpenGL 3.0 and above

#import "vmath.h"

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};

static GLfloat anglePyramid =0.0f;

GLfloat light_ambient[] = {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};
GLfloat light_diffuse[] = {1.0f,0.0f,0.0f,0.0f,0.0f,1.0f};
GLfloat light_specular[] = {1.0f,0.0f,0.0f,0.0f,0.0f,1.0f};
GLfloat light_position[] = {-2.0f,1.0f,0.0f,2.0f,1.0f,0.0f};

// 'C' style global function declarations 

CVReturn MyDisplayLinkCallback (CVDisplayLinkRef , const CVTimeStamp *,const CVTimeStamp *
                                ,CVOptionFlags,CVOptionFlags *,void *);   // Void is for the object for which callback is done  

// global variables

FILE *gpFile = NULL;

// interface declaration

@interface AppDelegate : NSObject <NSApplicationDelegate ,NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function

int main(int argc, const char * argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];

    NSApp = [NSApplication sharedApplication];

    [NSApp setDelegate : [[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}


// interface implementation

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView; 
}

-(void) applicationDidFinishLaunching : (NSNotification *) aNotification 
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath ];
    const char * pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
    gpFile = fopen(pszLogFileNameWithPath,"w");

    if(gpFile == NULL)
    {
        printf("Cannot create Log file.\n Exiting......\n ");
        [self release];
        [NSApp terminate : self];
    }

    fprintf( gpFile,"Program is started successfully \n");

    // window 
    NSRect win_rect;
    win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

    // create window

    window = [[NSWindow alloc] initWithContentRect:win_rect
              styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
              backing:NSBackingStoreBuffered
              defer:NO ];

    [window setTitle:@"macOS  OpenGL Two Lights"];
    [window center] ;                         

    glView = [[GLView alloc]initWithFrame : win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];

}

-(void) applicationWillTerminate :(NSNotification *) notification 
{
   fprintf(gpFile,"Program is terminated successfully \n");

   if(gpFile)
   {
       fclose(gpFile);
       gpFile = NULL;
   }
}

-(void) windowWillClose:(NSNotification *) notification
{
    [NSApp terminate : self];
}

-(void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}

@end

@implementation GLView
{
    @private 
        CVDisplayLinkRef displayLink;

		GLuint vertexShaderObject;
		GLuint fragmentShaderObject;
		GLuint shaderProgramObject;

		GLuint vao_Pyramid;
		
		GLuint vbo_Pyramid_Position;
		GLuint vbo_Pyramid_Normal;

	    GLuint ModelMatrixUniform,ViewMatrixUniform,ProjectionMatrixUniform;
		GLuint LaUniform,LdUniform, LsUniform,LightPositionUniform;
		GLuint KaUniform,KdUniform, KsUniform, MaterialShininessUniform;

		

		vmath::mat4 perspectiveProjectionMatrix;


}

-(id) initWithFrame : (NSRect) frame
{
    self = [super initWithFrame : frame];

    if(self)
    {
        [[self window] setContentView : self];

        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // Must specify the 4.1 core profile to use OpenGL 4.1

            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core, //version 4.1

            // specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery, // If hardware renderer is not found then give error .Do not give software renderer
            NSOpenGLPFAAccelerated, // For hardware acceleration
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0    // Last zero is must .It indicates end of an array  
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
        
        if(pixelFormat == nil)
        {
            fprintf(gpFile,"No valid OpenGL Pixel Format is available.Exiting...../n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext =[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // shareContext: nil means do not share context.
        [self setPixelFormat : pixelFormat];
        [self setOpenGLContext:glContext]; // It automatically releases the older context , if present and sets the newer one.
    }

    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *) pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];

    return (kCVReturnSuccess);
}


-(void) prepareOpenGL  // overrided method. called automatically.Analogous to initialise() of windows
{
    // OpenGL Info
    fprintf(gpFile,"OpenGL Version : %s \n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext]; 
     
    GLint swapInt = 1;

    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval]; // CP for Context Parameter 


	// Vertex Shader 

	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode=
			"#version 410 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;" \
			"uniform mat4 u_model_matrix ;" \
			"uniform mat4 u_view_matrix ;" \
			"uniform mat4 u_projection_matrix ;" \
			"uniform vec3 u_La[2] ;" \
			"uniform vec3 u_Ld[2] ;" \
			"uniform vec3 u_Ls[2] ;" \
			"uniform vec4 u_light_position[2] ;" \
			"uniform vec3 u_Ka ;" \
			"uniform vec3 u_Kd ;" \
			"uniform vec3 u_Ks ;" \
			"uniform float u_material_shininess ;" \
			"out vec3 phong_ads_color;" \
			"void main(void)" \
			"{" \
			/*"phong_ads_color= vec3(1.0,1.0,1.0);"\*/
			"vec4 eye_coordinates = u_view_matrix *u_model_matrix* vPosition;"\
			"vec3 transformed_normals = normalize(mat3 (u_view_matrix * u_model_matrix) * vNormal );"\
			"vec3 light_direction;"\
			"float tn_dot_ld;"\
			"vec3 ambient[2];"\
			"vec3 diffuse[2];"\
			"vec3 reflection_vector;"\
			"vec3 viewer_vector;"\
			"vec3 specular[2];"\
			"for(int i=0;i<2;i++)"\
			"{"\
			"light_direction = normalize (vec3 (u_light_position[i]) - eye_coordinates.xyz);"\
			"tn_dot_ld = max(dot (transformed_normals ,light_direction), 0.0);"\
			"ambient[i] = u_La[i] * u_Ka;"\
			"diffuse[i] = u_Ld[i] * u_Kd * tn_dot_ld ;"\
			"reflection_vector = reflect(-light_direction,transformed_normals);"\
			"viewer_vector = normalize(-eye_coordinates.xyz);"\
			"specular[i] = u_Ls[i] * u_Ks * pow (max(dot (reflection_vector,viewer_vector),0.0),u_material_shininess);"\
			"}"\
			/*"for(int j=0;j<2;j++)"\
			"{"\*/
			"phong_ads_color = ambient[0] + ambient[1] + diffuse[0] + diffuse[1] + specular[0] + specular[1] ;"\
			/*"}"\*/
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;" \
			"}";

	glShaderSource(vertexShaderObject,1,(const GLchar **) &vertexShaderSourceCode,NULL);

	glCompileShader(vertexShaderObject);
	
	GLint iInfoLogLength =0;
	GLint iShaderCompiledStatus =0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	// Fragment Shader

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
			"#version 410"\
			"\n" \
			"in vec3 phong_ads_color;" \
			"out vec4 FragColor;" \
			"void main(void)" \
			"{" \
			"FragColor = vec4(phong_ads_color,1.0) ;" \
			"}";
			

	glShaderSource(fragmentShaderObject,1, (const GLchar **)&fragmentShaderSourceCode,NULL);

	glCompileShader(fragmentShaderObject);

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	// Shader Program

	shaderProgramObject = glCreateProgram();

	// Attach Shaders 

	glAttachShader(shaderProgramObject,vertexShaderObject);

	glAttachShader(shaderProgramObject,fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute

	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");

	// link Shader

	glLinkProgram(shaderProgramObject);

	GLint iShaderProgramLinkStatus=0;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if(iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);

			if(szInfoLog !=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Shader Program Link Log:%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	// get uniform location 

			ModelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
    
            ViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
    
            ProjectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
    
            LaUniform = glGetUniformLocation(shaderProgramObject, "u_La");
    										 
            LdUniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
    										 
            LsUniform = glGetUniformLocation(shaderProgramObject, "u_Ls");
    
            LightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
    
            KaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
    
            KdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
    
            KsUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
    
            MaterialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");


	const GLfloat pyramidVertices []=
    {
		// front face

			0.0f, 1.0f, 0.0f,   // Apex

			-1.0f, -1.0f, 1.0f, // left-corner of front face

			 1.0f, -1.0f, 1.0f,   //right corner of front face

			//right face

			0.0f, 1.0f, 0.0f,     // apex

			1.0f, -1.0f, 1.0f,    //left corner of the right face

			1.0f, -1.0f, -1.0f,  // right corner of the right face

			//Back face

			0.0f, 1.0f, 0.0f, // apex

			1.0f, -1.0f, -1.0f, //left corner of the back face

			-1.0f, -1.0f, -1.0f, //right corner of the back face

			//left face

			0.0f, 1.0f, 0.0f, // apex

			-1.0f, -1.0f, -1.0f, //left corner of the back face

			-1.0f, -1.0f, 1.0f, //right corner of the back face
    };

    // Pyramid Vao

    glGenVertexArrays(1,&vao_Pyramid);
    glBindVertexArray(vao_Pyramid);

    // Pyramid Position Vbo 

    glGenBuffers(1,& vbo_Pyramid_Position);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Pyramid_Position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidVertices),pyramidVertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind Pyramid position Vbo
      
    const GLfloat pyramidNormals []=
    {
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f
    };

	// Pyramid Normal Vbo 

    glGenBuffers(1,& vbo_Pyramid_Normal);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Pyramid_Normal);
    glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidNormals),pyramidNormals,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind square position Vbo
    glBindVertexArray(0); // Unbind Square vao

    glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	//glEnable(GL_CULL_FACE);

    // set background color
    glClearColor(0.0f,0.0f,0.0f,0.0f);  // black

	// set projection matrix to identity matrix

	perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink); // CGDisplays - core graphics display
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self); // self is the 6th parameter of the callback
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj]; 
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink); // This will start the thread.
}

-(void) reshape  // This is overriden method . called automatically . Analogous to WM_SIZE
{
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]); 
    NSRect rect = [self bounds];

    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
    {
        height = 1;
    }

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void) drawView
{
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);
	
	glUniform3fv(LaUniform,2,light_ambient);
    glUniform3fv(LdUniform, 2,light_diffuse);
    glUniform3fv(LsUniform, 2, light_specular);
    glUniform4fv(LightPositionUniform, 2,light_position);

	// OGL drawing
	// set modelView & model view projection matrices to identity

	vmath::mat4 modelMatrix = vmath::mat4::identity(); 

	vmath::mat4 viewMatrix = vmath::mat4::identity(); 

	vmath::mat4 rotationMatrix = vmath::mat4::identity(); 

	
	modelMatrix = vmath::translate(0.0f,0.0f,-6.0f);

	rotationMatrix = vmath::rotate(anglePyramid,0.0f, 1.0f, 0.0f);

	modelMatrix = modelMatrix * rotationMatrix ;

	glUniformMatrix4fv(ModelMatrixUniform,1,GL_FALSE,modelMatrix);

	glUniformMatrix4fv(ViewMatrixUniform, 1, GL_FALSE, viewMatrix);

	// multiply modelView and perspective matrix to get modelViewProjectionMatrix

	//modelViewProjectionMatrix =  perspectiveProjectionMatrix * modelViewMatrix;

	// pass above modelViewProjectionMatrix to vertex shader in 'u_mvp_matrix' shader variable whose position is already calculated

	glUniformMatrix4fv(ProjectionMatrixUniform,1,GL_FALSE,perspectiveProjectionMatrix);

	// bind vao_Pyramid

	glBindVertexArray(vao_Pyramid);

	// Draw Pyramid

	glDrawArrays(GL_TRIANGLES, 0, 12);

	glBindVertexArray(0);

	glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	[self updateAngle];
}


-(void) updateAngle
{
   anglePyramid = anglePyramid - 0.7f;
	if (anglePyramid <= 0.0f)
		anglePyramid = 360.0f;
}

-(BOOL) acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    
    return (YES);
}

-(void) keyDown:(NSEvent *) theEvent
{
    
    int key = (int) [[theEvent characters]characterAtIndex:0];

    switch(key)
    {
        case 27:     // Esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;

        default:
            break;
    }
}

-(void) mouseDown:(NSEvent *) theEvent
{

}

-(void) mouseDragged:(NSEvent *)theEvent
{

}

-(void) rightMouseDown:(NSEvent *)theEvent
{

}

-(void) dealloc
{
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

	if(vao_Pyramid)
	{
		glDeleteVertexArrays(1,&vao_Pyramid);
		vao_Pyramid =0;
	}

	if(vbo_Pyramid_Position)
	{
		glDeleteBuffers(1,&vbo_Pyramid_Position);
		vbo_Pyramid_Position =0;
	}

	if(vbo_Pyramid_Normal)
	{
		glDeleteBuffers(1,&vbo_Pyramid_Normal);
		vbo_Pyramid_Normal =0;
	}

	glDetachShader(shaderProgramObject,vertexShaderObject);

	glDetachShader(shaderProgramObject,fragmentShaderObject);

	glDeleteShader(vertexShaderObject);
	vertexShaderObject=0;

	glDeleteShader(fragmentShaderObject);
	fragmentShaderObject =0;

	glDeleteProgram(shaderProgramObject);
	shaderProgramObject =0;

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback (CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result =[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];

    return(result);
}


