
// OGLPP 3D Rotation

// headers

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>  // CV for core video

#import <OpenGL/gl3.h>  // for OpenGL Core profile - 3.0 and above
#import <OpenGL/gl3ext.h> //For extensions of OpenGL 3.0 and above

#import "vmath.h"

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};

static GLfloat anglePyramid =0.0f;
static GLfloat angleCube =0.0f;

// 'C' style global function declarations 

CVReturn MyDisplayLinkCallback (CVDisplayLinkRef , const CVTimeStamp *,const CVTimeStamp *
                                ,CVOptionFlags,CVOptionFlags *,void *);   // Void is for the object for which callback is done  

// global variables

FILE *gpFile = NULL;

// interface declaration

@interface AppDelegate : NSObject <NSApplicationDelegate ,NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function

int main(int argc, const char * argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];

    NSApp = [NSApplication sharedApplication];

    [NSApp setDelegate : [[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}


// interface implementation

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView; 
}

-(void) applicationDidFinishLaunching : (NSNotification *) aNotification 
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath ];
    const char * pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
    gpFile = fopen(pszLogFileNameWithPath,"w");

    if(gpFile == NULL)
    {
        printf("Cannot create Log file.\n Exiting......\n ");
        [self release];
        [NSApp terminate : self];
    }

    fprintf( gpFile,"Program is started successfully \n");

    // window 
    NSRect win_rect;
    win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

    // create window

    window = [[NSWindow alloc] initWithContentRect:win_rect
              styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
              backing:NSBackingStoreBuffered
              defer:NO ];

    [window setTitle:@"macOS  OpenGL 2D Rotation"];
    [window center] ;                         

    glView = [[GLView alloc]initWithFrame : win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];

}

-(void) applicationWillTerminate :(NSNotification *) notification 
{
   fprintf(gpFile,"Program is terminated successfully \n");

   if(gpFile)
   {
       fclose(gpFile);
       gpFile = NULL;
   }
}

-(void) windowWillClose:(NSNotification *) notification
{
    [NSApp terminate : self];
}

-(void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}

@end

@implementation GLView
{
    @private 
        CVDisplayLinkRef displayLink;

		GLuint vertexShaderObject;
		GLuint fragmentShaderObject;
		GLuint shaderProgramObject;
        
		GLuint vao_Pyramid;
		GLuint vao_Cube;
		
		GLuint vbo_Pyramid_Position;
		GLuint vbo_Pyramid_Color;
		
		GLuint vbo_Cube_Position;
		GLuint vbo_Cube_Color;

		GLuint mvpUniform;
		

		vmath::mat4 perspectiveProjectionMatrix;

}

-(id) initWithFrame : (NSRect) frame
{
    self = [super initWithFrame : frame];

    if(self)
    {
        [[self window] setContentView : self];

        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // Must specify the 4.1 core profile to use OpenGL 4.1

            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core, //version 4.1

            // specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery, // If hardware renderer is not found then give error .Do not give software renderer
            NSOpenGLPFAAccelerated, // For hardware acceleration
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0    // Last zero is must .It indicates end of an array  
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
        
        if(pixelFormat == nil)
        {
            fprintf(gpFile,"No valid OpenGL Pixel Format is available.Exiting...../n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext =[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // shareContext: nil means do not share context.
        [self setPixelFormat : pixelFormat];
        [self setOpenGLContext:glContext]; // It automatically releases the older context , if present and sets the newer one.
    }

    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *) pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];

    return (kCVReturnSuccess);
}


-(void) prepareOpenGL  // overrided method. called automatically.Analogous to initialise() of windows
{
    // OpenGL Info
    fprintf(gpFile,"OpenGL Version : %s \n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext]; 
     
    GLint swapInt = 1;

    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval]; // CP for Context Parameter 


	// Vertex Shader 

	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode=
	"#version 410"\
	"\n"\
	"in vec4 vPosition;"\
	"in vec4 vColor;" \
	"out vec4 outColor;" \
	"uniform mat4 u_mvp_matrix;"\
	"void main(void)"\
	"{"\
	"gl_Position = u_mvp_matrix * vPosition ; "\
	"outColor = vColor;" \
	"}";

	glShaderSource(vertexShaderObject,1,(const GLchar **) &vertexShaderSourceCode,NULL);

	glCompileShader(vertexShaderObject);
	
	GLint iInfoLogLength =0;
	GLint iShaderCompiledStatus =0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	// Fragment Shader

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
	"#version 410"\
	"\n"\
	"in vec4 outColor;" \
	"out vec4 FragColor;"\
	"void main(void)"\
	"{"\
	"FragColor = outColor;"\
	"}";

	glShaderSource(fragmentShaderObject,1, (const GLchar **)&fragmentShaderSourceCode,NULL);

	glCompileShader(fragmentShaderObject);

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	// Shader Program

	shaderProgramObject = glCreateProgram();

	// Attach Shaders 

	glAttachShader(shaderProgramObject,vertexShaderObject);

	glAttachShader(shaderProgramObject,fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute

	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_COLOR,"vColor");

	// link Shader

	glLinkProgram(shaderProgramObject);

	GLint iShaderProgramLinkStatus=0;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if(iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);

			if(szInfoLog !=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Shader Program Link Log:%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	// get MVP uniform location 

	mvpUniform = glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

	// Pyramid vertices

	const GLfloat pyramidVertices[]=
	{
		// front face

	0.0f, 1.0f, 0.0f,   // Apex

	-1.0f, -1.0f, 1.0f, // left-corner of front face

	 1.0f, -1.0f, 1.0f,   //right corner of front face

		//right face

	0.0f, 1.0f, 0.0f,     // apex

	1.0f, -1.0f, 1.0f,    //left corner of the right face

	1.0f, -1.0f, -1.0f,  // right corner of the right face

		//Back face

	0.0f, 1.0f, 0.0f, // apex

	1.0f, -1.0f, -1.0f, //left corner of the back face

	-1.0f, -1.0f, -1.0f, //right corner of the back face

		//left face

	0.0f, 1.0f, 0.0f, // apex

	-1.0f, -1.0f, -1.0f, //left corner of the back face

	-1.0f, -1.0f, 1.0f, //right corner of the back face
	};

	glGenVertexArrays(1,&vao_Pyramid);
	glBindVertexArray(vao_Pyramid);

	glGenBuffers(1,&vbo_Pyramid_Position);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_Pyramid_Position);
	glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidVertices),pyramidVertices,GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER,0);

	
	const GLfloat pyramidColorCoordinates []=
    {
        // front face
	
	1.0f, 0.0f, 0.0f, // red
	
		// Apex

	0.0f, 1.0f, 0.0f,    // green
	
		// left-corner of front face

	0.0f, 0.0f, 1.0f,    // blue
	
		//right corner of front face

        //right face

	1.0f, 0.0f, 0.0f, // red
        // apex

	0.0f, 0.0f, 1.0f,  // blue

		//left corner of the right face

	0.0f, 1.0f, 0.0f,  //green
	
		// right corner of the right face

		//Back face

	1.0f, 0.0f, 0.0f, //red
	
		// apex

	0.0f, 1.0f, 0.0f,// green
	
		//left corner of the back face

	0.0f, 0.0f, 1.0f, // blue
	
		//right corner of the back face

		//left face
	 
	1.0f, 0.0f, 0.0f,//red
	   
		// apex

	0.0f, 0.0f, 1.0f,  // blue
   
	//left corner of the back face

	0.0f, 1.0f, 0.0f  // green
    };

	glGenBuffers(1,&vbo_Pyramid_Color);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_Pyramid_Color);
	glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidColorCoordinates),pyramidColorCoordinates,GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER,0);

	glBindVertexArray(0);

	// Square

	const GLfloat cubeVertices []=
    {
        // Top face

	1.0f, 1.0f, -1.0f,   // Right-top corner of top face 
	-1.0f, 1.0f, -1.0f, // left-top corner of the top face
	-1.0f, 1.0f, 1.0f,   //left bottom corner of front face
	1.0f, 1.0f, 1.0f, //right bottom corner of front face

        //Bottom face

	1.0f, -1.0f, -1.0f,   // Right-top corner of bottom face 
	-1.0f, -1.0f, -1.0f, // left-top corner of the bottom face
	-1.0f, -1.0f, 1.0f,   //left bottom corner of bottom face
	1.0f, -1.0f, 1.0f, //right bottom corner of bottom face

		//Front face

	1.0f, 1.0f, 1.0f,   // Right-top corner of front face 
	-1.0f, 1.0f, 1.0f, // left-top corner of the front face
	-1.0f, -1.0f, 1.0f,   //left bottom corner of front face
	1.0f, -1.0f, 1.0f, //right bottom corner of front face


		//Back face
	
	1.0f, 1.0f, -1.0f,  // Right-top corner of back face 
	-1.0f, 1.0f, -1.0f, // left-top corner of the back face
	-1.0f, -1.0f, -1.0f,   //left bottom corner of back face
	1.0f, -1.0f, -1.0f, //right bottom corner of back face

		//Right face
	
	1.0f, 1.0f, -1.0f,   // Right-top corner of right face 
	1.0f, 1.0f, 1.0f, // left-top corner of the right face
	1.0f, -1.0f, 1.0f,   //left bottom corner of right face
	1.0f, -1.0f, -1.0f, //right bottom corner of right face

		//Left face

	-1.0f, 1.0f, 1.0f,   // Right-top corner of left face 
	-1.0f, 1.0f, -1.0f, // left-top corner of the left face
	-1.0f, -1.0f, -1.0f,   //left bottom corner of left face
	-1.0f, -1.0f, 1.0f //right bottom corner of left face

    };

    // Square Vao

    glGenVertexArrays(1,&vao_Cube);
    glBindVertexArray(vao_Cube);

    // Square Position Vbo 

    glGenBuffers(1,& vbo_Cube_Position);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Cube_Position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(cubeVertices),cubeVertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind square position Vbo
      
    const GLfloat cubeColorCoordinates []=
    {
		// Top face
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red

	     //Bottom face
	0.0f, 1.0f, 0.0f, // Green
	0.0f, 1.0f, 0.0f, // Green
	0.0f, 1.0f, 0.0f, // Green
	0.0f, 1.0f, 0.0f, // Green

		//Front face
	0.0f, 0.0f, 1.0f, // Blue
	0.0f, 0.0f, 1.0f, // Blue
	0.0f, 0.0f, 1.0f, // Blue
	0.0f, 0.0f, 1.0f, // Blue

		//Back face
	0.0f, 1.0f, 1.0f, // Cyan
	0.0f, 1.0f, 1.0f, // Cyan
	0.0f, 1.0f, 1.0f, // Cyan
	0.0f, 1.0f, 1.0f, // Cyan

		//Right face
	1.0f, 0.0f, 1.0f, // Magenta
	1.0f, 0.0f, 1.0f, // Magenta
	1.0f, 0.0f, 1.0f, // Magenta
	1.0f, 0.0f, 1.0f, // Magenta

		//Left face
	1.0f, 1.0f, 0.0f, // Yellow
	1.0f, 1.0f, 0.0f,// Yellow
	1.0f, 1.0f, 0.0f, // Yellow
	1.0f, 1.0f, 0.0f // Yellow        
    };

	// Square Color Vbo 

    glGenBuffers(1,& vbo_Cube_Color);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Cube_Color);
    glBufferData(GL_ARRAY_BUFFER,sizeof(cubeColorCoordinates),cubeColorCoordinates,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind square position Vbo
    glBindVertexArray(0); // Unbind Square vao

    glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	//glEnable(GL_CULL_FACE);

    // set background color
    glClearColor(0.0f,0.0f,0.0f,0.0f);  // blue

	// set projection matrix to identity matrix

	perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink); // CGDisplays - core graphics display
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self); // self is the 6th parameter of the callback
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj]; 
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink); // This will start the thread.
}

-(void) reshape  // This is overriden method . called automatically . Analogous to WM_SIZE
{
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]); 
    NSRect rect = [self bounds];

    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
    {
        height = 1;
    }

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void) drawView
{
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	// OGL drawing
	// set modelView & model view projection matrices to identity

	vmath::mat4 modelViewMatrix = vmath::mat4::identity(); 

	vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity(); 

	vmath::mat4 rotationMatrix = vmath::mat4::identity(); 

	
	modelViewMatrix = vmath::translate(-1.5f,0.0f,-6.0f);

	rotationMatrix = vmath::rotate(anglePyramid,0.0f,1.0f,0.0f);

	modelViewMatrix = modelViewMatrix * rotationMatrix ;

	// multiply modelView and perspective matrix to get modelViewProjectionMatrix

	modelViewProjectionMatrix =  perspectiveProjectionMatrix * modelViewMatrix;

	// pass above modelViewProjectionMatrix to vertex shader in 'u_mvp_matrix' shader variable whose position is already calculated

	glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

	// bind vao_Pyramid

	glBindVertexArray(vao_Pyramid);

	glDrawArrays(GL_TRIANGLES,0,12);

	glBindVertexArray(0);

	
	modelViewMatrix = vmath::mat4::identity();

	modelViewProjectionMatrix = vmath::mat4::identity();

    rotationMatrix = vmath::mat4::identity();

    // Translate model view matrix.

    modelViewMatrix = vmath::translate(1.5f,0.0f,-6.0f);

	rotationMatrix = vmath::rotate(angleCube,angleCube,angleCube);

	modelViewMatrix = modelViewMatrix * rotationMatrix ;

	// multiply modelView and perspective matrix to get modelViewProjectionMatrix

	modelViewProjectionMatrix =  perspectiveProjectionMatrix * modelViewMatrix;

	// pass above modelViewProjectionMatrix to vertex shader in 'u_mvp_matrix' shader variable whose position is already calculated

	glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);


    // *** Bind Cube Vao ***//

    glBindVertexArray(vao_Cube);
    
	// Draw Cube

    glDrawArrays(GL_TRIANGLE_FAN,0,4); 
	glDrawArrays(GL_TRIANGLE_FAN,4,4); 
	glDrawArrays(GL_TRIANGLE_FAN,8,4); 
	glDrawArrays(GL_TRIANGLE_FAN,12,4); 
	glDrawArrays(GL_TRIANGLE_FAN,16,4); 
	glDrawArrays(GL_TRIANGLE_FAN,20,4); 
    
        // unbind vao
    
    glBindVertexArray(0);


	glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	[self updateAngle];
}


-(void) updateAngle
{
	anglePyramid = anglePyramid + 0.7f;
	angleCube = angleCube - 0.7f;
	if (anglePyramid >= 360.0f)
		anglePyramid = 0.0f;
	if (angleCube <= 0.0f)
		angleCube = 360.0f;
}

-(BOOL) acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    
    return (YES);
}

-(void) keyDown:(NSEvent *) theEvent
{
    int key = (int) [[theEvent characters]characterAtIndex:0];

    switch(key)
    {
        case 27:     // Esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;

        default:
            break;
    }
}

-(void) mouseDown:(NSEvent *) theEvent
{

}

-(void) mouseDragged:(NSEvent *)theEvent
{

}

-(void) rightMouseDown:(NSEvent *)theEvent
{

}

-(void) dealloc
{
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

	if(vao_Pyramid)
	{
		glDeleteVertexArrays(1,&vao_Pyramid);
		vao_Pyramid =0;
	}

	if(vbo_Pyramid_Position)
	{
		glDeleteBuffers(1,&vbo_Pyramid_Position);
		vbo_Pyramid_Position =0;
	}

	if(vbo_Pyramid_Color)
	{
		glDeleteBuffers(1,&vbo_Pyramid_Color);
		vbo_Pyramid_Color =0;
	}

	if(vao_Cube)
	{
		glDeleteVertexArrays(1,&vao_Cube);
		vao_Cube =0;
	}

	if(vbo_Cube_Position)
	{
		glDeleteBuffers(1,&vbo_Cube_Position);
		vbo_Cube_Position =0;
	}

	if(vbo_Cube_Color)
	{
		glDeleteBuffers(1,&vbo_Cube_Color);
		vbo_Cube_Color =0;
	}

	glDetachShader(shaderProgramObject,vertexShaderObject);

	glDetachShader(shaderProgramObject,fragmentShaderObject);

	glDeleteShader(vertexShaderObject);
	vertexShaderObject=0;

	glDeleteShader(fragmentShaderObject);
	fragmentShaderObject =0;

	glDeleteProgram(shaderProgramObject);
	shaderProgramObject =0;

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback (CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result =[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];

    return(result);
}


