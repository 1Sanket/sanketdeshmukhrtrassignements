
// OGLPP Checkerboard

// headers

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>  // CV for core video

#import <OpenGL/gl3.h>  // for OpenGL Core profile - 3.0 and above
#import <OpenGL/gl3ext.h> //For extensions of OpenGL 3.0 and above

#import "vmath.h"

enum
{
	VDG_ATTRIBUTE_VERTEX =0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};

// 'C' style global function declarations 

CVReturn MyDisplayLinkCallback (CVDisplayLinkRef , const CVTimeStamp *,const CVTimeStamp *
                                ,CVOptionFlags,CVOptionFlags *,void *);   // Void is for the object for which callback is done  

// global variables

FILE *gpFile = NULL;

// interface declaration

@interface AppDelegate : NSObject <NSApplicationDelegate ,NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function

int main(int argc, const char * argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];

    NSApp = [NSApplication sharedApplication];

    [NSApp setDelegate : [[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}


// interface implementation

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        GLView *glView; 
}

-(void) applicationDidFinishLaunching : (NSNotification *) aNotification 
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath ];
    const char * pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
    gpFile = fopen(pszLogFileNameWithPath,"w");

    if(gpFile == NULL)
    {
        printf("Cannot create Log file.\n Exiting......\n ");
        [self release];
        [NSApp terminate : self];
    }

    fprintf( gpFile,"Program is started successfully \n");

    // window 
    NSRect win_rect;
    win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

    // create window

    window = [[NSWindow alloc] initWithContentRect:win_rect
              styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
              backing:NSBackingStoreBuffered
              defer:NO ];

    [window setTitle:@"macOS  OpenGL Checkerboard"];
    [window center] ;                         

    glView = [[GLView alloc]initWithFrame : win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];

}

-(void) applicationWillTerminate :(NSNotification *) notification 
{
   fprintf(gpFile,"Program is terminated successfully \n");

   if(gpFile)
   {
       fclose(gpFile);
       gpFile = NULL;
   }
}

-(void) windowWillClose:(NSNotification *) notification
{
    [NSApp terminate : self];
}

-(void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}

@end

@implementation GLView
{
    @private 
        CVDisplayLinkRef displayLink;

		GLuint vertexShaderObject;
		GLuint fragmentShaderObject;
		GLuint shaderProgramObject;
        
		GLuint Vao_Checkerboard;
		GLuint Vbo_Checkerboard_Position;
		GLuint Vbo_Checkerboard_Texture;

		GLuint checkerboard_Texture;		
		GLuint mvpUniform;
		GLuint texture_sampler_uniform;

		GLubyte checkImage[64][64][4];

		vmath::mat4 perspectiveProjectionMatrix;

}

-(id) initWithFrame : (NSRect) frame
{
    self = [super initWithFrame : frame];

    if(self)
    {
        [[self window] setContentView : self];

        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // Must specify the 4.1 core profile to use OpenGL 4.1

            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core, //version 4.1

            // specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery, // If hardware renderer is not found then give error .Do not give software renderer
            NSOpenGLPFAAccelerated, // For hardware acceleration
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0    // Last zero is must .It indicates end of an array  
        };

        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
        
        if(pixelFormat == nil)
        {
            fprintf(gpFile,"No valid OpenGL Pixel Format is available.Exiting...../n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext =[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // shareContext: nil means do not share context.
        [self setPixelFormat : pixelFormat];
        [self setOpenGLContext:glContext]; // It automatically releases the older context , if present and sets the newer one.
    }

    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *) pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];

    return (kCVReturnSuccess);
}


-(void) prepareOpenGL  // overrided method. called automatically.Analogous to initialise() of windows
{
    // OpenGL Info
    fprintf(gpFile,"OpenGL Version : %s \n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext]; 
     
    GLint swapInt = 1;

    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval]; // CP for Context Parameter 


	// Vertex Shader 

	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode=
	"#version 410"\
	"\n"\
	"in vec4 vPosition;"\
	"in vec2 vTexture0_Coord;" \
	"out vec2 out_texture0_coord ;" \
	"uniform mat4 u_mvp_matrix;"\
	"void main(void)"\
	"{"\
	"gl_Position = u_mvp_matrix * vPosition ; "\
	"out_texture0_coord = vTexture0_Coord ; " \
	"}";

	glShaderSource(vertexShaderObject,1,(const GLchar **) &vertexShaderSourceCode,NULL);

	glCompileShader(vertexShaderObject);
	
	GLint iInfoLogLength =0;
	GLint iShaderCompiledStatus =0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	// Fragment Shader

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
	"#version 410"\
	"\n"\
	"in vec2 out_texture0_coord;" \
	"out vec4 FragColor;"\
	"uniform sampler2D u_texture0_sampler;" \
	"void main(void)"\
	"{"\
	"vec3 tex = vec3(texture(u_texture0_sampler,out_texture0_coord));"\
	"FragColor = vec4(tex,1.0f);"\
	"}";

	glShaderSource(fragmentShaderObject,1, (const GLchar **)&fragmentShaderSourceCode,NULL);

	glCompileShader(fragmentShaderObject);

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	// Shader Program

	shaderProgramObject = glCreateProgram();

	// Attach Shaders 

	glAttachShader (shaderProgramObject,vertexShaderObject);

	glAttachShader (shaderProgramObject,fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute

	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

	// link Shader

	glLinkProgram(shaderProgramObject);

	GLint iShaderProgramLinkStatus=0;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if(iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);

			if(szInfoLog !=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Shader Program Link Log:%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	// get MVP uniform location 

	mvpUniform = glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

	texture_sampler_uniform = glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");

	
	// load texture
	[self loadTexture];
	
	// Square

	/*const GLfloat squareVertices []=
    {
		 1.0f, 1.0f, 1.0f,

    
		 -1.0f, 1.0f, 1.0f,

    
		-1.0f, -1.0f, 1.0f,

    
		 1.0f, -1.0f, 1.0f
    };*/


    // Square Vao

    glGenVertexArrays(1,&Vao_Checkerboard);
    glBindVertexArray(Vao_Checkerboard);

    // Square Position Vbo 

    glGenBuffers(1,& Vbo_Checkerboard_Position);
    glBindBuffer(GL_ARRAY_BUFFER,Vbo_Checkerboard_Position);
    glBufferData(GL_ARRAY_BUFFER,4 * 2 * sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind square position Vbo
      
    
	/*const GLfloat SquareTexcoords[]=
    {
        1.0f, 0.0f,
    
        0.0f, 0.0f,
    
        0.0f, 1.0f,
    
        1.0f, 1.0f
    }; */

	// Square Texture Vbo 

    glGenBuffers(1,& Vbo_Checkerboard_Texture);
    glBindBuffer(GL_ARRAY_BUFFER,Vbo_Checkerboard_Texture);
    glBufferData(GL_ARRAY_BUFFER,4 * 2 * sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0,2,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind square position Vbo
    glBindVertexArray(0); // Unbind Square vao

    glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glEnable(GL_CULL_FACE);
	
	glEnable(GL_TEXTURE_2D);

    // set background color
    glClearColor(0.0f,0.0f,0.0f,0.0f);  // blue

	// set projection matrix to identity matrix

	perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink); // CGDisplays - core graphics display
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self); // self is the 6th parameter of the callback
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj]; 
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink); // This will start the thread.
}

-(void) reshape  // This is overriden method . called automatically . Analogous to WM_SIZE
{
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]); 
    NSRect rect = [self bounds];

    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
    {
        height = 1;
    }

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

}


-(void) loadTexture
{
	int i,j,c;

  for (i=0;i<64;i++)
  {
      for (j=0;j<64;j++)
      {
        c = (((i & 0x8)==0) ^ ((j & 0x8)==0)) *255;
        checkImage[i][j][0] = (GLubyte)c;
        checkImage[i][j][1] = (GLubyte)c;
        checkImage[i][j][2] = (GLubyte)c;
        checkImage[i][j][3] = (GLubyte)255;
      }
  }

  glGenTextures(1,&checkerboard_Texture);
  glPixelStorei(GL_UNPACK_ALIGNMENT,1);
  glBindTexture(GL_TEXTURE_2D,checkerboard_Texture);

  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

  glTexImage2D(GL_TEXTURE_2D,
    0,
    GL_RGBA,
    64,
    64,
    0,
    GL_RGBA,
    GL_UNSIGNED_BYTE,
    checkImage);

    glGenerateMipmap(GL_TEXTURE_2D);
}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}


-(void) drawView
{

    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	GLfloat squareVertices[] =
    {

            0.0f, 1.0f, 0.0f,
    
            -2.0f, 1.0f, 0.0f,
    
            -2.0f, -1.0f, 0.0f,
    
            0.0f, -1.0f, 0.0f,
    
    
            2.414210f, 1.0f, -1.41421f,
    
            1.0f, 1.0f, 0.0f,
    
            1.0f, -1.0f, 0.0f,
    
            2.41421f, -1.0f, -1.41421f

    };


    const GLfloat squareTexcoords[] =
    {

        0.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,
		1.0f,0.0f,

		////// for tilted square
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,
		1.0f,0.0f
    
    };

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	// OGL drawing
	// set modelView & model view projection matrices to identity

	vmath::mat4 modelViewMatrix = vmath::mat4::identity(); 

	vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity(); 

	modelViewMatrix = vmath::translate(0.0f,0.0f,-6.0f);

	// multiply modelView and perspective matrix to get modelViewProjectionMatrix

	modelViewProjectionMatrix =  perspectiveProjectionMatrix * modelViewMatrix;

	// pass above modelViewProjectionMatrix to vertex shader in 'u_mvp_matrix' shader variable whose position is already calculated

	glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

	
	// Bind with texture

	glActiveTexture(GL_TEXTURE0);

	glBindTexture(GL_TEXTURE_2D,checkerboard_Texture) ;

	glUniform1i(texture_sampler_uniform,0);

	// bind vao_Pyramid

	glBindVertexArray(Vao_Checkerboard);

	glBindBuffer(GL_ARRAY_BUFFER,Vbo_Checkerboard_Position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(squareVertices),squareVertices,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    glBindBuffer(GL_ARRAY_BUFFER,Vbo_Checkerboard_Texture);
    glBufferData(GL_ARRAY_BUFFER,sizeof(squareTexcoords),squareTexcoords,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    glDrawArrays(GL_TRIANGLE_FAN,0,4); // draw Straight Checkerboard
    glDrawArrays(GL_TRIANGLE_FAN,4,4); // draw Tilted Checkerboard


	glBindVertexArray(0);

	glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

}


-(BOOL) acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    
    return (YES);
}

-(void) keyDown:(NSEvent *) theEvent
{
    int key = (int) [[theEvent characters]characterAtIndex:0];

    switch(key)
    {
        case 27:     // Esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;

        default:
            break;
    }
}

-(void) mouseDown:(NSEvent *) theEvent
{

}

-(void) mouseDragged:(NSEvent *)theEvent
{

}

-(void) rightMouseDown:(NSEvent *)theEvent
{

}

-(void) dealloc
{
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

	if(Vao_Checkerboard)
	{
		glDeleteVertexArrays(1,&Vao_Checkerboard);
		Vao_Checkerboard =0;
	}

	if(Vbo_Checkerboard_Position)
	{
		glDeleteBuffers(1,&Vbo_Checkerboard_Position);
		Vbo_Checkerboard_Position =0;
	}

	if(checkerboard_Texture)
	{
		glDeleteTextures(1,&checkerboard_Texture);
		checkerboard_Texture =0;
	}

	if(Vbo_Checkerboard_Texture)
	{
		glDeleteBuffers(1,&Vbo_Checkerboard_Texture);
		Vbo_Checkerboard_Texture =0;
	}


	glDetachShader(shaderProgramObject,vertexShaderObject);

	glDetachShader(shaderProgramObject,fragmentShaderObject);

	glDeleteShader(vertexShaderObject);
	vertexShaderObject=0;

	glDeleteShader(fragmentShaderObject);
	fragmentShaderObject =0;

	glDeleteProgram(shaderProgramObject);
	shaderProgramObject =0;

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback (CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result =[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];

    return(result);
}


