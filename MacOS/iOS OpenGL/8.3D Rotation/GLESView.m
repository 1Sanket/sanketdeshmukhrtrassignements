
// 3D Rotation.

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h> 

#import "GLESView.h"

#import "vmath.h"

enum
{
	VDG_ATTRIBUTE_VERTEX=0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};


@implementation GLESView
{
	EAGLContext *eaglContext;

	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;

	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;

    	
	GLuint vao_Cube;
	GLuint vao_Pyramid;

	GLuint vbo_Cube_Position;
	GLuint vbo_Cube_Color;

	GLuint vbo_Pyramid_Position;
	GLuint vbo_Pyramid_Color;

	static GLfloat anglePyramid =0.0f;
	static GLfloat angleCube =0.0f;

	GLuint mvpUniform;

	vmath::mat4 perspectiveProjectionMatrix;
} 

-(id) initWithFrame:(CGRect) frame
{
	self = [super initWithFrame:frame];

	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys : 
																					[NSNumber numberWithBool:FALSE ],
																					kEAGLDrawablePropertyRetainedBacking,
																					kEAGLColorFormatRGBA8,
																					kEAGLDrawablePropertyColorFormat,
																					nil
																					];
		eaglContext =[[EAGLContext alloc]initWithAPI : kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];

			return(nil);
		}

		[EAGLContext setCurrentContext:eaglContext];

		glGenFramebuffers(1,&defaultFramebuffer);
		glGenRenderbuffers(1,&colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);

		GLint backingWidth;
		GLint backingHeight;

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,,&backingWidth);

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,,&backingHeight);

		glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed to create complete frame buffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));

			glDeleteFramebuffers(1,&defaultFramebuffer);
			glDeleteRenderbuffers(1,&colorRenderbuffer);
			glDeleteRenderbuffers(1,&depthRenderbuffer);

			return(nil);
		}

		printf("Renderer is %s | GL Version:%s | GLSL Version:%s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));

		
		// hard coded initializations

		isAnimating = NO;
		animationFrameInterval=60;  // default since iOS 8.2

        
		// Vertex Shader //

		vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
		"#version 300 es" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 outColor;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"outColor = vColor;" \
		"}";

		glShaderSource(vertexShaderObject,1,(const GLchar**) &vertexShaderSourceCode,NULL);

		glCompileShader(vertexShaderObject);
        GLint iInfoLogLength =0;
        GLint iShaderCompiledStatus =0;
		char *szInfoLog = NULL;

		glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

		if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength>0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Vertex Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

        
		// Fragment Shader 

		iInfoLogLength =0;
		iShaderCompiledStatus =0;
		szInfoLog = NULL;

		fragmentShaderObject= glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode= 
		"#version 300 es" \
		"\n" \
		"precision highp float;" \
		"in vec4 outColor;" \
		"out vec4 FragColor;" \
		"void main(void)"\
		"{"\
		"FragColor = outColor;"\
		"}";

		glShaderSource(fragmentShaderObject,1,(const GLchar **)&fragmentShaderSourceCode,NULL);
        
		glCompileShader(fragmentShaderObject);
		glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

        if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength >0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Fragment Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

        
		// Shader Program

		shaderProgramObject = glCreateProgram();

		glAttachShader(shaderProgramObject,vertexShaderObject);

		glAttachShader(shaderProgramObject,fragmentShaderObject);

		
		// pre-link binding of shader program object with vertex shader position attribute

		glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");

        glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_COLOR,"vColor");
		
		//link shader
		glLinkProgram(shaderProgramObject);

		GLint iShaderProgramLinkStatus =0;

		glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

        
		if(iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			
			if(iInfoLogLength >0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
					printf("Shader Program Link Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

         
		// get MVP uniform location

		mvpUniform = glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		// Pyramid vertices

		const GLfloat pyramidVertices[]=
		{
			// front face

		0.0f, 1.0f, 0.0f,   // Apex

		-1.0f, -1.0f, 1.0f, // left-corner of front face

		 1.0f, -1.0f, 1.0f,   //right corner of front face

			//right face

		0.0f, 1.0f, 0.0f,     // apex

		1.0f, -1.0f, 1.0f,    //left corner of the right face

		1.0f, -1.0f, -1.0f,  // right corner of the right face

			//Back face

		0.0f, 1.0f, 0.0f, // apex

		1.0f, -1.0f, -1.0f, //left corner of the back face

		-1.0f, -1.0f, -1.0f, //right corner of the back face

			//left face

		0.0f, 1.0f, 0.0f, // apex

		-1.0f, -1.0f, -1.0f, //left corner of the back face

		-1.0f, -1.0f, 1.0f, //right corner of the back face
		};



		glGenVertexArrays(1,&vao_Pyramid);
		glBindVertexArray(vao_Pyramid);

		glGenBuffers(1,&vbo_Pyramid_Position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_Pyramid_Position);
		glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidVertices),pyramidVertices,GL_STATIC_DRAW);

		glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);

		glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

		glBindBuffer(GL_ARRAY_BUFFER,0);

    
			const GLfloat pyramidColorCoordinates []=
    {
        // front face
	
	1.0f, 0.0f, 0.0f, // red
	
		// Apex

	0.0f, 1.0f, 0.0f,    // green
	
		// left-corner of front face

	0.0f, 0.0f, 1.0f,    // blue
	
		//right corner of front face

        //right face

	1.0f, 0.0f, 0.0f, // red
        // apex

	0.0f, 0.0f, 1.0f,  // blue

		//left corner of the right face

	0.0f, 1.0f, 0.0f,  //green
	
		// right corner of the right face

		//Back face

	1.0f, 0.0f, 0.0f, //red
	
		// apex

	0.0f, 1.0f, 0.0f,// green
	
		//left corner of the back face

	0.0f, 0.0f, 1.0f, // blue
	
		//right corner of the back face

		//left face
	 
	1.0f, 0.0f, 0.0f,//red
	   
		// apex

	0.0f, 0.0f, 1.0f,  // blue
   
	//left corner of the back face

	0.0f, 1.0f, 0.0f  // green
    };

	glGenBuffers(1,&vbo_Pyramid_Color);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_Pyramid_Color);
	glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidColorCoordinates),pyramidColorCoordinates,GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER,0);

	glBindVertexArray(0);

	// Square

	const GLfloat cubeVertices []=
    {
        // Top face

	1.0f, 1.0f, -1.0f,   // Right-top corner of top face 
	-1.0f, 1.0f, -1.0f, // left-top corner of the top face
	-1.0f, 1.0f, 1.0f,   //left bottom corner of front face
	1.0f, 1.0f, 1.0f, //right bottom corner of front face

        //Bottom face

	1.0f, -1.0f, -1.0f,   // Right-top corner of bottom face 
	-1.0f, -1.0f, -1.0f, // left-top corner of the bottom face
	-1.0f, -1.0f, 1.0f,   //left bottom corner of bottom face
	1.0f, -1.0f, 1.0f, //right bottom corner of bottom face

		//Front face

	1.0f, 1.0f, 1.0f,   // Right-top corner of front face 
	-1.0f, 1.0f, 1.0f, // left-top corner of the front face
	-1.0f, -1.0f, 1.0f,   //left bottom corner of front face
	1.0f, -1.0f, 1.0f, //right bottom corner of front face


		//Back face
	
	1.0f, 1.0f, -1.0f,  // Right-top corner of back face 
	-1.0f, 1.0f, -1.0f, // left-top corner of the back face
	-1.0f, -1.0f, -1.0f,   //left bottom corner of back face
	1.0f, -1.0f, -1.0f, //right bottom corner of back face

		//Right face
	
	1.0f, 1.0f, -1.0f,   // Right-top corner of right face 
	1.0f, 1.0f, 1.0f, // left-top corner of the right face
	1.0f, -1.0f, 1.0f,   //left bottom corner of right face
	1.0f, -1.0f, -1.0f, //right bottom corner of right face

		//Left face

	-1.0f, 1.0f, 1.0f,   // Right-top corner of left face 
	-1.0f, 1.0f, -1.0f, // left-top corner of the left face
	-1.0f, -1.0f, -1.0f,   //left bottom corner of left face
	-1.0f, -1.0f, 1.0f //right bottom corner of left face

    };

    // Square Vao

    glGenVertexArrays(1,&vao_Cube);
    glBindVertexArray(vao_Cube);

    // Square Position Vbo 

    glGenBuffers(1,& vbo_Cube_Position);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Cube_Position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(cubeVertices),cubeVertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind square position Vbo
      
    const GLfloat cubeColorCoordinates []=
    {
		// Top face
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red

	     //Bottom face
	0.0f, 1.0f, 0.0f, // Green
	0.0f, 1.0f, 0.0f, // Green
	0.0f, 1.0f, 0.0f, // Green
	0.0f, 1.0f, 0.0f, // Green

		//Front face
	0.0f, 0.0f, 1.0f, // Blue
	0.0f, 0.0f, 1.0f, // Blue
	0.0f, 0.0f, 1.0f, // Blue
	0.0f, 0.0f, 1.0f, // Blue

		//Back face
	0.0f, 1.0f, 1.0f, // Cyan
	0.0f, 1.0f, 1.0f, // Cyan
	0.0f, 1.0f, 1.0f, // Cyan
	0.0f, 1.0f, 1.0f, // Cyan

		//Right face
	1.0f, 0.0f, 1.0f, // Magenta
	1.0f, 0.0f, 1.0f, // Magenta
	1.0f, 0.0f, 1.0f, // Magenta
	1.0f, 0.0f, 1.0f, // Magenta

		//Left face
	1.0f, 1.0f, 0.0f, // Yellow
	1.0f, 1.0f, 0.0f,// Yellow
	1.0f, 1.0f, 0.0f, // Yellow
	1.0f, 1.0f, 0.0f // Yellow        
    };

	// Square Color Vbo 

    glGenBuffers(1,& vbo_Cube_Color);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Cube_Color);
    glBufferData(GL_ARRAY_BUFFER,sizeof(cubeColorCoordinates),cubeColorCoordinates,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); // unbind square position Vbo
    glBindVertexArray(0); // Unbind Square vao

    glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glEnable(GL_CULL_FACE);

		// clear color

		glClearColor(0.0f,0.0f,1.0f,1.0f);

		perspectiveProjectionMatrix = vmath::mat4::identity();

		// Gesture recognition

		// Tap gesture code

        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)]; // callback is given by @selector

		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
		[singleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];


		UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)]; // callback is given by @selector

		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];


		// To allow to differentiate between single tap and double tap

		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];

		
		// swipe Gesture

		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];

		[self addGestureRecognizer:swipeGestureRecognizer];

		//long press gesture

		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];

		[self addGestureRecognizer:longPressGestureRecognizer];

	}

	return(self);

}


+(Class)layerClass
{
	return([CAEAGLLayer class]);
}


-(void) drawView:(id) sender
{
	[EAGLContext setCurrentContext:eaglContext];

	glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	vmath::mat4 modelViewMatrix = vmath::mat4::identity();
	vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
	vmath::mat4 rotationMatrix = vmath::mat4::identity(); 

	modelViewMatrix = vmath::translate(-1.5f,0.0f,-6.0f);

	rotationMatrix = vmath::rotate(anglePyramid,0.0f,1.0f,0.0f);

	modelViewMatrix = modelViewMatrix * rotationMatrix ;

    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

	glBindVertexArray(vao_Pyramid);

	glDrawArrays(GL_TRIANGLES,0,12);

	glBindVertexArray(0);


	modelViewMatrix = vmath::mat4::identity(); 
	modelViewProjectionMatrix = vmath::mat4::identity(); 
	rotationMatrix = vmath::mat4::identity(); 

    // Translate model view matrix.

    modelViewMatrix = vmath::translate(1.5f,0.0f,-6.0f);

	rotationMatrix = vmath::rotate(angleCube,1.0f,0.0f,0.0f);

	modelViewMatrix = modelViewMatrix * rotationMatrix ;

	// multiply modelView and perspective matrix to get modelViewProjectionMatrix

	modelViewProjectionMatrix =  perspectiveProjectionMatrix * modelViewMatrix;

	// pass above modelViewProjectionMatrix to vertex shader in 'u_mvp_matrix' shader variable whose position is already calculated

	glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);


    // *** Bind Square Vao ***//

    glBindVertexArray(vao_Cube);
    
    glDrawArrays(GL_TRIANGLE_FAN,0,4); 
	glDrawArrays(GL_TRIANGLE_FAN,4,4); 
	glDrawArrays(GL_TRIANGLE_FAN,8,4); 
	glDrawArrays(GL_TRIANGLE_FAN,12,4); 
	glDrawArrays(GL_TRIANGLE_FAN,16,4); 
	glDrawArrays(GL_TRIANGLE_FAN,20,4); 
    
        // unbind vao
    
    glBindVertexArray(0);
	
	glUseProgram(0);

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

	[eaglContext presentRenderbuffer:GL_RENDERBUFFER];

	[self updateAngle];

}

-(void) updateAngle
{
	anglePyramid = anglePyramid + 0.7f;
	angleCube = angleCube - 0.7f;
	if (anglePyramid >= 360.0f)
		anglePyramid = 0.0f;
	if (angleCube <= 0.0f)
		angleCube = 360.0f;
}

-(void)layoutSubviews
{
	GLint width;
	GLint height;

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

	[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);

	glGenRenderbuffers(1,&depthRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

	glViewport(0,0,width,height);

    perspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Failed to create complete frame buffer object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
	}

	[self drawView:nil];
}

-(void) startAnimation
{
	if(!isAnimating)
	{
		displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];

		[displayLink setPreferredFramesPerSecond : animationFrameInterval];

		[displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

		isAnimating = YES;
	}
}

-(void) stopAnimation
{
	if(isAnimating)
	{
		[displayLink invalidate];

		displayLink = nil;

		isAnimating = NO;
	}
}

-(BOOL) acceptsFirstResponder
{
	return(YES);
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
{

}

-(void) onSingleTap:(UITapGestureRecognizer *)gr
{

}

-(void) onDoubleTap:(UITapGestureRecognizer *) gr
{
	
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
	[self release];

	exit(0);
}

-(void) onLongPress :(UILongPressGestureRecognizer *)gr
{

}

-(void) dealloc
{
 
    if(vao_Pyramid)
	{
		glDeleteVertexArrays(1,&vao_Pyramid);
		vao_Pyramid =0;
	}

	if(vbo_Pyramid_Position)
	{
		glDeleteBuffers(1,&vbo_Pyramid_Position);
		vbo_Pyramid_Position =0;
	}

	if(vbo_Pyramid_Color)
	{
		glDeleteBuffers(1,&vbo_Pyramid_Color);
		vbo_Pyramid_Color =0;
	}

	if(vao_Cube)
	{
		glDeleteVertexArrays(1,&vao_Cube);
		vao_Cube =0;
	}

	if(vbo_Cube_Position)
	{
		glDeleteBuffers(1,&vbo_Cube_Position);
		vbo_Cube_Position =0;
	}

	if(vbo_Cube_Color)
	{
		glDeleteBuffers(1,&vbo_Cube_Color);
		vbo_Cube_Color =0;
	}

	glDetachShader(shaderProgramObject,vertexShaderObject);

	glDetachShader(shaderProgramObject,fragmentShaderObject);

	glDeleteShader(vertexShaderObject);
	vertexShaderObject =0;

	glDeleteShader(fragmentShaderObject);
	fragmentShaderObject=0;

	glDeleteProgram(shaderProgramObject);
	shaderProgramObject=0;


	if(depthRenderbuffer)
	{
		glDeleteRenderbuffers(1,&depthRenderbuffer);
		depthRenderbuffer=0;
	}

	if(colorRenderbuffer)
	{
		glDeleteRenderbuffers(1,&colorRenderbuffer);
		colorRenderbuffer=0;
	}

	if(defaultFramebuffer)
	{
		glDeleteRenderbuffers(1,&defaultFramebuffer);
		defaultFramebuffer=0;
	}

	if([EAGLContext currentContext]==eaglContext)
	{
		[EAGLContext setCurrentContext:nil];
	}

	[eaglContext release];
	eaglContext = nil;

	[super dealloc];
}

@end