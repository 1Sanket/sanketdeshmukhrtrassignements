
// headers

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

// interface declaration 

@interface AppDelegate : NSObject <NSApplicationDelegate , NSWindowDelegate>
@end 

@interface MyView : NSView
@end 


// Entry point function

int main(int argc, const char* argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];   //alloc is like new NSAutoReleasePool() and init is constructor

    NSApp = [NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc] init]];

    [NSApp run];  // This is run loop same as message loop

    [pPool release];   // This calls the dealloc for the particular class

    return(0);

}


// interface implementation

@implementation AppDelegate
{
    @private 
        NSWindow *window;
        MyView *view;
}

- (void) applicationDidFinishLaunching:(NSNotification *) aNotification  // - for instance method
{
   // window 

   NSRect win_rect;  // NSRect is internally struct 

   win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

   // create window

   window = [[NSWindow alloc] initWithContentRect:win_rect 
                              styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable
                              | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                              backing: NSBackingStoreBuffered
                              defer: NO ];

                              [window setTitle:@"macOS Window"];
                              [window center];

  view = [[MyView alloc] initWithFrame:win_rect];

  [window setContentView : view];
  [window setDelegate:self];
  [window makeKeyAndOrderFront:self];

}  


- (void) applicationWillTerminate:(NSNotification *) notification  // analogous to WM_DESTROY or WM_CLOSE
{
  
}

- (void) windowWillClose:(NSNotification *) notification
{
    [NSApp terminate: self];
}

-(void) dealloc
{
    [view release];

    [window release];

    [super dealloc];
}

@end

@implementation MyView 
{
    NSString *centerText;
}

-(id)initWithFrame:(NSRect) frame
{
    self = [super initWithFrame:frame]; // to tell parent class to create us
    
    if(self)
    {
        [[self window]setContentView : self];

        centerText = @"Hello World";
    }

    return (self);
}

-(void) drawRect:(NSRect) dirtyRect
{
    // Black background
    NSColor *fillColor = [NSColor blackColor];
    [fillColor set];

    NSRectFill(dirtyRect); // This is Carbon function

    // dictionary with kvc

    NSDictionary *dictionaryForTextAttributes=[NSDictionary dictionaryWithObjectsAndKeys: 
                                              [NSFont fontWithName:@"Helvetica" size:32],
                                              NSFontAttributeName,
                                              [NSColor greenColor],
                                              NSForegroundColorAttributeName,
                                              nil]; // nil to indicate end of the dictionary

    NSSize textSize = [centerText sizeWithAttributes:dictionaryForTextAttributes];

    NSPoint point;

    point.x = (dirtyRect.size.width/2) - (textSize.width/2);
    point.y = (dirtyRect.size.height/2) - (textSize.height/2) + 12; //12 is size of doc

    [centerText drawAtPoint: point withAttributes:dictionaryForTextAttributes];


}

-(BOOL) acceptsFirstResponder  // This is overriden method
{
   [[self window] makeFirstResponder:self];

   return (YES);
}


-(void) keyDown :(NSEvent *) theEvent
{
    int key =(int) [[theEvent characters] characterAtIndex:0];

    switch(key)
    {
        case 27 : // Esc key
                [self release];
                [NSApp terminate:self];
                break;
        
        case 'F':
        case 'f':
                centerText = @" 'F' or 'f' key is pressed ";
                [[self window] toggleFullScreen:self]; // repainting occurs automatically.
                break;

        default: 
                break;
    }
}

-(void)mouseDown :(NSEvent *) theEvent
{
    centerText = @"Left mouse button is clicked";
    [self setNeedsDisplay:YES]; // repainting
}

-(void) mouseDragged:(NSEvent *)theEvent
{

}

-(void) rightMouseDown:(NSEvent *)theEvent
{
    centerText =@"Right Mouse button is clicked";
    [self setNeedsDisplay:YES];  // repainting
}

-(void) dealloc
{
    [super dealloc];
}

@end
