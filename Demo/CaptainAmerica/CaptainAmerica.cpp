#include <GL\freeglut.h>
#include <math.h>

#define PI 3.1415926535898

bool bFullScreen = false;

struct Point
{
	GLfloat xAxis;
	GLfloat yAxis;
};

struct Color
{
	GLfloat red;
	GLfloat green;
	GLfloat blue;
};

int main(int argc, char** argv)
{
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(800, 800);
	glutInitWindowPosition(100, 0);
	glutCreateWindow("Deathly Hallows");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	void drawLine(Point, Point, Color, GLfloat);
	void drawTriangle(Point, Point, Point, Color, GLfloat);
	void drawCircle(GLfloat radius, Color color);
	void drawStar(void);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//color red 0.556863 green 0.137255 blue 0.419608
	//color red 0.59 green 0.41 blue 0.31
//	color red 0.137255 green 0.137255 blue 0.556863
	drawCircle(0.75f, Color{ 1.0f,0.0f, 0.0f });
	drawCircle(0.60f, Color{ 1.0f,1.0f, 1.0f });
	drawCircle(0.45f, Color{ 1.0f,0.0f, 0.0f });
	drawCircle(0.30f, Color{ 0.137255f,0.137255f, 0.556863f });
	drawStar();

	//drawTriangle(Point{ 0.0f,0.60f }, Point{ -0.5f,-0.30f }, Point{ 0.5f,-0.30f }, Color{ 1.0f,1.0f,1.0f }, 1.0f);
	//drawLine(Point{ 0.0f, 0.60f }, Point{ 0.0f, -0.30f }, Color{ 1.0f,1.0f,1.0f }, 1.0f);

	glutSwapBuffers();

}


void drawStar()
{
	//GLint circle_points = 1000000;
	//GLfloat angle;

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.0f,0.30f,0.0f);
	glVertex3f(-0.1f,0.1f,0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.1f,0.1f,0.0f);

	glEnd();

	glBegin(GL_QUADS);

	//glColor3f(1.0f, 1.0f, 0.0f);

	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.12f, -0.06f, 0.0f);
	glVertex3f(0.28f, 0.1f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	//glColor3f(0.6f,0.6f,0.5f);

	glVertex3f(-0.1f, 0.1f, 0.0f);
	glVertex3f(-0.28f, 0.1f, 0.0f);
	glVertex3f(-0.12f, -0.06f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	//glColor3f(0.6f, 0.6f, 0.5f);

	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.12f, -0.06f, 0.0f);
	glVertex3f(-0.18f, -0.24f, 0.0f);
	glVertex3f(0.0f, -0.1f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	//glColor3f(0.6f, 0.6f, 0.5f);

	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.1f, 0.0f);
	glVertex3f(0.18f, -0.24f, 0.0f);
	glVertex3f(0.12f, -0.06f, 0.0f);
	glEnd();

}

void drawCircle(GLfloat radius, Color color)
{
	GLint circle_points = 1000000;
	GLfloat angle;

	glBegin(GL_POLYGON);

	glColor3f(color.red, color.green, color.blue);

	for (int i = 0.0f; i<circle_points; i++)
	{
		angle = 2 * PI * i / circle_points;
		glVertex3f(cos(angle)*radius,
			sin(angle)*radius, 0.0f);
	}

	glEnd();

}

void drawTriangle(Point top, Point left, Point right, Color color, GLfloat width)
{
	glLineWidth(width);
	glBegin(GL_LINE_LOOP);

	glColor3f(color.red, color.green, color.blue);

	glVertex3f(top.xAxis, top.yAxis, 0.0f);
	glVertex3f(left.xAxis, left.yAxis, 0.0f);
	glVertex3f(right.xAxis, right.yAxis, 0.0f);

	glEnd();
}

void drawLine(Point xCoordinate, Point yCoordinate, Color color, GLfloat width)
{
	glLineWidth(width);
	glBegin(GL_LINES);

	glColor3f(color.red, color.green, color.blue);
	glVertex3f(xCoordinate.xAxis, xCoordinate.yAxis, 0.0f);
	glVertex3f(yCoordinate.xAxis, yCoordinate.yAxis, 0.0f);

	glEnd();
}


void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScreen == false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{

}