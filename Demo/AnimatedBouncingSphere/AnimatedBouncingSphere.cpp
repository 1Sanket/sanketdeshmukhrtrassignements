#include <Windows.h>
#include <gl/GL.h>
#include <GL\GLU.h>
#include <stdio.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbIsEscapeKeyPressed = false;
bool gbFullscreen = false;
bool gbIsLightingEnabled = false;
void resize(int, int);
float angleCube = 0.0f;
float anglePyramid = 0.0f;
GLfloat x1 = 0.0f; 
GLfloat y1 = 0.0f; 
GLfloat rsize = 15;
// Step size in x and y directions // (number of pixels to move each time) 
GLfloat xstep = 0.1f; 
GLfloat ystep = 0.1f;
GLfloat SquareSide = 200.0f;

FILE *gpFile = NULL;


// Arrays required for lighting.
GLfloat light_ambient[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f,0.0f,1.0f,1.0f };
GLUquadric *quadric = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void updateAngle(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OGL_Light_PyramidNCube");
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created. \n Exiting ..... "), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}

	else
	{
		fprintf(gpFile, "Log file is successfully opened.....");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = CreateSolidBrush(RGB(1, 1, 1));
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx
	(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGl fixed function pipeline : Light - Pyramid and Cube"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{

			if (gbActiveWindow == true)
			{
				if (gbIsEscapeKeyPressed == true)
					bDone = true;
				updateAngle();
				display();
			}
		}

	}

	uninitialize();
	return (int(msg.wParam));

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbIsEscapeKeyPressed = true;
			break;

		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C: // for 'l' or 'L'
			if (gbIsLightingEnabled == false)
			{
				glEnable(GL_LIGHTING);
				gbIsLightingEnabled = true;
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbIsLightingEnabled = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}


void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}

void initialize(void)
{

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glEnable(GL_LIGHT0);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	quadric = gluNewQuadric();

	glTranslatef(x1, y1, 0.0f);

	gluSphere(quadric, rsize, 30, 30);

	glColor3f(1.0f, 0.0f, 0.0f);

	//glRectf(x1, y1, x1 + rsize, y1 - rsize);

	/*glBegin(GL_LINE);

	glVertex3f(-400.0f, 400.0f, 0.0f);
	glVertex3f(-400.0f, -400.0f, 0.0f);

	glEnd();

	glBegin(GL_LINE);
	glVertex3f(SquareSide, -SquareSide, 0.0f);
	glVertex3f(SquareSide, SquareSide, 0.0f);

	glEnd();*/
	//glVertex3f(SquareSide, SquareSide, 0.0f);

	//glEnd();

 	/*glBegin(GL_QUADS);

	glVertex3f(x1,y1,0.0f);
	glVertex3f(-x1, y1, 0.0f);
	glVertex3f(-x1, -y1, 0.0f);
	glVertex3f(x1,-y1,0.0f);
	glEnd();*/

	//x1 = x1 + rsize;
	//y1 = y1 - rsize;
	//glTranslatef(x1 + rsize, y1 + rsize, 0.0f);
	//gluSphere(quadric, 0.50, 30, 30);

	SwapBuffers(ghdc);

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	
	//glLoadIdentity();
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 50.0f);
	if (width <= height)
		glOrtho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
	else
		glOrtho(-100.0f, 100.0f, (-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f);

}

void updateAngle()
{
	/*angleCube = angleCube + 0.1f;
	if (angleCube >= 360.0f)
		angleCube = 0.0f;
	anglePyramid = anglePyramid + 0.1f;
	if (anglePyramid >= 360.0f)
		anglePyramid = 0.0f;*/
		// Reverse direction when you reach left or right edge 

	fprintf(gpFile, "\n X1 %f", x1);

	//if (x1 > WIN_WIDTH - rsize || x1 < -WIN_WIDTH)
	if (x1 > 100.0f - rsize || x1 < -100.0f + rsize)
		xstep = -xstep;
	// Reverse direction when you reach top or bottom edge
	if (y1 > 100.0f -rsize || y1 < -100.0f + rsize)
		ystep = -ystep;
	// Actually move the square 
	x1 += xstep;
	y1 += ystep;
	// Check bounds. This is in case the window is made 
	// smaller while the rectangle is bouncing and the 
	// rectangle suddenly finds itself outside the new 
	// clipping volume 
	//if (x1 >(500.0f - rsize + xstep))
	//	x1 =500.0f - rsize - 1;
	////else if (x1 < -(1000.0f + xstep))
	//else if(x1 < -(500.0f + xstep))
	//	x1 = -500.0f - 1;
	//
	//if (y1 >(500.0f + ystep))
	//	y1 = 500.0f - 1;
	//else if (y1 < -(500.0f - rsize + ystep))
	//	y1 = -500.0f + rsize - 1;
}

//void timerFunction(int value)
//{
	//// Reverse direction when you reach left or right edge 
	//if(x1 > windowWidth-rsize || x1 < -windowWidth)
	//	xstep = -xstep;
	//// Reverse direction when you reach top or bottom edge
	//if(y1 > windowHeight || y1 < -windowHeight + rsize) 
	//	ystep = -ystep;
	//// Actually move the square 
	//x1 += xstep; 
	//y1 += ystep;
	//// Check bounds. This is in case the window is made 
	//// smaller while the rectangle is bouncing and the 
	//// rectangle suddenly finds itself outside the new 
	//// clipping volume 
	//if(x1 > (windowWidth-rsize + xstep))
	//	x1 = windowWidth-rsize-1; 
	//else if(x1 < -(windowWidth + xstep))
	//	x1 = - windowsWidth -1;
	//if (y1 > (windowHeight + ystep)) 
	//	y1 = windowHeight - 1; 
	//else if (y1 < -(windowHeight - rsize + ystep)) 
	//	y1 = -windowHeight + rsize - 1;
	// Redraw the scene with new coordinates 
	//glutPostRedisplay(); 
	//glutTimerFunc(33,TimerFunction, 1); 
//}

void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}
