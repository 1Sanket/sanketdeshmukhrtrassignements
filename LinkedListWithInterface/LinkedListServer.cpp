
using namespace std;
#include <iostream>
#include "LinkedList.h"


class LinkedList : public ILinkedList
{
	private: struct Node
		{
			int data;
			Node* nextNode;
		};
	
	Node *dummyHead;

   public : LinkedList()
	{
		dummyHead = (Node *)malloc(sizeof(Node));
		dummyHead->data = 0;
		dummyHead->nextNode = NULL;
	}

	~LinkedList()
	{
		if(dummyHead->nextNode == NULL)
		delete dummyHead;
	}


	 void insertEnd(int data)
	{
		Node *newNode = new Node;
		newNode->data = data;
		newNode->nextNode = NULL;

		Node *currentNode = dummyHead;

		while (currentNode)
		{
			if (currentNode->nextNode == NULL)
			{
				currentNode->nextNode = newNode;
				return;
			}

			currentNode = currentNode->nextNode;
		}
	}

    void insertBegining(int data)
	{
		Node *newNode = new Node;
		newNode->data = data;
		newNode->nextNode = dummyHead->nextNode;
		dummyHead->nextNode = newNode;
	}

	 int findData(int data)
	{
		Node *currentNode = dummyHead;
		int i = 0;

		while (currentNode)
		{
			if (currentNode->data == data)
			{
				return i;
			}
			currentNode = currentNode->nextNode;
			i++;
		}

		return 0;
	}

	 bool deleteNode(int location)
	{
		Node *currentNode = dummyHead->nextNode;
		int i = 0;
		Node *tempNode;
		Node *previousNode = (Node *)malloc(sizeof(Node));

		while (currentNode)
		{
			if ( location==1)
			{
				previousNode = dummyHead;
		    }
			
			if (i== location-1)
			{
				tempNode = currentNode->nextNode;
				previousNode->nextNode = tempNode;

				free (currentNode);
				return true;
			}

		
			previousNode = currentNode;
			currentNode = currentNode->nextNode;
			i++;
		}

		return false;
	}

	 void deleteLinkedList()
	{
		Node *tempNode;
		while (dummyHead)
		{
			tempNode = dummyHead;
			dummyHead = tempNode->nextNode;
			free (tempNode);
		}
	}

	 void display()
	{

		 //cout << "In display of Linked List";
		 cout << "\n";
		 cout << " List :  ";

		 if (dummyHead == NULL)
		 {
			 cout << "\n List is empty";
			 return;
		 }

		 Node *list = (Node *)malloc(sizeof(Node));
		 list = dummyHead->nextNode;

		while (list)
		{
			cout << list->data << " ";
		
			list = list->nextNode;

			cout << " - ";
			
		}

		free(list);
	}
	 
};

 ILinkedList * ILinkedList::getServerObject(void)
{
	ILinkedList* serverObject = new LinkedList();

    return serverObject;
}

