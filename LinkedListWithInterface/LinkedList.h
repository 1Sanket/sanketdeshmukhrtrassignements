#pragma once
 
class ILinkedList
{
public :
	virtual void insertEnd(int data) = 0;

	virtual void insertBegining(int data) = 0;

	virtual int findData(int data) = 0;

	virtual bool deleteNode(int location) = 0;

	virtual void deleteLinkedList() = 0;

	virtual void display() = 0;

    public : static ILinkedList * getServerObject(void);

};
