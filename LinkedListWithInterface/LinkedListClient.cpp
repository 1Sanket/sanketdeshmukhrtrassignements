#include <iostream>
#include <conio.h>
using namespace std;
#include "LinkedList.h"

//class LinkedListClient
//{
	int main()
	{

		//ILinkedList *ptr;

		//ILinkedList* client = ptr->getServerObject();

		ILinkedList* client = ILinkedList::getServerObject();

		cout << "\n Inserting data at the end of list ";

		client->insertEnd(5);

		cout << "\n Data inserted successfully";

		client->display();

		cout << "\n Inserting data at the beginning of list ";

		client->insertBegining(2);

		cout << "\n Data inserted successfully";

		client->display();

		cout << "\n Inserting data at the beginning of list ";

		client->insertBegining(22);

		cout << "\n Data inserted successfully";

		client->display();
		
		cout << "\n Searching 5 in list";

		int location=client->findData(5);

		cout << "\n Data found at location :" << location;

		cout << "\n Deleting data at location :2";

		client->deleteNode(2);

		cout << "\n Data deleted successfully";

		client->display();

		cout << "\n Deleting list";

		client->deleteLinkedList();

		cout << "\n List deleted successfully";

		client->display();

		getchar();
	}

//};